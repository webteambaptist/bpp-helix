﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BHUser.Models;
using System.DirectoryServices.AccountManagement;
using Sitecore;
using System.Configuration;
using Sitecore.Configuration;

namespace BHUser.Components
{
    public class Login
    {
        /// <summary>
        /// Sample Authentication check
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool IsAuthenticated(string userName, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                //throw;
                return false;
            }
        }

        public User GetUser(string userName, string password)
        {
            User _bppUser = new User();
            try
            {
                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    if (userName.ToString().ToLower().Equals("admin"))
                        _bppUser.UserType = BH_User_Types.Admin;
                    else
                        _bppUser.UserType = BH_User_Types.Provider;
                }
                else
                {
                    _bppUser.UserType = BH_User_Types.Public;
                }
                return _bppUser;
            }
            catch (Exception)
            {
                //throw;
                return _bppUser;
            }
        }

        public static bool IfUserLoggedIn()
        {
            try
            {
                Sitecore.Diagnostics.Log.Info("BHUser.Components.Login : IfUserLoggedIn -> Logged In User - " + Sitecore.Context.User.Name, Sitecore.Context.User);
                return Context.User.IsAuthenticated;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BHUser.Components.Login : IfUserLoggedIn -> Exception - " + ex.Message, ex, ex.Data);
                return false;
            }
        }

        public static bool IfAdminLoggedIn()
        {
            try
            {
                Sitecore.Diagnostics.Log.Info("BHUser.Components.Login : IfAdminLoggedIn -> Logged In User - " + Sitecore.Context.User.Name, Sitecore.Context.User);
                try
                {
                    if (Settings.GetSetting("DEBUG") == "y")
                    {
                        return true;
                    }
                    return Sitecore.Context.User.IsInRole(@"ad\BPP Admin");
                }
                catch (Exception)
                {
                    return Sitecore.Context.User.IsInRole(@"ad\BPP Admin");
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BHUser.Components.Login : IfAdminLoggedIn -> Exception - " + ex.ToString(), ex, ex.Data);
                return false;
            }        
        }

        public static bool IfProviderLoggedIn()
        {
            try
            {
                Sitecore.Diagnostics.Log.Info("BHUser.Components.Login : IfProviderLoggedIn -> Logged In User - " + Sitecore.Context.User.Name, Sitecore.Context.User);
                return Context.User.IsInRole(@"ad\BPP Provider");
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BHUser.Components.Login : IfProviderLoggedIn -> Exception - " + ex.ToString(), ex, ex.Data);
                return false;
            }          
        }

        public static string[] GetADGroups(string username = "tomtest2")
        {
            string[] output = null;
            try
            {
                Sitecore.Diagnostics.Log.Info("BHUser.Components.Login.GetADGroups : Initiated", username);
                using (var ctx = new PrincipalContext(ContextType.Domain, "AD"))
                {
                    Sitecore.Diagnostics.Log.Info("BHUser.Components.Login.GetADGroups : Loaded Princiapl Context from AD", ctx);
                    using (var user = UserPrincipal.FindByIdentity(ctx, username))
                    {
                        Sitecore.Diagnostics.Log.Info("BHUser.Components.Login.GetADGroups : Loaded user from AD", user);
                        if (user != null)
                        {
                            Sitecore.Diagnostics.Log.Info("BHUser.Components.Login.GetADGroups : User Is not null", user);
                            output = user.GetGroups()
                                .Select(x => x.SamAccountName)
                                .ToArray();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BHUser.Components.Login.GetADGroups : Exception - " + ex.ToString(), ex, ex.Data);
                //throw;
            }

            return output;
        }

        public static bool IsValidAD(string _username, string _password)
        {
            bool isValid = false;
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "BH"))
            {
                // validate the credentials
                isValid = pc.ValidateCredentials(_username, _password);

            }

            return isValid;
        }
    }
}
