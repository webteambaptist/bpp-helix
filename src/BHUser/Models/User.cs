﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHUser.Models
{

    public enum BH_User_Types
    {
        Admin,
        Provider,
        Public
    }

    public class User
    {
        #region User Standard Properties
        public string UserName { get; set; }
        public string Email { get; set; }
        public BH_User_Types UserType { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        #endregion

        #region Additional Properties

        #endregion
    }
}
