﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHUser.Models
{
    public class Physician : User
    {
        #region Physician Specific Properties
        public string PrimarySpecialty { get; set; }
        public string PrimaryLocation { get; set; }
        public string PracticeName { get; set; }
        public string JobTitle { get; set; }
        public string Suffix { get; set; }
        //public string Title { get; set; }
        //public string Degree { get; set; }
        public string Languages { get; set; }
        public string PrimaryLocationLatLng { get; set; }
        public string OtherLocations { get; set; }
        public string OtherLocationsLatLngs { get; set; }
        #endregion

        #region Additional Properties

        #endregion
    }
}
