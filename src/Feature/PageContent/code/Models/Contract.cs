﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPP93Helix.Feature.PageContent.Models
{
    public partial class Contract
    {
      public int ID { get; set; }
      public string ContractName { get; set; }
      public bool isSelected { get; set; }
  }
}
