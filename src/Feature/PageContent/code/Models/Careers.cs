﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Data;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class Careers
    {
        #region Properties
        public string JobTitle { get; set; }
        public string JobID { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
        public string Type { get; set; }
        public string Link { get; set; }
        public List<Careers> Jobs { get; set; }
        #endregion

        public static List<Careers> readXML()
        {

            List<Careers> j = new List<Careers>();

            string file = Settings.GetSetting("CAREERS");

            DataSet ds = new DataSet();

            try
            {

                System.IO.FileStream fsReadXml = new System.IO.FileStream(file, System.IO.FileMode.Open);

                ds.ReadXml(fsReadXml);

                foreach (DataTable table in ds.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        if (dr["BusinessUnitName"].ToString() == "Baptist Physician Partners LLC")
                        {
                            Careers x = new Careers();

                            x.JobID = dr["JobId"].ToString();
                            x.JobTitle = dr["PostingTitle"].ToString();
                            x.Type = dr["ScheduleType"].ToString();
                            x.Link = dr["URL"].ToString();
                            x.LocationName = dr["LocationName"].ToString();
                            x.DepartmentName = dr["DepartmentName"].ToString();

                            j.Add(x);
                        }

                    }
                }
                fsReadXml.Close();
            }
            catch (Exception)
            {
                // not used?
            }

            return j;
        }
    }
}