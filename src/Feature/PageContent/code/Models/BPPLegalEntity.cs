﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BPP93Helix.Feature.PageContent.Models
{
        public class BPPLegalEntity
    {
        public int ID { get; set; }
        public string LegalEntityID { get; set; }
        [Display(Name = "Group Taxpayer Identification Number (TIN)")]
        [RegularExpression("^[0-9]{2}(?:-[0-9]{7})?$", ErrorMessage = "Invalid Format")]
        [Required, MinLength(10), MaxLength(10), StringLength(10)]
        public string TaxID { get; set; }
        [Display(Name = "Group (Legal Business Name) ")]
        [Required(ErrorMessage = "LBN is Required")]
        public string LE_Name { get; set; }
        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address is Required")]
        public string LE_Address1 { get; set; }
        [Display(Name = "Address 2")]
        public string LE_Address2 { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "City is Required")]
        public string LE_City { get; set; }
        [Display(Name = "St")]
        [Required(ErrorMessage = "State is Required")]
        public string LE_State { get; set; }
        [Display(Name = "Zip")]
        [RegularExpression("[0-9]{5}-?([0-9]{4})?", ErrorMessage = "Invalid Format (xxxxx-xxxx)")]
        [Required(ErrorMessage = "Zip is Required"), MinLength(5), MaxLength(10), StringLength(10)]
        public string LE_Zip { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is Required")]
        public string POCName { get; set; }
        [Display(Name = "Phone")]
        [Required(ErrorMessage = "Phone is Required")]
        [MinLength(14), MaxLength(14), StringLength(14)]
        public string POCPhoneNumber { get; set; }
        public string POCPhone { get; set; }
        [Display(Name = "Ext.")]
        public string POCExtension { get; set; }
        [Display(Name = "Fax")]
        [MinLength(14), MaxLength(14), StringLength(14)]
        public string POCFax { get; set; }
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "The value of the Email field is not valid")]
        public string POCEmail { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is Required")]
        public string CAPName { get; set; }
        [Display(Name = "Mailing Address")]
        [Required(ErrorMessage = "Address is Required")]
        public string CAPAddress1 { get; set; }
        [Display(Name = "Mailing Address 2")]
        public string CAPAddress2 { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "City is Required")]
        public string CAPCity { get; set; }
        [Display(Name = "St")]
        [Required(ErrorMessage = "State is Required")]
        public string CAPState { get; set; }
        [Display(Name = "Zip")]
        [Required(ErrorMessage = "Zip is Required")]
        public string CAPZip { get; set; }
        public bool CAPCompleted { get; set; }
        public Nullable<System.DateTime> MemberApplicationSigned { get; set; }
        public Nullable<System.DateTime> ParticipationAgreementSigned { get; set; }
        public Nullable<System.DateTime> BusinessAssociationSigned { get; set; }
        public Nullable<System.DateTime> OutcomeNotification { get; set; }
        public string OutcomeNotificationNote { get; set; }
        public Nullable<System.DateTime> BPPEffectiveDate { get; set; }
        public Nullable<System.DateTime> ExecutedAgreementReturned { get; set; }
        public string Status { get; set; }
        public System.DateTime Created { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public Nullable<System.DateTime> InactiveDate { get; set; }
        public string InactiveNotes { get; set; }
        //public int LegalEntityStatusID { get; set; }
        //public string LegalEntityStatus { get; set; }
        public IEnumerable<SelectListItem> _LegalEntityStatus { get; set; }
        public Nullable<bool> InNetwork { get; set; }
  }
}