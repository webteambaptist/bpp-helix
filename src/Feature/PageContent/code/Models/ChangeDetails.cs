﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class ChangeDetails
    {
        public long Id { get; set; }
        [Display(Name = "Name")]
        public string SubmitterName { get; set; }
        [Display(Name = "Physician Group (Legal Business Name)")]
        public string SubmitterGroup { get; set; }
        [Display(Name = "Submitted Date")]
        public DateTime SubmittedDate { get; set; }
        [Display(Name = "TIN")]
        public string SubmitterTIN { get; set; }
        [Display(Name = "Email Address")]
        public string SubmitterEmail { get; set; }
        [Display(Name = "Phone Number")]
        public string SubmitterPhone { get; set; }
        [Display(Name = "Effective Date")]
        public DateTime? EffectiveDate { get; set; }
        [Display(Name = "NPI")]
        public string ProviderNPI { get; set; }
        [Display(Name = "Name")]
        public string ProviderName { get; set; }
        [Display(Name = "Change Reason")]
        public string ChangeReason { get; set; }
        [Display(Name = "Status Change Reason")]
        public string StatusChangeReason { get; set; }
        [Display(Name = "Name Change")]
        public string NameChange { get; set; }
        [Display(Name = "Additional Notes")]
        public string Other { get; set; }
        [Display(Name = "Practice Name")]
        public string PracticeName { get; set; }
        public Guid? Practice { get; set; }
        public int? ProcessedResults { get; set; }
    }
}