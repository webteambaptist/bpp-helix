﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class MakeADifference
    {
        public String Title { get; set; }
        public String Heading { get; set; }
        public string SubHeading { get;set; }
        public string Body { get; set; }
        public string ImagePath { get; set; }
        public string ItemPath { get; set; }
        public Sitecore.Data.Fields.ImageField ImgField { get; set; }
    }

    public class MakeADiffItems
    {
        public List<MakeADifference> _Items { get; set; }
    }
}