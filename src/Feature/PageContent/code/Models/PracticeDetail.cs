﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class PracticeDetail
    {
        public List<BPPPractice> _Practices { get; set; }
        public List<BPPProvider> _Providers { get; set; }
        public List<BPPFacilities> _Facilities { get; set; }
        public List<BPPSpecialties> _Specialties { get; set; }
        public List<BPPContracts> _Contracts { get; set; }
            //public List<ProviderApplicationWorkflow> _Workflows { get; set; }
    }
}