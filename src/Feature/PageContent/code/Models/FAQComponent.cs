﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class FAQ
    {        
        public string Question { get; set; }
        public string Answer { get; set; }
    }

    public class FAQComponent
    {
        public string ComponentTitle { get; set; }
        public List<FAQ> _Items { get; set; }
        public string FullPage { get; set; }
    }
}