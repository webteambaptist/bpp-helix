﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class BppHeader
    {
        public BPPMenu menu { get; set; }
        public List<BPPNav> nav { get; set; }
    }
    public class BPPMenu
    {
        public string SiteLogo { get; set; }
        public string SiteLogoURL { get; set; }
        public bool GetAuthMenu { get; set; }
        public List<BPPMenuItem> MenuItems { get; set; }
    }
    public class BPPMenuItem
    {
        public string MenuItemText { get; set; }
        public string MenuItemLogo { get; set; }
        public string MenuItemURL { get; set; }
        public BPPMenuItem()
        {
            MenuItemText = "";
            MenuItemLogo = "";
            MenuItemURL = "";
        }
    }
    public class BPPNav
    {
        public string NavItemText { get; set; }
        public string NavItemURL { get; set; }
        public List<BPPNavLinks> NavLinks { get; set; }
    }
    public class BPPNavLinks
    {
        public string NavLinkText { get; set; }
        public string NavLinkURL { get; set; }
    }
    public class BPPBreadcrumb
    {
        public List<Crumb> Crumbtrail { get; set; }
    }
    public class Crumb
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
    }
}