﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{

    public class BPPSurveyList
    {
        public string User { get; set; }
        public DateTime SubmittedDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        //public string EntityName { get; set; }
        public string SubmitterGroup { get; set; }
        public string ProviderName { get; set; }
        public string ProviderNPI { get; set; }
        public string ChangeReason { get; set; }
        public string NameChange { get; set; }
        public string SubmitterTIN { get; set; }
        public long Id { get; set; }
        public Guid? Practice { get; set; }
        //public string StatusChange { get; set; }
    }

    public class BPPSurveyArchiveList
    {
        public string User { get; set; }
        public DateTime SubmittedDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string SubmitterGroup { get; set; }
        public string ProviderName { get; set; }
        public string ProviderNPI { get; set; }
        public string ChangeReason { get; set; }
        public string NameChange { get; set; }
        public int? ProcessedResults { get; set; }
        public string SubmitterTIN { get; set; }
        public long Id { get; set; }
        public Guid? Practice { get; set; }
    }
}