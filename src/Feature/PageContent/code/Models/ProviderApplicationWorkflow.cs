﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class ProviderApplicationWorkflow
    {
        public int ID { get; set; }
        public System.DateTime CreatedDT { get; set; }
        public string NPINumber { get; set; }
        public int AppStatusID { get; set; }
        public int AppSubStatusID { get; set; }
        public System.DateTime LastUpdateDT { get; set; }
    }
}