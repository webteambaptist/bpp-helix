﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class LegalEntity
    {
        [Display(Name = "Group (Legal Business Name) ")]
        [Required(ErrorMessage = "LBN is Required")]
        public string LegalBusinessName { get; set; }
        [Display(Name = "Group Taxpayer Identification Number (TIN)")]
        [Required(ErrorMessage = "TIN is Required")]
        [MinLength(10, ErrorMessage = "Invalid TaxID")]
        [MaxLength(10, ErrorMessage = "Invalid TaxID")]
        public string TaxID { get; set; }
        [Display(Name = "Mailing Address 1")]
        [Required(ErrorMessage = "Address is Required")]
        public string Address1 { get; set; }
        [Display(Name = "Mailing Address 2")]
        public string Address2 { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "City is Required")]
        public string City { get; set; }
        [Display(Name = "St")]
        [Required(ErrorMessage = "State is Required")]
        public string State { get; set; }
        [Display(Name = "Zip")]
        [Required(ErrorMessage = "Zip is Required")]
        public string ZipCode { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is Required")]
        public string POCName { get; set; }
        [Display(Name = "Phone")]
        [Required(ErrorMessage = "Phone is Required")]
        public string POCPhone { get; set; }
        [Display(Name = "Extension")]
        public string POCExtension { get; set; }
        [Display(Name = "Fax")]
        public string POCFax { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is Required")]
        public string POCEmail { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is Required")]
        public string CAPName { get; set; }
        [Display(Name = "Mailing Address 1")]
        [Required(ErrorMessage = "Address is Required")]
        public string CAPAddress1 { get; set; }
        [Display(Name = "Mailing Address 2")]
        public string CAPAddress2 { get; set; }
        [Display(Name = "City")]
        [Required(ErrorMessage = "City is Required")]
        public string CAPCity { get; set; }
        [Display(Name = "St")]
        [Required(ErrorMessage = "State is Required")]
        public string CAPState { get; set; }
        [Display(Name = "Zip")]
        [Required(ErrorMessage = "Zip is Required")]
        public string CAPZipCode { get; set; }

    }
}