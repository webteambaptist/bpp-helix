﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class MemberRequest
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "The Name field is required")]
        public string Name { get; set; }
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "The Email field is required")]
        [EmailAddress(ErrorMessage = "The value of the Email field is not valid")]
        public string Email { get; set; }
    }
}