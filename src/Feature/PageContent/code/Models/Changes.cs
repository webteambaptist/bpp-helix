﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class Changes
    {
        [Display(Name="Name: ")]
        [Required(ErrorMessage = "Submitter Name is Required")]
        public string SubmitterName { get; set; }
        [Display(Name="Physician Group (Legal Business Name): ")]
        [Required(ErrorMessage = "Physician Group is Required")]
        public string SubmitterPhysicianGroup { get; set; }
        [Display(Name="TIN: ")]
        [Required(ErrorMessage = "TIN is Required")]
        public string SubmitterTIN { get; set; }
        [Display(Name ="Email Address: ")]
        [Required(ErrorMessage = "Email Address is Required")]
        public string SubmitterEmailAddress { get; set; }
        [Display(Name ="Phone Number: ")]
        [Required(ErrorMessage = "Phone Number is Required")]
        public string SubmitterPhoneNumber { get; set; }
        [Display(Name = "Status Change")]
        [Required(ErrorMessage = "Status Change is Required")]
        public List<StatusChange> Change { get; set; }
        [Display(Name = "Status Change Reason")]
        [Required(ErrorMessage = "Status Change Reason Name is Required")]
        public List<StatusChangeReason> Reason { get; set; }      
        public List<Provider> _Providers { get; set; }
        
    }
    public class StatusChange
    {
        public int ID { get; set; }
        public string StatusChange1 { get; set; }
        public bool selected { get; set; }
    }
    public class StatusChangeReason
    {
        public int ID { get; set; }
        public string StatusChangeReason1 { get; set; }
        public bool selected { get; set; }
    }
     public class Provider
    {
        [Display(Name = "Provider's Name: ")]
        [Required(ErrorMessage = "Provider's Name is Required")]
        public string ProviderName { get; set; }
        [Display(Name = "Provider's NPI: ")]
        [Required(ErrorMessage = "Provider's NPI is Required")]
        public string ProviderNPI { get; set; }
        [Display(Name = "Effective Date for Status Change: ")]
        [Required(ErrorMessage = "Effective Date Status Change is Required")]
        public DateTime EffectiveDate { get; set; }
        public int StatusChange { get; set; }
        public int StatusChangeReason { get; set; }
        public int UpdateStatus { get; set; }
        public string UpdateLastName { get; set; }
        public string Other { get; set; }
        public Guid? Practice { get; set; }
    }
}
