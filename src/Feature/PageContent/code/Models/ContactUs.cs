﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class ContactUs
    {
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is Required")]
        public string FirstName { get; set; }
        [Display(Name="Last Name")]
        [Required(ErrorMessage = "Last Name is Required")]
        public string LastName { get; set; }
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name="Email Address")]
        [Required(ErrorMessage = " Email is Required")]
        public string Email { get; set; }
        [Display(Name="Phone")]
        [Required(ErrorMessage = "Phone is Required")]
        public string Phone { get; set; }
        public IEnumerable<SelectListItem> RoleDD { get; set; }
        [Display(Name="Role")]
        [Required(ErrorMessage = "Role is Required")]
        public string Role { get; set; }
        public string Message { get; set; }
    }
}