﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Web.Mvc;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using System.Collections;
using System.Diagnostics.Contracts;

namespace BPP93Helix.Feature.PageContent.Models
{
    #region Custom Models

    public class BPPApplication
    {
        public List<BPPPracticeNames> _PracticeNames { get; set; }
        public BPPMemberRequest _Member { get; set; }
        public BPPReturningMember _ReturningMember { get; set; }
        public Downloads _Downloads { get; set; }
        public List<BPPPractice> _Practices { get; set; }
        public BPPLegalEntity _Entity { get; set; }
        public List<BPPProvider> _Providers { get; set; }
        public List<BPPFacilities> _Facilities { get; set; }
        public List<BPPSpecialties> _Specialties { get; set; }
            public List<Contract> _AcceptingNewPatientContracts { get; set; }

        public List<BPPContracts> _Contracts { get; set; }
        public List<BPPDegrees> _Degrees { get; set; }
        //public List<StatusChangeReason> _Reasons { get; set; }
        public List<MSOVerificationStatu> _MsoDD { get; set; }
        public List<MQRecommendationStatu> _MqRecommendDD { get; set; }
        public List<BoardOutcomeStatu> BoardOutcomeDD { get; set; }
        public List<DenialReasonStatu> DenialReasonDD { get; set; }
        public List<ProviderMemberStatu> ProviderMemberStatusDD { get; set; }
        public List<InactiveReasonStatu> InactiveReasonStatusDD { get; set; }
        public List<LegalEntityStatu> LegalEntityStatusDD { get; set; }
        public NPIValidator _NPIValidator { get; set; }
    //public List<Contracts> Contracts { get; set; }
  }


  public partial class NPIValidator
    {
    public int result_count { get; set; }
    public List<results> results { get; set; }
    }
    public partial class results
    {
        public string enumeration_type { get; set; }
        public int number { get; set; }
        public int last_updated_epoch { get; set; }
        public int created_epoch { get; set; }
        public basic basic { get; set; }
        public List<other_names> other_names { get; set; }
        public List<addresses> addresses { get; set; }
        public List<taxonomies> taxonomies { get; set; }
        public List<identifiers> identifiers { get; set; }
    }
    public partial class basic
    {
        public string name_prefix { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string credential { get; set; }
        public string sole_proprietor { get; set; }
        public string gender { get; set; }
        public string enumeration_date { get; set; }
        public string last_updated { get; set; }
        public string status { get; set; }
        public string name { get; set; }
    }
    public partial class identifiers
    {
        public string identifier { get; set; }
        public string code { get; set; }
        public string desc { get; set; }
        public string state { get; set; }
        public string issuer { get; set; }

    }
    public partial class taxonomies
    {
        public string code { get; set; }
        public string desc { get; set; }
        public bool primary { get; set; }
        public string state { get; set; }
        public string license { get; set; }
    }
    public partial class addresses
    {
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string address_purpose { get; set; }
        public string address_type { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postal_code { get; set; }
        public string telephone_number { get; set; }
    }
    public partial class other_names { }
    public partial class InactiveReasonStatu
    {
        public int ID { get; set; }
        public string StatusName { get; set; }
    }
    public partial class ProviderMemberStatu
    {
        public int ID { get; set; }
        public string MemberStatus { get; set; }
        public int StatusID { get; set; }
    }
    public partial class DenialReasonStatu
    {
        public int ID { get; set; }
        public string StatusName { get; set; }
    }
    public partial class BoardOutcomeStatu
    {
        public int ID { get; set; }
        public string StatusName { get; set; }
    }
    public partial class MQRecommendationStatu
    {
        public int ID { get; set; }
        public string StatusName { get; set; }
    }
    public partial class MSOVerificationStatu
    {
        public int ID { get; set; }
        public string StatusName { get; set; }
    }
    public partial class LegalEntityStatu
    {
        public int ID { get; set; }
        public string StatusName { get; set; }
    }
    public class BPPDegrees
    {
        public int ID { get; set; }
        public int DegreeID { get; set; }
        public string Degree { get; set; }
        public string DegreeName { get; set; }
    }

    public class BPPPracticeNames
    {
        [Display(Name = "PracticeName")]
        [Required(ErrorMessage = "The PracticeName field is required")]
        public string PracticeName { get; set; }
    }
    public class BPPMemberRequest
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "The Name field is required")]
        public string Name { get; set; }
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "The Email field is required")]
        [EmailAddress(ErrorMessage = "The value of the Email field is not valid")]
        public string Email { get; set; }
        public string ActivationKey { get; set; }
        public string LE_Guid { get; set; }
        public Nullable<System.DateTime> CreateDt { get; set; }
        public Nullable<System.DateTime> ModifiedDt { get; set; }
    }
    public class BPPApplicationList
    {
        public BPPLegalEntity _LegalEntity { get; set; }
        public BPPProvider _Provider { get; set; }
        public BPPPractice _Practice { get; set; }
        public List<BPPPracticeLocation> PracticeLocations { get; set; }
    }

    public class BPPApplicationDetails
    {
        public BPPPractice _Practice { get; set; }
        public BPPPracticeLocation _PrimaryPracticeLocation { get; set; }
        public List<BPPProvider> _Providers { get; set; }
    }

    #endregion

    #region Base Models    
    public class BPPPractice
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Practice Name is Required.")]
        public string PracticeName { get; set; }
        public string LegalEntityID { get; set; }
        public string PracticeGUID { get; set; }
        public Nullable<enumPracticeApprovalStatus> PracticeApprovalStatus { get; set; }
        public Nullable<System.DateTime> CreatedDT { get; set; }
        public Nullable<System.DateTime> ModifiedDT { get; set; }
        public List<BPPPracticeLocation> PracticeLocations { get; set; }
        public BPPPractice()
        {
            ID = 0;
            PracticeName = "";
            //  TaxID = "";
            PracticeApprovalStatus = enumPracticeApprovalStatus.None;
        }
    }

    public class BPPPracticeLocation
    {
        public int ID { get; set; }
        public System.Guid PracticeGUID { get; set; }
        public Nullable<System.Guid> LocationGUID { get; set; }
        public string LocationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [Required(ErrorMessage = "Zip is Required")]
        [MinLength(5, ErrorMessage = "Invalid Zip")]
        [MaxLength(10, ErrorMessage = "Invalid Zip")]
        [RegularExpression("^[0-9]{5}(?:-[0-9]{4})?$", ErrorMessage = "Invalid Zip Format")]
        public string Zip { get; set; }
        public Nullable<bool> IsPrimary { get; set; }
        [Required(ErrorMessage = "Practice Phone is Required")]
        [MinLength(10, ErrorMessage = "Phone must be 10 digits")]
        [MaxLength(10, ErrorMessage = "Phone must be 10 digits")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Value must be numbers")]
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        [Required(ErrorMessage = "Practice Fax is Required")]
        [MinLength(10, ErrorMessage = "Fax must be 10 digits")]
        [MaxLength(10, ErrorMessage = "Fax must be 10 digits")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Value must be numbers")]
        public string Fax { get; set; }
        public string EMRVendor { get; set; }
        public string EMRVersion { get; set; }
        public string OfficeManagerName { get; set; }
        [Required(ErrorMessage = "Office Manager Phone is Required")]
        [MinLength(10, ErrorMessage = "Phone must be 10 digits")]
        [MaxLength(10, ErrorMessage = "Phone must be 10 digits")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Value must be numbers")]
        public string OfficeManagerPhoneNumber { get; set; }
        public string OfficeManagerExtension { get; set; }
        public string OfficeManagerEmail { get; set; }
        public string BillingManagerName { get; set; }
        public string BillingManagerPhoneNumber { get; set; }
        public string BillingManagerExtension { get; set; }
        public string BillingManagerEmail { get; set; }
        public System.DateTime CreatedDT { get; set; }
        public Nullable<System.DateTime> ModifiedDT { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public string FullAddress { get; set; }
        public Boolean isRemoved { get; set; }
        public List<BPPProvider> Providers { get; set; }
    }

    public class BPPProvider
    {
        public int ID { get; set; }
        //public string TaxID { get; set; }
        [Required(ErrorMessage = "First Name is Required.")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "Last Name is Required.")]
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string FullNameWithSuffix { get; set; }
        [Required(ErrorMessage = "Degree is Required.")]
        public string Degree { get; set; }
        public string DateOfBirth { get; set; }
        [Required(ErrorMessage = "Phone is Required")]
        [MinLength(10, ErrorMessage = "Phone must be at least 14 digits")]
        [MaxLength(14, ErrorMessage = "Phone must be 14 digits")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "The Email field is required")]
        [EmailAddress(ErrorMessage = "The value of the Email field is not valid")]
        public string Email { get; set; }
        [Required(ErrorMessage = "NPI is Required")]
        [MinLength(10, ErrorMessage = "Invalid NPI#")]
        [MaxLength(10, ErrorMessage = "Invalid NPI#")]
        public string NPINumber { get; set; }
        public DateTime? CreatedDT { get; set; }
        public DateTime? ModifiedDT { get; set; }
        public bool? isBHPrivileged { get; set; }
        public Nullable<System.Guid> GUID { get; set; }
        public string CMS { get; set; }
        public BPPProviderAncillary ProviderAncillary { get; set; }
        public string PrimarySpecialty { get; set; }
        public string SecondarySpecialty { get; set; }
        public string SpecialtyNotes { get; set; }
        //public string StatusChangeReason { get; set; }
        //public Nullable<int> StatusChangeReasonID { get; set; }
        //public string OtherReason { get; set; }

        //public BPPProviderApplicationWorkflow ProviderApplicationWorkFlow { get; set; }
        public BPPProviderPracticeWorkflow ProviderPracticeWorkFlow { get; set; }
        //public List<BPPProviderSpecialty> ProviderSpecialties { get; set; }
        public Nullable<int> ProviderMemberStatus { get; set; }
        public BPPProviderMemberStatus MemberStatus { get; set; }
        //public BPPProviderApplicationStatus ApplicationStatus { get; set; }
        public int PrimaryPracticeLocation { get; set; }
        public List<int> OtherPracticeLocations { get; set; }
        public List<int> ProviderFacilities { get; set; }
        public List<BPPFacilities> Facilties { get; set; }
        public List<int> ProviderAcceptingPatients { get; set; }
        //public List<BPPProviderAcceptingPatients> AcceptingNewPatientsContracts { get; set; }
        public List<AcceptingContracts> Contracts { get; set; }
    //public List<BPPPracticeLocation> Locations { get; set; }
    public IEnumerable<SelectListItem> Specialties { get; set; }
        public IEnumerable<SelectListItem> Degrees { get; set; }

        //public List<MSOVerificationStatu> _MsoDD { get; set; }
        public string MSOVerification { get; set; }
        public IEnumerable<SelectListItem> _MSOVerficationStatus { get; set; }
        //public List<MQRecommendationStatu> _MqRecommendDD { get; set; }
        public string MQRecommendation { get; set; }
        public IEnumerable<SelectListItem> _MQRecommendationStatus { get; set; }
        //public List<BoardOutcomeStatu> BoardOutcomeDD { get; set; }
        public string BoardOutcomeStatus { get; set; }
        public IEnumerable<SelectListItem> _BoardOutcome { get; set; }
        //public List<DenialReasonStatu> DenialReasonDD { get; set; }
        public string DenialReason { get; set; }
        public IEnumerable<SelectListItem> _DenialReasons { get; set; }
        //public List<ProviderMemberStatu> ProviderMemberStatusDD { get; set; }
        public string ProvidersMemberStatus { get; set; }
        public IEnumerable<SelectListItem> _ProviderMemberStatus { get; set; }
        //public List<InactiveReasonStatu> InactiveReasonStatusDD { get; set; }
        public string InactiveReason { get; set; }
        public IEnumerable<SelectListItem> _InactiveReasons { get; set; }
        public DateTime? EndDate { get; set; }
        public ProviderPracticeMatrix matrix { get; set; }
        public bool isNew { get; set; }
    }
    public class ProviderPracticeMatrix
    {
        public int? ID { get; set; }
        public System.Guid PracticeGUID { get; set; }
        public string NPINumber { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
    }
    public class BPPProviderAncillary
    {
        public string NPINumber { get; set; }
        public DateTime CreatedDT { get; set; }
        public string AKA { get; set; }
        public string Maiden_Name { get; set; }
        public string EmploymentStatus { get; set; }
        public string SpecialtyType { get; set; }
        public string ServicePopulation { get; set; }
        public string FacilitySetting { get; set; }
    public string PracticeGuid { get; set; }
        public int? ProviderPracticeMatrixID { get; set; }
    }
    public class BPPContracts
    {
        public int ContractID { get; set; }
        public string ContractName { get; set; }
        public bool OptOut { get; set; }
    }
    public class AcceptingContracts
  {
      public int ID { get; set; }
      public string ContractName { get; set; }
      public bool isSelected { get; set; }
      public int? ProviderPracticeMatrixID { get; set; }
    }
    public class Contracts
    {
        public int ID { get; set; }
        public string ContractName { get; set; }
        public bool isSelected { get; set; }
        public int? ProviderPracticeMatrixID { get; set; }
  }
    public class BPPFacilities
    {
        public int ID { get; set; }
        public string FacilityName { get; set; }
        public bool isSelected { get; set; }
        public int? ProviderPracticeMatrixID { get; set; }
    }

  public class BPPProviderLocations
    {
        public int ID { get; set; }
        public string NPINumber { get; set; }
        public int LocationID { get; set; }
        public bool? IsPrimary { get; set; }
        public int? ProviderPracticeMatrixID { get; set; }
    }
    public class BPPProviderMemberStatus
    {
        public int ID { get; set; }
        public string MemberStatus { get; set; }
        public int StatusID { get; set; }
    }
    //public class BPPProviderApplicationStatus
    //{
    //    public int ID { get; set; }
    //    public string ApplicationStatus { get; set; }
    //    public int StatusID { get; set; }
    //}
    public class BPPSpecialties
    {
        public int ID { get; set; }
        public string Specialty { get; set; }
        public string Code { get; set; }
    }
    public class BPPPractices
    {
        public string ID { get; set; }
        public string PracticeName { get; set; }
    }
    public class BPPProviderApplicationWorkflow
    {
        public int ID { get; set; }
        public DateTime? CreateDT { get; set; }
        public string NPINumber { get; set; }
        public int AppStatusID { get; set; }
        public int AppSubStatusID { get; set; }
        public DateTime? LastUpdateDT { get; set; }
        public Guid? PracticeGUID { get; set; }
    }
    public class BPPProviderPracticeWorkflow
    {
        public int ID { get; set; }
        public string NPINumber { get; set; }
        public System.Guid PracticeGUID { get; set; }
        public DateTime? DateSignedMemberApp { get; set; }
        public DateTime? DateSignedJoinder { get; set; }
        public DateTime? DateJoinderReceived { get; set; }
        public DateTime? DatePresentedMQProcess { get; set; }
        public DateTime? DatePresentedReady { get; set; }
        public DateTime? DateRecommendationSent { get; set; }
        public DateTime? BoardConsiderationDate { get; set; }
        public string Outcome { get; set; }
        public DateTime? DateNotifcationOutcome { get; set; }
        public string OutcomeNotes { get; set; }
        public string PrimaryPracticeLocation { get; set; }
        public DateTime? EligibilityCertDate { get; set; }
        public string MedStaffPrivileges { get; set; }
        public DateTime? BPPEffectiveDate { get; set; }
        public string Witness { get; set; }
        //public int? ProviderApprovalStatus { get; set; }

        public int? MSOVerificationStatusID { get; set; }
        public int? MQRecommendationStatusID { get; set; }
        public int? BoardOutcomeStatusID { get; set; }
        public int? DenialReasonStatusID { get; set; }
        public int? ProviderStatusID { get; set; }
        public int? InactiveReasonStatusID { get; set; }
        public DateTime? ProviderInactiveDate { get; set; }
        public DateTime? NotificationDate { get; set; }
        public int? ProviderPracticeMatrixID { get; set; }
    }
    public class BPPProviderSpecialty
    {
        public int ID { get; set; }
        public string NPINumber { get; set; }
        public string Specialty { get; set; }
        public bool? isPrimary { get; set; }
        public int? ProviderPracticeMatrixID { get; set; }
    }

    public class UpdateProviderStatus
    {
        public int rowID;
        public int wfStage;
        public int memStatus;
    }

    #endregion

    #region Enumerations

    public enum enumPracticeApprovalStatus
    {
        None = 0,
        New = 1,
        Accepted = 2,
        Completed = 3,
        Approved = 4,
        Rejected = 5,
        Deferred = 6,
        Archived = 7,
        Deleted = 8
    }

    //public enum enumProviderMemberStatus
    //{
    //    None = 0,
    //    New = 1,
    //    Pending = 2,
    //    Member = 3,
    //    Rejected = 4,
    //    Deferred = 5,
    //    Archived = 6,
    //    Deleted = 7
    //}

    // Workflow Stage Enum
    //public enum enumProviderApprovalStatus
    //{
    //    None = 0,
    //    New = 1,
    //    [EnumMember(Value = "Joinder Received")]
    //    JoinderReceived = 2,
    //    [EnumMember(Value = "Med Staff Review")]  //9
    //    MSReview = 3,
    //    [EnumMember(Value = "Committee Review")] //10
    //    CReview = 4,
    //    [EnumMember(Value = "Board Review")] //11
    //    BReview = 5,
    //    [EnumMember(Value = "Approved")]
    //    Approved = 6,
    //    [EnumMember(Value = "Package Pending")]
    //    PackagePending = 7,
    //    [EnumMember(Value = "Complete")] //12
    //    Complete = 8

    //}

    public enum enumSSRSReportType
    {
        [Description("Med Staff Report")]
        MReport = 1,
        [Description("Committee Report")]
        CReport = 2,
        [Description("Board Report")]
        BReport = 3,
        //[Description("Payor Report")]
        [Description("Left Network Report")]
        PReport = 4,
        //[Description("Area Of Practice Report")]
        //APReport = 5,
        [Description("Current Members")]
        CustReport = 6,
        [Description("Master Lookup")]
        MasterReport = 7,
        [Description("Legal Entity Report")]
        LegalEntityReport = 8,
        [Description("Group Point of Contact (POC) Report")]
        TINPOCReport = 9,
        [Description("Active Adult PCP Report")]
        PCPReport = 10,
        [Description("Active Adult Specialists Report Report")] 
        SpecialistsReport = 11,
        [Description("Active Pediatrician Report")]
        PediatricianReport = 12,
        [Description("Active Pediatric Specialists Report")]
        PediatricianSpecialistReport = 13,
    }

    #endregion
}

public static class EnumHelper<T>
{
    public static IList<T> GetValues(Enum value)
    {
        var enumValues = new List<T>();

        foreach (FieldInfo fi in value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public))
        {
            enumValues.Add((T)Enum.Parse(value.GetType(), fi.Name, false));
        }
        return enumValues;
    }

    public static T Parse(string value)
    {
        return (T)Enum.Parse(typeof(T), value, true);
    }

    public static IList<string> GetNames(Enum value)
    {
        return value.GetType().GetFields(BindingFlags.Static | BindingFlags.Public).Select(fi => fi.Name).ToList();
    }

    public static IList<string> GetDisplayValues(Enum value)
    {
        return GetNames(value).Select(obj => GetDisplayValue(Parse(obj))).ToList();
    }

    private static string lookupResource(Type resourceManagerProvider, string resourceKey)
    {
        foreach (PropertyInfo staticProperty in resourceManagerProvider.GetProperties(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public))
        {
            if (staticProperty.PropertyType == typeof(System.Resources.ResourceManager))
            {
                System.Resources.ResourceManager resourceManager = (System.Resources.ResourceManager)staticProperty.GetValue(null, null);
                return resourceManager.GetString(resourceKey);
            }
        }

        return resourceKey; // Fallback with the key name
    }

    public static string GetDisplayValue(T value)
    {
        var fieldInfo = value.GetType().GetField(value.ToString());

        var descriptionAttributes = fieldInfo.GetCustomAttributes(
            typeof(DisplayAttribute), false) as DisplayAttribute[];

        if (descriptionAttributes[0].ResourceType != null)
            return lookupResource(descriptionAttributes[0].ResourceType, descriptionAttributes[0].Name);

        if (descriptionAttributes == null) return string.Empty;
        return (descriptionAttributes.Length > 0) ? descriptionAttributes[0].Name : value.ToString();
    }
}
