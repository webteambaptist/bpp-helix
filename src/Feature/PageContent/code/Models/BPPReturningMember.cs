﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace BPP93Helix.Feature.PageContent.Models
{
    public class BPPReturningMember
    {
        [Display(Name = "Group Taxpayer Identification Number (TIN)")]
        [Required(ErrorMessage = "TIN is Required")]
        public string TaxID { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "The Email field is required")]
        [EmailAddress(ErrorMessage = "The value of the Email field is not valid")]
        public string POCEmail { get; set; }
        public Nullable<System.DateTime> CreateDt { get; set; }
        public Nullable<System.DateTime> ModifiedDt { get; set; }
    }
}