﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class BPPProviders
    {
        public int ID { get; set; }
        //public string TaxID { get; set; }
        [Required(ErrorMessage = "First Name is Required.")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "Last Name is Required.")]
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string FullNameWithSuffix { get; set; }
        [Required(ErrorMessage = "Degree is Required.")]
        //public string Degree { get; set; }
        public List<DegreeType> Degrees { get; set; }
        public string DateOfBirth { get; set; }
        public List<SpecialtyMatrix> Specialities { get; set; }
        //public string PrimarySpecialty { get; set; }
        //public string SecondarySpecialty { get; set; }
        [Required(ErrorMessage = "NPI is Required")]
        [MinLength(10, ErrorMessage = "Invalid NPI#")]
        [MaxLength(10, ErrorMessage = "Invalid NPI#")]
        public string NPINumber { get; set; }
        [Required(ErrorMessage = "Phone is Required")]
        [MinLength(10, ErrorMessage = "Phone must be 10 digits")]
        [MaxLength(10, ErrorMessage = "Phone must be 10 digits")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Value must be numbers")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Personal Email is Required.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        public bool? isBoardCert { get; set; }
        public bool? isBHPrivileged { get; set; }
        public string ResidencyCompletionDate { get; set; }
        public string DateOfBoardExam { get; set; }
        public DateTime? CreatedDT { get; set; }
        public DateTime? ModifiedDT { get; set; }
        public BPPProviderApplicationWorkflow ProviderApplicationWorkFlow { get; set; }
        public BPPProviderPracticeWorkflow ProviderPracticeWorkFlow { get; set; }
        public List<BPPProviderSpecialty> ProviderSpecialties { get; set; }
        public Nullable<int> ProviderMemberStatus { get; set; }
        public BPPProviderMemberStatus MemberStatus { get; set; }
        //public BPPProviderApplicationStatus ApplicationStatus { get; set; }
        public Nullable<System.Guid> GUID { get; set; }
        public string CMS { get; set; }
        public int PrimaryPracticeLocation { get; set; }
        public List<int> OtherPracticeLocations { get; set; }
        public Nullable<int> CareType { get; set; }
        public Nullable<int> ProviderType { get; set; }
        public Nullable<int> Based { get; set; }
        public List<int> ProviderFacilities { get; set; }
        public List<BPPFacilities> Facilties { get; set; }
        //public List<BPPProviderLocations> Locations { get; set; }
        //public BPPPracticeLocation Locations { get; set; }
        public List<BPPPracticeLocation> Locations { get; set; }
        public IEnumerable<SelectListItem> Specialties { get; set; }
        public string AlsoKnownAs { get; set; }
    }
    public class DegreeType
    {
        public int ID { get; set; }
        public int DegreeID { get; set; }
        public string Degree { get; set; }
        public string DegreeName { get; set; }
    }
    public class SpecialtyMatrix
    {
        public int ID { get; set; }
        public string Specialty { get; set; }
        public string Code { get; set; }
        public Nullable<int> SpecialtyID { get; set; }
    }
}