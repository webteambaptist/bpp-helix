﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class CarouselComponent
    {        
        public string ImageTitle { get; set; }
        public string ImageText { get; set; }
        public ImageField ImgField { get; set; }
        public string ButtonText { get; set; }
        public string ButtonURL { get; set; }
    }

    public class CarouselComponentItems
    {
        public List<CarouselComponent> _Items { get; set; }
    }    
}