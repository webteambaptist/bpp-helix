﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class NewestMembers
    {
        public NewestMembers() { }
        public string FirstName { get;set;}
        public string LastName { get; set; }
        public string Bio { get; set; }
        public string PrimarySpecialty { get; set; }
        public string Image { get; set; }
        //public DateTime NewMemberApprovalDT { get; set; }
        public string ItemPath { get; set; }
        public List<NewestMembers> _Items { get; set; }
        public DateTime MemberDate { get; set; }
    }

    public class Members
    {
        public List<NewestMembers> _Items { get; set; }
    }
}

