﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class Report
    {
        public DataTable Providers { get; set; }
        public DataTable Practices { get; set; }
    }
}