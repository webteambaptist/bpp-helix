﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Models
{
    public class NewMemberApplication
    {
        public string GUID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int AppStep { get; set; }
    }
}