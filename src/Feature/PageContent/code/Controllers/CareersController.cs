﻿using BPP93Helix.Feature.PageContent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class CareersController : Controller
    {
        // GET: Careers
        public ActionResult Index()
        {
            Careers car = new Careers();

            car.Jobs = Careers.readXML();

            return View(car);
        }
    }
}