﻿using BPP93Helix.Feature.PageContent.Helper;
using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class MasterLookupController : Controller
    {
      bool auth = false;
      public MasterLookupController()
      {
          //auth = User.Identity.IsAuthenticated;
          //HttpContext.GetOwinContext().Authentication.Challenge("openID");
          //var claims = User.Identity as System.Security.Claims.ClaimsIdentity;
          //auth = User.Identity.IsAuthenticated;
          //if (!auth)
          //{
          //  
          //}
      }
        // GET: MasterLookup
        public ActionResult Index()
        {
            //if (auth)
                return View();
            //else
            //{
            //    return Redirect("~/Login/");
            //}
        }
        private readonly static string spAPIGetEntityDetails = Settings.GetSetting("spAPIGetEntityDetails");
        private readonly static string spAPIUpdEntityDetails = Settings.GetSetting("spAPIUpdEntityDetails");
        private readonly static string spAPISelLegalEntityStatus = Settings.GetSetting("spAPISelLegalEntityStatus");
        public ActionResult EntityProfile(string ID)
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                BPPApplication _bppApplication = new BPPApplication();
                try
                {
                    if (!string.IsNullOrEmpty(ID))
                    {
                        _bppApplication = GetEntityProfile(ID);
                        if (_bppApplication != null)
                        {
                            var _legalentitystatus = LegalEntityStatus(_bppApplication);
                            _bppApplication._Entity._LegalEntityStatus = GetSelectListItems(_legalentitystatus);

                            //var legalstatus = GetLegalStatus(_bppApplication);
                            //_bppApplication._Entity.Status = legalstatus;
                            return View(_bppApplication);
                        }
                        else
                            return Redirect(Url.Content("~/"));
                    }
                    else
                    {
                        return Redirect(Url.Content("~/"));
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: EntityProfile -> Exception : " + ex.ToString(), this);
                    _bppApplication = null;
                    return View(_bppApplication);
                }
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: EntityProfile -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/"));
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: EntityProfile -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/Login"));
                }
            }
        }
        
        [HttpGet]
        public static IEnumerable<string> LegalEntityStatus(BPPApplication _bppApplication)
        {
            List<LegalEntityStatu> legalEntityStatus = new List<LegalEntityStatu>();
            legalEntityStatus = _bppApplication.LegalEntityStatusDD;

            List<string> _legalstatus = new List<string>();
            foreach (var item in legalEntityStatus)
            {
                _legalstatus.Add(item.StatusName);
            }
            return _legalstatus;
        }
        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="Specialty Name">Specialty Name</option>
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
        public BPPApplication GetEntityProfile(string ID)
        {
            BPPApplication _bppApplication = new BPPApplication();
            if (!string.IsNullOrEmpty(ID))
            {
                try
                {
                    HttpWebResponse httpWebResponse = null;
                    httpWebResponse = API.GetDataFromAPI(spAPIGetEntityDetails, "LegalEntityID|" + ID);
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: GetEntityProfile -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: GetEntityProfile -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            JsonSerializer js = new JsonSerializer();
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            _bppApplication = (BPPApplication)JsonConvert.DeserializeObject(objText, typeof(BPPApplication), settings);
                        }
                        if (_bppApplication != null)
                        {
                            return _bppApplication;
                        }
                        else
                        {
                            _bppApplication = null;
                            TempData["appListResponse"] = "500 Internal Error";
                        }

                    }
                    else
                    {
                        _bppApplication = null;
                        TempData["appListResponse"] = "500 Internal Error";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: GetEntityProfile -> Exception : " + ex.ToString(), this);
                    _bppApplication = null;
                }
            }
            else
            {
                _bppApplication = null;
                TempData["appListResponse"] = "500 Internal Error";
            }
            return _bppApplication;
        }
        [HttpPost]
        // public ActionResult SaveEntityProfile(BPPLegalEntity legalEntity)
        public ActionResult SaveEntityProfile(BPPApplication _bppApplication)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                try
                {
                    if (_bppApplication != null && _bppApplication._Entity !=null)
                    {
                        var UserName = Sitecore.Context.GetUserName().Split('\\');
                        string jo = JsonConvert.SerializeObject(_bppApplication);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: Update EntityProfile -> Submitting JSON Final Copy:" + _bppApplication.ToString(), _bppApplication);
                        HttpWebResponse httpWebResponse = API.PostDataToAPI(spAPIUpdEntityDetails, jo.ToString(), "UserName|" + UserName[1]);
                        if (httpWebResponse != null)
                        {
                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: Update EntityProfile -> API Response : " + httpWebResponse, httpWebResponse);
                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: Update EntityProfile -> API Response Code: " + (int)httpWebResponse.StatusCode, httpWebResponse);
                            if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
                            {
                                status = "success";
                                message = "Legal Entity details have been updated successfully.";
                            }
                            else
                            {
                                status = "error";
                                message = httpWebResponse.StatusCode + ": An Error occurred while processing your request";
                            }
                        }
                        else
                        {
                            status = "error";
                            message = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";
                        }

                    }
                    else
                    {
                        status = "error";
                        message = "500 Internal Error";
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: Update EntityProfile -> Empty Practice Profile.", _bppApplication);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: Update EntityProfile -> Exception : " + ex.ToString(), this);
                }
                returnList.Add(status);
                returnList.Add(message);
                return Json(returnList, JsonRequestBehavior.DenyGet);
            }
            if (BHUser.Components.Login.IfUserLoggedIn())
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: Update EntityProfile -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                return Redirect(Url.Content("~/"));
            }
            else
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.MasterLookupController :: Update EntityProfile -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                return Redirect(Url.Content("~/Login"));
            }
        }
    }
}