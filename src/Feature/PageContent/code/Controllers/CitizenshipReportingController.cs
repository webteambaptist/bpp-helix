﻿using BHSearch.Components;
using BHSearch.Helpers;
using BHSearch.Models;
using ClosedXML.Excel;
using Sitecore.Configuration;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Newtonsoft.Json;
using Sitecore.Diagnostics;

namespace BPP93Helix.Feature.PageContent.Controllers
{
  public class CitizenshipReportingController : Controller
  {
    // GET: DocumentCenter
    public ActionResult Index()
    {
      return View();
    }
    [HttpPost]
    public JsonResult NpiSearch(string npi, string year)
    {
      string id = "";
      //var directory = @"D:\Temp\CitizenReporting\";
      var directory = $@"D:\Temp\CitizenReporting\{year}\";
      if (string.IsNullOrEmpty(npi))
      {
        throw new ArgumentException($"'{nameof(npi)}' cannot be null or empty.", nameof(npi));
      }
      var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
      defaultUrlOptions.EncodeNames = true;
      try
      {
        // Search Sitecore for Title of file with Tin in it.
        var master = Factory.GetDatabase("web");
        //var item = master.GetItem("/sitecore/media library/BPP/Citizenship Reporting");
        var item = master.GetItem("/sitecore/media library/BPP/Citizenship Reporting/" + year);
        var items = item.GetChildren();

        if (items.Count > 0)
        {
          // create directory if it doesn't exist
          if (!Directory.Exists(directory))
          {
            Directory.CreateDirectory(directory);
          }

          // copy files to file system
          foreach (Item file in items)
          {
            var media = new MediaItem(file);

            var fileType = file.Fields["Extension"].Value;
            if (fileType == "xls" || fileType == "xlsx")
            {
              var stream = MediaManager.GetMedia(media).GetStream().Stream;

              var filename = directory + file.ID.ToString().Trim(new char[] {'{', '}' }) + "." + fileType; // name it the ID

              using (var fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write))
              {
                stream.CopyTo(fileStream);
              }
            }
          }

          // see if tin exists in the file
          var files = Directory.GetFiles(directory);
          for (int i = 0; i < files.Length; i++)
          {

            string f = files[i];
            if (!string.IsNullOrEmpty(id))
            {
              break;
            }
            using (XLWorkbook workbook = new XLWorkbook(f))
            {
              var rows = workbook.Worksheet(1).RangeUsed().RowsUsed().Skip(1); // skip first row
              foreach (var cellValue in from row in rows
                                        let cellValue = row.Cell(2).Value
                                        select cellValue)
              {
                  if (cellValue.ToString() != npi)
                  {
                      continue;
                  }

                  var fn = f.Split('\\').Last();
                  id = fn;
                  break;
              }
            }
          }

          // delete the files in the file system
          DirectoryInfo info = new DirectoryInfo(directory);
          foreach (FileInfo fi in info.GetFiles())
          {
            fi.Delete();
          }
          return Json(id);
        }
        return Json("");
      }
      catch (Exception e)
      {
        return BuildJsonErrorResponse(e);
      }

    }
    [HttpPost]
    public JsonResult DocumentQuery(SolrQuery obj, string year)
    {
      try
      {
        // Execute Search
        var search = new ExecuteCitizenshipSearch();
        obj.Handler = "/bpp_citizenshipreporting_index/citizenship";
        obj.Facets = new List<string>(new string[] {
                    "__semantics"
                 });
        if (obj.Sort == null || string.IsNullOrEmpty(obj.Sort))
          obj.Sort = "__created_tdt";
        if (obj.SortOrder == null || string.IsNullOrEmpty(obj.SortOrder))
          obj.SortOrder = "desc";
        
        var response = search.Execute(obj, year);
        // Map Results to Physician Model
        var documents = new List<Document>();
        var properties = typeof(Document).GetProperties();
        // calculate real total hits
        
        foreach (var doc in response.Results)
        {
          var document = new Document();
          
          foreach (var prop in properties)
          {
            if (doc.TryGetValue(prop.Name, out var value))
            {
              // change thumbnail size, can handle from front end as well
              if (prop.Name.Equals("Thumbnail"))
              {
                prop.SetValue(document, value.ToString().Replace("16", "50"));
              }
              else if (prop.Name.ToLower().Equals("extension"))
              {
                switch (value.ToString().ToLower())
                {
                  case "xls":
                  case "xlsx":
                    {
                      prop.SetValue(document, "excel.png");
                      break;
                    }
                  default:
                    {
                      prop.SetValue(document, "alternatedoc.png");
                      break;
                    }
                }
              }
              else
              {
                prop.SetValue(document, value);
              }
            }
            else
            {
              prop.SetValue(document, null);
            }
          }

          documents.Add(document);
        }

        // Map Views to Response
        var json = new JsonResponse
        {
          // etc
          TotalHits = response.TotalHits,
          OriginalQuery = response.OriginalQuery,
          //results
          Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", new DocumentResultView()
          {
            Start = obj.Start,
            Documents = documents
          })
        };
        
        // facets
        RenderFacetView(response, "__semantics", json, "_FacetCheckboxes");

        return Json(json);
      }
      catch (Exception e)
      {
        Log.Error("Exception occurred getting results " + e.Message, this);
        return BuildJsonErrorResponse(e);
      }
    }
    
    [HttpPost]
    public JsonResult DocumentTypeahead(SolrQuery obj)
    {
      try
      {
        // Execute Search
        var search = new ExecuteSearch();
        obj.Handler = "/bpp_citizenshipreporting_index/citizenshipSuggest";
        
        var response = search.Execute(obj);

        // Map Results to Physician Model
        var documents = new List<Document>();
        var properties = typeof(Document).GetProperties();

        foreach (var doc in response.Results)
        {
          var document = new Document();

          foreach (var prop in properties)
          {
            prop.SetValue(document, doc.TryGetValue(prop.Name, out object value) ? value : null);
          }

          documents.Add(document);
        }

        return Json(documents);
      }

      catch (Exception e)
      {
        Log.Error($"Exception :: CitizenshipReportingController::DocumentTypeahead :: {JsonConvert.SerializeObject(obj)} :: {e.Message}", this);
        return BuildJsonErrorResponse(e);
      }
    }

    [HttpPost]
    public JsonResult DocumentFacets(SolrQuery obj)
    {
      try
      {
        // Execute Search
        var search = new ExecuteSearch();
        obj.Query = "*";
        obj.Rows = 1;
        obj.Handler = "/bpp_citizenshipreporting_index/citizenship";
        obj.Facets = new List<string>(new string[] {
                    "title_s"
                 });
        var response = search.Execute(obj);

        return Json(RenderFacetList(response, obj));
      }
      catch (Exception e)
      {
        return BuildJsonErrorResponse(e);
      }
    }

    private void RenderFacetView(SolrResponse response, string facetField, JsonResponse json, string partial)
    {
      FacetView facetView = new FacetView();
      ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
      if (response.Facets.TryGetValue(facetField, out facets))
      {
        string filter = "";
        if (response.OriginalQuery.Filters.TryGetValue(facetField, out filter))
        {
          facetView.Filter = filter.Split(',').ToList();
        }

        facetView.Facet = new KeyValuePair<string, ICollection<KeyValuePair<string, int>>>(facetField, facets);
        json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
      }
    }

    private Dictionary<string, ICollection<KeyValuePair<string, int>>> RenderFacetList(SolrResponse response, SolrQuery obj)
    {
      Dictionary<string, ICollection<KeyValuePair<string, int>>> Facets = new Dictionary<string, ICollection<KeyValuePair<string, int>>>();

      foreach (string facetField in obj.Facets)
      {
        ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
        if (response.Facets.TryGetValue(facetField, out facets))
        {
          Facets.Add(facetField, facets);
        }
      }

      return Facets;
    }

    private JsonResult BuildJsonErrorResponse(Exception e)
    {
      Log.Error($"Error::{e.Message}", this);
      var error = new Dictionary<string, object> {{"Status", "Error"}};
      return Json(error);
    }

    public FileResult Download(string FilePath)
    {
      try
      {
        FilePath = "/sitecore/media library" + FilePath;
        Sitecore.Data.Database current = Sitecore.Context.Database;
        Sitecore.Data.Items.Item item = Sitecore.Context.Database.Items[FilePath];
        if (item != null)
        {
          Item sampleMedia = new Sitecore.Data.Items.MediaItem(item);
          var mediaItem = (MediaItem)sampleMedia;
          Stream stream = mediaItem.GetMediaStream();
          long fileSize = stream.Length;
          byte[] buffer = new byte[(int)fileSize];
          stream.Read(buffer, 0, (int)stream.Length);
          stream.Close();
          string fileName = mediaItem.Name + "." + mediaItem.Extension;
          return File(buffer, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        else
        {
          return null;
        }
      }
      catch (Exception ex)
      {
        Log.Error("BPP93Helix.Feature.PageContent.Controllers.DocumentCenterController :: Download -> Exception : " + ex.ToString(), this);
        return null;
      }

    }

  }
}