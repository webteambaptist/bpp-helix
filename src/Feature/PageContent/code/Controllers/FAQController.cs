﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BPP93Helix.Feature.PageContent.Models;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data;
using BPP93Helix.Feature.PageContent.Helper;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class FAQController : Controller
    {
        // GET: FAQ
        public ActionResult Index()
        {
            FAQComponent faqs = new FAQComponent();
            // Get Title from Rendering Parameters
            faqs.ComponentTitle = BHSitecore.GetValueFromCurrentRenderingParameters("ComponentTitle");
            faqs._Items = new List<FAQ>();
            faqs.FullPage = BHSitecore.GetValueFromCurrentRenderingParameters("FullPage");

            //Get tags from Rendering Parameters
            string[] selectedCategories = BHSitecore.GetValuesFromCurrentRenderingParameters("SelectedCategories");

            
            //Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(domainUser, false);
            // UserSwitcher allows below code to run under a specific user using
            //(new Sitecore.Security.Accounts.UserSwitcher(user))
            //{ var ReadAccess = item.Access.CanRead(); }

            //Getting ahold of each FAQ in the FAQ folder
            ID faqFolderID = new ID("9356A56A-49FB-434B-92EB-1C2476366E04");
            Item faqFolder = Sitecore.Context.Database.GetItem(faqFolderID);

            //We need to ignore any Sub Folders found, so we will exclude any
            //items with the following TemplateID
            ID commonFolderID = new ID("A87A00B1-E6DB-45AB-8B54-636FEC3B5523");

            foreach (Item faqItem in faqFolder.GetChildren())
            {
                // only filter if tags are found on rendering parameters
                if (selectedCategories.Length > 0)
                {
                    // get tags on this faq item
                    MultilistField tags = faqItem.Fields["__semantics"];
                    // check for any matches between this faq item's tags and selectedCategories from rendering parameters
                    var matches = BHSitecore.GetValuesFromMultiListField(tags).Intersect(selectedCategories, StringComparer.OrdinalIgnoreCase);
                    if (matches.Count<string>() == 0)
                    {
                        // if no matches, skip this FAQ
                        continue;
                    }
                }

                // Filter out any subfolder items
                if (faqItem.TemplateID != commonFolderID)
                {
                    // If we made it here, add this FAQ to the list
                    FAQ faq = new FAQ();
                    faq.Question = faqItem.Fields["Question"].Value;
                    faq.Answer = faqItem.Fields["Answer"].Value;
                    faqs._Items.Add(faq);
                }
            }

            return PartialView(faqs);
        }

        public ActionResult AboutUsFAQs()
        {
            FAQComponent faqs = new FAQComponent();
            faqs.ComponentTitle = "Frequently Asked Questions";
            faqs._Items = new List<FAQ>();

            ID faqFolderID = new ID("9356A56A-49FB-434B-92EB-1C2476366E04");
            Item faqFolder = Sitecore.Context.Database.GetItem(faqFolderID);

            string[] aboutUsTag = new string[1];
            aboutUsTag[0] = "AboutUs";

            foreach (Item faqItem in faqFolder.GetChildren())
            {
               
                // get tags on this faq item
                MultilistField tags = faqItem.Fields["__semantics"];
                // Only add FAQs with the AboutUs tag to the list
                var matches = BHSitecore.GetValuesFromMultiListField(tags).Intersect(aboutUsTag, StringComparer.OrdinalIgnoreCase);
                if (matches.Count<string>() == 0)
                {
                    // if no matches, skip this FAQ
                    continue;
                }
              

                // If we made it here, add this FAQ to the list
                FAQ faq = new FAQ();
                faq.Question = faqItem.Fields["Question"].Value;
                faq.Answer = faqItem.Fields["Answer"].Value;
                faqs._Items.Add(faq);
            }

            return PartialView(faqs);
        }
    }
}