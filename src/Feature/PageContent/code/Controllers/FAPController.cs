﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using BHSearch.Models;
using BHSearch.Components;
using System.Collections.ObjectModel;
using System.Reflection;
using BHSearch.Helpers;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Links;
using Sitecore.Diagnostics;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class FAPController : Controller
    {
        // GET: FAP
        public ActionResult FapResults()
        {
            return View();
        }

        public ActionResult FapProfilePP()
        {
            return View();
        }

        public ActionResult FapProfileBPP()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PhysicianQuery(SolrQuery obj)
        {
            try
            {
                var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
                defaultUrlOptions.EncodeNames = true;
                var solrPhyIndex = Settings.GetSetting("solrPhyIndex");
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
                obj.Handler = solrPhyIndex;
                obj.Facets = new List<string>(new string[] {
                    "specialties_pipe",
                    "gender_s",
                    "languages_pipe",
                    "hospitalaffiliations_pipe"
                 });
                obj.Sort = "lastname_s";
                SolrResponse response = search.Execute(obj);

                // Map Results to Physician Model
                List<PhysicianSearch> Physicians = new List<PhysicianSearch>();
                PropertyInfo[] properties = typeof(PhysicianSearch).GetProperties();

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    try
                    {
                        PhysicianSearch physician = new PhysicianSearch();


                        foreach (PropertyInfo prop in properties)
                        {
                            if (doc.TryGetValue(prop.Name, out object value))
                            {
                                prop.SetValue(physician, value);
                            }
                            else
                            {
                                prop.SetValue(physician, null);
                            }
                        }
                        var path = doc.Where(k => k.Key == "Path").Select(v => v.Value).FirstOrDefault();
                        var id = Context.Database.GetItem(path.ToString());
                        var link = LinkManager.GetItemUrl(id);
                        physician.Path = link;
                        Physicians.Add(physician);
                    }
                    catch(Exception)
                    {
                        continue;
                    }
                }

                // Map Views to Response
                ViewResponse json = new ViewResponse();

                // etc
                json.TotalHits = response.TotalHits;
                json.OriginalQuery = response.OriginalQuery;

                // results
                json.Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", new PhysicianResultsView()
                {
                    Start = obj.Start,
                    Physicians = Physicians
                });

                // facets
                RenderFacetView(response, "specialties_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "languages_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "hospitalaffiliations_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "gender_s", json, "_FacetGender");

                return Json(json);
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.FAPController -> PhysicianQuery : " + ex.ToString(), this);
                return BuildJsonErrorResponse(ex);
            }
        }

        [HttpPost]
        public JsonResult PhysicianTypeahead(SolrQuery obj)
        {
            try
            {
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
                obj.Handler = "/bpp_physician_index/physicianSuggest";
                SolrResponse response = search.Execute(obj);

                // Map Results to Physician Model
                List<PhysicianSearch> Physicians = new List<PhysicianSearch>();
                PropertyInfo[] properties = typeof(PhysicianSearch).GetProperties();

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    PhysicianSearch physician = new PhysicianSearch();

                    foreach (PropertyInfo prop in properties)
                    {
                        if (doc.TryGetValue(prop.Name, out object value))
                        {
                            prop.SetValue(physician, value);
                        }
                        else
                        {
                            prop.SetValue(physician, null);
                        }
                    }
                    Physicians.Add(physician);
                }

                return Json(Physicians);
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }

        [HttpPost]
        public JsonResult PhysicianFacets(SolrQuery obj)
        {
            try
            {
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
                obj.Query = "*";
                obj.Rows = 1;
                obj.Handler = "/bpp_physician_index/physician";
                obj.Facets = new List<string>(new string[] {
                    "specialties_pipe",
                    "hospitalaffiliations_pipe"
                 });
                SolrResponse response = search.Execute(obj);

                return Json(RenderFacetList(response, obj));
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }

        private void RenderFacetView(SolrResponse response, string facetField, ViewResponse json, string partial)
        {
            FacetView facetView = new FacetView();
            ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
            if (response.Facets.TryGetValue(facetField, out facets))
            {
                if (response.OriginalQuery.Filters.TryGetValue(facetField, out string filter))
                {
                    facetView.Filter = filter.Split(',').ToList();
                }

                facetView.Facet = new KeyValuePair<string, ICollection<KeyValuePair<string, int>>>(facetField, facets);
                json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
            }
        }

        private Dictionary<string, ICollection<KeyValuePair<string, int>>> RenderFacetList(SolrResponse response, SolrQuery obj)
        {
            Dictionary<string, ICollection<KeyValuePair<string, int>>> Facets = new Dictionary<string, ICollection<KeyValuePair<string, int>>>();

            foreach (string facetField in obj.Facets)
            {
                ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
                if (response.Facets.TryGetValue(facetField, out facets))
                {
                    Facets.Add(facetField, facets);
                }
            }

            return Facets;
        }

        private JsonResult BuildJsonErrorResponse(Exception e)
        {
            Dictionary<string, object> error = new Dictionary<string, object>();
            error.Add("Status", "Error");
            // Comment Below to Hide Debugging
            error.Add("Exception", new
            {
                e.Message,
                Trace = e.StackTrace,
                InnerException = new
                {
                    Message = e.InnerException.Message ?? "",
                    Trace = e.InnerException.StackTrace ?? ""
                }
            });

            return Json(error);
        }
    }
}