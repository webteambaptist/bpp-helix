﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Net;
using System.Web.Mvc;
using BPP93Helix.Feature.PageContent.Helper;
using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using Sitecore.Diagnostics;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class ContactUsController : Controller
    {
        private static readonly string spAPISendMail = Settings.GetSetting("spAPISendMail");
        private static readonly string contactUsRecipient = Settings.GetSetting("contactUsRecipient");
    // GET: ContactUs
        public ActionResult Index()
        {
            // build drop down for Role
            ContactUs _contactUs = new ContactUs();
            List<string> _roles = new List<string>();
            _roles.Add("Provider");
            _roles.Add("Health Plan");
            _roles.Add("Employer");
            _roles.Add("Patient");
            _roles.Add("Other");
            
            _contactUs.RoleDD = GetSelectListItems(_roles);
            return View(_contactUs);
        }
        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> roles)
        {
            var selectList = new List<SelectListItem>();
            foreach (var role in roles)
            {
                selectList.Add(new SelectListItem
                {
                    Value = role,
                    Text = role
                });
            }
            
            return selectList;
        }
        public ActionResult SubmitContact(ContactUs _us)
        {
            Message message = new Message
            {
                Body = ""
            };
            message.Body += "Dear BPP Support Member,<br/>";
            message.Body += "<br/>A new contact us request has been submitted by <b>" + _us.FirstName + " " + _us.LastName + "</b><br/>";
            message.Body += "<br/>Below are the additional details<br/>";
            message.Body += "<br/><b>Email</b> - " + _us.Email + " <br/>";
            message.Body += "<br/><b>Phone</b> - " + _us.Phone + " <br/>";
            message.Body += "<br/><b>Role</b> - " + _us.Role + " <br/>";
            message.Body += "<br/><b>Message</b> - " + _us.Message + " <br/><br/><br/>";
            message.Body += "<i>*** This is an automatically generated email, please do not reply***</i>";
            message.Subject = "New BPP contact form submitted by " + _us.FirstName + " " + _us.LastName;
            message.Recipient = contactUsRecipient; 
            message.Sender = "no-reply@bmcjax.com";
            string jo = JsonConvert.SerializeObject(message);
            HttpWebResponse response = null;
            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ContactUsController :: Contact Us -> Submitting JSON Final Copy:" + jo.ToString(), jo);
            response = API.PostDataToAPI(spAPISendMail, jo.ToString());
            if (response != null)
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.ContactUsController :: Contact Us -> API Response : " + response, response);
                Log.Info(" -> API Response Code: " + (int)response.StatusCode, response);
            }
            else
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.ContactUsController :: Contact Us", this);
            }
            return Redirect("~/Home/ContactUs/SubmitContact");
            //return View("SubmitContact");
        }
    }
}