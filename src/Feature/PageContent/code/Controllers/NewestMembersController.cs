﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using System.Data;
using System.Net;
using Sitecore.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Sitecore.Links;
using Sitecore.Diagnostics;
//using BPP.ExtModels;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class NewestMembersController : Controller
    {
        private static readonly string spAPIBaseAddress = Settings.GetSetting("spAPIBaseAddress");
        private static readonly string spAPIGetNewestMembers = Settings.GetSetting("spAPIGetNewestMembers");
        // GET: NewestMembers
        public ActionResult Index()
        {
            try
            {
                List<NewestMembers> members = new List<NewestMembers>();
                Members nm = new Members();
                List<BPPProvider> _bppProviders = new List<BPPProvider>();
                HttpWebResponse httpWebResponse = null;
                var items = Sitecore.Context.Database.SelectItems("/sitecore/content/search/providers/*");
                var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
                defaultUrlOptions.EncodeNames = true;
                items = items.Where(x => x["IsBPP"].ToString() == "1").ToArray();

                httpWebResponse = GetDataFromAPI(spAPIGetNewestMembers);

                if (httpWebResponse != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.NewestMembers :: Index -> API Response : " + httpWebResponse, httpWebResponse);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.NewestMembers :: Index -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
                    
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        _bppProviders = (List<BPPProvider>)js.Deserialize(objText, typeof(List<BPPProvider>));
                    }
                }
                else
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.NewestMembers :: Index -> httpWebResponse Null Exception : ", this);
                }
                
                //foreach (var i in _bppProviders)
                foreach (var i in items)
                {
                    NewestMembers m = new NewestMembers();
                    string npi = i["NPI"].ToString();
                    var p = _bppProviders.Where(x => x.NPINumber == npi).FirstOrDefault();
                    //string npi = i.NPINumber; 
                    m.FirstName = i["FirstName"].ToString();//i.FirstName;
                    m.LastName = i["LastName"].ToString();//i.LastName;
                    m.PrimarySpecialty = i["PrimarySpecialty"].ToString();//i.PrimarySpecialty;
                    //m.Bio = i["Biography"].ToString();
                    //m.Image = "http://bhwebapps.bmcjax.com/WolfsonFAP/PhysicianImages/" + i.Name + ".jpg";
                    //m.ItemPath = i.Paths.Path.ToString().ToLower();
                    m.ItemPath = LinkManager.GetItemUrl(i, defaultUrlOptions);
                    try
                    {
                        m.MemberDate = (DateTime)p.ProviderPracticeWorkFlow.BPPEffectiveDate; //(DateTime)i.BPPEffectiveDate;
                    }
                    catch { m.MemberDate = DateTime.Parse("01/01/2000"); }
                    members.Add(m);
                }

                nm._Items = members.OrderBy(x => x.MemberDate).Reverse().Take(30).ToList() ;

                return View(nm);
            } catch(Exception)
            {
                return null;
            }
        }

        private HttpWebResponse GetDataFromAPI(string strAPIMethod, string strMethod = "GET")
        {
            HttpWebResponse webResponse = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = strMethod;
                httpWebRequest.Timeout = 20000;
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.NewestMembersController :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") -> APIException : " + wex.ToString(), this);
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.NewestMembersController :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") -> Exception : " + ex.ToString(), this);
            }
            return webResponse;
        }
    }
}