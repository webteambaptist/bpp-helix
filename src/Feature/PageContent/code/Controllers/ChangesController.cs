﻿using BPP93Helix.Feature.PageContent.Models;
using BPP93Helix.Feature.PageContent.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Sitecore.Diagnostics;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class ChangesController : Controller
    {
        private readonly static string spAPIGetScreenStatuses = Settings.GetSetting("spAPIGetScreenStatuses");
        private readonly static string spAPIGetChangesList = Settings.GetSetting("spAPIGetChangesList");
        private readonly static string spAPIRequestChanges = Settings.GetSetting("spAPIRequestChanges");
        private readonly static string spAPIAcceptChanges = Settings.GetSetting("spAPIAcceptChanges");
        private readonly static string spAPIRejectChanges = Settings.GetSetting("spAPIRejectChanges");
        private readonly static string spAPIGetSurveyDetails = Settings.GetSetting("spAPIGetSurveyDetails");
        private readonly static string spAPIGetPracticesforSurvey = Settings.GetSetting("spAPIGetPracticesforSurvey");
        private readonly static string spAPIGetSurveyArchiveList = Settings.GetSetting("spAPIGetSurveyArchiveList");
        public ActionResult Index()
        {
            // get the values from the API for the check boxes
            Changes _Changes = new Changes();
            try
            {

                HttpWebResponse res = null;
                res = API.GetDataFromAPI(spAPIGetScreenStatuses);

                if (res != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: Index -> API Response : " + res, res);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: Index -> API Response Code: " + res.StatusCode, res);

                    using (var reader = new StreamReader(res.GetResponseStream()))
                    {
                        JsonSerializer js = new JsonSerializer();
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        List<StatusChange> _Change = new List<StatusChange>();
                        _Changes = (Changes)JsonConvert.DeserializeObject(objText, typeof(Changes), settings);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: Index -> Exception : " + ex.ToString(), this);
                _Changes = null;
            }

            return View(_Changes);
        }

        [HttpPost]
        public ActionResult SubmitChanges(FormCollection collection)
        {
            try
            {
                HttpWebResponse res = null;
                try
                {
                    string jsonStringChange = GenerateJson(collection);
                    if (jsonStringChange != null && jsonStringChange.Length > 0)
                    {
                        res = API.PostDataToAPI(spAPIRequestChanges, jsonStringChange);
                    }
                    else
                    {
                        res = null;
                    }
                    if (res != null)
                    {
                        if ((int)res.StatusCode >= 200 && (int)res.StatusCode <= 200)
                        {
                            TempData["updChangeRequest"] = "Thank you for submitting provider updates. They have been successfully transmitted to the BPP team.";
                            return View("Submitted");
                        }
                        else
                        {
                            TempData["updChangeRequest"] = res.StatusCode;
                        }
                    }
                    else
                    {
                        TempData["updChangeRequest"] = "500 Internal Server Error";
                    }

                }
                catch (Exception e)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangeController :: SubmitChanges -> Exception : " + e.ToString(), e.Source);
                    TempData["SubmitMsg"] = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";
                }
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: SubmitChanges Changes -> Exception : " + ex.ToString(), ex.Source);
            }
            return Redirect("~/Home/Changes/Changes");
        }
        [HttpPost]
        public ActionResult ApproveChanges(string providers)
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                if (!string.IsNullOrEmpty(providers))
                {
                    string strUpdateJson = string.Empty;
                    strUpdateJson += @"{""Change"":[" + providers + "]}";
                    HttpWebResponse response = API.PostDataToAPI(spAPIAcceptChanges, strUpdateJson);

                    if (response != null)
                    {
                        TempData["updAppResponse"] = "The selected Provider(s) have been updated succesfully.";
                    }
                    else
                    {
                        return Json(new { data = "bad-request" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json("success", JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public ActionResult RejectChanges(string providers)
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                if (!string.IsNullOrEmpty(providers))
                {
                    string strUpdateJson = string.Empty;
                    strUpdateJson += @"{""Change"":[" + providers + "]}";
                    HttpWebResponse response = API.PostDataToAPI(spAPIRejectChanges, strUpdateJson);

                    if (response != null)
                    {
                        TempData["updAppResponse"] = "The selected Provider(s) have been updated succesfully.";
                    }
                    else
                    {
                        return Json(new { data = "bad-request" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            return Json("success", JsonRequestBehavior.DenyGet);
        }
        // This gets a list of changes to approve for the Provider Status Updates Screen
        [HttpGet]
        public ActionResult ManageSurvey()
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                var jSonData = "";
                HttpWebResponse httpWebResponse = null;
                try
                {
                    List<BPPSurveyList> _bppSurveyList = new List<BPPSurveyList>();
                    httpWebResponse = API.GetDataFromAPI(spAPIGetChangesList);
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var objText = reader.ReadToEnd();
                            JObject data = JObject.Parse(objText);

                            _bppSurveyList = data["_ChangeList"].Select(c => c.ToObject<BPPSurveyList>()).ToList();
                            jSonData = Newtonsoft.Json.JsonConvert.SerializeObject(_bppSurveyList);
                        }
                    }
                    else
                    {
                        Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> httpWebResponse Null Exception : ", this);
                        jSonData = "";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> Exception : " + ex.ToString(), ex.Source);
                    jSonData = "";
                }
                var jsonResult = Json(new { data = jSonData }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangesController :: ManageMembers -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-access" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangesController :: ManageMembers -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-login" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public ActionResult SurveyDetail(string ProviderNPI, string SubmitterTIN, string Id)
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                ChangeDetails _ChangeDetails = new ChangeDetails();
                try
                {
                    if (!string.IsNullOrEmpty(ProviderNPI))
                    {
                        _ChangeDetails = GetChangesDetails(ProviderNPI, SubmitterTIN, Id);
                        return View(_ChangeDetails);
                    }
                    else
                    {
                        return Redirect(Url.Content("~/"));
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangeController :: SurveyDetail -> Exception : " + ex.ToString(), ex.Source);
                    _ChangeDetails = null;
                    return View(_ChangeDetails);
                }
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangeController :: SurveyDetail -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/"));
                }
                else
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangeController :: SurveyDetail -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/Login"));
                }
            }
        }
        public ChangeDetails GetChangesDetails(string ProviderNPI, string SubmitterTIN, string Id)
        {
            ChangeDetails _ChangeDetails = new ChangeDetails();
            if (!string.IsNullOrEmpty(ProviderNPI))
            {
                try
                {
                    HttpWebResponse httpWebResponse = null;
                    httpWebResponse = API.GetDataFromAPI(spAPIGetSurveyDetails, "ProviderNPI|" + ProviderNPI + "^SubmitterTIN|" + SubmitterTIN + "^Id|" + Id);

                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: GetChangesDetails -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: GetChangesDetails -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {

                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };


                            _ChangeDetails = (ChangeDetails)JsonConvert.DeserializeObject(objText, typeof(ChangeDetails), settings);
                        }
                    }
                    else
                    {
                        _ChangeDetails = null;
                        TempData["appListResponse"] = "500 Internal Error";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: GetChangesDetails -> Exception : " + ex.ToString(), ex.Source);
                    _ChangeDetails = null;
                }
            }
            else
            {
                _ChangeDetails = null;
                TempData["appListResponse"] = "500 Internal Error";
            }
            return _ChangeDetails;
        }
        public string GenerateJson(FormCollection collection)
        {
            Changes _changes = new Changes();
            try
            {
                var provKeys = collection.AllKeys
                    .Where(k => k.StartsWith("ProviderNPI"))
                    .ToDictionary(k => k, k => collection[k]);
                _changes.SubmitterName = collection["SubmitterName"].ToString();
                _changes.SubmitterPhysicianGroup = collection["SubmitterPhysicianGroup"].ToString();
                _changes.SubmitterTIN = collection["SubmitterTIN"].ToString();
                _changes.SubmitterEmailAddress = collection["SubmitterEmailAddress"].ToString();
                _changes.SubmitterPhoneNumber = collection["SubmitterPhoneNumber"].ToString();

                _changes._Providers = new List<Provider>();
                for (int i = 0; i < provKeys.Count; i++)
                {
                    Provider _provider = new Provider();
                    _provider.ProviderNPI = collection["ProviderNPI-" + i].ToString();
                    _provider.ProviderName = collection["ProviderName-" + i].ToString();
                    _provider.EffectiveDate = DateTime.Parse(collection["effectiveDate-" + i].ToString());
                    _provider.StatusChange = int.Parse(collection["statusChange-" + i].ToString());
                    _provider.StatusChangeReason = int.Parse(collection["statusChangeReason-" + i].ToString());
                    _provider.UpdateLastName = collection["LastNameChange-" + i].ToString();
                    _provider.Other = collection["Other-" + i].ToString();
                    _provider.Practice = Guid.Parse(collection["Practice-" + i].ToString());
                    _changes._Providers.Add(_provider);

                }
                string json = JsonConvert.SerializeObject(_changes);
                json = json.Replace("},\"_Providers\":[", ",\"_Providers\":[");
                return json;
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: GenerateJson -> Exception : " + ex.ToString(), ex.Source);
                return "";
            }
        }


        [HttpPost]
        public ActionResult GetPracticeNamesforSurvey(string TIN)
        {
            List<BPPPractice> _practices = new List<BPPPractice>();

            try
            {
                HttpWebResponse res = API.GetDataFromAPI(spAPIGetPracticesforSurvey, "TIN|" + TIN);
                if (res != null)
                {
                    using (var reader = new StreamReader(res.GetResponseStream()))
                    {
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        _practices = (List<BPPPractice>)JsonConvert.DeserializeObject(objText, typeof(List<BPPPractice>), settings);
                    }
                }
            }
            catch (Exception)
            {
                _practices = null;
            }
            return Json(_practices);
        }

        [HttpGet]
        public ActionResult ManageArchiveList()
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                var jSonData = "";
                HttpWebResponse httpWebResponse = null;
                try
                {
                    List<BPPSurveyArchiveList> _bppSurveyArchiveList = new List<BPPSurveyArchiveList>();
                    httpWebResponse = API.GetDataFromAPI(spAPIGetSurveyArchiveList);
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var objText = reader.ReadToEnd();
                            JObject data = JObject.Parse(objText);

                            _bppSurveyArchiveList = data["_ChangeList"].Select(c => c.ToObject<BPPSurveyArchiveList>()).ToList();
                            jSonData = Newtonsoft.Json.JsonConvert.SerializeObject(_bppSurveyArchiveList);
                        }
                    }
                    else
                    {
                        Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> httpWebResponse Null Exception : ", this);
                        jSonData = "";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ChangesController :: ManageSurvey -> Exception : " + ex.ToString(), ex.Source);
                    jSonData = "";
                }
                var jsonResult = Json(new { data = jSonData }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangesController :: ManageMembers -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-access" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangesController :: ManageMembers -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-login" }, JsonRequestBehavior.AllowGet);
                }
            }

        }

        //public ActionResult SurveyDetail(string ProviderNPI, string SubmitterTIN)
        //{
        //    if (BHUser.Components.Login.IfAdminLoggedIn())
        //    {
        //        ChangeDetails _ChangeDetails = new ChangeDetails();
        //        try
        //        {
        //            if (!string.IsNullOrEmpty(ProviderNPI))
        //            {
        //                _ChangeDetails = GetChangesDetails(ProviderNPI, SubmitterTIN);
        //                return View(_ChangeDetails);
        //            }
        //            else
        //            {
        //                return Redirect(Url.Content("~/"));
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Sitecore.Diagnostics.Log.Error("BPP.Controllers.ChangeController :: SurveyDetail -> Exception : " + ex.ToString(), ex.Source);
        //            _ChangeDetails = null;
        //            return View(_ChangeDetails);
        //        }
        //    }
        //    else
        //    {
        //        if (BHUser.Components.Login.IfUserLoggedIn())
        //        {
        //            Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangeController :: SurveyDetail -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
        //            return Redirect(Url.Content("~/"));
        //        }
        //        else
        //        {
        //            Sitecore.Diagnostics.Log.Info("BPP.Controllers.ChangeController :: SurveyDetail -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
        //            return Redirect(Url.Content("~/Login"));
        //        }
        //    }
        //}
    }
}
