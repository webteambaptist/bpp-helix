﻿using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Sitecore.Configuration;

namespace BPP93Helix.Feature.PageContent.Controllers.ApplicationProcess
{
    public class PracticeNameController : Controller
    {
        //static string spAPISubmitPracticeName = ConfigurationManager.AppSettings["spAPISubmitPracticeName"];
        //static readonly string spAPIUpdateMemberStep = ConfigurationManager.AppSettings["spAPIUpdateMemberStep"];
        //static readonly string spAPIValidatePracticeName = ConfigurationManager.AppSettings["spAPIValidatePracticeName"];

    static readonly string spAPISubmitPracticeName = Settings.GetSetting("spAPISubmitPracticeName");
    static readonly string spAPIUpdateMemberStep = Settings.GetSetting("spAPIUpdateMemberStep");
    static readonly string spAPIValidatePracticeName = Settings.GetSetting("spAPIValidatePracticeName");

    public ActionResult PracticeName(string ActivationKey)
        {
            BPPApplication _bppApplication = new BPPApplication();
            BPPMemberRequest mr = new BPPMemberRequest();
            mr.ActivationKey = ActivationKey;
            _bppApplication._Member = mr;
            return View(_bppApplication);
        }

        [HttpPost]
        public ActionResult SubmitPracticeNames(FormCollection collection)
        {
            string ActivationKey = collection["_Member.ActivationKey"].ToString();
            var resGuid = "ActivationKey|" + ActivationKey.ToString();
            try
            {
                if (collection != null)
                {
                    string strJsonSubmitPracticeNames = Helper.ApplicationProcessHelperController.GenerateJson(collection);
                    if (!String.IsNullOrEmpty(strJsonSubmitPracticeNames))
                    {
                        HttpWebResponse res = Helper.API.PostDataToAPI(spAPISubmitPracticeName, strJsonSubmitPracticeNames, resGuid);
                        if (res != null)
                        {
                            Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.PracticeNameController :: SubmitPracticeNames -> API Response Code: " + res.StatusCode, res);
                            if (((int)res.StatusCode >= 200) && ((int)res.StatusCode <= 299))
                            {
                                using (var reader = new StreamReader(res.GetResponseStream()))
                                {
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    var objText = reader.ReadToEnd();

                                    var settings = new JsonSerializerSettings
                                    {
                                        NullValueHandling = NullValueHandling.Ignore,
                                        MissingMemberHandling = MissingMemberHandling.Ignore
                                    };
                                }
                            }

                            // Update the Member Table Application Step.
                            res = Helper.API.PostDataToAPI(spAPIUpdateMemberStep, "", resGuid);
                            if (res != null)
                            {
                                Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.PracticeNameController :: SubmitPracticeNames -> API Response Code: " + res.StatusCode, res);
                                if (!(((int)res.StatusCode >= 200) || ((int)res.StatusCode <= 299)))
                                {
                                    Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.PracticeNameController :: SubmitPracticeNames -> Bad Request : " + res.StatusDescription, this);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.PracticeNameController :: SubmitPracticeNames -> Exception : " + ex.ToString(), ex.Source);
            }
            return Redirect("/Application/Locations?ActivationKey=" + ActivationKey);
        }

        [HttpPost]
        public ActionResult ValidatePracticeName(string PracticeName, string ActivationKey)
        {
            List<string> returnList = new List<string>();
            string status;
            try
            {
                HttpWebResponse res = Helper.API.GetDataFromAPI(spAPIValidatePracticeName, "PracticeName|" + PracticeName + "^ActivationKey|" + ActivationKey);
                if (res != null)
                {
                    if (res.StatusCode.ToString() != "OK")
                    {
                        Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ValidateController :: ValidatePracticeName -> Bad Request : " + res.StatusDescription, this);
                        status = "error";
                    }
                    else
                    {
                        status = "success";
                    }
                }
                else
                {
                    status = "error";
                }
            }
            catch (Exception)
            {
                status = "error";
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }
    }
}
