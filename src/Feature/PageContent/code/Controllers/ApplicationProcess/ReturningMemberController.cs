﻿using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sitecore.Configuration;

namespace BPP93Helix.Feature.PageContent.Controllers.ApplicationProcess
{
    public class ReturningMemberController : Controller
    {
        //static readonly string spAPIReturningMember = ConfigurationManager.AppSettings["spAPIReturningMember"];

    static readonly string spAPIReturningMember = Settings.GetSetting("spAPIReturningMember");
    // GET: ReturningMember
    public ActionResult Index()
        {
            return View();
        }       

        [HttpPost]
        public ActionResult RequestApplication(FormCollection collection)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            try
            {
                HttpWebResponse res = null;
                res = Helper.API.GetDataFromAPI(spAPIReturningMember, "TaxID|" + collection["_ReturningMember.TaxID"].ToString() + "^" + "Email|" + collection["_ReturningMember.POCEmail"] + "^" + "Selection|" + collection["SelectionOption-0"].ToString());
                
                if (res != null)
                {
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ReturningMemberController :: RequestApplication -> API Response Code: " + res.StatusCode, res);
                    if (!(((int)res.StatusCode >= 200) || ((int)res.StatusCode <= 299)))
                    {
                        Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ReturningMemberController :: RequestApplication -> Bad Request : " + res.StatusDescription, this);
                        status = "error";
                        message = "An Error Occurred processing your request";
                    }
                    else
                    {
                        status = "success";
                        message = "Thank you for submitting a request to continue your application process. An email will be sent to you shortly....";
                    }
                }
            }
            catch (Exception e)
            {
                status = "error";
                message = "An Error Occurred processing your request";
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ReturningMemberController :: RequestApplication -> Bad Request : " + e.ToString(), e.Source);
            }
            returnList.Add(status);
            returnList.Add(message);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }
    }
}