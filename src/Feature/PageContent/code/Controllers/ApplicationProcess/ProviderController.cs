﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using BPP93Helix.Feature.PageContent.Helper;
using Sitecore.Configuration;


namespace BPP93Helix.Feature.PageContent.Controllers.ApplicationProcess
{
    public class ProviderController : Controller
    {
    //static readonly string spAPISubmitProviders = ConfigurationManager.AppSettings["spAPISubmitProviders"];
    //static readonly string spiAPIGetDegrees = ConfigurationManager.AppSettings["spiAPIGetDegrees"];
    //static readonly string spAPISelSpecials = ConfigurationManager.AppSettings["spAPISelSpecials"];
    //static readonly string spAPIUpdateMemberStep = ConfigurationManager.AppSettings["spAPIUpdateMemberStep"];
    //static readonly string spAPIGetPracticeLocations = ConfigurationManager.AppSettings["spAPIGetPracticeLocations"];
    //static readonly string DefaultEmailSet = ConfigurationManager.AppSettings["DefaultEmailSet"];

    static readonly string spAPISubmitProviders = Settings.GetSetting("spAPISubmitProviders");
    static readonly string spiAPIGetDegrees = Settings.GetSetting("spiAPIGetDegrees");
    static readonly string spAPISelSpecials = Settings.GetSetting("spAPISelSpecials");
    static readonly string spAPIUpdateMemberStep = Settings.GetSetting("spAPIUpdateMemberStep");
    static readonly string spAPIGetPracticeLocations = Settings.GetSetting("spAPIGetPracticeLocations");
    static readonly string DefaultEmailSet = Settings.GetSetting("DefaultEmailSet");

    // GET: Provider
    //[HttpGet]
    public ActionResult Index(string ActivationKey)
        {
            if (DefaultEmailSet == "enabled")
            {
                ViewBag.SetDefaultEmail = "enabled";
            }
            else
            {
                ViewBag.SetDefaulEmail = "disabled";
            }
            BPPApplication _bppApplication = new BPPApplication
            {
                _Providers = new List<BPPProvider>(),
                _Degrees = new List<BPPDegrees>()
            };
            try
            {
                BPPMemberRequest mr = new BPPMemberRequest
                {
                    ActivationKey = ActivationKey
                };
                _bppApplication._Member = mr;
                HttpWebResponse res = null;

                res = API.GetDataFromAPI(spiAPIGetDegrees, "ActivationKey|" + ActivationKey);

                if (res != null)
                {
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ProviderController :: Index -> API Response : " + res, res);
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ProviderController :: Index -> API Response Code: " + res.StatusCode, res);

                    using (var reader = new StreamReader(res.GetResponseStream()))
                    {
                        JsonSerializer js = new JsonSerializer();
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };

                      _bppApplication = (BPPApplication)JsonConvert.DeserializeObject(objText, typeof(BPPApplication), settings);
                        mr.ActivationKey = ActivationKey;
                        _bppApplication._Member = mr;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(_bppApplication);
        }
        [HttpPost]
        public ActionResult SubmitProviders(FormCollection collection)
        {
            string ActivationKey = collection["_Member.ActivationKey"].ToString();
            try
            {
                if (collection != null)
                {
                    string strJsonBppAddProviders = GenerateJson(collection);
                    if (!String.IsNullOrEmpty(strJsonBppAddProviders))
                    {
                        //res = Helper.API.PostDataToAPI(spAPISubmitProviders, strJsonBppAddProviders);
                        HttpWebResponse res = API.PostDataToAPI(spAPISubmitProviders, strJsonBppAddProviders, "ActivationKey|" + ActivationKey);
                        if (res != null)
                        {
                            Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ProviderController :: SubmitProviders -> API Response Code: " + res.StatusCode, res);
                            if (((int)res.StatusCode >= 200) && ((int)res.StatusCode <= 299))
                            {
                                using (var reader = new StreamReader(res.GetResponseStream()))
                                {
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    var objText = reader.ReadToEnd();

                                    var settings = new JsonSerializerSettings
                                    {
                                        NullValueHandling = NullValueHandling.Ignore,
                                        MissingMemberHandling = MissingMemberHandling.Ignore
                                    };
                                    // Update the Member Table Application Step.
                                    var resGuid = "ActivationKey|" + ActivationKey.ToString();
                                    res = API.PostDataToAPI(spAPIUpdateMemberStep, "", resGuid);
                                    if (res != null)
                                    {
                                        Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ProvidersController :: SubmitProviders -> API Response Code: " + res.StatusCode, res);
                                        if (!(((int)res.StatusCode >= 200) || ((int)res.StatusCode <= 299)))
                                        {
                                            Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ProvidersController :: SubmitProviders -> Bad Request : " + res.StatusDescription, this);
                                        }
                                    }
                                    else
                                    {
                                        return null;
                                    }
                                    return Redirect("/Application/JoinderAgreement?ActivationKey=" + ActivationKey);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Controllers.ProvidersController :: SubmitProviders -> Bad Request : " + e.ToString(), e.Source);
            }
            return Redirect("/Application/Providers");
        }
        private string GenerateJson(FormCollection collection)
        {
            var provKeys = collection.AllKeys
                  .Where(p => p.StartsWith("ProviderfirstName"))
                  .ToDictionary(p => p, p => collection[p]);
            if (provKeys.Count > 0)
            {
                BPPApplication _bppAppForm = new BPPApplication();
                _bppAppForm._Providers = new List<BPPProvider>();
                for (int p = 0; p < provKeys.Count; p++)
                {
                    List<int> provOtherLoc = new List<int>();
                    BPPProvider _bppProvider = new BPPProvider();
                    _bppProvider.FirstName = collection["ProviderfirstNameMulti-" + p].ToString();
                    _bppProvider.MiddleName = collection["ProvidermiddleInitial-" + p].ToString();
                    _bppProvider.LastName = collection["ProviderlastNameMulti-" + p].ToString();
                    _bppProvider.PhoneNumber = collection["ProviderPersonalPhone-" + p].ToString();                   
                    if (collection["SetDefaultEmail-" + p].ToString() == "enabled")
                    {
                        var email = collection["ProviderPersonalEmail-" + p].ToString() + collection["emailAlias"].ToString();
                        _bppProvider.Email = email;
                    }
                    else
                    {
                        _bppProvider.Email = collection["ProviderPersonalEmail-" + p].ToString();
                    }                    
                    _bppProvider.Degree = collection["ProviderDegree-" + p].ToString();
                    _bppProvider.NPINumber = collection["ProvidernpiNumber-" + p].ToString();
                    _bppProvider.ProviderAncillary = new BPPProviderAncillary
                    {
                        AKA = collection["ProviderAlsoKnownAs-" + p].ToString()
                    };
                    _bppProvider.PrimarySpecialty = collection["ProviderprimarySpecialtyMulti-" + p].ToString();
                    _bppProvider.SecondarySpecialty = collection["ProvidersecondarySpecialtyMulti-" + p].ToString();
                    _bppProvider.PrimaryPracticeLocation = Convert.ToInt32(collection["ProviderPrimaryLocationMulti-" + p]);
                    _bppProvider.Suffix = collection["ProviderSuffix-" + p].ToString();
                    try
                    {
                        var otherLocs = collection["ProviderOtherLocationMulti-" + p].ToString().Split(',');
                        foreach (var item in otherLocs)
                        {
                            provOtherLoc.Add(Convert.ToInt32(item));
                        }
                        _bppProvider.OtherPracticeLocations = provOtherLoc;
                    }
                    catch (Exception)
                    {
                            _bppProvider.OtherPracticeLocations = null;
                    }

                    _bppProvider.isBHPrivileged = (collection["ProviderprivilegesMulti-" + p].ToString().ToLower().Equals("yes"));
                    _bppAppForm._Providers.Add(_bppProvider);
                    provOtherLoc = null;
                }
                string json = JsonConvert.SerializeObject(_bppAppForm);
                json = json.Replace("},\"_Providers\":[", ",\"_Providers\":[");
                return json;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult GetPracticeLocations(string PracticeGUID)
        {
            Array returnList = new Array[2];
            List<BPPPracticeLocation> _PracticeLocations = new List<BPPPracticeLocation>();
            try
            {
                HttpWebResponse res = API.GetDataFromAPI(spAPIGetPracticeLocations, "PracticeGuid|" + PracticeGUID);
                if (res != null)
                {
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ProviderController :: GetPracticeLocations -> API Response : " + res, res);
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ProviderController :: GetPracticeLocations -> API Response Code: " + res.StatusCode, res);
                    using (var reader = new StreamReader(res.GetResponseStream()))
                    {
                        JsonSerializer js = new JsonSerializer();
                        var objText = reader.ReadToEnd();

                        var settings = new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore,
                            MissingMemberHandling = MissingMemberHandling.Ignore
                        };
                        _PracticeLocations = (List<BPPPracticeLocation>)JsonConvert.DeserializeObject(objText, typeof(List<BPPPracticeLocation>), settings);
                        //status = "success";
                    }
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ProvidersController :: GetPracticeLocations -> Bad Request : " + e.ToString(), e.Source);
            }
            //returnList.SetValue(status, 0);
            //returnList.SetValue(_PracticeLocations, 1);
            return Json(_PracticeLocations);
            //return View();
           // return View(_bppApplication);
        }
    }
}

