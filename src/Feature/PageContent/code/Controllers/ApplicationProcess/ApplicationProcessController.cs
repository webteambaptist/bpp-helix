﻿using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BPP93Helix.Feature.PageContent.Controllers.ApplicationProcess
{
    public class ApplicationProcessController : Controller
    {
        static readonly string spAPIMemberRequest = Settings.GetSetting("spAPIMemberRequest");
        static readonly string spAPIGetAppStep = Settings.GetSetting("spAPIGetAppStep");
        static readonly string spAPISubmitLegalEntity = Settings.GetSetting("spAPISubmitLegalEntity");
        static readonly string spAPIGetContracts = Settings.GetSetting("spAPIGetContracts");
        static readonly string spAPIGetJoinder = Settings.GetSetting("spAPIGetJoinder");
        static readonly string spAPIUpdateMemberJoin = Settings.GetSetting("spAPIUpdateMemberJoin");
        static readonly string spAPIUpdateMemberStep = Settings.GetSetting("spAPIUpdateMemberStep");
        static readonly string spAPIUpdatePracticeNames = Settings.GetSetting("spAPIUpdatePracticeNames");

        // GET: ApplicationProcess
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        //public ActionResult RequestApplication(FormCollection collection)
        public ActionResult RequestApplication(string Name, string Email)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            try
            {
                if (Name != null)
                {
                    MemberRequest request = new MemberRequest();
                    request.Name = Name;
                    request.Email = Email;
                    string jsonStringChange = JsonConvert.SerializeObject(request);
                    if (!String.IsNullOrEmpty(jsonStringChange))
                    {
                        HttpWebResponse res = null;
                        res = Helper.API.PostDataToAPI(spAPIMemberRequest, jsonStringChange);
                        if (res != null)
                        {
                            Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: RequestApplication -> API Response Code: " + res.StatusCode, res);
                            if (!(((int)res.StatusCode >= 200) || ((int)res.StatusCode <= 299)))
                            {
                                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: RequestApplication -> Bad Request : " + res.StatusDescription, this);
                                status = "error";
                                message = "An Error Occurred processing your request.  Please try again.";
                            }
                            else
                            {
                                message = "Thank you for submitting a request to receive membership application materials and begin your application process. An email will be sent to you shortly....";
                                status = "success";
                            }
                        }
                        else
                        {
                            status = "error";
                            message = "An Error Occurred processing your request. Please try again.";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                status = "error";
                message = "An Error Occurred processing your request. Please try again.";
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: RequestApplication -> Bad Request : " + e.ToString(), e.Source);
            }
            returnList.Add(status);
            returnList.Add(message);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }
        public string GenerateJson_MemberRequest(FormCollection collection)
        {
            MemberRequest memberRequest = new MemberRequest();
            try
            {
                memberRequest.Name = collection["Name"].ToString();
                memberRequest.Email = collection["Email"].ToString();
                string json = JsonConvert.SerializeObject(memberRequest);
                return json;
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: GenerateJson -> Exception : " + e.ToString(), this);
                return "";
            }
        }
        public ActionResult LegalEntity(string ActivationKey = "")
        {
            BPPApplication _bppApplication = new BPPApplication();
            BPPMemberRequest mr = new BPPMemberRequest();
            mr.ActivationKey = ActivationKey;
            _bppApplication._Member = mr;
            return View(_bppApplication);
        }

        public ActionResult GetAppStep(string ActivationKey, string SelectionOption = "")
        {
            NewMemberApplication newMemberApplication = new NewMemberApplication();
            ReturningMemberApplication returningMemberApplication = new ReturningMemberApplication();
            if (!string.IsNullOrEmpty(ActivationKey))
            {
                try
                {
                    HttpWebResponse res = null;
                    if (SelectionOption != null)
                    {
                        var resGuid = "ActivationKey|" + ActivationKey + "^" + "SelectionOption|" + SelectionOption;
                        res = Helper.API.GetDataFromAPI(spAPIGetAppStep, resGuid);
                    }
                    else
                    {
                        res = Helper.API.GetDataFromAPI(spAPIGetAppStep, "ActivationKey|" + ActivationKey);
                    }

                    if (res != null)
                    {
                        Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: GetAppStep -> API Response : " + res, res);
                        Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: GetAppStep -> API Response Code: " + res.StatusCode, res);

                        using (var reader = new StreamReader(res.GetResponseStream()))
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            newMemberApplication = (NewMemberApplication)JsonConvert.DeserializeObject(objText, typeof(NewMemberApplication), settings);
                            returningMemberApplication = (ReturningMemberApplication)JsonConvert.DeserializeObject(objText, typeof(ReturningMemberApplication), settings);
                            var appstep = 0;
                            if (newMemberApplication == null)
                            {
                                appstep = returningMemberApplication.AppStep;
                            }
                            else
                            {
                                appstep = newMemberApplication.AppStep;
                            }

                            //Directs user to the Group/Legal Entity View
                            if (appstep == 1)
                            {
                                return Redirect("/Application/LegalName?ActivationKey=" + ActivationKey);
                            }
                            //Directs user to the Participation & Business Associate Agreement Download View
                            else if (appstep == 2)
                            {
                                return Redirect("/Application/LegalName/LegalEntityDownload?ActivationKey=" + ActivationKey);
                            }
                            //Directs user to the Add Practice(s) View
                            else if (appstep == 3)
                            {
                                return Redirect("/Application/PracticeName?ActivationKey=" + ActivationKey);
                            }
                            //Directs user to the Add Location(s) View
                            else if (appstep == 4)
                            {
                                return Redirect("/Application/Locations?ActivationKey=" + ActivationKey);
                            }
                            //Directs user to the Add Provider(s) View
                            else if (appstep == 5)
                            {
                                return Redirect("/Application/Providers?ActivationKey=" + ActivationKey);
                            }
                            //Directs user to the Joinder Agreement View 
                            else if (appstep == 6)
                            {
                                return Redirect("/Application/JoinderAgreement?ActivationKey=" + ActivationKey);
                            }
                            else if (appstep == 7)
                            {
                                return Redirect("~/Home");
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            else
            {
                return Redirect("/Application");
            }
            return Redirect("/Application");
        }

        [HttpPost]
        public ActionResult AddEntity(FormCollection formCollection)
        {
            string guid = null;
            HttpWebResponse response = null;
            string ActivationKey = formCollection["_Member.ActivationKey"].ToString();
            try
            {
                if (formCollection != null)
                {
                    string strJsonBppAppForm = Helper.ApplicationProcessHelperController.GenerateJson(formCollection);
                    if (!String.IsNullOrEmpty(strJsonBppAppForm))
                    {
                        response = Helper.API.PostDataToAPI(spAPISubmitLegalEntity, strJsonBppAppForm);
                        if (response != null)
                        {
                            Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: AddEntity -> API Response Code: " + response.StatusCode, response);
                            if (((int)response.StatusCode >= 200) && ((int)response.StatusCode <= 299))
                            {
                                using (var reader = new StreamReader(response.GetResponseStream()))
                                {
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    var objText = reader.ReadToEnd();

                                    var settings = new JsonSerializerSettings
                                    {
                                        NullValueHandling = NullValueHandling.Ignore,
                                        MissingMemberHandling = MissingMemberHandling.Ignore
                                    };

                                    guid = (string)JsonConvert.DeserializeObject(objText, typeof(string), settings);
                                }
                            }
                            // Update the Member Table to join it with the Entity created
                            var resGuid = "ActivationKey|" + ActivationKey + "^" + "EntityGuid|" + guid;
                            //response = Helper.API.PostDataToAPI(spAPIUpdateMemberJoin, resGuid);
                            response = Helper.API.PostDataToAPI(spAPIUpdateMemberJoin, "", resGuid);
                            if (response != null)
                            {
                                Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: AddEntity -> API Response Code: " + response.StatusCode, response);
                                if (!(((int)response.StatusCode >= 200) || ((int)response.StatusCode <= 299)))
                                {
                                    Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: AddEntity -> Bad Request : " + response.StatusDescription, this);
                                }
                            }
                        }
                    }
                    else
                    {
                        Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: AddEntity -> Bad Request : " + response.StatusDescription, this);
                    }
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: AddEntity -> Exception : " + ex.ToString(), ex.Source);
            }
            return Redirect("~/Home/Application/LegalName/LegalEntityDownload?ActivationKey=" + ActivationKey);
        }
        public ActionResult LegalEntityDownload(FormCollection formCollection, string ActivationKey)
        {
            Agreement agreement = new Agreement();
            Agreement f = agreement;
            BPPApplication _bppApplication = new BPPApplication();
            try
            {
                if (formCollection != null)
                {
                    BPPMemberRequest mr = new BPPMemberRequest();
                    mr.ActivationKey = ActivationKey;
                    _bppApplication._Member = mr;
                    string strJsonBppAppForm = Helper.ApplicationProcessHelperController.GenerateJson(formCollection);

                    var resGuid = "ActivationKey|" + ActivationKey;
                    HttpWebResponse response = Helper.API.PostDataToAPI(spAPIGetContracts, "", resGuid);
                    if (response != null)
                    {
                        Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: LegalEntityDownload -> API Response Code: " + response.StatusCode, response);
                        if (((int)response.StatusCode >= 200) && ((int)response.StatusCode <= 299))
                        {
                            using (var reader = new StreamReader(response.GetResponseStream()))
                            {
                                JavaScriptSerializer js = new JavaScriptSerializer();
                                string objText = reader.ReadToEnd();

                                // GET Practice Details
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                Downloads d = new Downloads
                                {
                                    DownloadLinks = new List<Agreement>()
                                };
                                d = (Downloads)JsonConvert.DeserializeObject(objText, typeof(Downloads), settings);

                                _bppApplication._Downloads = d;

                            }
                        }
                        return View(_bppApplication);
                    }
                    else
                    {
                        _bppApplication = null;
                        return View(_bppApplication);
                    }

                }
                else
                {
                    _bppApplication = null;
                    return View(_bppApplication);
                }

            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: LegalEntityDownload -> Exception : " + e.ToString(), this);
                TempData["newMemberMsg"] = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";

            }
            _bppApplication = null;
            return View(_bppApplication);
        }
        [HttpPost]
        public ActionResult LegalDownloadAddEntity(FormCollection formCollection)
        {
            //Update Member Step
            HttpWebResponse response = null;
            string ActivationKey = formCollection["_Member.ActivationKey"].ToString();
            var resGuid = "ActivationKey|" + ActivationKey.ToString();
            response = Helper.API.PostDataToAPI(spAPIUpdateMemberStep, "", resGuid);
            if (response != null)
            {
                Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: LegalDownloadAddEntity -> API Response Code: " + response.StatusCode, response);
                if (!(((int)response.StatusCode >= 200) || ((int)response.StatusCode <= 299)))
                {
                    Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: LegalDownloadAddEntity -> Bad Request : " + response.StatusDescription, this);
                }
            }
            return Redirect("~/Home/Application/PracticeName?ActivationKey=" + ActivationKey);
        }

        public ActionResult JoinderAgreement(string ActivationKey)
        {
            Downloads d = new Downloads
            {
                DownloadLinks = new List<Agreement>()
            };
            Agreement agreement = new Agreement();
            Agreement f = agreement;
            BPPApplication _bppApplication = new BPPApplication();
            try
            {
                BPPMemberRequest mr = new BPPMemberRequest();
                mr.ActivationKey = ActivationKey;
                _bppApplication._Member = mr;

                var resGuid = "ActivationKey|" + ActivationKey;
                HttpWebResponse response = Helper.API.PostDataToAPI(spAPIGetJoinder, "", resGuid);
                if (response != null)
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: LegalEntityDownload -> API Response Code: " + response.StatusCode, response);
                    if (((int)response.StatusCode >= 200) && ((int)response.StatusCode <= 299))
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            string objText = reader.ReadToEnd();

                            // GET Practice Details
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            d = (Downloads)JsonConvert.DeserializeObject(objText, typeof(Downloads), settings);

                            _bppApplication._Downloads = d;
                        }
                    }
                    return View(_bppApplication);
                }
                else
                {
                    _bppApplication = null;
                    return View(_bppApplication);
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: LegalEntityDownload -> Exception : " + e.ToString(), e.Source);
                TempData["newMemberMsg"] = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";
            }
            _bppApplication = null;
            return View(_bppApplication);
        }

        [HttpPost]
        public ActionResult JoinderAgreementDownload(FormCollection formCollection)
        {
            HttpWebResponse response = null;
            string ActivationKey = formCollection["_Member.ActivationKey"].ToString();
            var resGuid = "ActivationKey|" + ActivationKey.ToString();
            try
            {
                //Update Practices
                response = Helper.API.PostDataToAPI(spAPIUpdatePracticeNames, "", resGuid);
                if (response != null)
                {
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: JoinderAgreementDownload -> API Response Code: " + response.StatusCode, response);
                    if (((int)response.StatusCode >= 200) && ((int)response.StatusCode <= 299))
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                        }
                    }

                    //Update Member Step
                    response = Helper.API.PostDataToAPI(spAPIUpdateMemberStep, "", resGuid);
                    if (response != null)
                    {
                        Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: JoinderAgreementDownload -> API Response Code: " + response.StatusCode, response);
                        if (!(((int)response.StatusCode >= 200) || ((int)response.StatusCode <= 299)))
                        {
                            Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationProcessController :: JoinderAgreementDownload -> Bad Request : " + response.StatusDescription, this);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationProcessController :: JoinderAgreementDownload -> Exception : " + ex.ToString(), ex.Source);
            }
            return Redirect("~/Home/Application/NewMemberApplicationComplete?ActivationKey=" + ActivationKey);
        }

        public ActionResult NewMemberApplicationComplete()
        {
            return View();
        }
    }
}