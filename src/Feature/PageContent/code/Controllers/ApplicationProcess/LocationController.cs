﻿using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Sitecore.Configuration;

namespace BPP93Helix.Feature.PageContent.Controllers.ApplicationProcess
{
    public class LocationController : Controller
    {
    //static readonly string spAPIGetPracticeNames = ConfigurationManager.AppSettings["spAPIGetPracticeNames"];
    //static readonly string spAPISubmitLocation = ConfigurationManager.AppSettings["spAPISubmitLocation"];
    //static readonly string spAPIUpdateMemberStep = ConfigurationManager.AppSettings["spAPIUpdateMemberStep"];

    static readonly string spAPIGetPracticeNames = Settings.GetSetting("spAPIGetPracticeNames");
    static readonly string spAPISubmitLocation = Settings.GetSetting("spAPISubmitLocation");
    static readonly string spAPIUpdateMemberStep = Settings.GetSetting("spAPIUpdateMemberStep");

    public ActionResult Index(string ActivationKey)
        {
            BPPApplication _bppApplication = new BPPApplication();
            BPPMemberRequest mr = new BPPMemberRequest
            {
                ActivationKey = ActivationKey
            };
            _bppApplication._Member = mr;

            List<BPPPractice> practices = new List<BPPPractice>();
            try
            {
                HttpWebResponse res = Helper.API.GetDataFromAPI(spAPIGetPracticeNames, "ActivationKey|" + ActivationKey);
                if (res != null)
                {
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.LocationController :: Index -> API Response : " + res, res);
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.LocationController :: Index -> API Response Code: " + res.StatusCode, res);
                    using (var reader = new StreamReader(res.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        practices = (List<BPPPractice>)js.Deserialize(objText, typeof(List<BPPPractice>));
            //_bppApplication._Practices = new List<BPPPractice>();
            _bppApplication._Practices = practices;
                    }
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.LocationController :: Index -> Exception : " + e.ToString(), e.Source);
                practices = null;
            }

            return View(_bppApplication);
        }

        [HttpPost]
        public ActionResult SubmitLocations(FormCollection collection)
        {
            HttpWebResponse response = null;
            var storedKey = collection["_Member.ActivationKey"].ToString();

            try
            {
                if (collection != null & collection.Count > 0)
                {
                    string strJsonBppAddLocations = GenerateJson(collection);
                    if (!String.IsNullOrEmpty(strJsonBppAddLocations))
                    {
                        response = Helper.API.PostDataToAPI(spAPISubmitLocation, strJsonBppAddLocations);
                        if (response != null)
                        {
                            Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.LocationController :: SubmitLocations -> API Response : " + response, response);
                            Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.LocationController :: SubmitLocations -> API Response Code: " + response.StatusCode, response);
                            if (!(((int)response.StatusCode >= 200) || ((int)response.StatusCode <= 299)))
                            {
                                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.LocationController :: SubmitLocations -> Bad Request : " + response.StatusDescription, this);
                            }

                            // Update the Member Table Application Step.
                            response = Helper.API.PostDataToAPI(spAPIUpdateMemberStep, "", "ActivationKey|" + storedKey.ToString());
                            if (response != null)
                            {
                                Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.LocationController :: SubmitPraSubmitLocationscticeNames -> API Response Code: " + response.StatusCode, response);
                                if (!(((int)response.StatusCode >= 200) || ((int)response.StatusCode <= 299)))
                                {
                                    Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.LocationController :: SubmitLocations -> Bad Request : " + response.StatusDescription, this);
                                }
                            }

                            return Redirect("~/Home/Application/Providers?ActivationKey=" + storedKey);
                        }
                    }
                    else
                    {
                        Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.LocationController :: SubmitLocations -> Exception : GenerateJSON returned empty string", this);
                    }
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.LocationController :: SubmitLocations -> Exception : " + ex.ToString(), this);
            }
            return Redirect("~/Home/Application/Locations?ActivationKey=" + storedKey);
        }

        public string GenerateJson(FormCollection collection)
        {
            var practiceLocations = new List<BPPPracticeLocation>();
            try
            {
                var provKeys = collection.AllKeys.Where(p => p.StartsWith("locationName"))
                  .ToDictionary(p => p, p => collection[p]);
                if (provKeys.Count > 0)
                {
                    for (int p = 0; p < provKeys.Count; p++)
                    {
                        var location = new BPPPracticeLocation();

                        location.PracticeGUID = Guid.Parse(collection["practicesMulti-" + p].ToString());
                        location.LocationName = collection["locationName-" + p].ToString();
                        location.Address1 = collection["Address1-" + p].ToString();
                        location.Address2 = collection["Address2-" + p].ToString();
                        location.City = collection["City-" + p].ToString();
                        location.State = collection["State-" + p].ToString();
                        location.Zip = collection["Zip-" + p].ToString();
                        location.PhoneNumber = collection["Phone-" + p].ToString();
                        location.Fax = collection["Fax-" + p].ToString();
                        location.Extension = collection["Extension-" + p].ToString();
                        location.OfficeManagerName = collection["OfficeManagerName-" + p].ToString();
                        location.OfficeManagerPhoneNumber = collection["OfficeManagerPhoneNumber-" + p].ToString();
                        location.OfficeManagerEmail = collection["OfficeManagerEmail-" + p].ToString();
                        location.OfficeManagerExtension = collection["OfficeManagerExtension-" + p].ToString();
                        location.CreatedDT = DateTime.Now;
                        location.ModifiedDT = DateTime.Now;

                        practiceLocations.Add(location);
                    }
                }
                string json = JsonConvert.SerializeObject(practiceLocations);
                return json;
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.LocationController :: GenerateJson -> Exception : " + e.ToString(), this);
                return "";
            }
        }
    }
}