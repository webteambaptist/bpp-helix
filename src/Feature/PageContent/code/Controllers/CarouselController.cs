﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BPP93Helix.Feature.PageContent.Models;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using BPP93Helix.Feature.PageContent.Helper;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class CarouselController : Controller
    {
        // GET: Carousel
        public ActionResult Index()
        {
            var items = Sitecore.Context.Database.SelectItems("/sitecore/content/components/carousel/*");
            /*.OrderByDescending(x => x[Sitecore.FieldIDs.Created])
             Removed the sorting because sorting is done within sitecore */
            var carouselFields = (from i in items
                                  select new CarouselComponent
                                  {
                                      ImageTitle = i.Fields["ImageTitle"].Value,
                                      ImageText = i.Fields["ImageText"].Value,
                                      ImgField = ((ImageField)i.Fields["BackgroundImage"]),
                                      ButtonText = i.Fields["ButtonText"].Value,
                                      ButtonURL = BHSitecore.parseGeneralLink(i.Fields["ButtonURL"])
                                     // ButtonURL = i.Fields["ButtonURL"].Value
                                  }).ToList();
            var carouselItems = new CarouselComponentItems
            {
                _Items = carouselFields
            };
            return View(carouselItems);            
        }
    }
}