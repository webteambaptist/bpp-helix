﻿using BHSearch.Components;
using BHSearch.Helpers;
using BHSearch.Models;
using BPP93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;


namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class DocumentCenterController : Controller
    {
        // GET: DocumentCenter
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult DocumentQuery(SolrQuery obj)
        {
            try
            {
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
               // obj.Handler = "/bpp_physician_index/document";
                obj.Handler = "/bpp_documentcenter_index/document";
                obj.Facets = new List<string>(new string[] {
                    "__semantics"
                 });
                if (obj.Sort == null || string.IsNullOrEmpty(obj.Sort))
                    obj.Sort = "__created_tdt";
                if (obj.SortOrder == null || string.IsNullOrEmpty(obj.SortOrder))
                    obj.SortOrder = "desc";

                SolrResponse response = search.Execute(obj);

                // Map Results to Physician Model
                List<Document> Documents = new List<Document>();
                PropertyInfo[] properties = typeof(Document).GetProperties();

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    Document document = new Document();

                    foreach (PropertyInfo prop in properties)
                    {
                        object value = "";
                        if (doc.TryGetValue(prop.Name, out value))
                        {
                            // change thumbnail size, can handle from front end as well
                            if (prop.Name.Equals("Thumbnail"))
                            {
                                prop.SetValue(document, value.ToString().Replace("16", "50"));
                            }
                            else if (prop.Name.ToLower().Equals("extension"))
                            {
                                switch (value.ToString().ToLower())
                                {
                                    case "pdf":
                                        {
                                            prop.SetValue(document, "pdf.png");
                                            break;
                                        }
                                    case "doc":
                                    case "docx":
                                        {
                                            prop.SetValue(document, "word.png");
                                            break;
                                        }
                                    case "xls":
                                    case "xlsx":
                                        {
                                            prop.SetValue(document, "excel.png");
                                            break;
                                        }
                                    case "rtf":
                                        {
                                            prop.SetValue(document, "rtf-icon.png");
                                            break;
                                        }
                                    case "txt":
                                        {
                                            prop.SetValue(document, "txt-icon.png");
                                            break;
                                        }
                                    case "pptx":
                                        {
                                            prop.SetValue(document, "pptx-icon.png");
                                            break;
                                        }
                                    default:
                                        {
                                            prop.SetValue(document, "alternatedoc.png");
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                prop.SetValue(document, value);
                            }
                        }
                        else
                        {
                            prop.SetValue(document, null);
                        }
                    }
                    Documents.Add(document);
                }

                // Map Views to Response
                JsonResponse json = new JsonResponse();

                // etc
                json.TotalHits = response.TotalHits;
                json.OriginalQuery = response.OriginalQuery;

                //results
                json.Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", new DocumentResultView()
                {
                    Start = obj.Start,
                    Documents = Documents
                });

                // facets
                RenderFacetView(response, "__semantics", json, "_FacetCheckboxes");

                return Json(json);
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }

        [HttpPost]
        public JsonResult DocumentTypeahead(SolrQuery obj)
        {
            try
            {
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
                obj.Handler = "/bpp_documentcenter_index/documentSuggest";
                SolrResponse response = search.Execute(obj);

                // Map Results to Physician Model
                List<Document> Documents = new List<Document>();
                PropertyInfo[] properties = typeof(Document).GetProperties();

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    Document document = new Document();

                    foreach (PropertyInfo prop in properties)
                    {
                        if (doc.TryGetValue(prop.Name, out object value))
                        {
                            prop.SetValue(document, value);
                        }
                        else
                        {
                            prop.SetValue(document, null);
                        }
                    }
                    Documents.Add(document);
                }

                return Json(Documents);
            }

            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }

        [HttpPost]
        public JsonResult DocumentFacets(SolrQuery obj)
        {
            try
            {
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
                obj.Query = "*";
                obj.Rows = 1;
                obj.Handler = "/bpp_documentcenter_index/document";
                obj.Facets = new List<string>(new string[] {
                    "title_s"
                 });
                SolrResponse response = search.Execute(obj);

                return Json(RenderFacetList(response, obj));
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }

        private void RenderFacetView(SolrResponse response, string facetField, JsonResponse json, string partial)
        {
            FacetView facetView = new FacetView();
            ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
            if (response.Facets.TryGetValue(facetField, out facets))
            {
                string filter = "";
                if (response.OriginalQuery.Filters.TryGetValue(facetField, out filter))
                {
                    facetView.Filter = filter.Split(',').ToList();
                }

                facetView.Facet = new KeyValuePair<string, ICollection<KeyValuePair<string, int>>>(facetField, facets);
                json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
            }
        }

        private Dictionary<string, ICollection<KeyValuePair<string, int>>> RenderFacetList(SolrResponse response, SolrQuery obj)
        {
            Dictionary<string, ICollection<KeyValuePair<string, int>>> Facets = new Dictionary<string, ICollection<KeyValuePair<string, int>>>();

            foreach (string facetField in obj.Facets)
            {
                ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
                if (response.Facets.TryGetValue(facetField, out facets))
                {
                    Facets.Add(facetField, facets);
                }
            }

            return Facets;
        }

        private JsonResult BuildJsonErrorResponse(Exception e)
        {
            Dictionary<string, object> error = new Dictionary<string, object>();
            error.Add("Status", "Error");
            // Comment Below to Hide Debugging
            // error.Add("Exception", new {
            //    Message = e.Message,
            //    Trace = e.StackTrace,
            //    InnerException = new
            //    {
            //        Message = e.InnerException.Message != null ? e.InnerException.Message : "",
            //        Trace = e.InnerException.StackTrace != null ? e.InnerException.StackTrace : ""
            //    }
            // });

            return Json(error);
        }

        public FileResult Download(string FilePath)
        {
            try
            {
                FilePath = "/sitecore/media library" + FilePath;
                Sitecore.Data.Database current = Sitecore.Context.Database;
                Sitecore.Data.Items.Item item = Sitecore.Context.Database.Items[FilePath];
                if (item != null)
                {
                    Sitecore.Data.Items.Item sampleMedia = new Sitecore.Data.Items.MediaItem(item);
                    var mediaItem = (MediaItem)sampleMedia;
                    Stream stream = mediaItem.GetMediaStream();
                    long fileSize = stream.Length;
                    byte[] buffer = new byte[(int)fileSize];
                    stream.Read(buffer, 0, (int)stream.Length);
                    stream.Close();
                    string fileName = mediaItem.Name + "." + mediaItem.Extension;
                    return File(buffer, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.DocumentCenterController :: Download -> Exception : " + ex.ToString(), this);
                return null;
            }

        }

    }
}