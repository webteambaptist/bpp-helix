﻿using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ViewResult Index(string Specialty, string StartDate, string EndDate, string MemberStatus, string WorkflowStatus, string Name)
        {
            string api = "http://bpp.api.sitecore.bh.local/api/provider/SPRV";
            Report rpt = new Report();

            using (var w = new System.Net.WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(api);
                }
                catch (Exception) { }

                // if string with JSON data is not empty, deserialize it to class and return its instance 
                var dt = JsonConvert.DeserializeObject<DataTable>(json_data);
                rpt.Providers = dt;
            }

            if (!string.IsNullOrEmpty(Name))
            {
                try
                {
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<string>("FirstName").ToLower().Contains(Name.ToLower()) || row.Field<string>("LastName").ToLower().Contains(Name.ToLower())).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }
            if (!string.IsNullOrEmpty(Specialty))
            {
                try
                {
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<string>("PrimarySpecialty").ToLower().Contains(Specialty.ToLower()) || row.Field<string>("SecondarySpecialty").ToLower().Contains(Specialty.ToLower())).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }
            if (!string.IsNullOrEmpty(StartDate))
            {
                try
                {
                    DateTime start = DateTime.Parse(StartDate);
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<DateTime>("CreatedDt") >= start || row.Field<DateTime>("ModifiedDt") >= start).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                try
                {
                    DateTime end = DateTime.Parse(EndDate);
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<DateTime>("CreatedDt") <= end || row.Field<DateTime>("ModifiedDt") <= end).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }
            
            return View(rpt);
        }

        public void GenerateCSV(string Specialty, string StartDate, string EndDate, string MemberStatus, string WorkflowStatus, string Name)
        {
            string api = "http://bpp.api.sitecore.bh.local/api/provider/SPRV";
            Report rpt = new Report();

            using (var w = new System.Net.WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    json_data = w.DownloadString(api);
                }
                catch (Exception) { }

                // if string with JSON data is not empty, deserialize it to class and return its instance 
                var dt = JsonConvert.DeserializeObject<DataTable>(json_data);
                rpt.Providers = dt;

            }

            if (!string.IsNullOrEmpty(Name))
            {
                try
                {
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<string>("FirstName").ToLower().Contains(Name.ToLower()) || row.Field<string>("LastName").ToLower().Contains(Name.ToLower())).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }
            if (!string.IsNullOrEmpty(Specialty))
            {
                try
                {
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<string>("PrimarySpecialty").ToLower().Contains(Specialty.ToLower()) || row.Field<string>("SecondarySpecialty").ToLower().Contains(Specialty.ToLower())).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }
            if (!string.IsNullOrEmpty(StartDate))
            {
                try
                {
                    DateTime start = DateTime.Parse(StartDate);
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<DateTime>("CreatedDt") >= start || row.Field<DateTime>("ModifiedDt") >= start).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                try
                {
                    DateTime end = DateTime.Parse(EndDate);
                    rpt.Providers = rpt.Providers.AsEnumerable()
                        .Where(row => row.Field<DateTime>("CreatedDt") <= end || row.Field<DateTime>("ModifiedDt") <= end).CopyToDataTable();
                }
                catch { rpt.Providers = null; }
            }

            StringBuilder sb = new StringBuilder();

            IEnumerable<string> columnNames = rpt.Providers.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));
            sb = sb.Replace("ID", "Id");

            foreach (DataRow row in rpt.Providers.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                sb.AppendLine(string.Join(",", fields));
                
            }

            //This gives you the byte array.
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            formatter.Serialize(stream, rpt.Providers); // dt is a DataTable

            var csvbytes = System.Text.Encoding.UTF8.GetBytes(sb.ToString());

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AddHeader("Content-Disposition",
                "attachment;filename=\"CustomReport.csv\"");
            Response.BinaryWrite(csvbytes);
            Response.Flush();
            Response.End();

            Response.Redirect("/Home/Admin/Custom Reports/");
        }
    }
}