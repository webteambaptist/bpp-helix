﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using BHSearch.Helpers;
using BHSearch.Models;
using BHSearch.Components;
using System.Collections;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Web.Hosting;
using Sitecore.Configuration;
using Sitecore.Diagnostics;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PhysicianQuery(SolrQuery obj)
        {
            try
            {
                string solrPhyIndex = Settings.GetSetting("solrPhyIndex");
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
                obj.Handler = solrPhyIndex;
                obj.Facets = new List<string>(new string[] {
                    "primaryspecialty_s",
                    "gender_s",
                    "hospitalaffiliations_pipe"
                 });
                SolrResponse response = search.Execute(obj);

                // Map Results to Physician Model
                List<PhysicianSearch> Physicians = new List<PhysicianSearch>();
                PropertyInfo[] properties = typeof(PhysicianSearch).GetProperties();

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    PhysicianSearch physician = new PhysicianSearch();

                    foreach (PropertyInfo prop in properties)
                    {
                        if (doc.TryGetValue(prop.Name, out object value))
                        {
                            prop.SetValue(physician, value);
                        }
                        else
                        {
                            prop.SetValue(physician, null);
                        }
                    }
                    Physicians.Add(physician);
                }

                // Map Views to Response
                ViewResponse json = new ViewResponse();

                // etc
                json.TotalHits = response.TotalHits;
                json.OriginalQuery = response.OriginalQuery;

                // results
                json.Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", Physicians);

                // facets
                RenderFacetView(response, "primaryspecialty_s", json, "_FacetDropdown");
                RenderFacetView(response, "languages_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "hospitalaffiliations_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "gender_s", json, "_FacetGender");

                return Json(json);
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.SearchController -> PhysicianQuery : " + ex.ToString(), this);
                return BuildJsonErrorResponse(ex);
            }
        }

        [HttpPost]
        public JsonResult PhysicianTypeahead(SolrQuery obj)
        {
            try
            {
                // Execute Search
                ExecuteSearch search = new ExecuteSearch();
                obj.Handler = "/bpp_physician_index/physicianSuggest";
                SolrResponse response = search.Execute(obj);

                // Map Results to Physician Model
                List<PhysicianSearch> Physicians = new List<PhysicianSearch>();
                PropertyInfo[] properties = typeof(PhysicianSearch).GetProperties();

                foreach (Dictionary<string, object> doc in response.Results)
                {
                    PhysicianSearch physician = new PhysicianSearch();

                    foreach (PropertyInfo prop in properties)
                    {
                        if (doc.TryGetValue(prop.Name, out object value))
                        {
                            prop.SetValue(physician, value);
                        }
                        else
                        {
                            prop.SetValue(physician, null);
                        }
                    }
                    Physicians.Add(physician);
                }

                return Json(Physicians);
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }

        private void RenderFacetView(SolrResponse response, string facetField, ViewResponse json, string partial)
        {
            FacetView facetView = new FacetView();
            ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
            if (response.Facets.TryGetValue(facetField, out facets))
            {
                if (response.OriginalQuery.Filters.TryGetValue(facetField, out string filter))
                {
                    facetView.Filter = filter.Split(',').ToList();
                }

                facetView.Facet = new KeyValuePair<string, ICollection<KeyValuePair<string, int>>>(facetField, facets);
                json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
            }
        }

        private JsonResult BuildJsonErrorResponse(Exception e)
        {
            Dictionary<string, object> error = new Dictionary<string, object>();
            error.Add("Status", "Error");
            // Comment Below to Hide Debugging
            error.Add("Exception", new
            {
                e.Message,
                Trace = e.StackTrace,
                InnerException = new
                {
                    Message = e.InnerException.Message ?? "",
                    Trace = e.InnerException.StackTrace ?? ""
                }
            });

            return Json(error);
        }
    }
}