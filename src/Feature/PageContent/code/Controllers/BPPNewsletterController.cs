﻿using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class BPPNewsletterController : Controller
    {
        // GET: BPPNewsletter
        public ActionResult Newsletter()
        {
            if (BHUser.Components.Login.IfUserLoggedIn())
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.BPPNewsletterController :: Newsletter -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                 return View();
            }
            else
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.BPPNewsletterController :: Newsletter -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                return Redirect(Url.Content("~/Login"));
            }
        }
    }
}