﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BPP93Helix.Feature.PageContent.Models;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Configuration;
using System.Security.Claims;
using Newtonsoft.Json.Linq;
using Sitecore.Diagnostics;
using System.Web.Script.Serialization;
using System.Text;
using Newtonsoft.Json.Converters;
using BPP93Helix.Feature.PageContent.Helper;
using BHUser.Components;
using Sitecore;
using Sitecore.Text;
using Sitecore.Configuration;
using Sitecore.Extensions;
using Convert = System.Convert;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class ApplicationController : Controller
    {
        HttpClient client;
        private readonly string spAPIBaseAddress = Settings.GetSetting("spAPIBaseAddress");
        private readonly string spAPISubmitAppForm = Settings.GetSetting("spAPISubmitAppForm");
        private readonly string spAPIGetByProv = Settings.GetSetting("spAPIGetByProv");
        private readonly string spAPIUpdAppStatus = Settings.GetSetting("spAPIUpdAppStatus");
        private readonly string spAPIUpdPractice = Settings.GetSetting("spAPIUpdPractice");
        private readonly string spAPIUpdProvider = Settings.GetSetting("spAPIUpdProvider");
        private readonly string spAPICloneProvider = Settings.GetSetting("spAPICloneProvider");
        private readonly string spAPIGetPracAttachments = Settings.GetSetting("spAPIGetPracAttachments");
        private readonly string spAPIGetProvAttachments = Settings.GetSetting("spAPIGetProvAttachments");
        private readonly string spAPIDownloadAttachment = Settings.GetSetting("spAPIDownloadAttachment");
        private readonly string spAPIUploadAttachment = Settings.GetSetting("spAPIUploadAttachment");
        private readonly string spAPIDownloadAgreement = Settings.GetSetting("spAPIDownloadAgreement");
        private readonly string spAPIGetPracDetails = Settings.GetSetting("spAPIGetPracDetails");
        private readonly string spAPIGetProvDetails = Settings.GetSetting("spAPIGetProvDetails");
        private readonly string spAPIGetReportDetails = Settings.GetSetting("spAPIGetReportDetails");
        private readonly string spAPIGetJoinderPDF = Settings.GetSetting("spAPIGetJoinderPDFF");
        private readonly string spAPISaveBPPForm = Settings.GetSetting("spAPISaveBPPForm");
        private readonly string spAPISaveBAAForm = Settings.GetSetting("spAPISaveBAAForm");
        private readonly string spAPISelSpecials = Settings.GetSetting("spAPISelSpecials");
        private readonly string spAPISelPractices = Settings.GetSetting("spAPISelPractices");
        private readonly string spAPISelTaxId = Settings.GetSetting("spAPISelTaxId");
        private readonly string spAPIDeleteProvider = Settings.GetSetting("spAPIDeleteProvider");
        private readonly string spAPISelDegrees = Settings.GetSetting("spAPISelDegrees");
        private readonly string spAPISelStatusChangeReasons = Settings.GetSetting("spAPISelStatusChangeReasons");
        private readonly string spAPIRemoveProviderEndDate = Settings.GetSetting("spAPIRemoveProviderEndDate");
        private readonly string spAPIRemoveLocation = Settings.GetSetting("spAPIRemoveLocation");
        public ApplicationController()
        {
            //client = new HttpClient
            //{
            //    BaseAddress = new Uri("http://bpp.api.sitecore.bh.local/api/practiceappdata/ipad")
            //};
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        #region Applicaiton Request

        //public ActionResult Index()
        //{
        //    //BPPApplicationForm _bppAppForm = new Models.BPPApplicationForm();
        //    try
        //    {
        //        Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers :: ApplicationController -> Index Called.", this);
        //    }
        //    catch (Exception ex)
        //    {
        //        Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers :: ApplicationController -> Index Exception." + ex.ToString(), this);
        //        throw;
        //    }
        //    //return View(_bppAppForm);
        //    return View();
        //}
        #endregion

        #region Applicant List, Practice & Provider Profiles

        [HttpGet]
        public ActionResult ManageApplications(string GUID = "")
        {
            if (BHUser.Components.Login.IfProviderLoggedIn())
            {
              return Redirect("~/Home");
            }
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                var jSonData = "";
                HttpWebResponse httpWebResponse = null;
                try
                {
                    List<BPPApplicationList> _bppApplList = new List<BPPApplicationList>();
                    httpWebResponse = API.GetDataFromAPI(spAPIGetByProv, (!string.IsNullOrEmpty(GUID) ? "GUID|" + GUID : ""));
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ManageApplications -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ManageApplications -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var objText = reader.ReadToEnd();
                            JObject data = JObject.Parse(objText);

                            _bppApplList = data["_ProvidersShort"].Select(c => c.ToObject<BPPApplicationList>()).ToList();
                            jSonData = JsonConvert.SerializeObject(_bppApplList);
                        }
                    }
                    else
                    {
                        Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ManageApplications -> httpWebResponse Null Exception : ", this);
                        jSonData = "";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ManageApplications -> Exception : " + ex.ToString(), this);
                    jSonData = "";
                }
                var jsonResult = Json(new { data = jSonData }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ManageApplications -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-access" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ManageApplications -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-login" }, JsonRequestBehavior.AllowGet);
                }
            }

        }

        [HttpPost]
        public ActionResult UpdateApplicants(string strAppUpd, string page)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                HttpWebResponse httpWebResponse = null;

                try
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Incoming Request : " + strAppUpd, strAppUpd);
                    if (!string.IsNullOrEmpty(strAppUpd))
                    {
                        string[] splitStrAppUpd = strAppUpd.Split(',');
                        if (splitStrAppUpd != null && splitStrAppUpd.Length > 1)
                        {
                            string strStatus = splitStrAppUpd[0].ToString().TrimStart("[\"".ToArray()).TrimEnd("\"".ToArray());
                            Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Value passed to switch statement : " + strStatus, splitStrAppUpd);
                            //ApplicationSubStatus
                            if (page == "ProviderProfile")
                            {
                                string subStatus = splitStrAppUpd[1];
                                string memberStatus = splitStrAppUpd[2];
                                string data = splitStrAppUpd[3];
                                string inactivereason = splitStrAppUpd[4];
                                string otherreason = splitStrAppUpd[5];

                                ProviderAppData appdata = new ProviderAppData();
                                List<BPPProvider> providers = new List<BPPProvider>();
                                appdata.ApplicationSubStatus = subStatus;
                                BPPProvider prov = new BPPProvider();
                                prov.ID = Convert.ToInt32(data);
                                providers.Add(prov);
                                appdata.Providers = providers;

                            }
                            else
                            {
                                switch (strStatus)
                                {
                                    case "7":
                                        {
                                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Delete Selected on Provider: " + strStatus, strStatus);

                                            string data = "[" + strAppUpd.Substring(strAppUpd.IndexOf(",") + 1) + "]";
                                            List<UpdateProviderStatus> updates = new List<UpdateProviderStatus>();
                                            updates = JsonConvert.DeserializeObject<List<UpdateProviderStatus>>(data);

                                            string strUpdateJson = @"{ ""Providers"":[";
                                            foreach (var item in updates)
                                            {
                                                strUpdateJson += @"{ ""ID"": " + item.rowID + "}" + (((updates.IndexOf(item)) == updates.Count - 1) ? "]}" : ",");
                                            }
                                            httpWebResponse = API.PostDataToAPI(spAPIDeleteProvider, strUpdateJson);
                                            break;
                                        }
                                    default:
                                        {
                                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Option other than Next selected : " + strStatus, strStatus);

                                            string strUpdateJson = string.Empty;

                                            strUpdateJson += "{ \"ProviderMemberStatus\": \"" + splitStrAppUpd[0].ToString() + "\", \"ApplicationSubStatus\":\"0\", \"Providers\":[";
                                            for (int i = 1; i < splitStrAppUpd.Length; i++)
                                            {
                                                strUpdateJson += "{ \"ID\": " + splitStrAppUpd[i].ToString() + "}" + (((i + 1) == splitStrAppUpd.Length) ? "]}" : ",");
                                            }

                                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Update Provider Json : " + strUpdateJson, strUpdateJson);

                                            httpWebResponse = API.PostDataToAPI(spAPIUpdAppStatus, strUpdateJson);
                                            break;
                                        }
                                }
                            }
                            if (httpWebResponse != null)
                            {
                                Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> API Response : " + httpWebResponse, httpWebResponse);
                                Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
                                if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
                                {
                                    status = "success";
                                    message = "The selected Provider(s) have been updated succesfully.";
                                }
                                else
                                {
                                    status = httpWebResponse.StatusCode.ToString();
                                    message = "An error has occured while processing your request";
                                }
                            }
                            else
                            {
                                status = "error";
                                message = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";
                            }
                        }
                    }
                    else
                    {
                        status = "error";
                        message = "500 Internal Error";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Exception : " + ex.ToString(), this);
                    //throw;
                }
                returnList.Add(status);
                returnList.Add(message);
                return Json(returnList, JsonRequestBehavior.DenyGet);
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    returnList.Add("no-access");
                    return Json(returnList, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    returnList.Add("no-login");
                    return Json(returnList, JsonRequestBehavior.DenyGet);
                }
            }
        }

        //[Authorize]
        [HttpGet]
        public ActionResult GetAppReport(string strRepMode)
        {
            HttpResponseMessage file = new HttpResponseMessage();
            try
            {
                string strReportName = BHSitecore.GetEnumDescription((enumSSRSReportType)(Convert.ToInt32(strRepMode)));
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetAppReport -> API Request : " + strReportName, strReportName);
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("reportname", strReportName);
                    Task<HttpResponseMessage> response = httpClient.GetAsync(spAPIBaseAddress + spAPIGetReportDetails);
                    file = response.Result;
                }
                var headers = file.Content.Headers;
                
                string fileName = System.DateTime.Now.ToString();
                if (!String.IsNullOrEmpty(headers.ContentDisposition.FileName))
                {
                    fileName = headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');
                }
                return File(file.Content.ReadAsByteArrayAsync().Result, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetAppReport (" + strRepMode + ") -> Exception : " + ex.ToString(), this);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "File not Found");
            }
        }

        public ActionResult PracticeProfile(string ID)
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                BPPApplication _bppApplication = new BPPApplication();
                try
                {
                    if (!string.IsNullOrEmpty(ID))
                    {
                        _bppApplication = GetPracticeProfile(ID);
                        if (_bppApplication != null)
                        {
                            if (_bppApplication._Practices[0].PracticeApprovalStatus == null)
                                _bppApplication._Practices[0].PracticeApprovalStatus = 0;
                            return View(_bppApplication);
                        }
                        else
                            return Redirect(Url.Content("~/"));
                    }
                    else
                    {
                        return Redirect(Url.Content("~/"));
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: PracticeProfile -> Exception : " + ex.ToString(), this);
                    _bppApplication = null;
                    return View(_bppApplication);
                }
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: PracticeProfile -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/"));
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: PracticeProfile -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/Login"));
                }
            }
        }
        [HttpPost]
        public ActionResult RemoveLocation(string locationID)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            try
            {
                if (locationID != null)
                {
                    HttpWebResponse httpWebResponse = API.PostDataToAPI(spAPIRemoveLocation, "", "LocationID|" + locationID);
                    if (httpWebResponse != null)
                    {
                        if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
                        {
                            status = "success";
                            message = "Location has been successfully removed.";
                        }
                        else
                        {
                            status = httpWebResponse.StatusCode.ToString();
                            message = "An error occurred processing your request";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: RemoveLocation -> Exception : " + ex.ToString(), this);
                message = "Unexpected error";
                status = "error";
            }
            returnList.Add(status);
            returnList.Add(message);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public ActionResult PracticeProfile(BPPApplication _bppApplication)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            try
            {

                var UserName = Sitecore.Context.GetUserName().Split('\\');

                if (_bppApplication != null && _bppApplication._Practices != null)
                {
                    string jo = JsonConvert.SerializeObject(_bppApplication);
                    HttpWebResponse httpWebResponse = null;
                    Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update PracticeProfile -> Submitting JSON Final Copy:" + jo.ToString(), jo);
                    httpWebResponse = API.PostDataToAPI(spAPIUpdPractice, jo.ToString(), "UserName|" + UserName[1]);
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update PracticeProfile -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info(" -> API Response Code: " + (int)httpWebResponse.StatusCode, httpWebResponse);
                        if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
                        {
                            status = "success";
                            message = "Practice details have been updated successfully.";
                        }
                        else
                        {
                            status = httpWebResponse.StatusCode.ToString();
                            message = "An error occurred processing your request";
                        }

                    }
                    else
                    {
                        status = "error";
                        message = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";
                    }
                }
                else
                {
                    status = "error";
                    message = "500 Internal Error";
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update PracticeProfile -> Empty Practice Profile.", _bppApplication);
                }
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update PracticeProfile -> Exception : " + ex.ToString(), this);
            }
            
            returnList.Add(status);
            returnList.Add(message);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }

        public BPPApplication GetPracticeProfile(string ID)
        {
            BPPApplication _bppApplication = new BPPApplication();
            if (!string.IsNullOrEmpty(ID))
            {
                try
                {
                    HttpWebResponse httpWebResponse = null;
                    httpWebResponse = API.GetDataFromAPI(spAPIGetPracDetails, "GUID|" + ID);
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPracticeProfile -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPracticeProfile -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            JsonSerializer js = new JsonSerializer();
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            _bppApplication = (BPPApplication)JsonConvert.DeserializeObject(objText, typeof(BPPApplication), settings);
                        }
                        if (_bppApplication != null && _bppApplication._Practices != null && _bppApplication._Providers != null)
                        {
                            _bppApplication._Providers = BHSitecore.CustomParseProviders(_bppApplication._Providers);
                            for (int i = 0; i < _bppApplication._Providers.Count; i++)
                            {
                                _bppApplication._Providers[i].Facilties = new List<BPPFacilities>();
                                foreach (var bv in _bppApplication._Facilities)
                                {
                                    bool isSelected = false;
                                    if (_bppApplication._Providers[i].ProviderFacilities != null && _bppApplication._Providers[i].ProviderFacilities.Contains(bv.ID))
                                        isSelected = true;
                                    _bppApplication._Providers[i].Facilties.Add(new BPPFacilities { ID = bv.ID, FacilityName = bv.FacilityName, isSelected = isSelected });
                                }
                            }
                        }
                        else
                        {
                            _bppApplication = null;
                            TempData["appListResponse"] = "500 Internal Error";
                        }

                        TempData["PracticeProviders"] = Newtonsoft.Json.JsonConvert.SerializeObject(_bppApplication._Providers.ToArray());
                        //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: GetPracticeProfile -> _bppApplication: " + Newtonsoft.Json.JsonConvert.SerializeObject(_bppApplication), _bppApplication);
                    }
                    else
                    {
                        _bppApplication = null;
                        TempData["appListResponse"] = "500 Internal Error";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPracticeProfile -> Exception : " + ex.ToString(), this);
                    _bppApplication = null;
                }
            }
            else
            {
                _bppApplication = null;
                TempData["appListResponse"] = "500 Internal Error";
            }
            return _bppApplication;
        }

        [HttpGet]
        public ActionResult PracticeProviders(string GUID = "")
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                var jSonData = "";
                HttpWebResponse httpWebResponse = null;
                try
                {
                    List<BPPProvider> _bppPracProviders = new List<BPPProvider>();
                    httpWebResponse = API.GetDataFromAPI(spAPIGetPracDetails, "GUID|" + GUID);
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: PracticeProviders -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: PracticeProviders -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var objText = reader.ReadToEnd();
                            JObject data = JObject.Parse(objText);
                            _bppPracProviders = data["_Providers"].Select(c => c.ToObject<BPPProvider>()).ToList();
                            jSonData = Newtonsoft.Json.JsonConvert.SerializeObject(_bppPracProviders);
                        }
                    }
                    else
                    {
                        Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: PracticeProviders -> httpWebResponse Null Exception : ", this);
                        jSonData = "";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: PracticeProviders -> Exception : " + ex.ToString(), this);
                    jSonData = "";
                }
                //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: PracticeProviders -> _bppApplList Serialized: " + jSonData, jSonData);
                var jsonResult = Json(new { data = jSonData }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-access" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UpdateApplicants -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-login" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public JsonResult GetFileAttachments(string ID, int Mode, string PracticeID = "", string TaxID = "", string MatrixID = "")
        {
            //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: GetFileAttachments -> Incoming Req : ID - " + ID + " & Mode - " + Mode, this);
            var jSonData = "";
            try
            {
                HttpWebResponse httpWebResponse = (Mode == 1) ? API.GetDataFromAPI(spAPIGetPracAttachments, "TaxID|" + TaxID, "POST") : API.GetDataFromAPI(spAPIGetProvAttachments, "NPI|" + ID + PracticeID + MatrixID, "POST");
                if (httpWebResponse != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPracticeAttachments -> API Response : " + httpWebResponse, httpWebResponse);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPracticeAttachments -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        jSonData = reader.ReadToEnd();
                    }
                }
                else
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPracticeAttachments -> httpWebResponse Null Exception : ", this);
                    jSonData = "";
                }
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPracticeAttachments -> Exception : " + ex.ToString(), this);
                jSonData = "";
            }
            //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: GetPracticeAttachments -> Attachments Serialized: " + jSonData, jSonData);
            return Json(new { data = jSonData }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadAttachment(string ID, int Mode = 1, string PracticeID = "", string TaxID = "", string LegalEntityID = "", string MatrixID = "")
        {
            //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: UploadAttachment -> Incoming Req : ID - " + ID + " & Mode - " + Mode + " & GUID - " + GUID, this);
            try
            {
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];
                    //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: UploadAttachment -> Incoming Filename : " + file.FileName, file);
                    try
                    {
                        var message = new HttpRequestMessage();
                        var content = new MultipartFormDataContent();

                        byte[] buffer = new byte[file.InputStream.Length];
                        file.InputStream.Seek(0, SeekOrigin.Begin);
                        file.InputStream.Read(buffer, 0, Convert.ToInt32(file.InputStream.Length));
                        Stream objStream = new MemoryStream(buffer);
                        content.Add(new StreamContent(objStream), file.FileName, file.FileName);

                        message.Method = HttpMethod.Post;
                        message.Content = content;
                        message.RequestUri = new Uri(spAPIBaseAddress + spAPIUploadAttachment);
                        if (Mode == 1)
                        {
                            message.Headers.Add("TaxID", TaxID);
                        }
                        else
                        {
                            message.Headers.Add("NPI", ID + PracticeID + MatrixID); // npi
                        }
                        var client = new HttpClient();
                        client.SendAsync(message).ContinueWith(task =>
                        {

                        });
                        //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: UploadAttachment -> Upload Success.!!", file);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UploadAttachment -> API Exception : " + ex.ToString(), this);
                    }
                }
                if (Mode == 1)
                {
                    return Redirect("~/Home/Admin/Master Lookup/Entity Profile?ID=" + LegalEntityID);
                }
                else
                {
                    return Redirect("~/Home/Admin/Master Lookup/Provider Profile?ID=" + ID + "&PracticeID=" + PracticeID + "&MatrixID=" + MatrixID);
                }
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: UploadAttachment -> Exception : " + ex.ToString(), this);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }
        }

        public ActionResult DownloadAttachment(string ID)
        {
            try
            {

                WebClient client = new WebClient();
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                client.Headers.Add("ID", ID.ToString());
                byte[] result = client.UploadData(spAPIBaseAddress + spAPIDownloadAttachment,
                                                "POST",
                                                 System.Text.Encoding.UTF8.GetBytes("ID=abc"));
                
                string fileName = System.DateTime.Now.ToString();
                if (!String.IsNullOrEmpty(client.ResponseHeaders["Content-Disposition"]))
                {
                    fileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 9).Replace("\"", "");
                }
                return File(result, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: DownloadAttachment -> Exception : " + ex.ToString(), this);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "File not Found");
            }
        }


        public ActionResult ProviderProfile(string ID, string PracticeID, int matrixID)
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                BPPApplication _bppApplication = new BPPApplication();
                try
                {
                    if (!string.IsNullOrEmpty(ID))
                    {
                        _bppApplication = GetProviderProfile(ID, PracticeID, matrixID);
                        if (_bppApplication != null)
                        {
                            var specialties = GetAllSpecialties();
                            _bppApplication._Providers[0].Specialties = GetSelectListItems(specialties);

                            var degrees = GetAllDegrees();
                            _bppApplication._Providers[0].Degrees = GetSelectListItems(degrees);

                            var _msostatus = MSOVerificationStatus(_bppApplication);
                            _bppApplication._Providers[0]._MSOVerficationStatus = GetSelectListItems(_msostatus);

                            var _msorecstatus = MQRecStatus(_bppApplication);
                            _bppApplication._Providers[0]._MQRecommendationStatus = GetSelectListItems(_msorecstatus);

                            var _boardOutcome = BoardOutcome(_bppApplication);
                            _bppApplication._Providers[0]._BoardOutcome = GetSelectListItems(_boardOutcome);

                            var _denialreasons = DenialReasonStatus(_bppApplication);
                            _bppApplication._Providers[0]._DenialReasons = GetSelectListItems(_denialreasons);

                            var _providermemberstatus = ProviderMemberStatus(_bppApplication);
                            _bppApplication._Providers[0]._ProviderMemberStatus = GetSelectListItems(_providermemberstatus);

                            var _inactivereasons = InactiveReasonStatus(_bppApplication);
                            _bppApplication._Providers[0]._InactiveReasons = GetSelectListItems(_inactivereasons);
                            _bppApplication._Providers[0].matrix = new ProviderPracticeMatrix
                            {
                                ID = matrixID
                            };
                            return View(_bppApplication);
                        }
                        else
                        {
                            return Redirect(Url.Content("~/"));
                        }
                    }
                    else
                    {
                        return Redirect(Url.Content("~/"));
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ProviderProfile -> Exception : " + ex.ToString(), this);
                    _bppApplication = null;
                    return View(_bppApplication);
                }
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ProviderProfile -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/"));
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: ProviderProfile -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/Login"));
                }
            }
        }

        [HttpPost]
        public ActionResult RemoveProviderEndDate(string ProviderID, string PracticeID, int MatrixID)
        {
            BPPApplication _bppApplication = new BPPApplication();
            string status = null;
            List<string> returnList = new List<string>();
            if (!string.IsNullOrEmpty(ProviderID))
            {
                try
                {
                    HttpWebResponse httpWebResponse = null;
                    httpWebResponse = API.GetDataFromAPI(spAPIRemoveProviderEndDate, "ID|" + ProviderID + "^" + "PracticeID|" + PracticeID + "^" + "MatrixID|" + MatrixID);
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: RemoveProviderEndDate -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: RemoveProviderEndDate -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {

                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            _bppApplication = GetProviderProfile(ProviderID, PracticeID, MatrixID);
                            if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
                            {
                                status = "success";
                            }
                            //_bppApplication = (BPPApplication)JsonConvert.DeserializeObject(objText, typeof(BPPApplication), settings);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: RemoveProviderEndDate -> Exception : " + ex.ToString(), this);
                    _bppApplication = null;
                }
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public ActionResult ProviderProfile(BPPApplication _bppApplication)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                try
                {
                    if (_bppApplication != null && _bppApplication._Practices != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update ProviderProfile -> Incoming Model : " + JsonConvert.SerializeObject(_bppApplication), _bppApplication);
                        HttpWebResponse httpWebResponse = null;
                        if (_bppApplication._Providers[0].Facilties != null)
                        {
                            if (_bppApplication._Providers[0].ProviderFacilities != null)
                                _bppApplication._Providers[0].ProviderFacilities.Clear();
                            _bppApplication._Providers[0].ProviderFacilities = new List<int>();
                            List<int> ab = new List<int>();
                            foreach (var bv in _bppApplication._Providers[0].Facilties)
                            {
                                if (bv.isSelected)
                                    _bppApplication._Providers[0].ProviderFacilities.Add(bv.ID);
                            }
                        }

                        if (_bppApplication._Providers[0].Contracts != null)
                        {
                            if (_bppApplication._Providers[0].ProviderAcceptingPatients != null)
                                _bppApplication._Providers[0].ProviderAcceptingPatients.Clear();
                            _bppApplication._Providers[0].ProviderAcceptingPatients = new List<int>();
                            foreach (var b in _bppApplication._Providers[0].Contracts)
                            {
                                if (b.isSelected)
                                    _bppApplication._Providers[0].ProviderAcceptingPatients.Add(b.ID);
                            }
                        }

                        if (_bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes != null)
                        {
                            if (_bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes.Contains(Environment.NewLine))
                            {
                                var _ReformattedoutComeNotes = _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes.Replace(Environment.NewLine, ";");
                                _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes = _ReformattedoutComeNotes;
                            }
                            else
                            {
                                _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes = _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes;
                            }
                        }

                        JObject jo = JObject.Parse(JsonConvert.SerializeObject(_bppApplication));
                        Sitecore.Diagnostics.Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update PracticeProfile -> Submitting JSON :" + jo["_Providers"].First.ToString(), jo);
                        var UserName = Sitecore.Context.GetUserName().Split('\\');
                        httpWebResponse = API.PostDataToAPI(spAPIUpdProvider, jo["_Providers"].First.ToString(), "UserName|" + UserName[1]);
                        if (httpWebResponse != null)
                        {
                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update ProviderProfile -> API Response : " + httpWebResponse, httpWebResponse);
                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update ProviderProfile -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
                            if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
                            {
                                status = "success";
                                message = "Provider details have been updated successfully.";
                            }
                            else
                            {
                                status = "error";
                                message = httpWebResponse.StatusCode + ": An Error occurred while processing your request";
                            }
                        }
                        else
                        {
                            status = "error";
                            message = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";
                        }
                    }
                    else
                    {
                        status = "error";
                        message = "500 Internal Error";
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update ProviderProfile -> Empty Practice Profile.", _bppApplication);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Update ProviderProfile -> Exception : " + ex.ToString(), ex.Source);
                }
                returnList.Add(status);
                returnList.Add(message);
                return Json(returnList, JsonRequestBehavior.DenyGet);
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: Update ProviderProfile -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/"));
                }
                else
                {
                    Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: Update ProviderProfile -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/Login"));
                }
            }
        }

        [HttpPost]
        public ActionResult CloneProviderProfile(BPPApplication _bppApplication)
        {
            string message = null;
            string status = null;
            List<string> returnList = new List<string>();
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                try
                {
                    if (_bppApplication != null && _bppApplication._Practices != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> Incoming Model : " + JsonConvert.SerializeObject(_bppApplication), _bppApplication);
                        HttpWebResponse httpWebResponse = null;
                        if (_bppApplication._Providers[0].Facilties != null)
                        {
                            if (_bppApplication._Providers[0].ProviderFacilities != null)
                                _bppApplication._Providers[0].ProviderFacilities.Clear();
                            _bppApplication._Providers[0].ProviderFacilities = new List<int>();
                            List<int> ab = new List<int>();
                            foreach (var bv in _bppApplication._Providers[0].Facilties)
                            {
                                if (bv.isSelected)
                                    _bppApplication._Providers[0].ProviderFacilities.Add(bv.ID);
                            }
                        }
                        JObject jo = JObject.Parse(JsonConvert.SerializeObject(_bppApplication));
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> Submitting JSON :" + jo["_Providers"].First.ToString(), jo);
                        var UserName = Sitecore.Context.GetUserName().Split('\\');
                        httpWebResponse = API.PostDataToAPI(spAPICloneProvider, jo["_Providers"].First.ToString(), "UserName|" + UserName[1]);
                        if (httpWebResponse != null)
                        {
                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> API Response : " + httpWebResponse, httpWebResponse);
                            Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
                            if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
                            {
                                status = "success";
                                message = "Provider Profile has been cloned succesfully.";
                            }
                            else
                            {
                                status = "error";
                                message = httpWebResponse.StatusCode + ": An Error occurred while processing your request";
                            }
                        }
                        else
                        {
                            status = "error";
                            message = "We experienced a technical difficulty while processing your request. Your data may not have been correctly saved.";
                        }
                    }
                    else
                    {
                        status = "error";
                        message = "500 Internal Error";
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> Empty Practice Profile.", _bppApplication);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> Exception : " + ex.ToString(), ex.Source);
                }
                returnList.Add(status);
                returnList.Add(message);
                return Json(returnList, JsonRequestBehavior.DenyGet);
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/"));
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: Clone Provider Profile -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Redirect(Url.Content("~/Login"));
                }
            }
        }
        public string GetTaxId(string practiceName)
        {
            string taxId = null;
            try
            {
                HttpWebResponse httpWebResponse = API.GetDataFromAPI(spAPISelTaxId, "PracticeName|" + practiceName);
                if (httpWebResponse != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetTaxId -> API Response : " + httpWebResponse, httpWebResponse);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetTaxId -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        taxId = (string)js.Deserialize(objText, typeof(string));
                    }
                }

            }
            catch (Exception e)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetTaxId -> Exception : " + e.ToString(), e.Source);
                taxId = null;
            }
            return taxId;
        }
        public List<BPPPractices> GetPractices()
        {
            List<BPPPractices> practices = new List<BPPPractices>();
            try
            {
                HttpWebResponse httpWebResponse = API.GetDataFromAPI(spAPISelPractices);
                if (httpWebResponse != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPractices -> API Response : " + httpWebResponse, httpWebResponse);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPractices -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        practices = (List<BPPPractices>)js.Deserialize(objText, typeof(List<BPPPractices>));
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetPractices -> Exception : " + e.ToString(), e.Source);
                practices = null;
            }
            return practices;
        }
        public List<BPPSpecialties> GetSpecialties()
        {
            List<BPPSpecialties> specialties = new List<BPPSpecialties>();
            try
            {
                HttpWebResponse httpWebResponse = API.GetDataFromAPI(spAPISelSpecials);

                if (httpWebResponse != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> API Response : " + httpWebResponse, httpWebResponse);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        specialties = (List<BPPSpecialties>)js.Deserialize(objText, typeof(List<BPPSpecialties>));
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> Exception : " + e.ToString(), e.Source);
                specialties = null;
            }
            return specialties;
        }
        public List<BPPDegrees> GetDegrees()
        {
            List<BPPDegrees> degrees = new List<BPPDegrees>();
            try
            {
                HttpWebResponse httpWebResponse = API.GetDataFromAPI(spAPISelDegrees);

                if (httpWebResponse != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> API Response : " + httpWebResponse, httpWebResponse);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        degrees = (List<BPPDegrees>)js.Deserialize(objText, typeof(List<BPPDegrees>));
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> Exception : " + e.ToString(), e.Source);
                degrees = null;
            }
            return degrees;
        }
        public List<StatusChangeReason> GetStatusChangeReasons()
        {
            List<StatusChangeReason> reasons = new List<StatusChangeReason>();
            try
            {
                HttpWebResponse httpWebResponse = API.GetDataFromAPI(spAPISelStatusChangeReasons);

                if (httpWebResponse != null)
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> API Response : " + httpWebResponse, httpWebResponse);
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        reasons = (List<StatusChangeReason>)js.Deserialize(objText, typeof(List<StatusChangeReason>));
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetSpecialties -> Exception : " + e.ToString(), e.Source);
                reasons = null;
            }
            return reasons;
        }
        public BPPApplication GetProviderProfile(string ID, string PracticeID, int matrixID)
       {
            BPPApplication _bppApplication = new BPPApplication();
            if (!string.IsNullOrEmpty(ID))
            {
                try
                {
                    HttpWebResponse httpWebResponse = null;
                    //httpWebResponse = API.GetDataFromAPI(spAPIGetProvDetails, "ID|" + ID);
                    httpWebResponse = API.GetDataFromAPI(spAPIGetProvDetails, "ID|" + ID + "^" + "PracticeID|" + PracticeID + "^" + "MatrixID|" + matrixID);

                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetProviderProfile -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetProviderProfile -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {

                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            _bppApplication = (BPPApplication)JsonConvert.DeserializeObject(objText, typeof(BPPApplication), settings);

                            if (_bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes != null)
                            {
                                if (_bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes.Contains(";"))
                                {
                                    var _outComeNotes = _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes.Replace(";", Environment.NewLine);
                                    _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes = _outComeNotes;
                                }
                                else
                                {
                                    _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes = _bppApplication._Providers[0].ProviderPracticeWorkFlow.OutcomeNotes;
                                }
                            }
                        }
                        if (_bppApplication != null && _bppApplication._Practices != null && _bppApplication._Providers != null)
                        {
                            _bppApplication._Providers = BHSitecore.CustomParseProviders(_bppApplication._Providers);
                            if (_bppApplication._Practices[0].PracticeLocations != null)
                                _bppApplication._Practices[0].PracticeLocations = BHSitecore.CustomParseLocation(_bppApplication._Practices[0].PracticeLocations);
                            _bppApplication._Providers[0].Facilties = new List<BPPFacilities>();
                            foreach (var bv in _bppApplication._Facilities)
                            {
                                bool isSelected = false;
                                if (_bppApplication._Providers[0].ProviderFacilities != null && _bppApplication._Providers[0].ProviderFacilities.Contains(bv.ID))
                                    isSelected = true;
                                _bppApplication._Providers[0].Facilties.Add(new BPPFacilities { ID = bv.ID, FacilityName = bv.FacilityName, isSelected = isSelected });
                            }

                            _bppApplication._Providers[0].Contracts = new List<AcceptingContracts>();
                            foreach (var bv in _bppApplication._AcceptingNewPatientContracts)
                            {
                                bool isSelected = false;
                                if (_bppApplication._Providers[0].ProviderAcceptingPatients != null && _bppApplication._Providers[0].ProviderAcceptingPatients.Contains(bv.ID))
                                    isSelected = true;
                                _bppApplication._Providers[0].Contracts.Add(new AcceptingContracts { ID = bv.ID, ContractName = bv.ContractName, isSelected = isSelected });
                            }

                            //_bppApplication._Providers[0].ProviderPracticeWorkFlow.ProviderApprovalStatus = Convert.ToInt32(_bppApplication._Providers[0].ProviderPracticeWorkFlow.ProviderApprovalStatus);
                            _bppApplication._Specialties = GetSpecialties();
                        }
                        else
                        {
                            _bppApplication = null;
                            TempData["appListResponse"] = "500 Internal Error";
                        }
                    }
                    else
                    {
                        _bppApplication = null;
                        TempData["appListResponse"] = "500 Internal Error";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.ApplicationController :: GetProviderProfile -> Exception : " + ex.ToString(), this);
                    _bppApplication = null;
                }
            }
            else
            {
                _bppApplication = null;
                TempData["appListResponse"] = "500 Internal Error";
            }
            return _bppApplication;
        }

        #endregion

        #region Common API Calls

        //private HttpWebResponse GetDataFromAPI(string strAPIMethod, string strAPIInput = "", string strMethod = "GET")
        //{
        //    HttpWebResponse webResponse = null;
        //    try
        //    {
        //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);
        //        httpWebRequest.ContentType = "application/json";
        //        httpWebRequest.Method = strMethod;
        //        if (strMethod.ToLower().Equals("post"))
        //            httpWebRequest.ContentLength = 0;
        //        httpWebRequest.Timeout = 200000;
        //        if (!string.IsNullOrEmpty(strAPIInput))
        //        {
        //            if (strAPIInput.Contains('^'))
        //            {
        //                foreach (string strHeaders in strAPIInput.Split('^'))
        //                {
        //                    if (strHeaders.Contains('|'))
        //                    {
        //                        string[] strHeader = strHeaders.Split('|');
        //                        httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
        //                    }
        //                }
        //            }
        //            else if (strAPIInput.Contains('|'))
        //            {
        //                string[] strHeader = strAPIInput.Split('|');
        //                httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
        //            }
        //        }
        //        webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //    }
        //    catch (WebException wex)
        //    {
        //        Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationController :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") / Header Input(" + strAPIInput + ") -> APIException : " + wex.ToString(), this);
        //    }
        //    catch (Exception ex)
        //    {
        //        Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationController :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") / Header Input(" + strAPIInput + ") -> Exception : " + ex.ToString(), this);
        //    }
        //    return webResponse;
        //}

        //private HttpWebResponse PostDataToAPI(string strAPIMethod, string strAPIInput)
        //{
        //    HttpWebResponse webResponse = null;
        //    try
        //    {
        //        //Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationController :: PostDataToAPI(" + strAPIMethod + ") -> JSON DATA : " + strBody, strBody);
        //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);
        //        httpWebRequest.ContentType = "application/json";
        //        httpWebRequest.Method = "POST";
        //        httpWebRequest.Timeout = 200000;
        //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //        {
        //            streamWriter.Write(strAPIInput);
        //            streamWriter.Flush();
        //            streamWriter.Close();
        //        }
        //        webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //    }
        //    catch (WebException wex)
        //    {
        //        Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationController :: PostDataToAPI(" + strAPIMethod + ") -> APIException : " + wex.ToString(), this);
        //    }
        //    catch (Exception ex)
        //    {
        //        Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationController :: PostDataToAPI(" + strAPIMethod + ") -> Exception : " + ex.ToString(), this);
        //    }
        //    return webResponse;
        //}

        #endregion

        #region State and Specialty Dropdown Options
        //private IEnumerable<string> GetAllStates()
        //{
        //    return new List<string>
        //    {
        //        "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC",
        //        "DE", "FL", "GA", "HI", "IA", "ID", "IL", "IN",
        //        "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN",
        //        "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ",
        //        "NM", "NV", "NY", "OH", "OK", "OR", "PA", "RI",
        //        "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA",
        //        "WI", "WV", "WY"
        //    };
        //}

        //MSO Verification Status DropDown
        public static IEnumerable<string> MSOVerificationStatus(BPPApplication _bppApplication)
        {
            List<MSOVerificationStatu> _MSOVerificationStatus = new List<MSOVerificationStatu>();
            _MSOVerificationStatus = _bppApplication._MsoDD;

            List<string> MSOddl = new List<string>();
            foreach (var item in _MSOVerificationStatus)
            {
                MSOddl.Add(item.StatusName);
            }
            return MSOddl;
        }

        //MQ Recommendation Status DropDown
        public static IEnumerable<string> MQRecStatus(BPPApplication _bppApplication)
        {
            List<MQRecommendationStatu> _MQRecommendationStatus = new List<MQRecommendationStatu>();
            _MQRecommendationStatus = _bppApplication._MqRecommendDD;

            List<string> _MQRecDD = new List<string>();
            foreach (var item in _MQRecommendationStatus)
            {
                _MQRecDD.Add(item.StatusName);
            }
            return _MQRecDD;
        }

        //Board Outcome Status DropDown
        public static IEnumerable<string> BoardOutcome(BPPApplication _bppApplication)
        {
            List<BoardOutcomeStatu> _BoardOutcomeDD = new List<BoardOutcomeStatu>();
            _BoardOutcomeDD = _bppApplication.BoardOutcomeDD;

            List<string> _BoardOutcomeddl = new List<string>();
            foreach (var item in _BoardOutcomeDD)
            {
                _BoardOutcomeddl.Add(item.StatusName);
            }
            return _BoardOutcomeddl;
        }

        //Denial Reason Status DropDown
        public static IEnumerable<string> DenialReasonStatus(BPPApplication _bppApplication)
        {
            List<DenialReasonStatu> denialReasonStatus = new List<DenialReasonStatu>();
            denialReasonStatus = _bppApplication.DenialReasonDD;

            List<string> _denials = new List<string>();
            foreach (var item in denialReasonStatus)
            {
                _denials.Add(item.StatusName);
            }
            return _denials;
        }

        //Provider Member Status DropDown
        public static IEnumerable<string> ProviderMemberStatus(BPPApplication _bppApplication)
        {
            List<ProviderMemberStatu> providerMemberStatus = new List<ProviderMemberStatu>();
            providerMemberStatus = _bppApplication.ProviderMemberStatusDD;

            List<string> _providerMemberStatus = new List<string>();
            foreach (var item in providerMemberStatus)
            {
                _providerMemberStatus.Add(item.MemberStatus);
            }
            return _providerMemberStatus;
        }

        //Inactive Reasons Status DropDown
        public static IEnumerable<string> InactiveReasonStatus(BPPApplication _bppApplication)
        {
            List<InactiveReasonStatu> inactiveReasonsStatus = new List<InactiveReasonStatu>();
            inactiveReasonsStatus = _bppApplication.InactiveReasonStatusDD;

            List<string> _inactivereasons = new List<string>();
            foreach (var item in inactiveReasonsStatus)
            {
                _inactivereasons.Add(item.StatusName);
            }
            return _inactivereasons;
        }

        public static IEnumerable<string> GetAllSpecialties()
        {
            List<BPPSpecialties> specialties = new List<BPPSpecialties>();
            ApplicationController ac = new ApplicationController();
            specialties = ac.GetSpecialties();

            List<string> specList = new List<string>();
            foreach (var item in specialties)
            {
                specList.Add(item.Specialty);
            }
            return specList;

        }
        public static IEnumerable<string> GetAllDegrees()
        {
            List<BPPDegrees> degrees = new List<BPPDegrees>();
            ApplicationController ac = new ApplicationController();
            degrees = ac.GetDegrees();

            List<string> Degrees = new List<string>();
            foreach (var item in degrees)
            {
                Degrees.Add(item.Degree);
            }
            return Degrees;

        }
        [HttpGet]
        public JsonResult getTaxId(string practiceName)
        {
            ApplicationController ac = new ApplicationController();
            return Json(new { data = ac.GetTaxId(practiceName) }, JsonRequestBehavior.AllowGet);
        }
        public static List<BPPPractices> GetAllPractices()
        {
            ApplicationController ac = new ApplicationController();
            return ac.GetPractices();
        }

        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="Specialty Name">Specialty Name</option>
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element,
                    Text = element
                });
            }

            return selectList;
        }
        private List<SelectListItem> GetSelectSpecialties(List<BPPSpecialties> specialties)
        {
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="Specialty Name">Specialty Name</option>
            foreach (var element in specialties)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element.Specialty,
                    Text = element.Specialty
                });
            }

            return selectList;
        }

        #endregion

        #region Test Methods - Local JSON Parsing To be deleted once all API's are ready

        private string testApplicantsJsonData()
        {
            string jSonData = "";
            List<BPPApplicationList> _bppApplList = new List<BPPApplicationList>();
            using (var reader = new StreamReader("C:\\test_applicationlist_json.txt"))
            {
                var objText = reader.ReadToEnd();
                JObject data = JObject.Parse(objText);
                _bppApplList = data["_Providers"].Select(c => c.ToObject<BPPApplicationList>()).ToList();
                jSonData = Newtonsoft.Json.JsonConvert.SerializeObject(_bppApplList);
            }
            return jSonData;
        }

        private BPPApplication testProviderApplicationJsonData()
        {
            BPPApplication _bppApplication = new BPPApplication();
            try
            {
                using (var reader = new StreamReader("C:\\EB\\Projects\\Baptist\\BPP\\BPP JSON\\test_practice_json.txt"))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objText = reader.ReadToEnd();
                    _bppApplication = (BPPApplication)js.Deserialize(objText, typeof(BPPApplication));
                    if (_bppApplication != null && _bppApplication._Practices != null && _bppApplication._Practices[0].PracticeLocations != null)
                    {
                        _bppApplication._Practices[0].PracticeLocations = _bppApplication._Practices[0].PracticeLocations.OrderByDescending(loc => loc.IsPrimary).ToList();
                        if (_bppApplication._Providers != null)
                        {

                        }

                    }
                    //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: testProviderApplicationJsonData -> _bppApplication Before Finding Primary Location " + JsonConvert.SerializeObject(_bppAppDetails), _bppAppDetails);
                    //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: testProviderApplicationJsonData -> _bppAppDetails " + JsonConvert.SerializeObject(_bppAppDetails), _bppAppDetails);
                }
            }
            catch (Exception ex)
            {
                _bppApplication = new BPPApplication();
                _bppApplication._Practices = new List<BPPPractice>();
                _bppApplication._Practices[0].PracticeLocations = new List<BPPPracticeLocation>();
                _bppApplication._Providers = new List<BPPProvider>();
                Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationController :: testProviderApplicationJsonData -> Exception : " + ex.ToString(), this);
            }
            //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationController :: testProviderApplicationJsonData -> _bppAppDetails: " + JsonConvert.SerializeObject(_bppAppDetails), _bppAppDetails);
            return _bppApplication;
        }

        #endregion

    }
}
