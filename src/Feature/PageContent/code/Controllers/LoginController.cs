﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BHSearch;
using BHUser.Components;
using BPP93Helix.Feature.PageContent.Models;
using System.Web.Security;
using Sitecore.Security;
using Sitecore.Security.Accounts;
using Sitecore.Security.Authentication;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using Microsoft.Owin;
using Sitecore;
using Sitecore.Abstractions;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.GetSignInUrlInfo;
//using LightLDAP.ActiveDirectory;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            if (Sitecore.Context.User.IsAuthenticated)
            {
                return Redirect("~/Home/");
            }
            else
                return View();
        }

        public ActionResult MiniLogin()
        {
            return PartialView();
        }
        public ActionResult AccountProfile()
        {
            var user = User.Identity.Name;
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain,"BH");
            UserPrincipal u = UserPrincipal.FindByIdentity(ctx, user);
            string drNumb = "";
            try
            {
                if (u.GetUnderlyingObjectType() == typeof(DirectoryEntry))
                {
                    // Transition to directory entry to get other properties
                    using (var entry = (DirectoryEntry)u.GetUnderlyingObject())
                    {
                        if (entry.Properties["ExtensionAttribute4"] != null)
                            drNumb = entry.Properties["ExtensionAttribute4"].Value.ToString();
                    }
                }

            } catch (Exception) { }

            return Redirect("/sitecore/content/Search/Providers/" + drNumb.Trim());
        }
    }
}
