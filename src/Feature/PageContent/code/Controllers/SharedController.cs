﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore;
using Sitecore.Mvc.Presentation;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using BHUser.Components;
using BHUser.Models;
using BPP93Helix.Feature.PageContent.Models;
using BPP93Helix.Feature.PageContent.Helper;
using System.Web.Script.Serialization;
using Sitecore.Links;
using Sitecore.Diagnostics;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class SharedController : Controller
    {
        // GET: Shared
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult header()
        {
            //Sitecore.Diagnostics.Log.Info("Shared Header : Loading Dynamic Header.", this);
            //bool isUserLoggedIn = false;
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            try
            {
                User _bppUser = BHSitecore.GetBHUser();

                BppHeader _bppHeader = new BppHeader(); // this is my header object
                _bppHeader.nav = new List<BPPNav>();    // this is my navigation object
                _bppHeader.menu = new BPPMenu();
                _bppHeader.menu.MenuItems = new List<BPPMenuItem>();

                ID navItemId = new ID("204AA71D-2CF4-4CA6-A392-ED284A7C044B");
                Item headerNavItems = Context.Database.GetItem(navItemId);

                ID menuItemId = new ID("F12FAE1D-B4B0-4794-9C3F-204D0E5B3E63");
                Item HeaderMenuItem = Context.Database.GetItem(menuItemId);  // ("/sitecore/content/Components/Header Control/Header");

                ImageField imageField = HeaderMenuItem.Fields["Logo"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    MediaItem siteLogo = new MediaItem(imageField.MediaItem);
                    _bppHeader.menu.SiteLogoURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(siteLogo));
                    _bppHeader.menu.SiteLogo = siteLogo.Alt;
                }
                MultilistField headerMenuList = null;

                ID navList = null;
                string strHeaderDetails = BHSitecore.headerDetails();
                headerMenuList = HeaderMenuItem.Fields[strHeaderDetails.Split('|')[0]];
                navList = new ID(strHeaderDetails.Split('|')[1]);

                #region ///Build Menu Based on Authentication
                if (headerMenuList != null)
                {
                    foreach (Item headerMenuLink in headerMenuList.GetItems())
                    {
                        LinkField unAuthLink = headerMenuLink.Fields["Link"];
                        string url = "#";
                        if (unAuthLink != null)
                        {
                            url = unAuthLink.GetFriendlyUrl();
                            url = BHSitecore.parseGeneralLink(unAuthLink);
                        }
                        ImageField unAuthLogo = headerMenuLink.Fields["Icon"];
                        MediaItem unAuthLogoPath = null;
                        if (unAuthLogo != null)
                        {
                            unAuthLogoPath = new MediaItem(unAuthLogo.MediaItem);
                        }
                        _bppHeader.menu.MenuItems.Add(new BPPMenuItem
                        {
                            MenuItemLogo = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(unAuthLogoPath)),
                            MenuItemText = headerMenuLink.DisplayName,
                            MenuItemURL = url
                        });
                    }
                }
                #endregion

                #region ///Build Navigation Menu Based on Authorization
                Item headerNavList = Sitecore.Context.Database.GetItem(navList);
                MultilistField headerNavMulList = null;
                headerNavMulList = headerNavList.Fields["Navigation Links"];

                foreach (Item parentNavItem in headerNavMulList.GetItems())
                {
                    List<Item> childItems = null;
                    childItems = parentNavItem.GetChildren().ToList();
                    List<BPPNavLinks> _childNavItem = new List<BPPNavLinks>();
                    foreach (Item child in childItems)
                    {
                        _childNavItem.Add(new BPPNavLinks
                        {
                            NavLinkText = child.DisplayName,
                            //NavLinkURL = child.Paths.Path
                            NavLinkURL = LinkManager.GetItemUrl(child, defaultUrlOptions)
                        });
                    }
                    if (parentNavItem.ID.Guid.ToString().ToUpper().Equals("736FCC62-711A-4A2A-8D45-4C55B8A1331C"))
                        _bppHeader.nav.Add(new BPPNav
                        {
                            NavItemText = parentNavItem.DisplayName,
                            //NavItemURL = parentNavItem.Children[0].Paths.Path,
                            NavItemURL = LinkManager.GetItemUrl(parentNavItem.Children[0], defaultUrlOptions),
                            NavLinks = _childNavItem
                        });
                    else
                        _bppHeader.nav.Add(new BPPNav
                        {
                            NavItemText = parentNavItem.DisplayName,
                            //NavItemURL = parentNavItem.Paths.Path,
                            NavItemURL = LinkManager.GetItemUrl(parentNavItem, defaultUrlOptions),
                            NavLinks = _childNavItem
                        });
                }
                #endregion
                return PartialView(_bppHeader);
            }
            catch (Exception ex)
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers.SharedController :: Shared Header Exception : " + ex.ToString(), this);
                return null;
            }
        }

        public ActionResult footer()
        {
            return PartialView();
        }

        public ActionResult breadcrumb()
        {
            //Remember to set the correct Home Item ID in the GetBreadcrumbs method definition
            var breadcrumb = BHSitecore.GetBreadcrumb(Context.Item);
            if (breadcrumb != null)
                return PartialView(breadcrumb);
            else
                return null;
        }

        public ActionResult logout()
        {
            Sitecore.Security.Authentication.AuthenticationManager.Logout();
            //System.Web.Security.FormsAuthentication.SignOut();
            return Redirect("/Home");
        }
    }
}