﻿using BPP93Helix.Feature.PageContent.Helper;
using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class ValidationController : Controller
    {
        private static readonly string spAPIValidateTINReturning = Settings.GetSetting("spAPIValidateTINReturning");
        private static readonly string spAPIValidateTINApplication = Settings.GetSetting("spAPIValidateTINApplication");
        private static readonly string spAPIValidateNPI = Settings.GetSetting("spAPIValidateNPI");
        private static readonly string spAPIUniqueTIN = Settings.GetSetting("spAPIUniqueTIN");
        private static readonly string spAPIValidateLocationName = Settings.GetSetting("spAPIValidateLocationName");
        [HttpPost]
        public ActionResult ValidateTINReturning(string TIN)
        {
          string status = null;
            var returnList = new List<string>();
            try
            {
              var httpWebResponse = API.GetDataFromAPI(spAPIValidateTINReturning, "TIN|" + TIN);
              if (httpWebResponse != null)
              {
                if (httpWebResponse.StatusCode.ToString() != "OK")
                {
                  Log.Error("BPP93Helix.Feature.PageContent.Controllers.ValidateController :: ValidateTIN -> Bad Request : " + httpWebResponse.StatusDescription, this);
                  status = "error";
                }
                else
                {
                  status = "success";
                }
              }
              else
              {
                status = "error";
              }
            }
            catch (Exception)
            {
                status = "error";
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public ActionResult ValidateTINApplication(string tin)
        {
          string status = null;
            var returnList = new List<string>();
            try
            {
              var httpWebResponse = API.GetDataFromAPI(spAPIValidateTINApplication, "TIN|" + tin);
              if (httpWebResponse != null)
              {
                if (httpWebResponse.StatusCode.ToString() != "OK")
                {
                  Log.Error("BPP93Helix.Feature.PageContent.Controllers.ValidateController :: IsUniqueTIN -> Bad Request : " + httpWebResponse.StatusDescription, this);
                  status = "error";
                }
                else
                {
                  status = "success";
                }
              }
              else
              {
                status = "error";
              }
            }
            catch (Exception)
            {
                status = "error";
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }


        [HttpPost]
        public ActionResult IsUniqueTIN(string tin)
        {
          string status = null;
            var returnList = new List<string>();
            try
            {
              var httpWebResponse = API.GetDataFromAPI(spAPIUniqueTIN, "TIN|" + tin);
              if (httpWebResponse != null)
              {
                if (httpWebResponse.StatusCode.ToString() != "OK")
                {
                  Log.Error("BPP93Helix.Feature.PageContent.Controllers.ValidateController :: IsUniqueTIN -> Bad Request : " + httpWebResponse.StatusDescription, this);
                  status = "error";
                }
                else
                {
                  status = "success";
                }
              }
              else
              {
                status = "error";
              }
            }
            catch (Exception)
            {
                status = "error";
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public ActionResult ValidateNPI(string ProviderNPI, string TaxID)
        {
          string status = null;
            var returnList = new List<string>();
            try
            {
              var httpWebResponse = API.GetDataFromAPI(spAPIValidateNPI, "ProviderNPI|" + ProviderNPI + "^TaxID|" + TaxID);
              if (httpWebResponse != null)
              {
                if (httpWebResponse.StatusCode.ToString() != "OK")
                {
                  Log.Error("BPP93Helix.Feature.PageContent.Controllers.ValidateController :: ValidateNPI -> Bad Request : " + httpWebResponse.StatusDescription, this);
                  status = "error";
                }
                else
                {
                  status = "success";
                }
              }
              else
              {
                status = "error";
              }
            }
            catch (Exception)
            {
                status = "error";
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public ActionResult ValidateLocationName(string locationName, string practiceGuid)
        {
          string status = null;
            var returnList = new List<string>();
            try
            {
              var httpWebResponse = API.GetDataFromAPI(spAPIValidateLocationName, "LocationName|" + locationName + "^PracticeGuid|" + practiceGuid);
              if (httpWebResponse != null)
              {
                if (httpWebResponse.StatusCode.ToString() != "OK")
                {
                  Log.Error("BPP93Helix.Feature.PageContent.Controllers.ValidateController :: ValidateLocationName -> Bad Request : " + httpWebResponse.StatusDescription, this);
                  status = "error";
                }
                else
                {
                  status = "success";
                }
              }
              else
              {
                status = "error";
              }
            }
            catch (Exception)
            {
                status = "error";
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }

        public async Task<ActionResult> ValidateNPIAgaintRegistry(string number)
        {
            string status = null;
            var returnList = new List<string>();
            try
            {
                var npiValidator = new NPIValidator();

                var _npi = number;
        //https://npiregistry.cms.hhs.gov/api/?number=1093795437&version=2.1

        //var apiUrl = "https://npiregistry.cms.hhs.gov/api/?number=" + npi + "&version=2.1";
        string apiUrl = "https://npiregistry.cms.hhs.gov/api/?version=2.1&number=" + _npi;
        using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    var response = await client.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        Log.Info($"Validation :: ValidateNPIAgaintRegistry({number}) :: Data Returned {data}", this);
                        //npiValidator = JsonConvert.DeserializeObject<NPIValidator>(data);
                        if (data.Contains("Errors"))
                        {
                          status = "error";
                          Log.Error($"Validation :: ValidateNPIAgaintRegistry({number}) returned error from api", this);
                        }
                        if (data.Contains(@"{""resultCount"":0,""results"":[]}") || data.Contains(@"{""result_count"":0,""results"":[]}"))
                        {
                          status = "error";
                          Log.Error($"Validation :: ValidateNPIAgaintRegistry({number}) no results returned", this);
                        }
                        else if(data.Contains(@"{""resultCount"":1") || data.Contains(@"{""result_count"":1"))
                        {
                          status = "success";
                          Log.Info($"Validation :: ValidateNPIAgaintRegistry({number}) :: Results returned ", this);
                        }
                        //if (npiValidator != null)
                        //{
                        //    Log.Info($"Validation :: ValidateNPIAgaintRegistry({number}) :: npiValidate is not null", this);
                        //    if (npiValidator.results[0].basic.name != null || npiValidator.results[0].basic.name != "")
                        //    {
                        //        status = "success";
                        //    }
                        //    else
                        //    {
                        //        status = "error";
                        //        Log.Error($"Validation :: ValidateNPIAgaintRegistry({number}) :: {npiValidator.results[0].basic.name}", this);
                        //    }
                        //}
                        //else
                        //{
                        //    status = "error";
                        //    Log.Error($"Validation :: ValidateNPIAgaintRegistry({number}) :: npiValidator is null", this);
                        //}
                    }
                    else
                    {
                        status = "error";
                        Log.Error($"Validation :: ValidateNPIAgaintRegistry({number}) :: Error returned from {apiUrl} :: {response.StatusCode} :: {response.Content}", this);
                    }
                }
            }
            catch (Exception e)
            {
                status = "error";
                Log.Error($"Exception :: Validation :: ValidateNPIAgaintRegistry({number}) :: {e.Message}", this);
            }
            returnList.Add(status);
            return Json(returnList, JsonRequestBehavior.DenyGet);
        }
    }
}