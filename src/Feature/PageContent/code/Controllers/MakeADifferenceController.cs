﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BPP93Helix.Feature.PageContent.Models;
using Sitecore.Links;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class MakeADifferenceController : Controller
    {
        // GET: MakeADifference
        public ActionResult Index()
        {
            var items = Sitecore.Context.Database.SelectItems("/sitecore/content/components/make a difference/*");
            items = items.OrderBy(x => x[Sitecore.FieldIDs.Created]).Reverse().Take(6).ToArray();
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            
            var mad = ( from i in items.OrderBy(x => x[Sitecore.FieldIDs.Created]).Reverse().Take(6)
            select new MakeADifference{
                Title = i.Fields["Title"].Value,
                Heading = i.Fields["Heading"].Value,
                Body = i.Fields["Body"].Value,                
                ImgField = ((Sitecore.Data.Fields.ImageField)i.Fields["Main Image"]),
                ItemPath = LinkManager.GetItemUrl(i, defaultUrlOptions)
            }).ToList();

            MakeADiffItems m = new MakeADiffItems();
            m._Items = mad;

            return View(m);
        }

        public ActionResult Details()
        {
            return View();
        }
    }
}