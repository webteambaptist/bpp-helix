﻿using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json.Linq;
using Sitecore.Diagnostics;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace BPP93Helix.Feature.PageContent.Controllers
{
    public class MemberController : Controller
    {
        private HttpClient client;
        private static readonly string spAPIBaseAddress = Settings.GetSetting("spAPIBaseAddress");
        private static readonly string spAPIGetMemberList = Settings.GetSetting("spAPIGetMemberList");

        //public MemberController()
        //{
        //    client = new HttpClient
        //    {
        //        BaseAddress = new Uri("http://bpp.local/api/practiceappdata/ipad")
        //    };
        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //}
        // GET: Member
        public ActionResult Index()
        {
            try
            {
                Log.Info("BPP93Helix.Feature.PageContent.Controllers :: MemberController -> Index Called.", this);
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers :: MemberController -> Index Exception." + ex.ToString(), this);
                throw;
            }
            return View();
        }

        [HttpGet]
        public ActionResult ManageMembers(string GUID = "")
        {
            if (BHUser.Components.Login.IfAdminLoggedIn())
            {
                var jSonData = "";
                HttpWebResponse httpWebResponse = null;
                try
                {
                    List<BPPMemberList> _bppMemberList = new List<BPPMemberList>();
                    httpWebResponse = GetDataFromAPI(spAPIGetMemberList, (!string.IsNullOrEmpty(GUID) ? "GUID|" + GUID : ""));
                    if (httpWebResponse != null)
                    {
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.MemberController :: ManageMembers -> API Response : " + httpWebResponse, httpWebResponse);
                        Log.Info("BPP93Helix.Feature.PageContent.Controllers.MemberController :: ManageMembers -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var objText = reader.ReadToEnd();
                            JObject data = JObject.Parse(objText);

                            _bppMemberList = data["_ProvidersShort"].Select(c => c.ToObject<BPPMemberList>()).ToList();
                            jSonData = Newtonsoft.Json.JsonConvert.SerializeObject(_bppMemberList);
                        }
                    }
                    else
                    {
                        Log.Error("BPP93Helix.Feature.PageContent.Controllers.MemberController :: ManageMembers -> httpWebResponse Null Exception : ", this);
                        jSonData = "";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("BPP93Helix.Feature.PageContent.Controllers.MemberController :: ManageMembers -> Exception : " + ex.ToString(), this);
                    jSonData = "";
                }
                var jsonResult = Json(new { data = jSonData }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                if (BHUser.Components.Login.IfUserLoggedIn())
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.MemberController :: ManageMembers -> Unauthorized user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-access" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Log.Info("BPP93Helix.Feature.PageContent.Controllers.MemberController :: ManageMembers -> Anonymous user access attempt: " + DateTime.Now.ToString(), DateTime.Now);
                    return Json(new { data = "no-login" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        private HttpWebResponse GetDataFromAPI(string strAPIMethod, string strAPIInput = "", string strMethod = "GET")
        {
            HttpWebResponse webResponse = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = strMethod;
                if (strMethod.ToLower().Equals("post"))
                    httpWebRequest.ContentLength = 0;
                httpWebRequest.Timeout = 20000000;
                if (!string.IsNullOrEmpty(strAPIInput))
                {
                    if (strAPIInput.Contains('^'))
                    {
                        foreach (string strHeaders in strAPIInput.Split('^'))
                        {
                            if (strHeaders.Contains('|'))
                            {
                                string[] strHeader = strHeaders.Split('|');
                                httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                            }
                        }
                    }
                    else if (strAPIInput.Contains('|'))
                    {
                        string[] strHeader = strAPIInput.Split('|');
                        httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                    }
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.MemberController :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") / Header Input(" + strAPIInput + ") -> APIException : " + wex.ToString(), this);
            }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.MemberController :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") / Header Input(" + strAPIInput + ") -> Exception : " + ex.ToString(), this);
            }
            return webResponse;
        }

        private HttpWebResponse PostDataToAPI(string strAPIMethod, string strAPIInput)
        {
            HttpWebResponse webResponse = null;
            try
            {
                //Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationController :: PostDataToAPI(" + strAPIMethod + ") -> JSON DATA : " + strBody, strBody);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 20000;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(strAPIInput);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.MemberController :: PostDataToAPI(" + strAPIMethod + ") -> APIException : " + wex.ToString(), this); }
            catch (Exception ex)
            {
                Log.Error("BPP93Helix.Feature.PageContent.Controllers.MemberController :: PostDataToAPI(" + strAPIMethod + ") -> Exception : " + ex.ToString(), this);
            }
            return webResponse;
        }
    }
    
}