﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Configuration;

namespace BPP93Helix.Feature.PageContent.Pipelines.initialize
{
    using Sitecore.Diagnostics;
    using Sitecore.Pipelines;
    using SolrNet;
    using System.Collections.Generic;
    using System.Configuration;

    // TODO: \App_Config\include\InitConnection.config created automatically when creating InitConnection class.

    public class InitConnection
    {
        public void Process(PipelineArgs args)
        {
            Log.Info("Starting up Solr Connection is starting", this);
            var solrConnection = Settings.GetSetting("solrConnection");
            //SolrNet.Startup.Init<Dictionary<string, object>>(solrConnection);
            // SolrNet.SolrNet.GetBasicServer<Dictionary<string, object>>(solrConnection);
            Startup.Init<Dictionary<string, object>>(solrConnection);
        }
    }
}