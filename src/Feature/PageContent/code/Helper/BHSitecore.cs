﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Mvc.Presentation;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Links;
using Sitecore.Resources.Media;
using Sitecore.Security.Accounts;
using BHUser.Models;
using BPP93Helix.Feature.PageContent.Models;
using System.Reflection;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace BPP93Helix.Feature.PageContent.Helper
{
    public static class BHSitecore
    {
        public static string parseGeneralLink(LinkField lf)
        {
            switch (lf.LinkType.ToLower())
            {
                case "internal":
                    // Use LinkMananger for internal links, if link is not empty
                    return lf.TargetItem != null ? LinkManager.GetItemUrl(lf.TargetItem) : string.Empty;
                case "media":
                    // Use MediaManager for media links, if link is not empty
                    return lf.TargetItem != null ? MediaManager.GetMediaUrl(lf.TargetItem) : string.Empty;
                case "external":
                    // Just return external links
                    return lf.Url;
                case "anchor":
                    // Prefix anchor link with # if link if not empty
                    return !string.IsNullOrEmpty(lf.Anchor) ? "#" + lf.Anchor : string.Empty;
                case "mailto":
                    // Just return mailto link
                    return lf.Url;
                case "javascript":
                    // Just return javascript
                    return lf.Url;
                default:
                    // Just please the compiler, this
                    // condition will never be met
                    return lf.Url;
            }
        }

        public static string GetValueFromCurrentRenderingParameters(string parameterName)
        {
            var rc = RenderingContext.CurrentOrNull;
            if (rc == null || rc.Rendering == null) return (string)null;
            var parametersAsString = rc.Rendering.Properties["Parameters"];
            var parameters = HttpUtility.ParseQueryString(parametersAsString);
            return parameters[parameterName];
        }

        public static string[] GetValuesFromCurrentRenderingParameters(string parameterName)
        {
            List<string> result = new List<string>();
            var rc = RenderingContext.CurrentOrNull;
            if (rc != null)
            {
                var itemIds = GetValueFromCurrentRenderingParameters(parameterName)
                    ?? string.Empty;
                var db = rc.ContextItem.Database;

                var selectedItemIds = itemIds.Split('|');
                foreach (var itemId in selectedItemIds)
                {
                    Guid id = Guid.Empty;
                    if (Guid.TryParse(itemId, out id))
                    {
                        var found = db.GetItem(new ID(id));
                        if (found != null)
                        {
                            result.Add(found.Name);
                        }
                    }
                }
            }
            return result.ToArray();
        }

        public static Item[] GetItemsFromCurrentRenderingParameters(string parameterName)
        {
            List<Item> result = new List<Item>();
            var rc = RenderingContext.CurrentOrNull;
            if (rc != null)
            {
                var itemIds = GetValueFromCurrentRenderingParameters(parameterName)
                    ?? string.Empty;
                var db = rc.ContextItem.Database;

                var selectedItemIds = itemIds.Split('|');
                foreach (var itemId in selectedItemIds)
                {
                    Guid id = Guid.Empty;
                    if (Guid.TryParse(itemId, out id))
                    {
                        var found = db.GetItem(new ID(id));
                        if (found != null)
                        {
                            result.Add(found);
                        }
                    }
                }
            }
            return result.ToArray();
        }

        public static Item[] GetMultiListItems(Item item, ID fieldID)
        {
            return (new MultilistField(item.Fields[fieldID])).GetItems();
        }

        public static string[] GetValuesFromMultiListField(MultilistField multiField)
        {
            string[] values = new string[multiField.Count];
            int i = 0;
            foreach (Item item in multiField.GetItems())
            {
                values[i] = item.Name;
                i++;
            }
            return values;
        }

        public static string[] GetRoleNamesForCurrentUser()
        {
            string[] roles = new string[Sitecore.Context.User.Roles.Count];
            int i = 0;
            foreach (var role in Sitecore.Context.User.Roles)
            {
                roles[i] = role.Name;
                i++;
            }
            return roles;
        }

        public static BHUser.Models.User GetBHUser()
        {
            BHUser.Models.User bhUser = new BHUser.Models.User();

            Sitecore.Security.Accounts.User sitecoreUser = Sitecore.Context.User;

            bhUser.Email = sitecoreUser.Profile.Email;

            bhUser.UserType = BH_User_Types.Public;
            foreach (var role in sitecoreUser.Roles)
            {
                if (role.Name == "ad\\BPP Provider")
                {
                    bhUser.UserType = BH_User_Types.Provider;
                }
                if (role.Name == "ad\\BPP Admin")
                {
                    bhUser.UserType = BH_User_Types.Admin;
                }
            }

            return bhUser;
        }

        public static string headerDetails()
        {
            //string headerType = "UnAuthenticated|EDB5D75B-9C34-4207-BFF3-A863E67F7BF9";
            string headerType = "";
            try
            {
                headerType = BHUser.Components.Login.IfUserLoggedIn() ? "Authenticated|" : "UnAuthenticated|";

                if (Sitecore.Security.Accounts.User.Current.IsInRole(Sitecore.Security.Accounts.Role.FromName("ad\\BPP Admin")))
                {
                    //headerType = "Authenticated|28618B87-6F50-458E-8D07-1E5DF3BC5894";
                    headerType += "28618B87-6F50-458E-8D07-1E5DF3BC5894";
                }
                else if (Sitecore.Security.Accounts.User.Current.IsInRole(Sitecore.Security.Accounts.Role.FromName("ad\\BPP Provider")))
                {
                    //headerType = "Authenticated|174BBAB3-0BF2-4C67-A758-9FD19906B18A";
                    headerType += "174BBAB3-0BF2-4C67-A758-9FD19906B18A";
                }
                else
                {
                    //headerType = "UnAuthenticated|EDB5D75B-9C34-4207-BFF3-A863E67F7BF9";
                    headerType += "EDB5D75B-9C34-4207-BFF3-A863E67F7BF9";
                }

            }
            catch (Exception)
            {
                headerType = "UnAuthenticated|EDB5D75B-9C34-4207-BFF3-A863E67F7BF9";
            }

            return headerType;
        }

        public static List<BPPProvider> CustomParseProviders(List<BPPProvider> _bppProviders)
        {
            return _bppProviders.Select(
                c =>
                {
                    c.FullNameWithSuffix = c.FirstName +
                    (!string.IsNullOrEmpty(c.MiddleName) ? " " + c.MiddleName + ". " : " ") +
                    c.LastName +
                    (!string.IsNullOrEmpty(c.Suffix) ? ", " + c.Suffix.ToUpper() : "");
                    return c;
                }).ToList();
        }
        public static List<BPPPracticeLocation> CustomParseLocation(List<BPPPracticeLocation> _bppLocations)
        {
            return _bppLocations.Select(
                c =>
                {
                    c.FullAddress = c.Address1 +
                    (!string.IsNullOrEmpty(c.Address2) ? " " + c.Address2 : "") +
                    (!string.IsNullOrEmpty(c.City) ? ", " + c.City : "") +
                    (!string.IsNullOrEmpty(c.State) ? ", " + c.State.ToUpper() : "") +
                    (!string.IsNullOrEmpty(c.Zip) ? ", " + c.Zip : "");
                    return c;
                }).ToList();
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static MvcHtmlString EnumToJsonString<T>(this HtmlHelper helper)
        {
            var values = Enum.GetValues(typeof(T)).Cast<int>();
            var enumDictionary = values.ToDictionary(value => Enum.GetName(typeof(T), value));

            return new MvcHtmlString(JsonConvert.SerializeObject(enumDictionary));
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static BPPBreadcrumb GetBreadcrumb(Item current)
        {
            BPPBreadcrumb breadcrumb = new BPPBreadcrumb();
            breadcrumb.Crumbtrail = new List<Crumb>();
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            if (current != null)
            {
                Sitecore.Data.Fields.CheckboxField showBreadCrumb = current.Fields["Show Breadcrumb"];
                if (showBreadCrumb != null && showBreadCrumb.Checked)
                {
                    ID homeID = new ID("110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9");
                    //Item homeItem = Sitecore.Context.Database.GetItem(homeID);


                    while (current != null)
                    {
                        //Additional logic may be wanted here to filter items based on template ID etc

                        Crumb crumb = new Crumb();
                        crumb.Text = current.Name;
                        //crumb.Url = current.Paths.Path;
                        crumb.Url = LinkManager.GetItemUrl(current, defaultUrlOptions);

                        ID defaultFolderID = new ID("A87A00B1-E6DB-45AB-8B54-636FEC3B5523");

                        if (current.TemplateID == defaultFolderID)
                        {
                            crumb.Type = "label";
                        }
                        else
                        {
                            crumb.Type = "link";
                        }

                        breadcrumb.Crumbtrail.Add(crumb);

                        if (current.ID == homeID)
                        {
                            break;
                        }
                        else
                        {
                            current = current.Parent;
                        }
                    }

                    breadcrumb.Crumbtrail.Reverse();
                }
                else
                {
                    breadcrumb = null;
                }
            }
            else
            {
                breadcrumb = null;
            }
            //Sitecore.Diagnostics.Log.Info("BPP.Helper.BHSitecore :: GetBreadcrumb -> " + Newtonsoft.Json.JsonConvert.SerializeObject(breadcrumb), breadcrumb);
            return breadcrumb;
        }
    }
}