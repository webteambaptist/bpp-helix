﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Sitecore.XConnect;

namespace BPP93Helix.Feature.PageContent.Helper
{
    public class API
    {
        public static string spAPIBaseAddress = Settings.GetSetting("spAPIBaseAddress");
        public static HttpWebResponse GetDataFromAPI(string strAPIMethod, string strAPIInput = "", string strMethod = "GET")
        {
            HttpWebResponse webResponse = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = strMethod;
                if (strMethod.ToLower().Equals("post"))
                    httpWebRequest.ContentLength = 0;
                httpWebRequest.Timeout = 200000000;
                if (!string.IsNullOrEmpty(strAPIInput))
                {
                    if (strAPIInput.Contains('^'))
                    {
                        foreach (string strHeaders in strAPIInput.Split('^'))
                        {
                            if (strHeaders.Contains('|'))
                            {
                                string[] strHeader = strHeaders.Split('|');
                                httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                            }
                        }
                    }
                    else if (strAPIInput.Contains('|'))
                    {
                        string[] strHeader = strAPIInput.Split('|');
                        httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                    }
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Helpers.API :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") / Header Input(" + strAPIInput + ") -> APIException : " + wex.ToString(), wex.Source);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Helpers.API :: GetDataFromAPI(" + strMethod + ") - (" + strAPIMethod + ") / Header Input(" + strAPIInput + ") -> Exception : " + ex.ToString(), ex.Source);
            }
            return webResponse;
        }

        public static HttpWebResponse PostDataToAPI(string strAPIMethod, string strAPIInput)
        {
            HttpWebResponse webResponse = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 200000;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(strAPIInput);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Helpers.API :: PostDataToAPI(" + strAPIMethod + ") -> APIException : " + wex.ToString(), wex.Source);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Helpers.API :: PostDataToAPI(" + strAPIMethod + ") -> Exception : " + ex.ToString(), ex.Source);
            }
            return webResponse;
        }

        public static HttpWebResponse PostDataToAPI(string strAPIMethod, string strAPIInput, string headers)
        {
            HttpWebResponse webResponse = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(spAPIBaseAddress + strAPIMethod);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 200000;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(strAPIInput);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                if (!string.IsNullOrEmpty(headers))
                {
                    if (headers.Contains('^'))
                    {
                        foreach (string strHeaders in headers.Split('^'))
                        {
                            if (strHeaders.Contains('|'))
                            {
                                string[] strHeader = strHeaders.Split('|');
                                httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                            }
                        }
                    }
                    else if (headers.Contains('|'))
                    {
                        string[] strHeader = headers.Split('|');
                        httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
                    }
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Helpers.API :: PostDataToAPI(" + strAPIMethod + ") -> APIException : " + wex.ToString(),wex, wex.Source);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BPP.Helpers.API :: PostDataToAPI(" + strAPIMethod + ") -> Exception : " + ex.ToString(), ex, ex.Source);
            }
            return webResponse;
        }

    }
}
