﻿using BPP93Helix.Feature.PageContent.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BPP93Helix.Feature.PageContent.Helper
{
    public class ApplicationProcessHelperController
    {
        static string spAPIGetPracDetails = Settings.GetSetting("spAPIGetPracDetails");
        public static BPPApplication GetBPPApplication(string ActivationKey)
        {
            if (!string.IsNullOrEmpty(ActivationKey))
            {
                BPPApplication _bppApplication = new BPPApplication();
                HttpWebResponse httpWebResponse = null;
                try
                {
                    httpWebResponse = Helper.API.GetDataFromAPI(spAPIGetPracDetails, "ActivationKey|" + (ActivationKey));
                    if (httpWebResponse != null)
                    {
                        Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: GetBPPApplication -> API Response : " + httpWebResponse, httpWebResponse);
                        Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: GetBPPApplication -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var objText = reader.ReadToEnd();
                            _bppApplication = (BPPApplication)js.Deserialize(objText, typeof(BPPApplication));
                        }
                        //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: PracticeApplication -> API Response Data: " + Newtonsoft.Json.JsonConvert.SerializeObject(_bppApplication), _bppApplication);
                        if (_bppApplication != null)
                        {
                            _bppApplication._Providers = Helper.BHSitecore.CustomParseProviders(_bppApplication._Providers);
                            if (_bppApplication._Practices[0].PracticeLocations != null)
                            {
                                _bppApplication._Practices[0].PracticeLocations = Helper.BHSitecore.CustomParseLocation(_bppApplication._Practices[0].PracticeLocations);

                            }
                            for (int i = 0; i < _bppApplication._Providers.Count; i++)
                            {
                                _bppApplication._Providers[i].Facilties = new List<Models.BPPFacilities>();
                                foreach (var bv in _bppApplication._Facilities)
                                {
                                    bool isSelected = false;
                                    if (_bppApplication._Providers[i].ProviderFacilities != null && _bppApplication._Providers[i].ProviderFacilities.Contains(bv.ID))
                                        isSelected = true;
                                    _bppApplication._Providers[i].Facilties.Add(new BPPFacilities { ID = bv.ID, FacilityName = bv.FacilityName, isSelected = isSelected });
                                }
                            }
                        }
                        else
                        {
                            Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: GetBPPApplication -> Invalid Request : " + ActivationKey, ActivationKey);
                            return null;
                        }

                        //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: GetBPPApplication -> _bppAppDetails: " + Newtonsoft.Json.JsonConvert.SerializeObject(_bppApplication), _bppApplication);
                    }
                    else
                    {
                        Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationConApplicationProcessControllertroller :: PractGetBPPApplicationiceApplication -> Invalid/Expired Request : " + ActivationKey, ActivationKey);
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationProcessController :: GetBPPApplication -> Exception : " + ex.ToString(), ex.Source);
                    _bppApplication = new BPPApplication();
                    _bppApplication._Practices = new List<BPPPractice>();
                    _bppApplication._Providers = new List<BPPProvider>();
                    _bppApplication._Practices[0].PracticeLocations = new List<Models.BPPPracticeLocation>();
                    throw;
                }
                return _bppApplication;
            }
            else
            {
                return null;
            }
        }
        public static string GenerateJson(FormCollection collection)
        {
            var pracNameKeys = collection.AllKeys
                   .Where(k => k.StartsWith("practiceName"))
                   .ToDictionary(k => k, k => collection[k]);
            if (pracNameKeys.Count > 0)
            {
                BPPApplication _bppAppForm = new Models.BPPApplication();
                _bppAppForm._PracticeNames = new List<BPPPracticeNames>();
                for (int i = 0; i < pracNameKeys.Count; i++)
                {
                    BPPPracticeNames _bppPracticeNames = new BPPPracticeNames();
                    _bppPracticeNames.PracticeName = collection["practiceName-" + i].ToString();
                    _bppAppForm._PracticeNames.Add(_bppPracticeNames);
                }
                string json = JsonConvert.SerializeObject(_bppAppForm._PracticeNames);
                return json;
            }
            else
            {
                BPPApplication _bppAppForm = new Models.BPPApplication();
                try
                {
                   // Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: GenerateJson", null);
                    // Multiple Locations
                    var pracLocKeys = collection.AllKeys
                        .Where(k => k.StartsWith("contactEmailMulti"))
                        .ToDictionary(k => k, k => collection[k]);
                    // Multiple Providers
                    var provKeys = collection.AllKeys
                        .Where(k => k.StartsWith("firstNameMulti"))
                        .ToDictionary(k => k, k => collection[k]);
                    // Multiple Practices
                    var pracKeys = collection.AllKeys
                        .Where(k => k.StartsWith("firstPracticeName"))
                        .ToDictionary(k => k, k => collection[k]);
                    BPPLegalEntity _legalEntity = new BPPLegalEntity();
                    _legalEntity.LegalEntityID = collection["_Member.ActivationKey"].ToString();
                    _legalEntity.TaxID = collection["_Entity.TaxID"].ToString();
                    _legalEntity.LE_Name = collection["_Entity.LE_Name"].ToString();
                    _legalEntity.LE_Address1 = collection["_Entity.LE_Address1"].ToString();
                    _legalEntity.LE_Address2 = collection["_Entity.LE_Address2"].ToString();
                    _legalEntity.LE_City = collection["_Entity.LE_City"].ToString();
                    _legalEntity.LE_State = collection["_Entity.LE_State"].ToString();
                    _legalEntity.LE_Zip = collection["_Entity.LE_Zip"].ToString();
                    _legalEntity.POCName = collection["_Entity.POCName"].ToString();
                    _legalEntity.POCPhoneNumber = collection["_Entity.POCPhoneNumber"].ToString();
                    _legalEntity.POCExtension = collection["_Entity.POCExtension"].ToString();
                   // _legalEntity.POCFax = collection["_Entity.POCFax"].ToString();
                    _legalEntity.POCEmail = collection["_Entity.POCEmail"].ToString();
                    _legalEntity.CAPName = collection["_Entity.CAPName"].ToString();
                    _legalEntity.CAPAddress1 = collection["_Entity.CAPAddress1"].ToString();
                    _legalEntity.CAPAddress2 = collection["_Entity.CAPAddress2"].ToString();
                    _legalEntity.CAPCity = collection["_Entity.CAPCity"].ToString();
                    _legalEntity.CAPState = collection["_Entity.CAPState"].ToString();
                    _legalEntity.CAPZip = collection["_Entity.CAPZip"].ToString();
                    string json = JsonConvert.SerializeObject(_legalEntity);
                    json = json.Replace("{\"_Entity\":", "");
                    return json;
                    //_bppAppForm._Practices = new List<BPPPractice>();
                    //for (int j = 0; j < pracKeys.Count; j++)
                    //{
                    //    BPPPractice _bppPractice = new BPPPractice();
                    //    _bppPractice.PracticeName = collection["practiceName-" + j].ToString();
                    //    _bppPractice.TaxID = collection["practiceTaxID-" + j].ToString();
                    //    _bppPractice.PointOfContact = collection["pocContactName-" + j].ToString();
                    //    //_bppPractice.PracticeApprovalStatus = enumPracticeApprovalStatus.New;
                    //    _bppPractice.PracticeLocations = new List<Models.BPPPracticeLocation>();
                    //    for (int i = 0; i < pracLocKeys.Count; i++)
                    //    {
                    //        BPPPracticeLocation _bppPracLoc = new BPPPracticeLocation();
                    //        _bppPracLoc.tmp_loc_id = i + 1;
                    //        _bppPracLoc.ID = i + 1;
                    //        _bppPracLoc.OfficeManagerEmail = collection["contactEmailMulti-" + i].ToString();
                    //        _bppPracLoc.Phone = collection["contactPhoneMulti-" + i].ToString();
                    //        _bppPracLoc.Address = collection["address1Multi-" + i].ToString();
                    //        _bppPracLoc.Address1 = collection["address1Multi-" + i].ToString();
                    //        _bppPracLoc.Address2 = collection["address2Multi-" + i].ToString();
                    //        _bppPracLoc.City = collection["cityMulti-" + i].ToString();
                    //        _bppPracLoc.State = collection["stateMulti-" + i].ToString();
                    //        _bppPracLoc.Zip = collection["zipMulti-" + i].ToString();
                    //        _bppPracLoc.Fax = "";
                    //        _bppPracLoc.IsPrimary = _bppPracLoc.IsPrimaryLocation = ((i == 0) ? true : false);
                    //        _bppPractice.PracticeLocations.Add(_bppPracLoc);

                    //    }
                    //    //_bppAppForm._Practices = new List<BPPPractice>();
                    //    _bppAppForm._Practices.Add(_bppPractice);
                    //}

                    //_bppAppForm._Providers = new List<BPPProvider>();
                    //for (int i = 0; i < provKeys.Count; i++)
                    //{
                    //    BPPProvider _bppProvider = new BPPProvider();
                    //    _bppProvider.FirstName = collection["firstNameMulti-" + i].ToString();
                    //    _bppProvider.LastName = collection["lastNameMulti-" + i].ToString();
                    //    _bppProvider.PrimarySpecialty = collection["primarySpecialtyMulti-" + i].ToString();
                    //    _bppProvider.SecondarySpecialty = collection["secondarySpecialtyMulti-" + i].ToString();
                    //    _bppProvider.NPINumber = collection["npiNumberMulti-" + i].ToString();
                    //    _bppProvider.Email = collection["emailMulti-" + i].ToString();
                    //    _bppProvider.isBoardCert = (collection["certifiedMulti-" + i].ToString().ToLower().Equals("yes")) ? true : false;
                    //    _bppProvider.isBHPrivileged = (collection["privilegesMulti-" + i].ToString().ToLower().Equals("yes")) ? true : false;
                    //    _bppProvider.PrimaryPracticeLocation = Convert.ToInt32(collection["providerLocationMulti-" + i].ToString());
                    //    _bppProvider.ProviderMemberStatus = 1;// enumProviderMemberStatus.New;
                    //    _bppProvider.TaxID = collection["practiceTaxID"].ToString();

                    //    string otherLocList = string.Empty;
                    //    try
                    //    {
                    //        otherLocList = collection["providerOtherLocationsMulti-" + i].ToString();
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationProcessController :: GenerateJson -> Not found any Other Locations for - " + _bppProvider.FirstName, otherLocList);
                    //        Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationProcessController :: GenerateJson -> Exception : " + ex.ToString(), ex.Source);

                    //        otherLocList = string.Empty;
                    //    }


                    //    try
                    //    {
                    //        _bppProvider.OtherPracticeLocations = new List<int>();
                    //        if (!string.IsNullOrEmpty(otherLocList))
                    //        {
                    //            _bppProvider.OtherPracticeLocations = (otherLocList.Split(',').Select(Int32.Parse).ToList());
                    //        }
                    //        else
                    //        {
                    //            _bppProvider.OtherPracticeLocations = null;
                    //        }

                    //        _bppAppForm._Providers.Add(_bppProvider);
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationProcessController :: GenerateJson -> Exception : " + ex.ToString(), ex.Source);
                    //    }
                    //}
                    //string json = JsonConvert.SerializeObject(_bppAppForm);
                    //json = json.Replace("{\"_Entity\":", "");
                    //json = json.Replace("},\"_Practices\":[", ",\"_Practices\":[");
                    //json = json.Replace(",\"_Providers\":[", ",\"_Providers\":[");
                    //Sitecore.Diagnostics.Log.Info("BPP.Controllers.ApplicationProcessController :: GenerateJson -> Final Json to API : " + json, json);
                    //return json;
                }
                catch (Exception ex)
                {
                    Sitecore.Diagnostics.Log.Error("BPP.Controllers.ApplicationProcessController :: GenerateJson -> Exception : " + ex.ToString(), ex.Source);
                    return "";
                }

            }
        }
    }
}