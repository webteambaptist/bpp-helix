﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace BPP93Helix.Feature.PageContent.Helper
{
    public static class EMail
    {
        static string smtpHost = Settings.GetSetting("smtpHost");
        static int smtpPort = Convert.ToInt32(Settings.GetSetting("smtpPort"));
        static bool enableSSL = Convert.ToBoolean(Settings.GetSetting("enableSSL"));
        static string emailFrom = Settings.GetSetting("emailFrom");
        static string emailPassword = Settings.GetSetting("emailPassword");
        public static void SendEmail(string dispName, string to, string subject, string body)
        {
            try
            {
                //Sitecore.Diagnostics.Log.Info("Sending notification to - " + to, to);
                var fromAddress = new MailAddress(emailFrom, dispName);
                var smtp = new SmtpClient
                {
                    Host = smtpHost,
                    Port = smtpPort,
                    EnableSsl = enableSSL,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, emailPassword)
                };
                using (var message = new MailMessage(fromAddress.ToString(), to.Replace(';',','))
                {
                    IsBodyHtml = true,
                    Subject = subject,
                    Body = body
                })
                    smtp.Send(message);
                Sitecore.Diagnostics.Log.Info("Notification sent to - " + to, to);


                ////Sitecore.MainUtil.SendMail(emailMessage);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Error while sending notification - " + ex.ToString(), ex, ex.Source);
            }
        }
    }
}