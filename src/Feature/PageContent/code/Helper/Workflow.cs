﻿using System;
using System.Web;
using Sitecore.Workflows.Simple;
using System.Text;
using Sitecore.Configuration;
using Sitecore.Links;

namespace BPP93Helix.Feature.PageContent.Helper
{
    public class Workflow
    {

    }

    public class DraftSubmit
    {
        public void Process(WorkflowPipelineArgs args)
        {
            var contentItem = args.DataItem;
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            try
            { 
                var path = LinkManager.GetItemUrl(contentItem, defaultUrlOptions);
                Sitecore.Diagnostics.Log.Info("Content item - " + contentItem.Name + "' (" + path + ") '"
                    + "has been submitted for review.", contentItem);
                // We could create a template for each of these emails and implement them here
                string previewUrl = string.Format("{0}://{1}/?sc_itemid=%7b{2}%7d&sc_mode=preview&sc_lang={3}",
                    HttpContext.Current.Request.Url.Scheme,
                    HttpContext.Current.Request.Url.Host,
                    contentItem.ID.Guid.ToString().ToUpper(),
                    contentItem.Language.Name);
                StringBuilder strBody = new StringBuilder();
               
                //strBody.Append("Content item - <b>" + contentItem.Name + "</b> (" + contentItem.Paths.Path + ")" + " has been submitted for review.<br />");
                strBody.Append("Content item - <b>" + contentItem.Name + "</b> (" + path + ")" + " has been submitted for review.<br />");
                strBody.Append("<br /><br /><a href='" + previewUrl + "'>Click here</a> to preview the item.");
                if (!string.IsNullOrEmpty(args.CommentFields["Comments"]))
                    strBody.Append("<br />Here are additional comments: " + args.CommentFields["Comments"].ToString());
                strBody.Append("<br /><br /><br /> <i>*** This is an automatically generated email, please do not reply ***</i>");

                string workFlowEmailDispName = Settings.GetSetting("workFlowEmailDispName");
                // Below email list to be pulled from Sitecore role based on the current workflow state
                string emailTo = Settings.GetSetting("workFlowEmailTo");

                EMail.SendEmail(workFlowEmailDispName, emailTo, "Draft Item Submitted - " + contentItem.Name, strBody.ToString());
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Exception while submitting '" + contentItem.Name + "' - " + ex.ToString(), ex, this);
            }
        }
    }
}
