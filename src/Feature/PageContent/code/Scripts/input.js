﻿
function RaisePlaceholder(id) {
    id.parent().find('label').removeClass('active').addClass('active');
    // added to prevent autofill inputs from browsers and animations at the same time
    id.parent().find('label').removeClass('animate').addClass('animate');
}
function LowerPlaceholder(id) {
    if (id[0].value.length == 0) {
        id.parent().find('label').addClass('active').removeClass('active');
        // added to prevent autofill inputs from browsers and animations at the same time
        id.parent().find('label').addClass('animate').removeClass('animate');
    }
}

$(document).ready(function () {
    $('input, textarea').focusin(function () {
        $(this).parent().find('label').removeClass('active').addClass('active');
        // added to prevent autofill inputs from browsers and animations at the same time
        $(this).parent().find('label').removeClass('animate').addClass('animate');
    });
    $('input, textarea').focusout(function () {
        if ($(this)[0].value.length == 0) {
            $(this).parent().find('label').addClass('active').removeClass('active');
            // added to prevent autofill inputs from browsers and animations at the same time
            $(this).parent().find('label').addClass('animate').removeClass('animate');
        }
    });

    $('body').on('blur', '.phone', function (e) {
    //$(".phone").blur(function () {
        //var key = e.which || e.charCode || e.keyCode || 0;
        $phone = $(this);
        if ($phone.val().length == 10) {
            $phone.val('(' + $phone.val()[0] + $phone.val()[1] + $phone.val()[2] + ') ' + $phone.val()[3] + $phone.val()[4] + $phone.val()[5] + '-' + $phone.val()[6] + $phone.val()[7] + $phone.val()[8] + $phone.val()[9]);
        }
        var input = $phone.val();
        var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
        if (!phoneNumberPattern.test(input)) {
            this.setCustomValidity("Invalid Phone format ((xxx) xxx-xxxx)");
        }
        else {
            this.setCustomValidity("");
        }
    });

    //$('body').on('keyup', '.tin', function (e) {
    //$(".tin").keyup(function (e) {
    $(".tin").blur(function () {
       /// var key = e.which || e.charCode || e.keyCode || 0;
        $tin = $(this);
        if ($tin.val().length == 9) {
            $tin.val($tin.val()[0] + $tin.val()[1] + '-' + $tin.val()[2] + $tin.val()[3] + $tin.val()[4] + $tin.val()[5] + $tin.val()[6] + $tin.val()[7] + $tin.val()[8]);
        }
        var input = $tin.val();
        var tinPattern = /\d{2}[- ]?(\d{7})/;
        if (!tinPattern.test(input)) {
            this.setCustomValidity("Invalid TAXID (xx-xxxxxxx)");
        }
        else {
            this.setCustomValidity("");
        }
        //if ($tin.val().length === 1 && (key === 8 || key === 46)) {
        //    $tin.val('');
        //    return false;
        //}
        //if (key !== 8 && key !== 9) {
        //    if ($tin.val().substring($tin.val().length - 1)=='-') {
        //        $tin.val($tin.val().substr(0,$tin.val().length-1))
        //    }
        //    if ($tin.val().length === 2) {
        //        $tin.val($tin.val() + '-');
        //    }            
        //}
    });
    $('body').on('blur', '.extension', function (e) {
        var $ext = $(this);
        var input = $ext.val();
        var extPattern = /^[0-9]*$/;
        if (!extPattern.test(input)) {
            this.setCustomValidity("Extension must be a number");
        }
        else {
            this.setCustomValidity("");
        }
    });
    $('body').on('blur', '.zipcode', function (e) {
        //$(".zipcode").blur(function () {
        // var key = e.which || e.charCode || e.keyCode || 0;
        $zipcode = $(this);
        if ($zipcode.val().length == 9) {
            $zipcode.val($zipcode.val()[0] + $zipcode.val()[1] + $zipcode.val()[2] + $zipcode.val()[3] + $zipcode.val()[4] + '-' + $zipcode.val()[5] + $zipcode.val()[6] + $zipcode.val()[7] + $zipcode.val()[8]);
        }
        var input = $zipcode.val();
        var zipPattern = /^\d{5}(-\d{4})?$/;
        if (!zipPattern.test(input)) {
            this.setCustomValidity("Invalid Zip Code (xxxxx-xxxx or xxxxx)");
        }
        else {
            this.setCustomValidity("");
        }
    });
        //if ($zipcode.val().length === 1 && (key === 8 || key === 46)) {
        //    $zipcode.val('');
        //    return false;
        //}
        //if (key !== 8 && key !== 9) {
        //    if ($zipcode.val().substring($zipcode.val().length - 1) == '-') {
        //        $zipcode.val($zipcode.val().substr(0, $zipcode.val().length - 1))
        //    }
        //    if ($zipcode.val().length === 5) {
        //        $zipcode.val($zipcode.val());
        //    }
        //    if ($zipcode.val()[5] != null && $zipcode.val()[5] !== '-') {
        //        $zipcode.val($zipcode.val()[0] + $zipcode.val()[1] + $zipcode.val()[2] + $zipcode.val()[3] + $zipcode.val()[4] + '-' + $zipcode.val()[5]);

        //        //$zipcode.val($zipcode.val() + '-');
        //    }
        //}



   // $('body').on('keyup', '.date', function (e) {
    //$(".date").keyup(function () {
    $(".date").blur(function () {
        //var key = e.which || e.charCode || e.keyCode || 0;
        $date = $(this);
        
        if ($date.val().length == 6) {
            $date.val('0' + $zipcode.val()[0] + '/' + '0' + $date.val()[1] + '/' + $date.val()[2] + $date.val()[3] + $date.val()[4] + $date.val()[5] );
        }
        if ($date.val().length == 8) {
            $date.val($zipcode.val()[0] + $date.val()[1] + '/' + $date.val()[2] + $date.val()[3] + '/' + $date.val()[4] + $date.val()[5] + $date.val()[6] + $date.val()[7]);
        }
        var input = $zipcode.val();
        if ($date.val().length < 6) {
            this.setCustomValidity(input);
            return;
        }
        var datePattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (!datePattern.test(input)) {
            this.setCustomValidity("Invalid Date (xx/xx/xxxx)");
        }
        else {
            this.setCustomValidity("");
        }
        //this.type = 'text';
        //var input = this.value;
        //if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
        //var values = input.split('/').map(function (v) {
        //    return v.replace(/\D/g, '')
        //});
        //if ($date.val().substring($date.val().length - 1) == '/')
        //    {
        //    $date.val($date.val().substr(0, $date.val().length - 1))
        //}
        //if ($date.val().length == 2) {
        //    $date.val($date.val() + "/");
        //} else if ($date.val().length == 5) {
        //    $date.val($date.val() + "/");
        //}
        //if (values[0]) values[0] = checkValue(values[0], 12);
        //if (values[1]) values[1] = checkValue(values[1], 31);
        //var output = values.map(function (v, i) {
        //    return v.length == 2 && i < 2 ? v + '/' : v;
        //    //return v.length == 2 && i < 2 ? v : v;
        //});
        //this.value = output.join('').substr(0, 14);

        //function checkValue(str, max) {
        //    if (str.charAt(0) !== '0' || str == '00') {
        //        var num = parseInt(str);
        //        if (isNaN(num) || num <= 0 || num > max) num = 1;
        //        str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
        //    };
        //    return str;
        //};
    });
});