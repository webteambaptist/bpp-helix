﻿var rows_selected = [];

$(document).ready(function () {
    var table = $('#survey-table').DataTable({
        "bAutoWidth": false,
        "ajax": {
            "url": "/api/Sitecore/Changes/ManageSurvey",
            "error": function (errResp) {
                window.location.href = "../";
            },
            "dataSrc": function (json) {
                if (json != null && json.data != null && json.data.length > 0) {
                    if (json.data == "no-access")
                        window.location.href = "../";
                    else
                        return JSON.parse(json.data);
                }
                else { return ""; }
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "No applications available!"
        },
        "columns": [
            { "data": "Id" },
            { "data": "Practice" },
            {
                "data": "SubmittedDate", "render": function (data) {
                    var value = new Date(Date.parse(data));
                    var dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
                    var dat1 = value.getFullYear() + "-" + value.getMonth() + 1 + "-" + ("0" + value.getDate()).slice(-2);
                    return "<span style='display: none;'>{{ value|date:'" + dat1 + "' }}</span>" + dat;
                }
            },
            //{ "data": "_Survey.EntityName" },
            { "data": "SubmitterGroup" },
            {
                "data": "ProviderName", render: function (data, type, row, meta) {
                    var link = "/Home/Changes/Changes/ChangesDetails?ProviderNPI=" + row.ProviderNPI + "&SubmitterTIN=" + row.SubmitterTIN + "&ID=" + row.Id;
                    return "<a id='Provider' href='" + link + "'>" + data + "</a>";
                }
            },
            { "data": "ProviderNPI" },
            { "data": "ChangeReason" },
            {
                "data": "EffectiveDate", "render": function (data) {
                    var value = new Date(Date.parse(data));
                    var dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
                    var dat1 = value.getFullYear() + "-" + value.getMonth() + 1 + "-" + ("0" + value.getDate()).slice(-2);
                    return "<span style='display: none;'>{{ value|date:'" + dat1 + "' }}</span>" + dat;
                }
            },
            { "data": "NameChange" },
            { "data": "SubmitterTIN"},
            {
                "data": "Checkbox"
                , "render": function (data, type, row, meta) {
                    return "<input type='checkbox'  />";
                }
            }
        ],
       select: {
            style: 'os',
            selector: 'tr td:last-child'
        },
        order: [[0, 'asc'], [0, 'asc']],
    }).columns([0,1,9]).visible(false, false);

    $('#survey-table tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        var data = table.row($row).data();
        var rowId = data.ProviderNPI;
        var index = $.inArray(rowId, rows_selected);
        if (this.checked && index === -1) {
            rows_selected.push(rowId);
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }
        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        updateDataTableSelectAllCtrl(table);
        e.stopPropagation();
    });


    $('thead input[name="select_all"]', table.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#survey-table tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#survey-table tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    table.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });
    // I'll do this after page displays
    $('#btnApproveChanges').click(function () {
        var page = "SurveyList";
        if (rows_selected != null && rows_selected.length > 0) {
            var dataProviders = [];
            $('#survey-table input[type="checkbox"]:checked').each(function () {
                if ($(this).attr('name') != 'select_all') {
                    var $row = $(this).closest('tr');
                    var provs = table.row($row).data();
                                        
                    var providerData = {};
                    providerData.User = "";
                    providerData.ProviderNPI = provs.ProviderNPI;
                    providerData.NameChange = provs.NameChange;
                    providerData.SubmittedDate = provs.SubmittedDate;
                    providerData.Practice = provs.Practice;
                    providerData.ProviderName = provs.ProviderName;
                    providerData.ChangeReason = provs.ChangeReason;
                    providerData.EffectiveDate = provs.EffectiveDate;
                    providerData.Id = provs.Id;
                   
                    dataProviders.push(JSON.stringify(providerData));
                }
            });

            $.ajax({
                type: "POST",
                url: "/api/Sitecore/Changes/ApproveChanges",
                dataType: "json",
                data: { providers: dataProviders.toString()},
                success: function (response) {
                    if (response != null && response == 'success') {
                       // document.location.reload();
                        $('#successmodal').modal('show');
                    }
                },
                error: function (response) {
                    console.log("Error Occured : " + response.responseText);
                    $("#errormodal").modal('show');
                }
            })
        }
    });
    $('#btnRejectChanges').click(function () {
        var page = "SurveyList";
        if (rows_selected != null && rows_selected.length > 0) {
            var dataProviders = [];
            $('#survey-table input[type="checkbox"]:checked').each(function () {
                if ($(this).attr('name') != 'select_all') {
                    var $row = $(this).closest('tr');
                    var provs = table.row($row).data();

                    var providerData = {};
                    providerData.User = "tanth001";
                    providerData.ProviderNPI = provs.ProviderNPI;
                    providerData.NameChange = provs.NameChange;
                    providerData.SubmittedDate = provs.SubmittedDate;
                    providerData.PracticeName = provs.PracticeName;
                    providerData.ProviderName = provs.ProviderName;
                    providerData.ChangeReason = provs.ChangeReason;
                    providerData.EffectiveDate = provs.EffectiveDate;
                    providerData.Id = provs.Id;

                    dataProviders.push(JSON.stringify(providerData));
                }
            });

            $.ajax({
                type: "POST",
                url: "/api/Sitecore/Changes/RejectChanges",
                dataType: "json",
                data: { providers: dataProviders.toString() },
                success: function (response) {
                    if (response != null && response == 'success') {
                        //document.location.reload();
                        $('#rejectmodal').modal('show');
                    }
                },
                error: function (response) {
                    console.log("Error Occured : " + response.responseText);
                    $("#errormodal").modal('show');
                }
            })
        }
    });
});

function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function closeSuccess() {
    document.location.reload();
}