﻿$(document).ready(function () {
    $('#btnApproveChanges').click(function () {
        var $inputs = $('#surveyDetails :input');

        var values = {};
        var dataProviders = [];
        $inputs.each(function () {
            values[this.name] = $(this).val();
        });
        dataProviders.push(JSON.stringify(values));

        $.ajax({
            type: "POST",
            url: "/api/Sitecore/Changes/ApproveChanges",
            dataType: "json",
            data: { providers: dataProviders.toString() },
            success: function (response) {
                if (response != null && response == 'success') {
                    $('#successmodal').modal('show');
                   // window.history.back();
                }
            },
            error: function (response) {
                console.log("Error Occured : " + response.responseText);
                $("#errormodal").modal('show');
            }
        });
    });
    $('#btnRejectChanges').click(function () {
        var $inputs = $('#surveyDetails :input');

        var values = {};
        var dataProviders = [];
        $inputs.each(function () {
            values[this.name] = $(this).val();
        });
        dataProviders.push(JSON.stringify(values));

        $.ajax({
            type: "POST",
            url: "/api/Sitecore/Changes/RejectChanges",
            dataType: "json",
            data: { providers: dataProviders.toString() },
            success: function (response) {
                if (response != null && response == 'success') {
                   // document.location.reload();
                   $('#rejectmodal').modal('show');
                   //window.history.back();
                }
            },
            error: function (response) {
                console.log("Error Occured : " + response.responseText);
                $("#errormodal").modal('show');
            }
        });
    });
    $('#btnClose').click(function () {
        window.history.back();
    });
});
function closeSuccess() {
    window.history.back();
}