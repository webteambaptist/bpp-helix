﻿var rows_selected = [];
var table;
$(document).ready(function () {
    //Set Time interval to be equal to 20 minutes.
    setInterval("Page_Timeout();", 1200000);
    if (sessionStorage.length > 0) {
        message = sessionStorage.getItem("message");
        showModal('BPP: Master List Lookup', message, true);
        sessionStorage.removeItem("message");
    }
    table = $('#lookup-table').DataTable({
        "bAutoWidth": true,
        "ajax": {
            "url": "/api/Sitecore/Application/ManageApplications",
            "error": function (errResp) {
                window.location.href = "../";
            },
            "dataSrc": function (json) {
                if (json != null && json.data != null && json.data.length > 0) {
                    if (json.data == "no-access" || json.data == "no-login")
                        window.location.href = "../";
                    else
                        return JSON.parse(json.data);
                }
                else { return ""; }
            },
            "type": "GET",
           "datatype": "json"
        },
        "language": {
            "emptyTable": "No Results!"
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData._Provider.ProviderApplicationWorkFlow!=null && aData._Provider.ProviderApplicationWorkFlow.AppStatusID == 7)
            {
                $('td', nRow).css('background-color', '#FFE98C');
            }
            if (aData._Provider.ProviderApplicationWorkFlow != null && aData._Provider.ProviderApplicationWorkFlow.AppStatusID == 8)
            {
                $('td', nRow).css('background-color', '#DCDCDC');
            }
        },
        "columns": [
            {
                "width": "15%",
                "data": "_Provider.LastName"
                , render: function (data, type, row, meta) {
                    var link = "/Home/Admin/Master Lookup/Provider Profile?ID=" + row._Provider.ID + "&PracticeID=" + row._Practice.ID + "&matrixID=" + row._Provider.matrix.ID;
                    //return "<a id='Provider' href='" + link + "'>" + data + "</a>";
                    if (row._Provider.isNew == true && row._Provider.MemberStatus.MemberStatus!= "Withdrew Application") {
                        return "<i class='fa fa-asterisk' style='color:red !important'></i> <a id='Provider' href='" + link + "' target='_blank'>" + data + "</a>";
                    }
                    else {
                        return "<a id='Provider' href='" + link + "' target='_blank'>" + data + "</a>";
                    }
                }
            },
            {
                "width": "15%",
                "data": "_Provider.FirstName"
            },
            {
                "width": "20%",
                "data": "_Provider.PrimarySpecialty"
            },
            {
                "width": "25%",
                "data": "_Practice.PracticeName"
                , render: function (data, type, row, meta) {
                    var link = "/Home/Admin/Master Lookup/Practice Profile?ID=" + row._Practice.PracticeGUID;
                    return "<a id='Practice' href='" + link + "' target='_blank'>" + data + "</a>";
                }
            },
            {   "width" : "10%",
                "data": "_LegalEntity.TaxID",
                render: function (data, type, row, meta) {
                    var link = "/Home/Admin/Master Lookup/Entity Profile?ID=" + row._LegalEntity.LegalEntityID;
                    return "<a id='Legal Entity' href='" + link + "' target='_blank'>" + data + "</a>";
                }
            },
            {
                "width": "10%",
                "data": "_Provider.MemberStatus.MemberStatus"                
            },
            { "data": "_Practice.ID" },
            { "data": "_Provider.ID" },
            { "data": "_Provider.matrix.ID" },
            { "data": "_Provider.isNew" },
            { "data": "_Provider.NPINumber" }
        ],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        "pageLength": 25,
        "pagingType": "full_numbers",
        responsive: true,
        columnDefs: [
            {
                orderable: false,
                className: 'dt-body-center',
                targets: [7],
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" value="'
                        + $('<div/>').text(data).html() + '">';
                }
            }
        ],
        select: {
            style: 'os',
            selector: 'tr td:last-child'
        },
        order: [[0, 'asc']],
    }).columns([6, 7, 8, 9, 10]).visible(false, false);
   
    $('#ddlAppReports').change(function () {
        if (this.value != "0") {
            $('#btnGetReport').prop("disabled", false);
        }
        else {
            $('#btnGetReport').prop("disabled", true);
        }
    });
   
    $('#btnGetReport').click(function () {
        if ($("#ddlAppReports").val() && $("#ddlAppReports").val() != null && $("#ddlAppReports").val() != 0) {
            var dataProviders = [];
            dataProviders.push($("#ddlAppReports").val());
            if (rows_selected != null && rows_selected.length > 0) {
                $.each(rows_selected, function (index, rowId) {
                    dataProviders.push(rowId.toString());
                });
            }
            window.open('/api/Sitecore/Application/GetAppReport?strRepMode=' + dataProviders, '_blank');
        }
        else {
            alert("Please select a report type!");
            return false;
        }
    });
    if (formResponse) {
        showModal('BPP: Membership Application Update Submission', formResponse, true);
    }
    $("#searchbox").keyup(function () {
        table.rows().search(this.value).draw();
    });    
});
function Page_Timeout() {
    document.location.reload();
    sessionStorage.setItem("message", "To prevent the page from timing out, this page has been refreshed.");
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}