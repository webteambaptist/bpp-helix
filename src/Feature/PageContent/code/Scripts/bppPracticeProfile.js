﻿var rows_selected = [];
var message = null;
var formPracticeProviders;

$(".bpp-attachment").click(function () {
    getAttachments();
});
$('.bpp-date').datepicker({
    maxDate: 0,
    changeMonth: true,
    changeYear: true,
    yearRange: "-100:+0"
});
function goBack() {
    window.close();
    //window.history.back();
}
$('#btnUploadFile').on('click', function (e) {
    var files = $("#txtUploadFile").prop('files')[0];
    if (files) {
        return true;
    } else {
        alert("Please select file to upload!");
        return false;
    }
});
$('#PracticeProfile :checkbox').change(function () {
    var locCount = $("#locationCount").val();
    var checked = false;
    for (var i = 0; i < locCount; i++) {
        checked = $("#isRemoved-" + i).is(':checked');
        if (checked) {
            break;
        }
    }
    if (checked) {
        $("#btnRemove").prop("disabled", false);
        $("#btnRemove").css("background-color", "red");
    }
    else {
        $("#btnRemove").prop("disabled", true);
        $("#btnRemove").css("background-color", "gray");
    }
});

$(document).ready(function () {
    document.getElementsByTagName('th')[0].click();
    $("#btnRemove").prop("disabled", true);
    $("#btnRemove").css("background-color", "gray");
     var ID = 0;
    if (window.location.href && window.location.href.length > 0 && (window.location.href.indexOf("ID=") >= 0)) {
        var split = window.location.href.split("ID=");
        ID = split[1];
        //console.log("GUID - " + split[1]);
    }
    var $inputs = $('#PracticeProfile :hidden');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });
    var practiceID = values["_Practices[0].ID"];
    var table = $('#applicant-table').DataTable({
        "ajax": {
            "url": "/api/Sitecore/Application/PracticeProviders",
            "data": {
                "GUID": ID
            },
            "dataSrc": function (json) {
                if (json != null && json.data != null && json.data.length > 0) {
                    if (json.data == 'no-access')
                        window.location.href = "../";
                    else if (json.data == 'no-login')
                        window.location.href = "../../login";
                    else
                        return JSON.parse(json.data);
                }
                else { return ""; }
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "No Providers available!"
        },
        "columns": [
            {
                "data": "LastName"
                , render: function (data, type, row, meta) {
                    var link = "/Home/Admin/Master Lookup/Provider Profile?ID=" + row.ID + "&PracticeID=" + practiceID + "&matrixID=" + row.matrix.ID;
                    return "<a id='Provider' href='" + link + "' target='_blank'>" + data + "</a>";
                }
            },
            { "data": "FirstName" },
            { "data": "Degree" },
            { "data": "PrimarySpecialty" },
            { "data": "SecondarySpecialty" },
            {
                "data": "CreatedDT"
                , "render": function (data) {
                    var value = new Date(Date.parse(data));
                    var dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
                    return dat;
                }
            },
            {
                "data": "ModifiedDT"
                , "render": function (data) {
                    var dat = "";
                    if (data && data != null && data != "") {
                        var value = new Date(Date.parse(data));
                        if (value && value != null && value != "") {
                            dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
                        }
                    }
                    return dat;
                }
            },
            { "data": "MemberStatus.MemberStatus" },
            { "data": "MiddleName" },
            { "data": "matrix.ID"}
        ],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true,
        select: {
            style: 'os',
            selector: 'tr td:last-child'
        },
        order: [[0, 'asc']],
    }).columns([8, 9]).visible(false, false);
    $("#divLocations-0").removeClass("hidden");
    $('#divShowHideBasic').click(function () {
        if ($("#divShowHideBasic").children('i').hasClass("fa-chevron-down")) {
            $("#divShowHideBasic").children('i').removeClass("fa-chevron-down");
            $("#divShowHideBasic").children('i').addClass("fa-chevron-up");
            $('.BasicInfo').each(function (i, e) {
                $(".BasicInfo").children('div').hide();
            });
        }
        else {
            $("#divShowHideBasic").children('i').removeClass("fa-chevron-up");
            $("#divShowHideBasic").children('i').addClass("fa-chevron-down");
            $('.BasicInfo').each(function (i, e) {
                $(".BasicInfo").children('div').show();
            });
        }
    });
 
    $('#ddlAppReports').change(function () {
        if (this.value != "0") {
            $('#btnGetReport').prop("disabled", false);
        }
        else {
            $('#btnGetReport').prop("disabled", true);
        }
    });

    $('#btnGetReport').click(function () {
        if ($("#ddlAppReports").val() && $("#ddlAppReports").val() != null && $("#ddlAppReports").val() != 0) {
            var dataProviders = [];
            dataProviders.push($("#ddlAppReports").val());
            if (rows_selected != null && rows_selected.length > 0) {
                $.each(rows_selected, function (index, rowId) {
                    dataProviders.push(rowId.toString());
                });
            }
            window.open('/api/Sitecore/Application/GetAppReport?strRepMode=' + dataProviders, '_blank');
        }
        else {
            alert("Please select a report type!");
            return false;
        }
    });
    if (sessionStorage.length > 0) {
        message = sessionStorage.getItem("message");
        showModal('BPP: Practice Profile Update', message, true);
        sessionStorage.removeItem("message");
    }
});
function getAttachments() {
    var ID = $("#practTaxID").val();
    $.ajax({
        type: "GET",
        url: "/api/Sitecore/Application/GetFileAttachments",
        dataType: "json",
        cache: false,
        data: { ID: ID, Mode: "1" },
        success: function (response) {
            $(".provider-profile-table").empty();
            if (response != null & response.data != null && response.data != "[]") {
                var newAttachments = "";
                newAttachments += "<ol>";
                $.each(JSON.parse(response.data), function (m, n) {
                    newAttachments += "<li><a target='_blank' href='/api/Sitecore/Application/DownloadAttachment?ID=" + this.ContentTypeID + "'>" + this.Name + "</a></li>";
                    //console.log("Doc Name - " + this.Name);
                });
                newAttachments += "</ol>";
                $(".provider-profile-table").html(newAttachments);
            }
            else
                $(".provider-profile-table").html("<ul class='list-style-none text-center'><li><b>No attachments available!</b></li></ul>");
        },
        error: function (response) {
            console.log("Error Occured : " + response.responseText);
        }
    })
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function isValidForm(form) {
    var $inputs = form;
    for (var i = 0; i < $inputs.length; i++) {
        if (!$inputs[i].validity.valid) {
            $inputs[i].setCustomValidity("Invalid");
            message = "Invalid Entry (" + $inputs[i].placeholder + "). Check Value and Try Again";
            document.location.reload();
            sessionStorage.setItem("message", message);
            return false;
        }
    }
    return true;
}
$("#btnRemove").click(function (e) {
    var locCount = $("#locationCount").val();
    for (var i = 0; i < locCount; i++) {
        var removed = $("#isRemoved-"+i).is(':checked');
        if (removed) {
            var locationID = $("#LocationID-" + i).val();
            // now call ajax to remove by id
            $.ajax({
                type: "Post",
                url: "/api/Sitecore/Application/RemoveLocation",
                dataType: "json",
                data: { locationID: locationID },
                success: function (response) {
                    if (response != null && response[0] == 'success') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                    else {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                },
                error: function (response) {
                    console.log("Error Occured : " + response.responseText);
                    if (response != null && response[0] == 'error') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                }
            });
        }
    }
});
$(function () {
    $("#btnSave").click(function (e) {
        var valid = isValidForm($('#PracticeProfile :input'));

        if (valid) {
            var form = $("#PracticeProfile").serialize();
            $.ajax({
                type: "POST",
                url: "/api/Sitecore/Application/PracticeProfile",
                dataType: "json",
                data: form,
                success: function (response) {
                    if (response != null && response[0] == 'success') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                },
                error: function (response) {
                    console.log("Error Occured : " + response.responseText);
                    if (response != null && response[0] == 'error') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                }
            });
            $("#btnSave").attr("disabed", false);
        }
    });
    $("#btnSaveTop").click(function (e) {
        var valid = isValidForm($('#PracticeProfile :input'));

        if (valid) {
            var form = $("#PracticeProfile").serialize();
            $.ajax({
                type: "POST",
                url: "/api/Sitecore/Application/PracticeProfile",
                dataType: "json",
                data: form,
                success: function (response) {
                    if (response != null && response[0] == 'success') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                },
                error: function (response) {
                    console.log("Error Occured : " + response.responseText);
                    if (response != null && response[0] == 'error') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                }
            });
            $("#btnSaveTop").attr("disabed", false);
        }
    });
});
function locationClick(span) {
    var index = span.id.split('-')[1];
    if ($("#divShowHideLoc-" + index).children('i').hasClass("fa-chevron-down")) {
        $("#divShowHideLoc-" + index).removeClass("selectedLocation");
        $("#divShowHideLoc-" + index).children('i').removeClass("fa-chevron-down");
        $("#divShowHideLoc-" + index).children('i').addClass("fa-chevron-up");
        $("#divLocations-" + index).addClass("hidden");
    }
    else {
        $("#divShowHideLoc-" + index).addClass("selectedLocation");
        $("#divShowHideLoc-" + index).children('i').removeClass("fa-chevron-up");
        $("#divShowHideLoc-" + index).children('i').addClass("fa-chevron-down");
        $("#divLocations-" + index).removeClass("hidden");
    }
}
