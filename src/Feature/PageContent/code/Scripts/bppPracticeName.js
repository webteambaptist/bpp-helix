﻿var nextMultiPracticeID = 0;

function isJSON(data) {
    var ret = true;
    try {
        JSON.parse(data);
    } catch (e) {
        ret = false;
    }
    return ret;
}

function addSection(section, source, m) {
    if (typeof (m) != "undefined") {
        model = m;
    }
    if (!$(source).hasClass('disabled')) {
        var target = '';
        var newHtml = '';
        var checkProviderNum = false;
        var OtherLocId = "";
        switch (section) {
            case "practiceMulti":
                nextMultiPracticeID++
                target = 'practices-multi';
                checkProviderNum = true;

                newHtml = '<hr/>'
                newHtml = '<div class="practice-multi">'
                newHtml += '<div class="row">'
                //Practice Name
                newHtml += '<div class="col-md-4">'
                newHtml += '<label for="practiceName-' + nextMultiPracticeID + '" class="placeholder">Practice Name</label>'
                newHtml += '<input id="practiceName-' + nextMultiPracticeID + '" name="practiceName-' + nextMultiPracticeID + '" type="text" placeholder="" class="form-control input adaptive" required="required" title="Practice Name/Doing Business As" onfocus="RaisePlaceholder($(this))" onblur="validatePracticeName($(this));" /> '
                //onchange="CheckPracticeName();" onblur="LowerPlaceholder($(this));"

               // newHtml += '<div class="PracticeNameExists-' + nextMultiPracticeID + '" style="display:none;color:red">Practice already exist for this TIN</div>';
                newHtml += '</div>'//end div
                newHtml += '</div>'//end row
                //Practice Name already exists Message
                newHtml += '<div class="row">'
                newHtml += '<div class="col-md-12">'
                newHtml += '<div class="PracticeNameExists-' + nextMultiPracticeID + '" style="display:none;color:red">This Practice already exist for this TIN. If you\'re looking to add providers for this particular practice, please return to the Returning Member page and select the "Add Providers" option. If you continue, this Practice will not be saved.</div>'
                newHtml += '</div>'//end div
                newHtml += '</div>'//end row
                newHtml += '<div class="row savespace">'
                newHtml += '<div class="col-md-6">'
                //Remove Section
                newHtml += '<div class="remove-section" onclick="removeSection(\'practice-multi\', $(this))" style="display:none;">'
                newHtml += '<span class="fa fa-minus"></span> Remove Practice'
                newHtml += '</div>'//end remove-section
                newHtml += '</div>'//end div
                newHtml += '<div class="col-md-6">'
                //Add Section
                newHtml += '<div class="add-section" onclick="addSection(\'practiceMulti\', $(this),JSON.stringify(model))">'
                newHtml += '<span class="fa fa-plus"></span> Add Practice'
                newHtml += '</div>'//end add-section
                newHtml += '</div>'//end div
                newHtml += '</div>'//end container
                break;
        }
        $('.' + target).append(newHtml);
        showOneAddControl();
        showOneRemoveControl();
        setSelectTextColor();
        $('select').on('change', setSelectTextColor);
        if (checkProviderNum) {
            var $providers = $('.provider-num');

            for (var i = 0; i < $providers.length; i++) {
                $($providers[i]).text('Provider ' + (i + 1));
            }
        }
        window.scrollBy(0, $('.dynamic-section:first-child').height());
        if (OtherLocId && OtherLocId != "") {
            OtherLocId = '#' + OtherLocId;
            $(OtherLocId).chosen({ width: "100%" });
            $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
        }
    }
}

function removeSection(target, source) {
    if (!$(source).hasClass('disabled')) {
        if (target == "practice-multi") {
            nextMultiPracticeID--;
        } else if (target == "provider-multi") {
            nextMultiProviderID--;
        } else if (target == "practice-single") {
            nextSinglePracticeID--;
        }

        $(source).closest('.' + target).remove();
        showOneAddControl();
        showOneRemoveControl();
        var $providers = $('.provider-num');
        for (var i = 0; i < $providers.length; i++) {
            $($providers[i]).text('Provider ' + (i + 1));
        }
    }
}

function showOneAddControl() {
    $('.dynamic-section').each(function () {
        var addControls = $(this).find('.add-section');
        for (var i = 0; i < addControls.length; i++) {
            if (i == addControls.length - 1) {
                $(addControls[i]).show();
            } else {
                $(addControls[i]).hide();
            }
        }
    });
}

function showOneRemoveControl() {
    $('.dynamic-section').each(function () {
        var remControls = $(this).find('.remove-section');
        for (var i = 0; i < remControls.length; i++) {
            if (i > 0 && i == remControls.length - 1) {
                $(remControls[i]).show();
            } else {
                $(remControls[i]).hide();
            }
        }
    });
}
function disableInputs() {
    $('.disabled').on('keydown paste', function (e) {
        if ($(this).hasClass('disabled')) {
            e.preventDefault();
        }
    });
}

function setSelectTextColor() {
    $('select').each(function () {
        if (!$(this).val()) {
            $(this).css('color', '#999');
            $(this).css('font-style', 'italic');
        } else {
            $(this).css('color', '#555');
            $(this).css('font-style', 'normal');
        }
    })
}

$(document).ready(function () {
    $("ul.progressbar li:nth-child(2)").addClass("active");
    //document.getElementById("btnSavePracticeName").setAttribute('disabled', 'disabled');
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
});

function validatePracticeName(Practice) {
    var id = Practice.id;
    if (typeof (id) == "undefined") {
        id = Practice[0].id;
        LowerPlaceholder($("#" + id));
    }

    var index = id.substr(id.length - 1);
    var PracticeName = $("#" + id).val();
    practiceNameValid(PracticeName, index);

    var _activationkey = document.getElementById('ActivationKey').value;
    $.ajax({
        type: "POST",
        url: "/api/Sitecore/PracticeName/ValidatePracticeName",
        dataType: "json",
        data: {
            PracticeName: PracticeName,
            ActivationKey: _activationkey
        },
        success: function (response) {
            if (response != null && response[0] == 'success') {
                $("#" + id)[0].setCustomValidity("");
                $('.PracticeNameExists-' + nextMultiPracticeID).hide();
            }
            else {
                $("#" + id)[0].setCustomValidity("Practice Name already Used.");
                 $('.PracticeNameExists-' + nextMultiPracticeID).show();
            }
        },
        error: function (response) {
            $("#" + id)[0].setCustomValidity("Practice Name already Used.");
            $('.PracticeNameExists-' + nextMultiPracticeID).show();
        }
    });
}
function practiceNameValid(value, index) {
    var practicename = value;
    var $inputs = $('#practicenames :input');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });

    var indexInt = parseInt(index);
    for (var i = 0; i < indexInt; i++) {
        var practiceNameValue = values["practiceName-" + i];
        if (practicename == practiceNameValue) {
            $("#practiceName-" + i)[0].setCustomValidity("Practice Name already Used.");
            $('.PracticeNameExists-' + nextMultiPracticeID).show();
        }
        else {
            $("#practiceName-" + i)[0].setCustomValidity("");
            $('.PracticeNameExists-' + nextMultiPracticeID).hide();
        }
    }
}

