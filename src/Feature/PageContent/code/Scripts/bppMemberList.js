﻿var rows_selected = [];

$(document).ready(function () {
 
    var table = $('#member-table').DataTable({
        "bAutoWidth": false,
        "ajax": {
            "url": "/api/Sitecore/Member/ManageMembers",
            "error": function (errResp) {
                window.location.href = "../";
            },
            "dataSrc": function (json) {
                if (json != null && json.data != null && json.data.length > 0) {
                    if (json.data == "no-access")
                        window.location.href = "../";
                    else
                        return JSON.parse(json.data);
                }
                else { return ""; }
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "No applications available!"
        },
         "columns": [
            {
                "data": "_Provider.LastName"
                , render: function (data, type, row, meta) {
                   var link = "/Home/Admin/Applicants/Provider Profile?ID=" + row._Provider.ID;
                   return "<a id='Provider' href='" + link + "'>" + data + "</a>";
                }
            },
            { "data": "_Provider.FirstName" },
            { "data": "_Provider.Degree" },
            { "data": "_Provider.PrimarySpecialty" },
            { "data": "_Provider.SecondarySpecialty" },
            {
                "data": "_Practice.PracticeName"
                , render: function (data, type, row, meta) {
                    var link = "/Home/Admin/Applicants/Practice Profile?ID=" + row._Practice.GUID;
                    return "<a id='Practice' href='" + link + "'>" + data + "</a>";
                }
            },
            {
                "data": "_Provider.CreatedDT"
                , "render": function (data) {
                    var value = new Date(Date.parse(data));
                    var dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
                    var dat1 = value.getFullYear() + "-" + value.getMonth() + 1 + "-" + ("0" + value.getDate()).slice(-2);
                    return "<span style='display: none;'>{{ value|date:'" + dat1 + "' }}</span>" + dat;
                }
            },
    		{
    		    "data": "_Provider.ModifiedDT"
    		    , "render": function (data) {
    		        var dat = "";
    		        if (data && data != null && data != "") {
    		            var value = new Date(Date.parse(data));
    		            if (value && value != null && value != "") {
    		                dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
    		                var dat1 = value.getFullYear() + "-" + value.getMonth() + 1 + "-" + ("0" + value.getDate()).slice(-2);
    		                return "<span style='display: none;'>{{ value|date:'" + dat1 + "' }}</span>" + dat;
    		            }
    		            else {
    		                return dat;
    		            }
    		        }
    		        else {
    		            return dat;
    		        }
    		    }
    		},
    		{ "data": "_Provider.MemberStatus.MemberStatus" },
    		{ "data": "_Practice.MSSPDetails" },
            {
    		    "data": "Checkbox"
                , "render": function (data, type, row, meta) {
                        return "<input type='checkbox'  />";
                }
    		},
            { "data": "_Practice.ID" },
            { "data": "_Practice.TaxID" },
            { "data": "_Provider.ID" },
          //  { "data": "_Provider.ProviderApplicationWorkFlow.AppStatusID" },
        ],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        "pageLength": 25,
        "pagingType": "full_numbers",
        responsive: true,
        columnDefs: [
            {
                orderable: false,
                className: 'dt-body-center',
                targets: [10],
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" value="'
                        + $('<div/>').text(data).html() + '">';
                }
            },
         ],
        select: {
            style: 'os',
            selector: 'tr td:last-child'
        },
        order: [[0, 'asc'], [0, 'asc']],
    }).columns([11, 12, 13, 14]).visible(false, false);

    $('#member-table tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        var data = table.row($row).data();
        var rowId = data._Provider.ID;
        var index = $.inArray(rowId, rows_selected);
        if (this.checked && index === -1) {
            rows_selected.push(rowId);
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }
        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }
        updateDataTableSelectAllCtrl(table);
        e.stopPropagation();
    });


    $('thead input[name="select_all"]', table.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#member-table tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#member-table tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    table.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    $('#btnUpdateProviders').click(function () {
        var page = "MemberList";
        if (rows_selected != null && rows_selected.length > 0) {
            var dataProviders = [];
            var type = $("#ddlAppActions").val();
            dataProviders.push(type);
                $('#member-table input[type="checkbox"]:checked').each(function () {
                    if ($(this).attr('name') != 'select_all') {
                        var $row = $(this).closest('tr');
                        var data = table.row($row).data();
                        var rowId = data._Provider.ID;
                        var memStatus = data._Provider.MemberStatus.StatusID;
                        var providerData = {};
                        providerData.rowId = rowId;
                        providerData.wfStage = wfStage;
                        providerData.memStatus = memStatus;
                        providerData.subStatus = subStatus;
                        dataProviders.push(JSON.stringify(providerData));
                    }
                });
            
            $.ajax({
                type: "POST",
                url: "/api/Sitecore/Member/UpdateMembers",
                dataType: "json",
                data: { strAppUpd: dataProviders.toString(), page: page},
                success: function (response) {
                    if (response != null && response == 'success') {
                        document.location.reload();
                    }
                },
                error: function (response) {
                    console.log("Error Occured : " + response.responseText);
                }
            })
        }
        else {
        }
    });
    if (formResponse) {
        showModal('BPP: Member Update Submission', formResponse, true);
    }
});

function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}