﻿var addresses = [];
var activePane;

function resetOtherLocations(e) {
    //console.log('Item Before Change' + prevPrimLocID);
    element = $(e.target);
    value = element.val();
    clone = element.clone();
    clone.find('option[value=' + value + ']').remove();
    var otherLocID = e.target.name.replace('providerLocationMulti', '#providerOtherLocationsMulti');
    currentSelectedOtherLoc = $(otherLocID).chosen().val();
    $(otherLocID).html(clone.children());
    $(otherLocID).val(currentSelectedOtherLoc).trigger('chosen:updated');
    $(otherLocID).chosen({ width: "100%" });
    $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
}

$('#fullAppProviders').submit(function () {
    $('[id^="otherPracLocations_"]').each(function (i, e) {
        //console.log("other locations - " + i + " " + e);
        $("#otherPracLocations_" + i).empty();
        var myValues = $('#providerOtherLocationsMulti-' + i).chosen().val();
        myValues.forEach(function (index, val) {
            $("#otherPracLocations_" + i).append("<input name=\"_providers[" + i + "].otherpracticelocations[" + val + "]\" type=\"hidden\" value=\"" + index + "\">");
        })
    });
    $('[id^="facilities_"]').each(function (i) {
        var e = $('#facilities_' + i);
        var checked = e.find('input[type="checkbox"]:checked')
        var selected = [];
        for (a = 0; a < checked.length; a++) {
            selected.push(checked[a].id);
        }
        //console.log('#facilities_' + i + ' -- ' + selected);
        $.each(selected, function (index, value) {
            $("#facilities_" + i).append("<input name=\"_Providers[" + i + "].ProviderFacilities[" + index + "]\" type=\"hidden\" value=\"" + value + "\">");
        });
    });
});
var prevPrimLocID;
$('.provider-location').on('focus', function (event, ui) {
    prevPrimLocID = this.value;
}).change(function (e) {
    resetOtherLocations(e);
});

$('.provider-other-location').chosen({ width: "100%" });
$('.chosen-choices').addClass('form-control').css("padding", "3px 10px");

$(document).ready(function () {
    activePane = sessionStorage.getItem('activePane') ? parseInt(sessionStorage.getItem('activePane'), 10)+1 : 0;
    
    $("#accordion").accordion({
        heightStyle: "content",
        icons: false,
        active: activePane,
        animate: false
    });

    $('.bpp-save-btn').click(function () {
        var activePane = $('#accordion').accordion('option', 'active');
        sessionStorage.setItem('activePane', activePane);
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('#accordion button').click(function (e) {
        e.preventDefault();
        var delta = $(this).hasClass("pm-next") ? 1 : -1;
        $('#accordion').accordion('option', 'active', ($('#accordion').accordion('option', 'active') + delta));
        $('html, body').animate({
            scrollTop: $('h3.ui-state-active').offset().top - 70
        }, 100);
    });

    $('.pm-providerNum').each(function (index) {
        $(this).html('Provider ' + (index + 1));
    });

    $('.pm-text_field').keyup(function () {

        var empty = false;
        $('.pm-text_field').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#pm-register').attr('disabled', 'disabled').removeClass('pm-button-active-style ');
        } else {
            $('#pm-register').attr('disabled', false).addClass('pm-button-active-style ');
        }
    });

    $('.pm-agree-input1').keyup(function () {

        var empty = false;
        $('.pm-agree-input1').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#pm-agreement-submit1').attr('disabled', 'disabled').removeClass('pm-button-active-style ');
        } else {
            $('#pm-agreement-submit1').attr('disabled', false).addClass('pm-button-active-style ');
        }
    });

    $('.pm-agree-input').keyup(function () {

        var empty = false;
        $('.pm-agree-input').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#pm-agreement-submit').attr('disabled', 'disabled').removeClass('pm-button-active-style ');
        } else {
            $('#pm-agreement-submit').attr('disabled', false).addClass('pm-button-active-style ');
        }
    });

    $('.pm-text_field').keyup(function () {

        var empty = false;
        $('.pm-text_field').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#pm-checkbox').html('<i class="fa fa-spinner" aria-hidden="true">');
        } else {
            $('#pm-checkbox').html('<i class="fa fa-check"></i>');
        }
    });

    $('.pm-agree-input').keyup(function () {

        var empty = false;
        $('.pm-agree-input').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#pm-checkbox1').html('<i class="fa fa-spinner" aria-hidden="true">');
        } else {
            $('#pm-checkbox1').html('<i class="fa fa-check"></i>');
        }
    });

    $('.pm-agree-input1').keyup(function () {

        var empty = false;
        $('.pm-agree-input1').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#pm-checkbox2').html('<i class="fa fa-spinner" aria-hidden="true">');
        } else {
            $('#pm-checkbox2').html('<i class="fa fa-check"></i>');
        }
    });

    $('.pm-psubmit').keyup(function () {
        var empty = false;
        $('.pm-psubmit').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if ((empty) && ($('#my_checkbox').is(':checked'))) {
            $('#pm-checkbox3').html('<i class="fa fa-spinner" aria-hidden="true">');
        } else {
            $('#pm-checkbox3').html('<i class="fa fa-check"></i>');
        }
    });

    $('.pm-text_field, .pm-psubmit, .pm-agree-input, .pm-agree-input1').keyup(function () {
        var empty = false;
        $('.pm-text_field, .pm-psubmit, .pm-agree-input, .pm-agree-input1').each(function () {
            if ($(this).val().length == 0) {
                empty = true;
            }
        });

        if (empty) {
            $('#pm-apply-btn').attr('disabled', 'disabled').removeClass('pm-button-active-style ');
        } else {
            $('#pm-apply-btn').removeAttr('disabled').addClass('pm-button-active-style ');
        }
    });

    $('.bpp-date').datepicker({
        maxDate: 0,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
});

$('.pm-providerInputGroup').each(function (index) {

    var inputGroupId = index + 1;
    var groupEl = $(this);

    groupEl.data('groupId', inputGroupId);

    groupEl.find('.pm-providerNum').html('Provider ' + inputGroupId);

    groupEl.find('.pm-psubmit').each(function () {
        var cleanName = $(this).attr('placeholder').toLowerCase().replace(/[^a-z0-9]+/g, '');
        $(this).attr('name', 'Provider[' + inputGroupId + '][' + cleanName + ']');
    });

    groupEl.find('.pm-loc-section .pm-loc-inputWrapper').each(function (index2) {
        var locGroupId = index2 + 1;

        $(this).data('locGroupId', locGroupId);

        $(this).find('.pm-psubmit').each(function () {
            var curName = $(this).attr('name');
            $(this).attr('name', curName + '[' + locGroupId + ']');
        });
    });

});

$(document).on("click", ".pm-add-loc-Class", function () {
    var $sect = $(this).closest(".pm-loc-section");

    var lastLocGroup = $sect.find(".pm-loc-inputWrapper").eq(-1);

    var lastLocGroupId = lastLocGroup.data('locGroupId');

    var newLocGroup = lastLocGroup.clone();

    var newLocGroupId = lastLocGroupId + 1;

    newLocGroup.show().insertBefore($sect.find(".pm-loc-controls")).find('input').val('');

    newLocGroup.data('locGroupId', newLocGroupId);

    newLocGroup.find('.pm-psubmit').each(function (index2) {
        var curName = $(this).attr('name');
        var newName = curName.replace(/(\w+\[[^\]]+\]\[[^\]]+\])(\[[^\]]+\])/, '$1[' + newLocGroupId + ']');
        $(this).attr('name', newName);
    });
});


$(function () {
    $("#btnSavePracAppStep1").click(function (e) {
        e.preventDefault();
        var _this = $(this);
        var _form = _this.closest("form");

        var isvalid = _form.valid();

        if (isvalid) {
            _form.submit();
        }
        else {
            $("#btnSavePracAppStep1").attr("disabled", false);
        }
    })
})
$(function () {
    $("#btnSavePracAppStep2").click(function (e) {
        e.preventDefault();
        var _this = $(this);
        var _form = _this.closest("form");

        var isvalid = _form.valid();

        if (isvalid) {
            _form.submit();
        }
        else {
            $("#btnSavePracAppStep2").attr("disabled", false);
        }
    })
})




//dont delete these

//$(document).on("click", ".pm-add-loc-Class", function () {
//    var $sect = $(this).closest(".pm-loc-section");
//    $sect.find(".pm-loc-inputWrapper").eq(0).clone().show().insertBefore($sect.find(".pm-loc-controls")).find('input').val('');
//});





//$(document).on("click", ".pm-add-loc-Class", function () {
//    var $sect = $(this).closest(".pm-loc-section");
//    $sect.find(".pm-loc-inputWrapper").eq(0).clone().show().insertBefore($sect.find(".pm-loc-controls")).find('input').val('');
//});



////    $("#pm-do-clone").click(function () {
////        $(".pm-pro-clone .pm-clone-this").clone().appendTo(".pm-add-here");
////    });

//$('.pm-providerNum').each(function (index) {
//    $(this).html('Provider ' + (index + 1));
//});

//$("div.pm-Provider-first-wrap").each(function (index) {

//    $(this).find('input').each(function () {
//        $(this).attr('name', $(this).attr('name')+ '-' + index);

//    })
//});

