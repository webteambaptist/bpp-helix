﻿var rows_selected = [];

$(document).ready(function () {
    var table = $('#survey-table').DataTable({
        "bAutoWidth": false,
        "ajax": {
            "url": "/api/Sitecore/Changes/ManageArchiveList",
            "error": function (errResp) {
                window.location.href = "../";
            },
            "dataSrc": function (json) {
                if (json != null && json.data != null && json.data.length > 0) {
                    if (json.data == "no-access")
                        window.location.href = "../";
                    else
                        return JSON.parse(json.data);
                }
                else { return ""; }
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "No applications available!"
        },
        "columns": [
            { "data": "Id" },
            { "data": "Practice" },
            {
                "data": "SubmittedDate", "render": function (data) {
                    var value = new Date(Date.parse(data));
                    var dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
                    var dat1 = value.getFullYear() + "-" + value.getMonth() + 1 + "-" + ("0" + value.getDate()).slice(-2);
                    return "<span style='display: none;'>{{ value|date:'" + dat1 + "' }}</span>" + dat;
                }
            },
            //{ "data": "_Survey.EntityName" },
            { "data": "SubmitterGroup" },
            {
                "data": "ProviderName", render: function (data, type, row, meta) {
                    var link = "/Home/Changes/Changes/ChangesDetails?ProviderNPI=" + row.ProviderNPI + "&SubmitterTIN=" + row.SubmitterTIN + "&ID=" + row.Id;
                    return "<a id='Provider' href='" + link + "'>" + data + "</a>";
                }
            },
            { "data": "ProviderNPI" },
            { "data": "ChangeReason" },
            {
                "data": "EffectiveDate", "render": function (data) {
                    var value = new Date(Date.parse(data));
                    var dat = value.getMonth() + 1 + "/" + value.getDate() + "/" + value.getFullYear();
                    var dat1 = value.getFullYear() + "-" + value.getMonth() + 1 + "-" + ("0" + value.getDate()).slice(-2);
                    return "<span style='display: none;'>{{ value|date:'" + dat1 + "' }}</span>" + dat;
                }
            },
            { "data": "NameChange" },
            { "data": "SubmitterTIN" },
            {
                "data": "ProcessedResults", "render": function (data) {
                    if (data == 1) {
                        return "Approved"
                    }
                    else {
                        return "Rejected"
                    }                    
                }
            },
        ]      
    }).columns([0, 1]).visible(false, false);
    //.columns([0, 1, 9]).visible(false, false);
});