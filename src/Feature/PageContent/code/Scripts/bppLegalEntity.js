﻿$(document).ready(function () {
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

    $(".tin").focusout(function () {
        // check to make sure tin doesn't already exist
        var tin = $(this).val();
        if (tin.length == 9) {
            tin = tin[0] + tin[1] + '-' + tin[2] + tin[3] + tin[4] + tin[5] + tin[6] + tin[7] + tin[8];
        } 
        $.ajax({

            type: "POST",
            url: "/api/Sitecore/Validation/ValidateTINApplication",
            dataType: "json",
            data: { tin: tin },
            success: function (response) {
                if (response != null && response != 'success') {
                    //alert("TIN: " + tin + " already exists. If you're trying to add to this Group please go to the home page and click 'Add to my Group'. Otherwise, Please Check Value and Try again.");
                    //document.location.reload();
                    //sessionStorage.setItem("message", "TIN: " + tin + " already exists. If you're trying to add to this Group please go to the home page and click 'Add to my Group'. Otherwise, Please Check Value and Try again.");
                    //$("#tin").val("");
                    $(".tin")[0].setCustomValidity("TIN Already Exists");
                }
                else {
                    $(".tin")[0].setCustomValidity("");
                }
            },
            error: function (response) {
                console.log("Error Occured : " + response.responseText);
                //alert("Invalid TIN. Please Check Value and Try again.");
                $(".tin").setCustomValidity("TIN Already Exists.");
                //document.location.reload();
                //sessionStorage.setItem("message", "Invalid TIN. Please Check Value and Try again.");
            }
        });
    });
    //if (sessionStorage.length > 0) {
    //    message = sessionStorage.getItem("message");
    //    showModal('BPP: Legal Entity Application', message, true);
    //    sessionStorage.removeItem("message");
    //}
});