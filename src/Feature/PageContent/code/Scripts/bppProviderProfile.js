﻿var message = null;
var prevPrimLocID;
$('.provider-location').on('focus', function (event, ui) {
    prevPrimLocID = this.value;
}).change(function (e) {
    resetOtherLocations(e);
});
function resetOtherLocations(e) {
    element = $(e.target);
    value = element.val();
    $("#hdnPrimaryPracticeLocation").val(value);
    clone = element.clone();
    clone.find('option[value=' + value + ']').remove();
    currentSelectedOtherLoc = $(".otherLocations").chosen().val();
    $(".otherLocations").html(clone.children());
    $(".otherLocations").val(currentSelectedOtherLoc).trigger('chosen:updated');
    $(".otherLocations").chosen({ width: "100%" });
    $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
}

function isValidForm(form) {
    var $inputs = form;
    for (var i = 0; i < $inputs.length; i++) {
        if (!$inputs[i].validity.valid) {
            $inputs[i].setCustomValidity("Invalid");
            message = "Invalid Entry (" + $inputs[i].placeholder + "). Check Value and Try Again";
            document.location.reload();
            sessionStorage.setItem("message", message);
            return false;
        }
    }
    return true;
}

//$('#provProfile').submit(function () {
function Save() {
    $("#otherPracLocations").empty();
    var myValues = $('.otherLocations').chosen().val();
    myValues.forEach(function (index, val) {
        $("#otherPracLocations").append("<input name=\"_Providers[0].OtherPracticeLocations[" + val + "]\" type=\"hidden\" value=\"" + index + "\">");
    })
    var facilities = $('#facilities :input:checked');
    var ids = {};
    facilities.each(function (index) {
        $("#facilities").append("<input name=\"_Providers[0].ProviderFacilities[" + index + "]\" type=\"hidden\" value=\"" + $(this).attr('id') + "\">");
        //console.log(index + ': ' + $(this).attr('id'));
    });

  var acceptingNewPatients = $('#acceptingNewPatients :input:checked');
    var ids = {};
    acceptingNewPatients.each(function (index) {
      $("#acceptingNewPatients").append("<input name=\"_Providers[0].ProviderAcceptingPatients[" + index + "]\" type=\"hidden\" value=\"" + $(this).attr('id') + "\">");
      //console.log(index + ': ' + $(this).attr('id'));
    });

    var valid = isValidForm($('#provProfile :input'));
    if (valid) {
        var form = $("#provProfile").serialize();
        $.ajax({
            type: "POST",
            url: "/api/Sitecore/Application/ProviderProfile",
            dataType: "json",
            data: form,
            success: function (response) {
                if (response != null) {
                    if (response[0] == 'success') {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                    else {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                }
                //console.log(response);
            },
            error: function (response) {
                console.log("Error Occured : " + response.responseText);
                if (response != null && response[0] == 'error') {
                    if (response[1] && response[1] != null) {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                }
            }
        });
    }
}

$('.provider-other-location').chosen({ width: "100%" });
$('.chosen-choices').addClass('form-control').css("padding", "3px 10px");

$(".bpp-attachment").click(function () {
    getAttachments();
});
$('#btnUploadFile').on('click', function (e) {
    var files = $("#txtUploadFile").prop('files')[0];
    if (files) {
        return true;
    } else {
        alert("Please select file to upload!");
        return false;
    }
});
function goBack() {
    //window.history.back();
    window.close();
}
$(document).ready(function () {
    UpdateStatus();
    $("#otherPracLocations").attr('multiple', true);
    $('#otherPracLocations').attr('data-selected-text-format', 'count');

    $('select[name="_Providers[0].Degree"]').val("@Model._Providers[0].Degree");
    $('select[name="_Providers[0].PrimarySpecialty"]').val("@Model._Providers[0].PrimarySpecialty");
    $('select[name="_Providers[0].SecondarySpecialty"]').val("@Model._Providers[0].SecondarySpecialty");
    $('select[name="_Providers[0].Outcome"]').val("@Model._Providers[0].Outcome");

    var statusExists = false;
    $("#ddlAppActions option").each(function () {
        if ($(this).val() == $("#ApplicationStatus").val()) {
            statusExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!statusExists) {
        $('#ddlAppActions option:eq(0)').prop('selected', true);
    }
    var subStatusExists = false;
    $("#ddlSubStatus option").each(function () {
        if ($(this).val() == $("#ApplicationSubStatus").val()) {
            subStatusExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!subStatusExists) {
        $('#ddlSubStatus option:eq(0)').prop('selected', true);
    }
    var memberStatusExists = false;
    $("#ddlMemberStatus option").each(function () {
        if ($(this).val() == $("#ProviderMemberStatus").val()) {
            memberStatusExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!memberStatusExists) {
        $('#ddlMemberStatus option:eq(0)').prop('selected', true);
    }

    var inactiveReasonExists = false;
    $("#ddlStatusReasons option").each(function () {
        if ($(this).val() == $("#StatusReason").val()) {
            inactiveReasonExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!inactiveReasonExists) {
        $('#ddlStatusReasons option:eq(0)').prop('selected', true);
    }


    var otherReasonExists = false;
    $("#txtOtherReason").each(function () {
        if ($(this).val() == $("#OtherReason").val()) {
            otherReasonExists = true;
            //$(this).attr("selected", "selected");
            return false;
        }
    });

    var outcomeExists = false;
    $("#ddlOutcome option").each(function () {
        if ($(this).val() == $("#existingOutcome").val()) {
            outcomeExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!outcomeExists) {
        $('#ddlOutcome option:eq(0)').prop('selected', true);
    }
    var pSpecialtyExists = false;
    $('#ddlPrimarySpecialty option').each(function () {
        if ($(this).val().trim() == $("#existingPrimarySpecialty").val().trim()) {
            pSpecialtyExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!pSpecialtyExists) {
        $('#ddlPrimarySpecialty option:eq(0)').prop('selected', true);
    }
    var sSpecialtyExists = false;
    $('#ddlSecondarySpecialty option').each(function () {
        if ($(this).val().trim() == $("#existingSecondarySpecialty").val().trim()) {
            sSpecialtyExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    var pDegreeExists = false;
    $('#ddlDegree option').each(function () {
        if ($(this).val().trim() == $("#existingDegree").val().trim()) {
            pDegreeExists = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!pDegreeExists) {
        $('#ddlDegree option:eq(0)').prop('selected', true);
    }
    if (!sSpecialtyExists) {
        $('#ddlSecondarySpecialty option:eq(0)').prop('selected', true);
    }
    //MSO Verification
    var msoverification = false;
    $('#ddlMSOVerification option').each(function () {
        if ($(this).val().trim() == $("#existingMSOVerification").val().trim()) {
            msoverification = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!msoverification) {
        $('#ddlMSOVerification option:eq(0)').prop('selected', true);
    }
    //MQ Recommendation
    var mqrecommendation = false;
    $('#ddlMQRecommendation option').each(function () {
        if ($(this).val().trim() == $("#existingMQRecommendation").val().trim()) {
            mqrecommendation = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!mqrecommendation) {
        $('#ddlMQRecommendation option:eq(0)').prop('selected', true);
    }
    //Board Outcome
    var boardoutcome = false;
    $('#ddlBoardOutcome option').each(function () {
        if ($(this).val().trim() == $("#existingBoardOutcome").val().trim()) {
            boardoutcome = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!boardoutcome) {
        $('#ddlBoardOutcome option:eq(0)').prop('selected', true);
    }
    //Provider Status
    var providerMemberStatus = false;
    $('#ddlProviderMemberStatus option').each(function () {
        if ($(this).val().trim() == $("#existingProviderMemberStatus").val().trim()) {
            providerMemberStatus = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!providerMemberStatus) {
        $('#ddlProviderMemberStatus option:eq(0)').prop('selected', true);
    }
    //Denial Reason
    var denialreason = false;
    $('#ddlDenialReason option').each(function () {
        if ($(this).val().trim() == $("#existingDenialReason").val().trim()) {
            denialreason = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!denialreason) {
        $('#ddlDenialReason option:eq(0)').prop('selected', true);
    }
    //Inactive Reason
    var inactivereason = false;
    $('#ddlInactiveReason option').each(function () {
        if ($(this).val().trim() == $("#existingInactiveReason").val().trim()) {
            inactivereason = true;
            $(this).attr("selected", "selected");
            return false;
        }
    });
    if (!inactivereason) {
        $('#ddlInactiveReason option:eq(0)').prop('selected', true);
    }

    $('#divShowHideAdmin').click(function () {
        if ($("#divShowHideAdmin").children('i').hasClass("fa-chevron-down")) {
            $("#divShowHideAdmin").children('i').removeClass("fa-chevron-down");
            $("#divShowHideAdmin").children('i').addClass("fa-chevron-up");
            $('.divAdmin').each(function (i, e) {
                $(".divAdmin").children().hide();
            });
        }
        else {
            $("#divShowHideAdmin").children('i').removeClass("fa-chevron-up");
            $("#divShowHideAdmin").children('i').addClass("fa-chevron-down");
            $('.divAdmin').each(function (i, e) {
                $(".divAdmin").children().show();
            });
        }
    });

    $('#divShowHideLoc').click(function () {
        if ($("#divShowHideLoc").children('i').hasClass("fa-chevron-down")) {
            $("#divShowHideLoc").children('i').removeClass("fa-chevron-down");
            $("#divShowHideLoc").children('i').addClass("fa-chevron-up");
            $("#divLocations").children().hide();
        }
        else {
            $("#divShowHideLoc").children('i').removeClass("fa-chevron-up");
            $("#divShowHideLoc").children('i').addClass("fa-chevron-down");
            $("#divLocations").children().show();
        }
    });

    $('#divShowHideBasic').click(function () {
        if ($("#divShowHideBasic").children('i').hasClass("fa-chevron-down")) {
            $("#divShowHideBasic").children('i').removeClass("fa-chevron-down");
            $("#divShowHideBasic").children('i').addClass("fa-chevron-up");
            $('.BasicInfo').each(function (i, e) {
                $(".BasicInfo").children().hide();
            });
        }
        else {
            $("#divShowHideBasic").children('i').removeClass("fa-chevron-up");
            $("#divShowHideBasic").children('i').addClass("fa-chevron-down");
            $('.BasicInfo').each(function (i, e) {
                $(".BasicInfo").children().show();
            });
        }
    });

    $('.bpp-date').datepicker({
        //maxDate: 0,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
    //$('.bpp-effective-date').datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    yearRange: "-100:+0",
    //    dateFormat: 'mm/dd/yy'
    //});
    if (sessionStorage.length > 0) {
        message = sessionStorage.getItem("message");
        showModal('BPP: Provider Profile Update Submission', message, true);
        sessionStorage.removeItem("message");
    }
});
function getAttachments() {
    var ID = $("#ProviderID").val();
    var PracticeID = $("#practID").val();
    var MatrixID = $("#MatrixID").val();
    $.ajax({
        type: "GET",
        url: "/api/Sitecore/Application/GetFileAttachments",
        dataType: "json",
        cache: false,
        data: { ID: ID, PracticeID: PracticeID, MatrixID: MatrixID, Mode: "2" },
        success: function (response) {
            $(".provider-profile-table").empty();
            if (response != null & response.data != null && response.data != "[]") {
                var newAttachments = "";
                newAttachments += "<ol>";
                $.each(JSON.parse(response.data), function (m, n) {
                    newAttachments += "<li><a target='_blank' href='/api/Sitecore/Application/DownloadAttachment?ID=" + this.ContentTypeID + "'>" + this.Name + "</a></li>";
                    //console.log("Doc Name - " + this.Name);
                });
                newAttachments += "</ol>";
                $(".provider-profile-table").html(newAttachments);
            }
            else
                $(".provider-profile-table").html("<ul class='list-style-none text-center'><li><b>No attachments available!</b></li></ul>");
        },
        error: function (response) {
            console.log("Error Occured : " + response.responseText);
        }
    })
}
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function UpdateStatus() {
    var EffectiveDate = document.getElementById('BPPEffectiveDate').value;
    if (EffectiveDate == "") {
        $('#ddlMSOVerification').prop('disabled', false);
        $('#ddlMSOVerification').removeAttr('background-color');

        $('#ddlMQRecommendation').prop('disabled', false);
        $('#ddlMQRecommendation').removeAttr('background-color');

        $('#ddlBoardOutcome').prop('disabled', false);
        $('#ddlBoardOutcome').removeAttr('background-color');

        $('#ddlDenialReason').prop('disabled', false);
        $('#ddlDenialReason').removeAttr('background-color');
    }
    else {
        $('#ddlMSOVerification').css('background-color', 'darkgray');
        $('#ddlMSOVerification').prop('disabled', true);

        $('#ddlMQRecommendation').css('background-color', 'darkgray');
        $('#ddlMQRecommendation').prop('disabled', true);

        $('#ddlBoardOutcome').css('background-color', 'darkgray');
        $('#ddlBoardOutcome').prop('disabled', true);

        $('#ddlDenialReason').css('background-color', 'darkgray');
        $('#ddlDenialReason').prop('disabled', true);
    }
}

function StatusChange() {
    var existingProviderMemberStatus = document.getElementById('existingProviderMemberStatus').value;
    if (existingProviderMemberStatus == "Inactive") {
        var ddlProviderMemberStatus = document.getElementById('ddlProviderMemberStatus').value;
        if (ddlProviderMemberStatus == "Active") {
            var ProviderID = document.getElementById('ProviderID').value;
            var PracticeID = document.getElementById('PracticeID').value;
            var matrixID = document.getElementById("matrix").value;
            $.ajax({
                type: "POST",
                url: "/api/Sitecore/Application/RemoveProviderEndDate",
                dataType: "json",
                data: {
                    ProviderID: ProviderID,
                    PracticeID: PracticeID,
                    MatrixID: matrixID
                },
                success: function (response) {
                    if (response != null || response == "success") {
                        document.location.reload();
                    }
                }
            });
        }
    }
}


function GeneratecloneProviderModal() {
    showModal('BPP: Provider Profile Clone', 'Are you sure you want to Clone this Provider Profile? <br/><br/> <button id="CloneProviderProfile" class="bpp-submit-btn">Clone Profile</button>', true);
}
$(document).on('click', '#CloneProviderProfile', function () {
    $("#otherPracLocations").empty();
    var myValues = $('.otherLocations').chosen().val();
    myValues.forEach(function (index, val) {
        $("#otherPracLocations").append("<input name=\"_Providers[0].OtherPracticeLocations[" + val + "]\" type=\"hidden\" value=\"" + index + "\">");
    })
    var facilities = $('#facilities :input:checked');
    var ids = {};
    facilities.each(function (index) {
        $("#facilities").append("<input name=\"_Providers[0].ProviderFacilities[" + index + "]\" type=\"hidden\" value=\"" + $(this).attr('id') + "\">");
        //console.log(index + ': ' + $(this).attr('id'));
    });

    var acceptingNewPatients = $('#acceptingNewPatients :input:checked');
    var ids = {};
    acceptingNewPatients.each(function (index) {
      $("#acceptingNewPatients").append("<input name=\"_Providers[0].ProviderAcceptingPatients[" + index + "]\" type=\"hidden\" value=\"" + $(this).attr('id') + "\">");
      //console.log(index + ': ' + $(this).attr('id'));
    });

    var valid = isValidForm($('#provProfile :input'));
    if (valid) {
        var form = $("#provProfile").serialize();
        $.ajax({
            type: "POST",
            url: "/api/Sitecore/Application/CloneProviderProfile",
            dataType: "json",
            data: form,
            success: function (response) {
                if (response != null) {
                    if (response[0] == 'success') {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                    else {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                }
                //console.log(response);
            },
            error: function (response) {
                console.log("Error Occured : " + response.responseText);
                if (response != null && response[0] == 'error') {
                    if (response[1] && response[1] != null) {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                }
            }
        });
    }
});