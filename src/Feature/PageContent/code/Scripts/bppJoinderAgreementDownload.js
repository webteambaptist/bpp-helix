﻿var SaveandContinueButton = 0;
var updateValue;

function downloadFile(downloadFileName) {
    var url = null;
    url = downloadFileName;
    window.open(downloadFileName);
    SaveandContinueButton++;
    updateValue = SaveandContinueButton;
    DisableSaveButton(updateValue);
}

function DisableSaveButton(updateValue) {
    if (updateValue == 1) {
        //Submit button is enabled for user to proceed to next step.
        document.getElementById("btnSavePracAppStep2").removeAttribute('disabled');
    }
}

$(document).ready(function () {
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
});