﻿var nextProviderID = 0;
var model;
var date = new Date();
date.setDate(date.getDate());

$(document).ready(function () {
    $('.effectivedate').datepicker();
    //$('.effectivedate').datepicker('setDate', new Date());

    $('input, textarea').focusin(function () {
        $(this).parent().find('label').removeClass('active').addClass('active');
        // added to prevent autofill inputs from browsers and animations at the same time
        $(this).parent().find('label').removeClass('animate').addClass('animate');
    });
    $('input, textarea').focusout(function () {
        if ($(this)[0].value.length == 0) {
            $(this).parent().find('label').addClass('active').removeClass('active');
            // added to prevent autofill inputs from browsers and animations at the same time
            $(this).parent().find('label').addClass('animate').removeClass('animate');
        }
    });
    $('body').on('keydown', '.phone', function (e) {
            var key = e.which || e.charCode || e.keyCode || 0;
            $phone = $(this);

            // Don't let them remove the starting '('
            if ($phone.val().length === 1 && (key === 8 || key === 46)) {
                $phone.val('(');
                return false;
            }
            // Reset if they highlight and type over first char.
            else if ($phone.val().charAt(0) !== '(') {
                $phone.val('(' + $phone.val());
            }

            // Auto-format- do not expose the mask as the user begins to type
            if (key !== 8 && key !== 9) {
                if ($phone.val().length === 4) {
                    $phone.val($phone.val() + ')');
                }
                if ($phone.val().length === 5) {
                    $phone.val($phone.val() + ' ');
                }
                if ($phone.val().length === 9) {
                    $phone.val($phone.val() + '-');
                }
            }

            // Allow numeric (and tab, backspace, delete) keys only
            return (key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        })

});
function RaisePlaceholder(id) {
    id.parent().find('label').removeClass('active').addClass('active');
    // added to prevent autofill inputs from browsers and animations at the same time
    id.parent().find('label').removeClass('animate').addClass('animate');
}
function LowerPlaceholder(id) {
    if (id[0].value.length == 0) {
        id.parent().find('label').addClass('active').removeClass('active');
        // added to prevent autofill inputs from browsers and animations at the same time
        id.parent().find('label').addClass('animate').removeClass('animate');
    }
}
$("#btnDone").click(function (e) {
    var returnValue = true;
    "use strict";
    if ($(".invalidTIN").is(":visible")) {
        return false;
    }
    for (var i = 0; i < nextProviderID+1; i++) {
        if ($("#statusChange-" + i).val() == null || $("#statusChange-" + i).val() == "" || $("#statusChange-" + i).val() == "0") {
            $('.requiredChange-' + i).show();
            return false;
        }
        else {
            var selected = $("#statusChange-" + i + ' option:selected').text();
            if (selected == "Status Change" && ($("#statusChangeReason-" + i).val() == null || $("#statusChangeReason-" + i).val() == "0" || $("#statusChangeReason-" + i).val() == ""))
            {
                $('.requiredChangeReason-' + i).show();
                return false;
            }
                
            $('.requiredChange-' + i).hide();
        }

        if ($(".InvalidProviderNPI-" + i).is(":visible")) {
            return false;
        }

        

    }
    return returnValue;
});
$('body').on('focus', ".effectivedate", function () {
   $(this).datepicker();
    //    $(this).datepicker('setDate', new Date());
});
function isJSON(data) {
    var ret = true;
    try {
        JSON.parse(data);
    } catch (e) {
        ret = false;
    }
    return ret;
}
function addSection(section, source, m) {
    if (typeof (m) != "undefined") {
        model = m;
    }
    if (!$(source).hasClass('disabled')) {
        var target = '';
        var newHtml = '';
        if (isJSON(model)) {
            model = JSON.parse(model);
        }
        
        var changes = model.Change;
        var reasons = model.Reason;
        var statuses = model.Statuses;      

        var tin = $('#tin').val();

        switch (section) {
            case "providerMulti":
                nextProviderID++;
                GetPractices(tin, nextProviderID);

                target = 'providers-multi';
                newHtml = '<div class="provider-single" id="provider-' + nextProviderID + '">' 
                newHtml += '<hr class="sectionhr">'
                newHtml += '<div class="row">'
                newHtml += '<div class="col-sm-12 col-md-12 col-lg-12">'
                newHtml += '<h3 class="provider-num" id="providernum">Provider ' + (nextProviderID + 1) + '</h4>'
                newHtml += '</div>'
                newHtml += '</div>'
                newHtml += '<div class="row">'
                newHtml += '<div class="col-sm-3 col-md-3 col-lg-3 text-center" >'
                newHtml += '<label for="ProviderNPI-' + nextProviderID + '" class="placeholder" > NPI:</label > '
                newHtml += '<input name="ProviderNPI-' + nextProviderID + '" type="text" class="form-control input adaptive ProviderNPI-' + nextProviderID + '" placeholder="" required="required" maxlength="10" minLength="10" pattern="[0-9]{10}" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));isNPIValid();"/> '
                newHtml += '<div class="InvalidProviderNPI-' + nextProviderID + '" style="display: none; color: red">Invalid NPI for TIN</div></div >'
                newHtml += '<div class="col-sm-3 col-md-3 col-lg-3 text-center" > '
                newHtml += '<label for="ProviderName-' + nextProviderID + '" class="placeholder" > Name: </label > '
                newHtml += '<input name="ProviderName-' + nextProviderID + '" type="text" class="form-control input adaptive" placeholder="" required="required" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this))"/></div>'

                newHtml += '<div class="col-sm-3 col-md-3 col-lg-3 text-center">'
                newHtml += '<p><select name="Practice-' + nextProviderID + '" data-placeholder="Select Practice" id="Practice-' + nextProviderID + '" class="form-control input adaptive" onchange="setVisibility(this.name,this.value, this)" required="required"></select></p>'
                newHtml += '</div></div><br/>'
                newHtml += '<div class="row">'
                newHtml += '<div class="col-sm-12 col-md-12 col-lg-12">'
                newHtml += '<h4 class="h2color">Status Change Information</h>'
                newHtml += '</div>'
                newHtml += '</div>'
                if (changes != null && changes.length > 0) {
                    newHtml += '<div class="row" >' 
                    newHtml += '<div class="col-sm-3 col-md-3 col-lg-3 text-center">'
                    newHtml += '<label for="effectiveDate-' + nextProviderID + '" class="placeholder" > Effective Date</label > '
                    //newHtml += '<input type="text" class="form-control effectivedate input adaptive" placeholder="" name="effectiveDate-' + nextProviderID +'" required="required" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this))"></div ></div> <br />'
                    newHtml += '<input type="text" class="form-control effectivedate input adaptive" placeholder="" name="effectiveDate-' + nextProviderID + '" required="required" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this))"></div >'

                        newHtml += '<div class="col-md-3 text-center">'
                    newHtml += '<p> <select name="statusChange-' + nextProviderID + '" data-placeholder="Select Change" id = "statusChange-' + nextProviderID + '" class="form-control input adaptive" onchange="setVisibilities($(this))" required="required"> '
                    //newHtml += '<p> <select name="statusChange-' + nextProviderID + '" id="statusChange-' + nextProviderID + '" data-placeholder="Select Change" id = "statusChange-' + nextProviderID +'" class="form-control" onchange="setVisibilities($(this))" required="required"> '
                    newHtml += '<option value="0"> Select Change</option >'
                    for (var i = 0; i < changes.length; i++) {
                        newHtml += '<option value=' + changes[i].ID + '>' + changes[i].StatusChange1 + '</option>'
                    }
                    newHtml += '</select></p>'
                    newHtml += '<div class="requiredChange-' + nextProviderID + '" style="display:none;color:red"> Status Change is Required</div ></div> '
                }
                newHtml += '<div class="col-md-3 hidden text-center lastNameChange-' + nextProviderID + '" id="lastname - ' + nextProviderID + '">'
                //newHtml += '<div class="col-md-4 text-center lastNameChange-' + nextProviderID + '" id="lastname - ' + nextProviderID + '">'
                newHtml += '<label class="placeholder" for="LastNameChange-' + nextProviderID + '">Last Name:</label><input name="LastNameChange-' + nextProviderID + '" type="text" class="form-control input adaptive" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this))"/></div>'
                if (reasons != null && reasons.length > 0) {
                    newHtml += '<div class="col-md-3 text-center statusChangeReasonDiv-' + nextProviderID + '" hidden> '
                    //newHtml += '<div class="col-md-4 text-center statusChangeReasonDiv-' + nextProviderID + '"> '
                    newHtml += '<p> <select name="statusChangeReason-' + nextProviderID + '" data-placeholder="Select Change Reason" id="statusChangeReason-' + nextProviderID + '" class="form-control input adaptive" > ' //onchange="setOtherRequired()"
                    newHtml += '<option value="0">Select Reason</option >'
                    for (var i = 0; i < reasons.length; i++) {
                        newHtml += '<option value=' + reasons[i].ID + '>' + reasons[i].StatusChangeReason1 + '</option>'
                    }
                    newHtml += '</select></p>'
                    newHtml += '<div class="requiredChangeReason-' + nextProviderID + '" style="display:none;color:red"> Change Reason is Required</div ></div ></div>'
                }
                newHtml += '<br/>'
                newHtml += '<div class="row"><div class="col-md-12 text-center hidden"><p><label for="Other-' + nextProviderID + '" class="placeholder" > Additional Notes:</label > '
                newHtml += '<textarea name="Other-' + nextProviderID + '" id="Other-' + nextProviderID + '" class="form-control textarea adaptive" placeholder="" rows="5" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this))"> '
                newHtml += '</textarea ></p></div></div> <div class="row"><div class="col-md-6 text-left">'
                newHtml += '<div class="remove-section" onclick="removeSection(\'provider-multi\', $(this))" style="display:none;">'
                newHtml += '<span class="fa fa-minus" ></span > Remove This Provider</div ></div >'
                newHtml += '<div class="col-md-6 text-center" >'
                newHtml += '<div class="add-section" onclick="addSection(\'providerMulti\', $(this),JSON.stringify(model))">'
                newHtml += '<span class="fa fa-plus" ></span> Add Another Provider</div ></div></div></div> ';
                break;
                //$("#statusChange-" + nextProviderID).attr('required', true);
                //$("#statusChangeReason-" + nextProviderID).attr('required', true);
        }     
        $('.' + target).append(newHtml);
        $('html,body').animate({
            scrollTop: $(".ProviderNPI-" + nextProviderID).offset().top - 150
        }, 'slow');
        showOneAddControl();
        showOneRemoveControl();
        setSelectTextColor();
        $('select').on('change', setSelectTextColor);
        window.scrollBy(0, $('.dynamic-section:first-child').height());
    }
}

function removeSection(target, source) {
    if (!$(source).hasClass('disabled')) {
        var div = document.getElementById("provider-" + nextProviderID); // the last div
        // remove
        div.parentNode.removeChild(div);
        nextProviderID--; //decrement the id
        
        showOneAddControl();
        showOneRemoveControl();
    }
}


function showOneAddControl() {
    $('.dynamic-section').each(function () {
        var addControls = $(this).find('.add-section');
        for (var i = 0; i < addControls.length; i++) {
            if (i == addControls.length - 1) {
                $(addControls[i]).show();
            } else {
                $(addControls[i]).hide();
            }
        }
    });
}

function showOneRemoveControl() {
    $('.dynamic-section').each(function () {
        var remControls = $(this).find('.remove-section');
        for (var i = 0; i < remControls.length; i++) {
            if (i > 0 && i == remControls.length - 1) {
                $(remControls[i]).show();
            } else {
                $(remControls[i]).hide();
            }
        }
    });
}

function setSelectTextColor() {
    $('select').each(function () {
        if (!$(this).val()) {
            $(this).css('color', '#999');
            $(this).css('font-style', 'italic');
        } else {
            $(this).css('color', '#555');
            $(this).css('font-style', 'normal');
        }
    })
}
function isTINValid() {
    var $inputs = $('#changes :input');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });
    $.ajax({
        type: "POST",
        url: "/api/Sitecore/Validation/ValidateTINReturning",
        dataType: "json",
        data: {
            TIN: values["SubmitterTIN"]
        },
        success: function (response) {
            if (response != null && response[0] == 'success') {
                $('.InvalidTIN').hide();
                GetPractices(values["SubmitterTIN"]);
                //setError("tin", "Invalid TIN");
            }
            else {
                $('.InvalidTIN').show();
                setError("tin", "Invalid TIN");
            }
            //console.log(response);
        },
        error: function (response) {
            console.log("Error Occured : " + response.responseText);
            if (response != null && response[0] == 'error') {
                $('.InvalidTIN').show();
                setError("tin", "Invalid TIN");
            }
            else {
                $('.InvalidTIN').hide();
                setError("tin", "Invalid TIN");
            }
        }
    });
}
function GetPractices(tin, dynamicID) {
    if (dynamicID == null || dynamicID == "undefined" || dynamicID == "") {
        dynamicID = 0;
    }
    //Get All Practices associated with TIN
    $.ajax({
        type: "POST",
        url: "/api/Sitecore/Changes/GetPracticeNamesforSurvey",
        dataType: "json",
        data: {
            TIN: tin
        },
        success: function (response) {
            //build out dropdown here.
            $('#Practice-' + dynamicID).html("");
            $('#Practice-' + dynamicID).append('<option value="0" disabled selected>Select Practice</option>');
            for (var i = 0; i < response.length; i++) {
                $('#Practice-' + dynamicID).append('<option value=' + response[i].PracticeGUID + '>' + response[i].PracticeName + '</option>');
            }
        }
    })
}
function isNPIValid() {
    var $inputs = $('#changes :input');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });
    var SubmitterTIN = values["SubmitterTIN"];
    var ProviderNPI = values["ProviderNPI-" + nextProviderID];
    $.ajax({
        type: "POST",
        url: "/api/Sitecore/Validation/ValidateNPI",
        dataType: "json",
        data: {
            TaxID: SubmitterTIN,
            ProviderNPI: ProviderNPI 
        },
        success: function (response) {
            if (response != null && response[0] == 'success') {
                $('.InvalidProviderNPI-' + nextProviderID).hide();
                //setError("ProviderNPI-" + nextProviderID, "Invalid NPI for TIN");
            }
            else {
                $('.InvalidProviderNPI-' + nextProviderID).show();
                setError("ProviderNPI-" + nextProviderID, "Invalid NPI for TIN");
            }
            //console.log(response);
        },
        error: function (response) {
            console.log("Error Occured : " + response.responseText);
            if (response != null && response[0] == 'error') {
                $('.InvalidProviderNPI-' + nextProviderID).show();
                setError("ProviderNPI-" + nextProviderID, "Invalid NPI for TIN");
            }
            else {
                $('.InvalidProviderNPI-' + nextProviderID).hide();
                setError("ProviderNPI-" + nextProviderID, "Invalid NPI for TIN");
            }
        }
    });
}
function setError(name, message) {
    "use strict";

    var span = $("span[data-valmsg-for=\"" + name + "\"]");
    if (span && span.length > 0) {
        $(span).html(message);
        if (message) {
            $("input[name=\"" + name + "\"]").addClass("input-validation-error");
            $(span).removeClass("field-validation-valid");
            $(span).addClass("field-validation-error");
        } else {
            $("input[name=\"" + name + "\"]").removeClass("input-validation-error");
            $(span).removeClass("field-validation-error");
            $(span).addClass("field-validation-valid");
        }
    }
}
//}
function setVisibility(name, value, everything) {
    var id = everything[value].text;
    var index = name.substr(name.length - 1);
    // add class
    // remove class
    if (id == "Name Change") {
        $(".lastNameChange-"+index).addClass('visible').removeClass('hidden');
        $(".statusChangeReasonDiv-" + index).addClass('hidden').removeClass('visible');
        $("#statusChangeReason-" + nextProviderID).attr('required', false);
    }
    else if (id == "Status Change") {
        $(".statusChangeReasonDiv-"+index).addClass('visible').removeClass('hidden');
        $(".lastNameChange-" + index).addClass('hidden').removeClass('visible');
        $("#statusChangeReason-" + nextProviderID).attr('required', true);
    }
    else {
        $(".statusChangeReasonDiv-"+index).addClass('hidden').removeClass('visible');
        $(".lastNameChange-" + index).addClass('hidden').removeClass('visible');
        $("#statusChangeReason-" + nextProviderID).attr('required', false);
    }
}
function setVisibilities(everything) {
    var name = everything[0].name;
    var index = name.substr(name.length - 1);
    var selected = $('#'+name + ' option:selected').text();

    if (selected == "Name Change") {
        $(".lastNameChange-" + index).addClass('visible').removeClass('hidden');
        $(".statusChangeReasonDiv-" + index).addClass('hidden').removeClass('visible');
        $("#statusChangeReason-" + nextProviderID).attr('required', false);
    }
    else if (selected == "Status Change") {
        $(".statusChangeReasonDiv-" + index).addClass('visible').removeClass('hidden');
        $(".lastNameChange-" + index).addClass('hidden').removeClass('visible');
        $("#statusChangeReason-" + nextProviderID).attr('required', true);
    }
    else {
        $(".statusChangeReasonDiv-" + index).addClass('hidden').removeClass('visible');
        $(".lastNameChange-" + index).addClass('hidden').removeClass('visible');
        $("#statusChangeReason-" + nextProviderID).attr('required', false);
    }
}
