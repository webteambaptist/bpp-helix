﻿var message = null;
var name = null;
var emailaddress = null;
$(document).ready(function () {
    
    $("#btnNewMemberRequest").click(function (e) {

      var v = grecaptcha.getResponse();
      if (v.length == 0) {
        e.preventDefault(); 
        return false;
      }

        //var that = this;
        var Name = $("#Name").val().toString();
        var Email = $("#Email").val().toString();
        if (Name.length === 0 || Email.length === 0) {
          e.preventDefault();
          return false;
        }
        var email = document.getElementById("Email");
        var isEmailValid = email.checkValidity();
        if (isEmailValid) {
            $("#btnNewMemberRequest").attr("disabled", false);
            $("#btnNewMemberRequest").val("Please Wait.....");
            $('.InvalidEmail').hide();
            $.ajax({
                type: "POST",
                url: "/api/Sitecore/ApplicationProcess/RequestApplication",
                dataType: "json",
                data: { Name: Name, Email: Email },
                success: function (response) {
                    if (response != null && response[0] == 'success') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                    else if (response != null && response[0] == 'error') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();       
                            sessionStorage.setItem("message", message);
                            //sessionStorage.setItem("Name", Name);
                            //sessionStorage.setItem("Email", Email);
                        }
                    }
                },
                error: function (response) {
                    console.log("Error Occured : " + response.responseText);
                    if (response != null && response[0] == 'error') {
                        if (response[1] && response[1] != null) {
                            message = response[1];
                            document.location.reload();
                            sessionStorage.setItem("message", message);
                        }
                    }
                }
            });
        }
        else {
            $('.InvalidEmail').show();
            $("#btnSaveReturningMember").attr("disabled", false);
            $("#btnSaveReturningMember").val("Submit");
        }
    });
    if (sessionStorage.length > 0) {
        message = sessionStorage.getItem("message");
       // name = sessionStorage.getItem("Name");
       // emailaddress = sessionStorage.getItem("Email");
       // document.getElementById("Name").value = name;
      //  document.getElementById("Email").value = emailaddress;
        showModal('Membership Application Submission', message, true);
        sessionStorage.removeItem("message");
     //   sessionStorage.removeItem("Name");
      //  sessionStorage.removeItem("Email");
    }
});