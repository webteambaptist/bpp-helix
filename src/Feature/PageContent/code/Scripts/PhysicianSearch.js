﻿$(function () {
    var queryResponse;

    // on submit
    $('form').on('submit', function (e) {
        e.preventDefault();

        // main query
        query = $(this).find('input[name=query]').val();

        // filters
        filters = {};
        $('#filters select').each(function () {
            filters[$(this).attr('name')] = $(this).val();
        })
        filters['gender_s'] = $("input[name='gender_s']:checked").val();

        // distance
        latlng = $('#latlng').val();
        distance = $('#distance').val();

        // sort
        sort = $('#sort').val();

        // build search object
        var searchQuery = {};
        searchQuery.Query = query != '' ? query : '*';
        searchQuery.Filters = filters;
        searchQuery.Start = 0;
        searchQuery.Rows = 10;
        searchQuery.Location = latlng;
        searchQuery.Distance = distance != '' ? distance : null;
        searchQuery.Sort = sort != '' ? sort : '';

        // query solr
        querySolr(searchQuery);
    });

    // typeahead
    $('#query, #queryMobile').autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: '/Search/PhysicianTypeahead',
                data: "Query=" + request.term,
                dataType: "json",
                success: function (data) {
                    if (data.Status != "Error") {
                        response(parseTypeahead(data));
                    }
                }
            })
        },
        minLength: 3
    }).each(function () {
        $(this).data("ui-autocomplete")._renderItem = function (ul, item) {
            var newLabel = String(item.label).replace(
                    new RegExp(this.term.replace(/([^a-z0-9]+)/gi, ''), "gi"),
                    "<strong>$&</strong>");

            return $("<li>")
                .data("ui-autocomplete-item", item)
                .append("<div>" + newLabel + "</div>")
                .appendTo(ul);
        };
    });
});

// query solr - accepts SolrQuery model
function querySolr(query) {

    $.post('/Search/PhysicianQuery', query,
        function (data) {
            if (data.Status == "Error") {
                // insert error handling
                console.log(data);
            } else {
                console.log(data);
                queryResponse = data;
                $('#specialty').html(data.Facets.specialties_pipe);
                $('#language').html(data.Facets.language_pipe);
                $('#affiliation').html(data.Facets.hospitalaffiliations_pipe);
                $('#gender').html(data.Facets.gender_s);
                $('#results').html(data.Results);
                $('#pagination').html(data.Pagination);

                // pagination
                pagination();
            }
        }).fail(function (data) {
            // insert error handling
            console.log(data);
        });

}

// parses solr response to build typeahead array
function parseTypeahead(data) {
    var json = [];
    $(data).each(function () {
        json.push({
            "label": this.Title + ' ' + this.FirstName + ' ' + this.LastName
            // "value": actual value
            // "link": url
            // "type": categorization
        });
    });

    return json;
}

// pagination
function pagination() {
    $('#pagination').twbsPagination('destroy');
    if (queryResponse.TotalHits && queryResponse.OriginalQuery.Rows) {
        total = Math.ceil(queryResponse.TotalHits / queryResponse.OriginalQuery.Rows);

        if (total > 1) {
            $('#pagination').twbsPagination({
                totalPages: total,
                startPage: queryResponse.OriginalQuery.Start + 1,
                visiblePages: 5,
                initiateStartPageClick: false,
                first: '<<',
                prev: '<',
                next: '>',
                last: '>>',
                onPageClick: function (event, page) {
                    pageQuery(page - 1);
                }
            });
        }
    }
}

// pagination page query
function pageQuery(number) {
    var query = queryResponse.OriginalQuery;
    query.Start = number;
    querySolr(query);
}