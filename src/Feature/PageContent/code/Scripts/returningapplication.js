﻿var message = null;
var tinstatus = null;
$(document).ready(function () {
    $("#btnSaveReturningMember").click(function (e) {
      var v = grecaptcha.getResponse();
      if (v.length == 0) {
        e.preventDefault(); 
        return false;
      }
        var email = document.getElementById("POCEmail");
        var tin = document.getElementById("TaxID");

        // if they didn't fill in email or tin then we don't wnat to validate anything
        if (email.length === 0 || tin.length === 0) {
          e.preventDefault(); 
          return false;
        }

        var isEmailValid = email.checkValidity();
        if (tinstatus == "InvalidTIN") {
            tin.checkValidity = false;
        }
        else {
            tin.checkValidity = true;
        }
        var isTINValidCheck = tin.checkValidity;
        if (isEmailValid) {
            if (isTINValidCheck) {
                $("#btnSaveReturningMember").attr("disabled", false);
                $("#btnSaveReturningMember").val("Please Wait.....");
                $('.InvalidEmail').hide();
                var form = $("#returningMember").serialize();
                $.ajax({
                    type: "POST",
                    url: "/api/Sitecore/ReturningMember/RequestApplication",
                    dataType: "json",
                    data: form,
                    success: function (response) {
                        if (response != null && response[0] == 'success') {
                            if (response[1] && response[1] != null) {
                                message = response[1];
                                document.location.reload();
                                sessionStorage.setItem("message", message);
                            }
                        }
                        //console.log(response);
                    },
                    error: function (response) {
                        console.log("Error Occured : " + response.responseText);
                        if (response != null && response[0] == 'error') {
                            if (response[1] && response[1] != null) {
                                message = response[1];
                                document.location.reload();
                                sessionStorage.setItem("message", message);
                            }
                        }
                    }
                });
            }
            else {
                $('.InvalidTaxID').show();
                $("#btnSaveReturningMember").attr("disabled", false);
                $("#btnSaveReturningMember").val("Submit");
            }
        }
        else {
            $('.InvalidEmail').show();
            $("#btnSaveReturningMember").attr("disabled", false);
            $("#btnSaveReturningMember").val("Submit");
        }
    });
    if (sessionStorage.length > 0) {
        message = sessionStorage.getItem("message");
        showModal('Returning Member Application - Successful Submission', message, true);
        sessionStorage.removeItem("message");
    }
});

function isTINValid() {
    var $inputs = $('#returningMember :input');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });
    var TaxID = values["_ReturningMember.TaxID"];

    if (TaxID.length == 9) {
        TaxID = TaxID[0] + TaxID[1] + '-' + TaxID[2] + TaxID[3] + TaxID[4] + TaxID[5] + TaxID[6] + TaxID[7] + TaxID[8];
    } 

    $.ajax({
        type: "POST",
        url: "/api/Sitecore/Validation/ValidateTINReturning",
        dataType: "json",
        data: {
            TIN: TaxID
        },
        success: function (response) {
            if (response != null && response[0] == 'success') {
                $('.InvalidTaxID').hide();
                tinstatus = "SuccessTIN";
            }
            else {
                $('.InvalidTaxID').show();
                tinstatus = "InvalidTIN";
            }
            //console.log(response);
        },
        error: function (response) {
            console.log("Error Occured : " + response.responseText);
            if (response != null && response[0] == 'error') {
                $('.InvalidTaxID').show();
                tinstatus = "InvalidTIN";
            }
            else {
                $('.InvalidTaxID').hide();
                tinstatus = "SuccessTIN";
            }
        }
    });
    
}

