﻿function validateZip(input, form) {
    var len = input.value.length;
    if (len < 5) {
        // disable buttons
        if (form == "appForm") {
            $("#btnSaveMulti").prop("disabled", true);
            $("#btnSaveSingle").prop("disabled", true);
        }
        if (form == "ProviderApp") {
            $("#btnSubmitPracApp").prop("disabled", true);
            $("#btnSavePracAppStep1").prop("disabled", true);
            $("#btnSavePracAppStep4").prop("disabled", true);
        }
        if (form == "PracticeProfile") {
            $("#btnRejectPractice").prop("disabled", true);
            $("#btnAcceptPractice").prop("disabled", true);
            $("#btnSave").prop("disabled", true);
            $("#btnAttach").prop("disabled", true);
            $("#btnGetReport").prop("disabled", true);
            $("#btnUpdateProviders").prop("disabled", true);
        }
        alert("Zip must be at least 5 digits");
    }
    else enableButton(input, form);
}
function enableButton(input, form) {
    var length = input.length;
    if (length >= 5) {
        if (form == "appForm") {
            $("#btnSaveMulti").prop("disabled", false);
            $("#btnSaveSingle").prop("disabled", false);
        }
        if (form == "ProviderApp")
        {
            $("#btnSubmitPracApp").prop("disabled", false);
            $("#btnSavePracAppStep1").prop("disabled", false);
            $("#btnSavePracAppStep4").prop("disabled", false);
        }
        if (form == "PracticeProfile") {
            $("#btnRejectPractice").prop("disabled", false);
            $("#btnAcceptPractice").prop("disabled", false);
            $("#btnSave").prop("disabled", false);
            $("#btnAttach").prop("disabled", false);
            $("#btnGetReport").prop("disabled", false);
            $("#btnUpdateProviders").prop("disabled", false);
        }
    }
}
function formatTaxId(e, input) {
    var id = input;
    // Ignore Backspace and Delete keys
    var deleteKey = (e.keyCode == 8 || e.keyCode == 46);
    if (deleteKey)
        return id;
    var len = id.length;
    if (len > 0) {
        // check if last character is not a number
        var lastChar = id[id.length - 1];
        if (isNaN(lastChar) && (len != 3)) {
            // strip last character and return
            id = id.slice(0, -1);
        }
    }

    if (len == 0) id = id;
    else if (len == 2) id = id + '-';
    return id;
}