var nextSinglePracticeID = 0;
var nextMultiProviderID = 0;
var nextMultiPracticeID = 0;
var addresses = [];
var multiMode = "practices";
//HTML Snippets
var stateSelectOptions = '<option value="" disabled>State</option><option value="AL">AL</option><option value="AK">AK</option><option value="AR">AR</option><option value="AZ">AZ</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DC">DC</option><option value="DE">DE</option><option value="FL" selected>FL</option><option value="GA">GA</option><option value="HI">HI</option><option value="IA">IA</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="MA">MA</option><option value="MD">MD</option><option value="ME">ME</option><option value="MI">MI</option><option value="MN">MN</option><option value="MO">MO</option><option value="MS">MS</option><option value="MT">MT</option><option value="NC">NC</option><option value="NE">NE</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NV">NV</option><option value="NY">NY</option><option value="ND">ND</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PA">PA</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VA">VA</option><option value="WA">WA</option><option value="WI">WI</option><option value="WV">WV</option><option value="WY">WY</option>';
var specialtySelectOptions = '<option value="Allergy & Immunology">Allergy & Immunology</option><option value="Allergy & Otolaryngology">Allergy & Otolaryngology</option><option value="Anesthesiology">Anesthesiology</option><option value="Breast Surgery">Breast Surgery</option><option value="Cardiology">Cardiology</option><option value="Cardiothoracic Surgery">Cardiothoracic Surgery</option><option value="Cardiovascular Disease">Cardiovascular Disease</option><option value="Clinical Cardiac Electrophysiology">Clinical Cardiac Electrophysiology</option><option value="Clinical Neurophysiology">Clinical Neurophysiology</option><option value="Colorectal Surgery">Colorectal Surgery</option><option value="Dermatology">Dermatology</option><option value="Diagnostic Radiology">Diagnostic Radiology</option><option value="Emergency Medicine">Emergency Medicine</option><option value="Emergency Medicine (Fast Track)">Emergency Medicine (Fast Track)</option><option value="Endocrinology">Endocrinology</option><option value="Facial Plastic & Reconstructive Surgery">Facial Plastic & Reconstructive Surgery</option><option value="Family Medicine">Family Medicine</option><option value="Family Medicine (Geriatrics)">Family Medicine (Geriatrics)</option><option value="Gastroenterology">Gastroenterology</option><option value="General Pediatrics-Hospitalist>General Pediatrics-Hospitalist</option><option value="General Surgery>General Surgery</option><option value="General Surgery (Vascular)>General Surgery (Vascular)</option><option value="GYN Oncology">GYN Oncology</option><option value="Gynecology">Gynecology</option><option value="Hematology/Oncology">Hematology/Oncology</option><option value="Hospitalist">Hospitalist</option><option value="Hospitalist - Family Medicine">Hospitalist - Family Medicine</option><option value="Hospitalist - Internal Medicine">Hospitalist - Internal Medicine</option><option value="Infectious Diseases">Infectious Diseases</option><option value="Internal Medicine">Internal Medicine</option><option value="Interventional Cardiology">Interventional Cardiology</option><option value="Interventional Radiology">Interventional Radiology</option><option value="Maternal-Fetal Medicine">Maternal-Fetal Medicine</option><option value="Medical Oncology">Medical Oncology</option><option value="Nephrology Neuro Oncology">Nephrology Neuro Oncology</option><option value="Neurocritical Care">Neurocritical Care</option><option value="Neurology">Neurology</option><option value="Neurology (Interventional)">Neurology (Interventional)</option><option value="Neurology (Vascular)">Neurology (Vascular)</option><option value="Neuroradiology">Neuroradiology</option><option value="Neurosurgery">Neurosurgery</option><option value="OB / GYN">OB / GYN</option><option value="Ophthalmology">Ophthalmology</option><option value="Optometrist">Optometrist</option><option value="Orthopaedic Surgery">Orthopaedic Surgery</option><option value="Orthopaedic Surgery - Spine">Orthopaedic Surgery - Spine</option><option value="Otolaryngology">Otolaryngology</option><option value="Otolaryngology - Facial Plastic Surgery">Otolaryngology - Facial Plastic Surgery</option><option value="Otolaryngology - Hand/Neck Surgery"Otolaryngology - Hand/Neck Surgery</option><option value="Pain Medicine">Pain Medicine</option><option value="Pathology"></option><option value="Pediatric Allergy/Immunology">Pediatric Allergy/Immunology</option><option value="Pediatric Anesthesiology">Pediatric Anesthesiology</option><option value="Pediatric Cardiothoracic Surgery">Pediatric Cardiothoracic Surgery</option><option value="Pediatric Emergency Medicine">Pediatric Emergency Medicine</option><option value="Pediatric Emergency Medicine (Fast Track)">Pediatric Emergency Medicine (Fast Track)</option><option value="Pediatric Endocrinology">Pediatric Endocrinology</option><option value="Pediatric Gastroenterology">Pediatric Gastroenterology</option><option value="Pediatric Genetics">Pediatric Genetics</option><option value="Pediatric Hematology/Oncology">Pediatric Hematology/Oncology</option><option value="Pediatric Hospitalist">Pediatric Hospitalist</option><option value="Pediatric Infectious Diseases">Pediatric Infectious Diseases</option><option value="Pediatric Neurology">Pediatric Neurology</option><option value="Pediatric Ophthalmology">Pediatric Ophthalmology</option><option value="Pediatric Optometry">Pediatric Optometry</option><option value="Pediatric Orthopaedics">Pediatric Orthopaedics</option><option value="Pediatric Otolaryngology">Pediatric Otolaryngology</option><option value="Pediatric Otolaryngology">Pediatric Otolaryngology</option><option value="Pediatric Psychiatry">Pediatric Psychiatry</option><option value="Pediatric Pulmonology">Pediatric Pulmonology</option><option value="Pediatric Radiology">Pediatric Radiology</option><option value="Pediatric Surgery">Pediatric Surgery</option><option value="Pediatric Urology">Pediatric Urology</option><option value="Pediatrics">Pediatrics</option><option value="Physical Medicine & Rehabilitation">Physical Medicine & Rehabilitation</option><option value="Plastic Surgery">Plastic Surgery</option><option value="Plastic Surgery (Nemours) Podiatry">Plastic Surgery (Nemours) Podiatry</option><option value="Psychiatry">Psychiatry</option><option value="Pulmonary">Pulmonary</option><option value="Radiation Oncology">Radiation Oncology</option><option value="Reproductive Endocrinology">Reproductive Endocrinology</option><option value="Rheumatology">Rheumatology</option><option value="Sports Medicine">Sports Medicine</option><option value="Surgical Oncology">Surgical Oncology</option><option value="Surgical Oncology  (ENT)">Surgical Oncology  (ENT)</option><option value="Thoracic Surgery">Thoracic Surgery</option><option value="Urology">Urology</option><option value="Vascular & Interventional Radiology">Vascular & Interventional Radiology</option><option value="Vascular Surgery">Vascular Surgery</option><option value="Wound Care">Vascular Surgery</option>';

 

$('input[type=radio][name=isMulti]').change(function () {
    $('.show-after-practices').hide();
    var value = $(this).val();
    if (value == "Yes") {
        $('.multi-specialty-form').hide();
        $('.single-specialty-form').show();
        $('input:radio[name=isMulti]').filter('[value=No]').closest('label').removeClass('checked');
        $('input:radio[name=isMulti]').filter('[value=Yes]').closest('label').addClass('checked');
    } else {
        $('.single-specialty-form').hide();
        $('.multi-specialty-form').show();
        $('input:radio[name=isMulti]').filter('[value=Yes]').closest('label').removeClass('checked');
        $('input:radio[name=isMulti]').filter('[value=No]').closest('label').addClass('checked');
    }
});
$('input[type=radio][name=isNew]').change(function () {
    $('.show-after-practices').hide();
    var value = $(this).val();
    if (value == "Yes") {
        $('.solo-provider').show();
        $('.single-specialty-form').show();
        $('.multi-specialty-form').hide();
        $('.practicedd').hide();
        $('.practicetext').show();
        // set required fields 
        $('#practicenamemulti').prop('required', true);
        $('#contactPhoneMulti').prop('required', true);
        $('#contactemailmulti').prop('required', true);
        $('#address1multi').prop('required', true);
        $('#citymulti').prop('required', true);
        $('#statemulti').prop('required', true);
        $('#zipmulti').prop('required', true);
        $('.newadd').show();
        $('.newpoc').show();
        $('input:radio[name=isMulti]').filter('[value=Yes]').prop('disabled', false);
        $('input:radio[name=isMulti]').filter('[value=Yes]').prop('checked', true);
        $('input:radio[name=isNew]').filter('[value=Yes]').closest('label').addClass('checked');
        $('input:radio[name=isMulti]').filter('[value=Yes]').closest('label').addClass('checked');
        $('input:radio[name=isNew]').filter('[value=No]').closest('label').removeClass('checked');
        $('input:radio[name=isMulti]').filter('[value=No]').closest('label').removeClass('checked');
        /* Hide drop down and display text box instead */
        
    } else {
        $('.solo-provider').show();
        $('.single-specialty-form').hide();
        $('.multi-specialty-form').show();
        $('.practicedd').show();
        $('.practicetext').hide();
        $('.newadd').hide();
        $('.newpoc').hide();
        $('#practicenamemulti').prop('required', false);
        $('#contactPhoneMulti').prop('required', false);
        $('#contactemailmulti').prop('required', false);
        $('#address1multi').prop('required', false);
        $('#citymulti').prop('required', false);
        $('#statemulti').prop('required', false);
        $('#zipmulti').prop('required', false);
        $('input:radio[name=isMulti]').filter('[value=Yes]').prop('disabled', true);
        $('input:radio[name=isMulti]').filter('[value=No]').prop('checked', true);
        $('input:radio[name=isNew]').filter('[value=Yes]').closest('label').removeClass('checked');
        $('input:radio[name=isMulti]').filter('[value=Yes]').closest('label').removeClass('checked');
        $('input:radio[name=isNew]').filter('[value=No]').closest('label').addClass('checked');
        $('input:radio[name=isMulti]').filter('[value=No]').closest('label').addClass('checked');

        
    }
})

$('.toggle-active').click(function () {
    if (multiMode == 'practices') {

        //var inputs = [];
        var index = 0;
        var valid = true;
        var isNew = $('input:radio[name=isNew]:checked').val();
        if (isNew == "Yes") {
            $('.practices-multi.dynamic-section input').each(function () {
                //If any field other than an address2 field does not have a value, Practice info is not valid
                if ($(this).attr('name').indexOf("address2") == -1 && $(this).val() == '') {
                    valid = false;
                }
            });
        }
        //console.log(inputs);

        if (valid == true) {
            multiMode = 'providers';

            $('.practices-multi.dynamic-section').find('input').addClass('disabled');
            $('.practices-multi.dynamic-section').find('select').addClass('disabled');
            $('.practices-multi.dynamic-section .add-section').addClass('disabled');
            $('.practices-multi.dynamic-section .remove-section').addClass('disabled');
            disableInputs();

            addresses = [];
            var index = 0;
            $('.practice-multi').each(function () {
                var address2 = $(this).find('.address2-multi').val() != "" ? " " + $(this).find('.address2-multi').val() : "";
                addresses[index] = $(this).find('.address1-multi').val() + address2 + ", " + $(this).find('.city-multi').val() +
				" " + $(this).find('.state-multi').val() + " " + $(this).find('.zip-multi').val();
                index++;
            });

            // Primary Address
            $('.provider-location option').remove();
            if (addresses.length > 1) {
                $('.provider-location').append('<option value="" disabled="true" selected>Select Locaton</option>');
            }
            for (var i = 0; i < addresses.length; i++) {
                $('.provider-location').append('<option value=' + (i + 1) + '>' + addresses[i] + '</option>');
            }

            //Other Addresses
            $('.provider-other-location').chosen('destroy');
            $('.provider-other-location option').remove();
            if (addresses.length > 1) {
                $('.provider-other-location').append('<option value="" disabled="true">Select Locaton</option>');
            }
            for (var i = 0; i < addresses.length; i++) {
                $('.provider-other-location').append('<option value=' + (i + 1) + '>' + addresses[i] + '</option>');
            }
            $('.provider-other-location').chosen({ width: "100%" });
            $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");


            $('.show-after-practices').show();

            $('.toggle-message').removeClass('error');
            $('.toggle-message').text('Need to update Practice information?');
            $(this).text('Edit Practices');
        } else {
            $('.toggle-message').text('Please fill out all fields above');
            $('.toggle-message').addClass('error');
        }
    } else {
        multiMode = 'practices';

        $('.practices-multi.dynamic-section').find('input').removeClass('disabled');
        $('.practices-multi.dynamic-section').find('select').removeClass('disabled');
        $('.practices-multi.dynamic-section .add-section').removeClass('disabled');
        $('.practices-multi.dynamic-section .remove-section').removeClass('disabled');
        $('.show-after-practices').hide();

        $('.toggle-message').text('Finished adding Practices?');
        $(this).text('Add Providers');
    }
})
function resetOtherLocations(e) {
    newPrimLocID = e.value;
    var otherLocID = e.name.replace('providerLocationMulti', '#providerOtherLocationsMulti');
    $(otherLocID).chosen('destroy');
    $(otherLocID + ' option').remove();
    if (addresses.length > 1) {
        $(otherLocID).append('<option value="" disabled="true">Select Location</option>');
        for (var i = 0; i < addresses.length; i++) {
            if ((i + 1) != newPrimLocID)
                $(otherLocID).append('<option value=' + (i + 1) + '>' + addresses[i] + '</option>');
        }
        $(otherLocID).chosen({ width: "100%" });
        $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
    }
}

$(document).ready(function () {
    
    if (formResponse) {
        showModal('BPP : Membership Application Request Submission', formResponse, true);
    }
    $('select').on('change', setSelectTextColor);
    setSelectTextColor();

    $('.multi-specialty-form').hide();
    //$('.single-specialty-form').show();
    $('.single-specialty-form').show();
    $('.solo-provider').show();
    $('.new-applicant').show();
    $('.practicedd').hide();
    $('.practicetext').show();
    
   
});
// Function to add addition fields to the form
function addSection(section, source) {
    if (!$(source).hasClass('disabled')) {
        var target = '';
        var newHtml = '';
        var checkProviderNum = false;
        var setupLocations = true;
        var OtherLocId = "";
        switch (section) {
            case "practiceMulti":
                nextMultiPracticeID++;
                target = 'practices-multi';
                newHtml = '<div class="practice-multi"><hr><div class="row"><div class="col-md-6"><input name="contactPhoneMulti-' + nextMultiPracticeID + '" type="text" title="Numbers Only" placeholder="Contact Phone # (Numbers Only)" minlength="10" pattern="\\d+" class="form-control" required/></div><div class="col-md-6"><input name="contactEmailMulti-' + nextMultiPracticeID + '" type="email" placeholder="Contact Email" class="form-control" title="xxx@xxx.xxx" pattern="[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+\\.([a-zA-Z0-9-]+)*" required/></div></div><div class="row"><div class="col-md-3"><input name="address1Multi-' + nextMultiPracticeID + '" type="text" placeholder="Address1" class="form-control address1-multi" required/></div><div class="col-md-3"><input name="address2Multi-' + nextMultiPracticeID + '" type="text" placeholder="Address2" class="form-control address2-multi"/></div><div class="col-md-2"><input name="cityMulti-' + nextMultiPracticeID + '" type="text" placeholder="City" class="form-control city-multi" required/></div><div class="col-md-2"><select name="stateMulti-' + nextMultiPracticeID + '" type="text" class="form-control state-multi" required>' + stateSelectOptions + '</select></div><div class="col-md-2"><input name="zipMulti-' + nextMultiPracticeID + '" type="text" placeholder="Zip" maxlength="5" pattern="\\d+" class="form-control zip-multi" required/></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="remove-section" onclick="removeSection(\'practice-multi\', $(this))"><span class="fa fa-minus"></span> Remove Practice Location</div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="add-section" onclick="addSection(\'practiceMulti\', $(this))"><span class="fa fa-plus"></span> Add Practice Location</div></div></div></div>';
                break;
            case "providerMulti":
                nextMultiProviderID++
                target = 'providers-multi';
                checkProviderNum = true;

                // Build Primary Location Selection Dropdown HTML
                //console.log(addresses);
                var locationSelectHTML = '<div class="row"><div class="col-md-12"><div class="form-group"><label class="question-label">Provider\'s Primary Practice Location</label><select class="provider-location form-control" name="providerLocationMulti-' + nextMultiProviderID + '" required>';
                if (addresses.length > 1) {
                    locationSelectHTML += '<option value="" disabled>Select Locaton</option>';
                }
                for (var i = 0; i < addresses.length; i++) {
                    locationSelectHTML += '<option value=' + (i + 1) + '>' + addresses[i] + '</option>';
                }
                locationSelectHTML += '</select></div></div></div>';
                //console.log(locationSelectHTML);

                // Build Other Location Selection Dropdown HTML
                //console.log(addresses);
                OtherLocId = "providerOtherLocationsMulti-" + nextMultiProviderID;
                var otherLocationSelectHTML = '<div class="row"><div class="col-md-12"><div class="form-group"><label class="question-label">Provider\'s Other Locations</label><select class="provider-other-location form-control chosen-select" data-placeholder="Select Other Locations" multiple id="' + OtherLocId + '" name="' + OtherLocId + '">';
                if (addresses.length > 1) {
                    otherLocationSelectHTML += '<option value="" disabled>Select Other Locatons</option>';
                }
                for (var i = 0; i < addresses.length; i++) {
                    otherLocationSelectHTML += '<option value=' + (i + 1) + '>' + addresses[i] + '</option>';
                }
                otherLocationSelectHTML += '</select></div></div></div>';
                //console.log(otherLocationSelectHTML);
                /*' + */

                newHtml = '<div class="provider-multi"><h4 class="provider-num">Provider ' + (nextMultiProviderID + 1) + '</h4><div class="row"><div class="col-md-6"><input name="firstNameMulti-' + nextMultiProviderID + '" type="text" placeholder="First Name" class="form-control" required/></div><div class="col-md-6"><input name="lastNameMulti-' + nextMultiProviderID + '" type="text" placeholder="Last Name" class="form-control" required/></div></div><div class="row"><div class="col-md-6"><select name="primarySpecialtyMulti-' + nextMultiProviderID + '" class="form-control"><option value="" selected>Primary Specialty</option>' + specialtySelectOptions + '</select></div><div class="col-md-6"><select name="secondarySpecialtyMulti-' + nextMultiProviderID + '" class="form-control"><option value="" selected>Secondary Specialty (If Applicable)</option>' + specialtySelectOptions + '</select></div></div><div class="row"><div class="col-md-6"><input name="npiNumberMulti-' + nextMultiProviderID + '" type="text" placeholder="Provider NPI #" class="form-control" maxlength="10" pattern="[0-9]{10}" required title="NPI Must be exactly 10 digits long (numbers only)"></div><div class="col-md-6"><input name="emailMulti-' + nextMultiProviderID + '" type="email" placeholder="Provider Email" class="form-control" title="xxx@xxx.xxx" pattern="[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+\\.([a-zA-Z0-9-]+)*" required></div></div><div class="row"><div class="col-md-6"><p class="question-label">Board Certified/Eligible?</p><div class="app-form-radios"><label class="radio-inline checked"><input type="radio" value="Yes" name="certifiedMulti-' + nextMultiProviderID + '" checked="checked"/>Yes</label><label class="radio-inline"><input type="radio" value="No" name="certifiedMulti-' + nextMultiProviderID + '"/>No</label></div></div><div class="col-md-6"><p class="question-label">Maintain Privileges at a Baptist Health Facility?</p><div class="app-form-radios"><label class="radio-inline checked"><input type="radio" value="Yes" name="privilegesMulti-' + nextMultiProviderID + '" checked="checked"/>Yes</label><label class="radio-inline"><input type="radio" value="No" name="privilegesMulti-' + nextMultiProviderID + '"/>No</label></div></div></div>' + locationSelectHTML + otherLocationSelectHTML + '<div class="row"><div class="col-md-6"><div class="remove-section" onclick="removeSection(\'provider-multi\', $(this))"><span class="fa fa-minus"></span> Remove This Provider</div></div><div class="col-md-6"><div class="add-section" onclick="addSection(\'providerMulti\', $(this))"><span class="fa fa-plus"></span> Add Another Provider</div></div></div></div>';
                break;
            case "practiceSingle":
                nextSinglePracticeID++;
                target = 'practices-single';
                newHtml = '<div class="practice-single"><hr><div class="row"><div class="col-md-6"><input name="practiceEmailSingle-' + nextSinglePracticeID + '" type="email" placeholder="Contact Email" class="form-control" title="xxx@xxx.xxx" pattern="[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+\\.([a-zA-Z0-9-]+)*" required/></div><div class="col-md-6"><input name="practicePhoneSingle-' + nextSinglePracticeID + '" type="text" title="Numbers Only" placeholder="Contact Phone # (Numbers Only)" minlength="10" pattern="\\d+" class="form-control" required/></div></div><div class="row"><div class="col-md-3"><input name="practiceAddress1Single-' + nextSinglePracticeID + '" type="text" placeholder="Address1" class="form-control" required/></div><div class="col-md-3"><input name="practiceAddress2Single-' + nextSinglePracticeID + '" type="text" placeholder="Address2" class="form-control"/></div><div class="col-md-2"><input name="practiceCitySingle-' + nextSinglePracticeID + '" type="text" placeholder="City" class="form-control" required/></div><div class="col-md-2"><select name="practiceStateSingle-' + nextSinglePracticeID + '" type="text" class="form-control state-multi" required>' + stateSelectOptions + '</select></div><div class="col-md-2"><input name="practiceZipSingle-' + nextSinglePracticeID + '" type="text" placeholder="Zip" maxlength="5" pattern="\\d+" class="form-control" required/></div></div><div class="row"><div class="col-md-6 col-sm-6 col-xs-12"><div class="remove-section" onclick="removeSection(\'practice-single\', $(this))"><span class="fa fa-minus"></span> Remove This Location</div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="add-section" onclick="addSection(\'practiceSingle\', $(this))"><span class="fa fa-plus"></span> Add Another Location</div></div></div></div>';
                break;
        }
        $('.' + target).append(newHtml);
        showOneAddControl();
        showOneRemoveControl();
        setSelectTextColor();
        $('select').on('change', setSelectTextColor);
        if (checkProviderNum) {
            var $providers = $('.provider-num');

            for (var i = 0; i < $providers.length; i++) {
                $($providers[i]).text('Provider ' + (i + 1));
            }
        }
        window.scrollBy(0, $('.dynamic-section:first-child').height());
        if (OtherLocId && OtherLocId != "") {
            OtherLocId = '#' + OtherLocId;
            $(OtherLocId).chosen({ width: "100%" });
            $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
        }
    }
}

function removeSection(target, source) {
    if (!$(source).hasClass('disabled')) {
        if (target == "practice-multi") {
            nextMultiPracticeID--;
        } else if (target == "provider-multi") {
            nextMultiProviderID--;
        } else if (target == "practice-single") {
            nextSinglePracticeID--;
        }

        $(source).closest('.' + target).remove();
        showOneAddControl();
        showOneRemoveControl();
        var $providers = $('.provider-num');
        for (var i = 0; i < $providers.length; i++) {
            $($providers[i]).text('Provider ' + (i + 1));
        }
    }
}

function showOneAddControl() {
    $('.dynamic-section').each(function () {
        var addControls = $(this).find('.add-section');
        for (var i = 0; i < addControls.length; i++) {
            if (i == addControls.length - 1) {
                $(addControls[i]).show();
            } else {
                $(addControls[i]).hide();
            }
        }
    });
}

function showOneRemoveControl() {
    $('.dynamic-section').each(function () {
        var remControls = $(this).find('.remove-section');
        for (var i = 0; i < remControls.length; i++) {
            if (i > 0 && i == remControls.length - 1) {
                $(remControls[i]).show();
            } else {
                $(remControls[i]).hide();
            }
        }
    });
}

function disableInputs() {
    $('.disabled').on('keydown paste', function (e) {
        if ($(this).hasClass('disabled')) {
            e.preventDefault();
        }
    });
}

function setSelectTextColor() {
    $('select').each(function () {
        if (!$(this).val()) {
            $(this).css('color', '#999');
            $(this).css('font-style', 'italic');
        } else {
            $(this).css('color', '#555');
            $(this).css('font-style', 'normal');
        }
    })
}
function onChangePractice()
{
    var val = $(".practicedd option:selected").text();
    $.ajax({
        url: "/api/Sitecore/Application/getTaxId",
        type: 'GET',
        dataType: 'json',
        cache: false,
        data: { practiceName: val.toString() },
        success: function (response) {
            $("#taxidmulti").val(response.data);
        }
    });
}
