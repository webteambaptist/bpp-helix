﻿var nextMultiProviderID = 0;
var addresses = [];
var model;
var api = null;
var prevPrimLocID;

$('#ProviderPrimaryLocationMulti-0').on('focus', function (event, ui) {
    prevPrimLocID = this.value;
}).change(function (e) {
    resetOtherLocations(e);
});
function resetOtherLocations(e) {
    element = $(e.target);
    value = element.val();
    $("#hdnPrimaryPracticeLocation-" + nextMultiProviderID).val(value);
    clone = element.clone();
    clone.find('option[value=' + value + ']').prop('disabled', "true");
    currentSelectedOtherLoc = $(".otherLocations").chosen().val();
    $(".otherLocations").html(clone.children());
    $(".otherLocations").val(currentSelectedOtherLoc).trigger('chosen:updated');
    $(".otherLocations").chosen({ width: "100%" });
    $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
}
$('.provider-other-location').chosen({ width: "100%" });
$('.chosen-choices').addClass('form-control').css("padding", "3px 10px");

function PrimaryFocusEvent(event) {
    prevPrimLocID = event[0].id;
}
function PrimaryChangeEvent(event) {
    value = event.val();
    $("#hdnPrimaryPracticeLocation-" + nextMultiProviderID).val(value);
    clone = event.clone();
    clone.find('option[value=' + value + ']').prop('disabled', "true");
    currentSelectedOtherLoc = $(".otherLocations-" + nextMultiProviderID).chosen().val();
    $(".otherLocations-" + nextMultiProviderID).html(clone.children());
    $(".otherLocations-" + nextMultiProviderID).val(currentSelectedOtherLoc).trigger('chosen:updated');
    $(".otherLocations-" + nextMultiProviderID).chosen({ width: "100%" });
    $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
}

function isJSON(data) {
    var ret = true;
    try {
        JSON.parse(data);
    } catch (e) {
        ret = false;
    }
    return ret;
}
function addSection(section, source, m) {
    if (typeof (m) != "undefined") {
        model = m;
    }
    if (!$(source).hasClass('disabled')) {
        var target = '';
        var newHtml = '';
        var checkProviderNum = false;
        var OtherLocId = "";

        if (isJSON(model)) {
            model = JSON.parse(model);
        }

        var degrees = model._Degrees;
        var specialties = model._Specialties;
        var practicesNames = model._Practices;
        var locations = model._Practices[0].PracticeLocations;
        var DefaultEmailSetSettings = $('#SetDefaultEmail').val();

        switch (section) {
            case "providerMulti":
                nextMultiProviderID++
                target = 'providers-multi';
                checkProviderNum = true;

                newHtml = '<div class="provider-multi">'
                newHtml += '<span><hr></span>'
                newHtml += '<h4 class="provider-num">Provider' + (nextMultiProviderID + 1) + '</h4>'
                newHtml += '<div class="row">'
                //First Name
                newHtml += '<div class="col-md-3">'
                newHtml += '<label for="ProviderfirstNameMulti-' + nextMultiProviderID + '" class="placeholder">First Name</label>'
                newHtml += '<input id="ProviderfirstNameMulti-' + nextMultiProviderID + '" name="ProviderfirstNameMulti-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive ProviderfirstNameMulti-' + nextMultiProviderID + '" required="required" title="Providers First Name" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                //Middle Initial
                newHtml += '<div class="col-md-1">'
                newHtml += '<label for="ProvidermiddleInitial-' + nextMultiProviderID + '" class="placeholder">MI</label>'
                newHtml += '<input name="ProvidermiddleInitial-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive" title="Providers Middle Initial" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                //Last Name
                newHtml += '<div class="col-md-3">'
                newHtml += '<label for="ProviderlastNameMulti-' + nextMultiProviderID + '" class="placeholder">Last Name</label>'
                newHtml += '<input name="ProviderlastNameMulti-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive" required="required" title="Providers Last Name" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                // Suffix
                newHtml += '<div class="col-md-1">'
                newHtml += '<label class="placeholder">Suffix</label>'
                newHtml += '<input name="ProviderSuffix-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive" title="Suffix" />'
                newHtml += '</div >'
                //Also Known As
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="ProviderAlsoKnownAs-' + nextMultiProviderID + '" class="placeholder">Also Known As</label>'
                newHtml += '<input name="ProviderAlsoKnownAs-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive" title="Provider is Also Known As" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                //Degree
                if (degrees != null && degrees.length > 0) {
                    newHtml += '<div class="col-md-2">'
                    newHtml += '<select name="ProviderDegree-' + nextMultiProviderID + '" class="form-control input adaptive" required="required" title="Degree" GetDegreeSelected($(this))>'
                    newHtml += '<option value="">Degrees</option>'
                    for (var i = 0; i < degrees.length; i++) {
                        newHtml += '<option value=' + degrees[i].DegreeID + '>' + degrees[i].Degree + '</option>'
                    }
                    newHtml += '</select>'//end select
                    newHtml += '</div>'//end div
                }
                newHtml += '</div>'//end row

                newHtml += '<div class="row">'
                //NPI #
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="ProvidernpiNumber-' + nextMultiProviderID + '" class="placeholder">NPI</label>'
                newHtml += '<input name="ProvidernpiNumber-' + nextMultiProviderID + '" id="npimulti" type="text" placeholder="" class="form-control input adaptive" maxlength="10" pattern="[0-9]{10}" required title="NPI Must be exactly 10 digits long (numbers only)" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));validateNpi($(this))""/> '
                newHtml += '<div id="npiValid-' + nextMultiProviderID + '" style="display: none;>'
                newHtml += '<p style="color:green !important;">NPI is valid</p>'
                newHtml += '</div>'
                newHtml += '<div id="npiInValid-' + nextMultiProviderID + '" style="display: none;>'
                newHtml += '<p style="color:red !important;">NPI is not valid</p>'
                newHtml += '</div>'
                newHtml += '</div>'//end div
                //Personal Email
                if (DefaultEmailSetSettings == "enabled") {
                    newHtml += '<div class="col-md-3">'
                    newHtml += '<label for="ProviderPersonalEmail-' + nextMultiProviderID + '" class="placeholder">Email</label>'
                    newHtml += '<input name="ProviderPersonalEmail-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive" title="Personal Email (xxx) before alias" pattern="[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~.-]*" required="required" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                    newHtml += '<input type="hidden" value="enabled" name="SetDefaultEmail-' + nextMultiProviderID + '" />'
                    newHtml += '</div>'//end div
                }
                else {
                    newHtml += '<div class="col-md-3">'
                    newHtml += '<label for="ProviderPersonalEmail-' + nextMultiProviderID + '" class="placeholder">Email</label>'
                    newHtml += '<input name="ProviderPersonalEmail-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive" title="Personal Email (xxx@xxx.xxx)" pattern="[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+\\.([a-zA-Z0-9-]+)*" required="required" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                    newHtml += '<input type="hidden" value="disabled" name="SetDefaultEmail-' + nextMultiProviderID + '" />'
                    newHtml += '</div>'//end div
                }
                //Personal Phone #
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="ProviderPersonalPhone-' + nextMultiProviderID + '" class="placeholder">Phone</label>'
                newHtml += '<input name="ProviderPersonalPhone-' + nextMultiProviderID + '" type="text" placeholder="" class="form-control input adaptive phone" required="required" title="Personal Phone # (numbers only)" maxlength=14 onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                //// Practice Names
                //newHtml += '<div class="col-md-6">'
                //newHtml += '<div class="form-group">'
                //if (practicesNames != null && practicesNames.length > 0) {
                //    newHtml += '<select id="PracticeNames-' + nextMultiProviderID + '" name="PracticeNames-' + nextMultiProviderID + '" class="provider-location form-control input adaptive" title="Primary Location" required="required" onchange="SelectPractice($(this))" onfocus="PrimaryFocusEvent($(this));" onchange="PrimaryChangeEvent($(this));">'
                //    newHtml += '<option value="" disabled selected>Practice Names</option>'
                //    for (var pn = 0; pn < practicesNames.length > 0; pn++) {
                //        newHtml += '<option value=' + practicesNames[pn].PracticeGUID + '>' + practicesNames[pn].PracticeName + '</option>'
                //    }
                //    newHtml += '</select>'
                //    newHtml += '</div>'
                //    newHtml += '</div>'
                //}
                newHtml += '</div>'//end row

                newHtml += '<div class="row">'
                //Primary Specialty
                if (specialties != null && specialties.length > 0) {
                    newHtml += '<div class="col-md-6">'
                    newHtml += '<select name="ProviderprimarySpecialtyMulti-' + nextMultiProviderID + '" class="form-control input adaptive" required="required" title="Primary Specialties">'
                    newHtml += '<option value="">Primary Specialty</option>'
                    for (var i = 0; i < specialties.length; i++) {
                        newHtml += '<option value=' + specialties[i].ID + '>' + specialties[i].Specialty + '</option>'
                    }
                    newHtml += '</select>'//end select
                    newHtml += '</div>'//end div
                }
                //Secondary Specialty
                if (specialties != null && specialties.length > 0) {
                    newHtml += '<div class="col-md-6">'
                    newHtml += '<select name="ProvidersecondarySpecialtyMulti-' + nextMultiProviderID + '" class="form-control input adaptive" title="Seconday Specialties (If Applicable)">'
                    newHtml += '<option value="" selected>Secondary Specialty (If Applicable)</option>'
                    for (var i = 0; i < specialties.length; i++) {
                        newHtml += '<option value=' + specialties[i].ID + '>' + specialties[i].Specialty + '</option>'
                    }
                    newHtml += '</select>'//end select
                    newHtml += '</div>'//end div
                }
                newHtml += '</div>'//end row

                newHtml += '<div class="row">'
                newHtml += '<div class="col-md-6">'
                newHtml += '<div class="form-group">'
                if (practicesNames != null && practicesNames.length > 0) {
                    newHtml += '<select id="PracticeNames-' + nextMultiProviderID + '" name="PracticeNames-' + nextMultiProviderID + '" class="provider-location form-control input adaptive" title="Primary Location" required="required" onchange="SelectPractice($(this))" onfocus="PrimaryFocusEvent($(this));" onchange="PrimaryChangeEvent($(this));">'
                    newHtml += '<option value="" disabled selected>Practice Names</option>'
                    for (var pn = 0; pn < practicesNames.length > 0; pn++) {
                        newHtml += '<option value=' + practicesNames[pn].PracticeGUID + '>' + practicesNames[pn].PracticeName + '</option>'
                    }
                    newHtml += '</select>'
                    newHtml += '</div>'
                    newHtml += '</div>'
                }

                newHtml += '</div>'//end row

                //SelectPractice();
                ////Primary Practice Location
                newHtml += '<div class="row">'
                newHtml += '<div class="col-md-12">'
                newHtml += '<div class="form-group PrimaryLocations-' + nextMultiProviderID + '" style="display: none;">'
                newHtml += '<label class="question-label"> Primary Location</label>'
                //if (locations != null && locations.length > 0)
                //{
                newHtml += '<input id="hdnPrimaryPracticeLocation-' + nextMultiProviderID + '" name="_Providers[' + nextMultiProviderID + '].PrimaryPracticeLocation" type="hidden">'
                newHtml += '<select id="ProviderPrimaryLocationMulti-' + nextMultiProviderID + '" name="ProviderPrimaryLocationMulti-' + nextMultiProviderID + '" class="provider-location form-control" title="Primary Location" required="required" onfocus="PrimaryFocusEvent($(this));" onchange="PrimaryChangeEvent($(this));">'
                newHtml += '<option value="" disabled selected> Select Primary Location</option>'
                ////    for (var l = 0; l < locations.length; l++) {
                ////        newHtml += '<option value=' + locations[l].ID + '>' + locations[l].LocationName + " " + '--' + " " + locations[l].Address1 + " " + locations[l].City + ',' + " " + locations[l].State + " " + locations[l].Zip + " " + locations[l].Address2 + '</option>'
                ////    }
                newHtml += '</select>'
                newHtml += '</div>'
                newHtml += '</div>'
                newHtml += '</div>'
                //}
                OtherLocId = "ProviderOtherLocationMulti-" + nextMultiProviderID;
                var otherLocationSelectHTML = '<div class="row"><div class="col-md-12"><div class="form-group OtherLocations-' + nextMultiProviderID + '" style="display: none;"><label class="question-label">Other Practice Locations</label>'
                otherLocationSelectHTML += '<select id="' + OtherLocId + '" name="' + OtherLocId + '" class="provider-other-location form-control chosen-select otherLocations-' + nextMultiProviderID + '" data-placeholder="Select Other Locations" multiple> ';
                //if (locations.length > 1) {
                otherLocationSelectHTML += '<option value="" disabled>Select Other Locatons</option>';
                //}
                //for (var i = 0; i < locations.length; i++) {
                //    otherLocationSelectHTML += '<option value=' + locations[i].ID + '>' + locations[i].LocationName + " " + '--' + " " + locations[i].Address1 + " " + locations[i].City + ',' + " " + locations[i].State + " " + locations[i].Zip + " " + locations[i].Address2 + '</option>'
                //}
                otherLocationSelectHTML += '</select></div></div></div><br/>';
                newHtml += otherLocationSelectHTML;

                newHtml += '<div class="row">'
                //Medical Staff Privileges at a Baptist Health Facility
                newHtml += '<div class="col-md-6">'
                newHtml += '<p class="question-label">Medical Staff Privileges at a Baptist Health Facility?</p>'
                newHtml += '<div class="app-form-radios">'
                newHtml += '<label class="radio-inline checked">'
                newHtml += '<input type="radio" value="Yes" name="ProviderprivilegesMulti-' + nextMultiProviderID + '" checked="checked" />Yes'
                newHtml += '</label>'
                newHtml += '<label class="radio-inline checked">'
                newHtml += '<input type="radio" value="No" name="ProviderprivilegesMulti-' + nextMultiProviderID + '" />No'
                newHtml += '</label>'
                newHtml += '</div>'//end app-form-radios
                newHtml += '</div>'//end div
                newHtml += '</div>'//end row

                newHtml += '<div class="row">'
                //Remove Section
                newHtml += '<div class="col-md-6">'
                newHtml += '<div class="remove-section" onclick="removeSection(\'provider-multi\', $(this))" style="display:none;">'
                newHtml += '<span class="fa fa-minus"></span> Remove This Provider'
                newHtml += '</div>'//end remove-section
                newHtml += '</div>'//end div
                //Add Section
                newHtml += '<div class="col-md-6">'
                newHtml += '<div class="add-section" onclick="addSection(\'providerMulti\', $(this), JSON.stringify(model))">'
                newHtml += '<span class="fa fa-plus"></span> Add Another Provider'
                newHtml += '</div>'//end add-section
                newHtml += '</div>'//end div
                newHtml += '</div>'//end row

                newHtml += '</div>'//end container
                break;
        }
        $('.' + target).append(newHtml);
        $('html,body').animate({
            scrollTop: $(".ProviderfirstNameMulti-" + nextMultiProviderID).offset().top - 150
        }, 'slow');
        showOneAddControl();
        showOneRemoveControl();
        setSelectTextColor();
        $('select').on('change', setSelectTextColor);
        if (checkProviderNum) {
            var $providers = $('.provider-num');

            for (var i = 0; i < $providers.length; i++) {
                $($providers[i]).text('Provider ' + (i + 1));
            }
        }
        window.scrollBy(0, $('.dynamic-section:first-child').height());

        if (OtherLocId && OtherLocId != "") {
            OtherLocId = '#' + OtherLocId;
            $(OtherLocId).chosen({ width: "100%" });
            $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
        }

    }
}
function removeSection(target, source) {
    if (!$(source).hasClass('disabled')) {
        if (target == "practice-multi") {
            nextMultiPracticeID--;
        } else if (target == "provider-multi") {
            nextMultiProviderID--;
        } else if (target == "practice-single") {
            nextSinglePracticeID--;
        }

        $(source).closest('.' + target).remove();
        showOneAddControl();
        showOneRemoveControl();
        if (nextMultiProviderID == 0) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
        else {
            $('html,body').animate({
                scrollTop: $(".ProviderfirstNameMulti-" + nextMultiProviderID).offset().top - 150
            }, 'slow');
        }
        var $providers = $('.provider-num');
        for (var i = 0; i < $providers.length; i++) {
            $($providers[i]).text('Provider ' + (i + 1));
        }
    }
}

function showOneAddControl() {
    $('.dynamic-section').each(function () {
        var addControls = $(this).find('.add-section');
        for (var i = 0; i < addControls.length; i++) {
            if (i == addControls.length - 1) {
                $(addControls[i]).show();
            } else {
                $(addControls[i]).hide();
            }
        }
    });
}

function showOneRemoveControl() {
    $('.dynamic-section').each(function () {
        var remControls = $(this).find('.remove-section');
        for (var i = 0; i < remControls.length; i++) {
            if (i > 0 && i == remControls.length - 1) {
                $(remControls[i]).show();
            } else {
                $(remControls[i]).hide();
            }
        }
    });
}
function disableInputs() {
    $('.disabled').on('keydown paste', function (e) {
        if ($(this).hasClass('disabled')) {
            e.preventDefault();
        }
    });
}

function setSelectTextColor() {
    $('select').each(function () {
        if (!$(this).val()) {
            $(this).css('color', '#999');
            $(this).css('font-style', 'italic');
        } else {
            $(this).css('color', '#555');
            $(this).css('font-style', 'normal');
        }
    })
}

function SelectPractice(practicename) {
    var id = practicename.id;
    if (typeof (id) == "undefined") {
        id = practicename[0].id;
    }
    var _practiceGuid = $('#' + id).val();
    var index = id.substring(id.length - 1);
    $.ajax({
        type: "POST",
        url: "/api/Sitecore/Provider/GetPracticeLocations",
        dataType: "json",
        data: {
            PracticeGUID: _practiceGuid
        },
        success: function (response) {
            //if (response != null && response[0] == 'success') {
            if (response != null) {
                //Primary Locations
                $('#ProviderPrimaryLocationMulti-' + index).html("");
                $('#ProviderPrimaryLocationMulti-' + index).append('<option value="" disabled selected>Select Primary Location</option>');
                for (var i = 0; i < response.length; i++) {
                    if (response[i].Address2 == null) {
                        $('#ProviderPrimaryLocationMulti-' + index).append('<option value=' + response[i].ID + '>' + response[i].LocationName + " " + '--' + " " + response[i].Address1 + " " + response[i].City + ',' + " " + response[i].State + " " + response[i].Zip + '</option>');
                    }
                    else {
                        $('#ProviderPrimaryLocationMulti-' + index).append('<option value=' + response[i].ID + '>' + response[i].LocationName + " " + '--' + " " + response[i].Address1 + " " + response[i].City + ',' + " " + response[i].State + " " + response[i].Zip + " " + response[i].Address2 + '</option>');
                    }
                }

                //Other Locations
                $('#ProviderOtherLocationMulti-' + index).html("");
                $('#ProviderOtherLocationMulti-' + index).append('<option value="" disabled selected>Select Other Location</option>');
                for (var i = 0; i < response.length; i++) {
                    if (response[i].Address2 == null) {
                        $('#ProviderOtherLocationMulti-' + index).append('<option value=' + response[i].ID + '>' + response[i].LocationName + " " + '--' + " " + response[i].Address1 + " " + response[i].City + ',' + " " + response[i].State + " " + response[i].Zip + '</option>');
                    }
                    else {
                        $('#ProviderOtherLocationMulti-' + index).append('<option value=' + response[i].ID + '>' + response[i].LocationName + " " + '--' + " " + response[i].Address1 + " " + response[i].City + ',' + " " + response[i].State + " " + response[i].Zip + " " + response[i].Address2 + '</option>');
                    }
                }

                $('.PrimaryLocations-' + index).show();
                $('.OtherLocations-' + index).show();
            }
            else {
                $('.PrimaryLocations-' + index).hide();
                $('.OtherLocations-' + index).hide();
            }
        },
        error: function (response) {
            if (response != null && response[0] == 'success') {

            }
            else {

            }
        }
    });
}

$(document).ready(function () {
    $("ul.progressbar li:nth-child(2)").addClass("active");
    $("ul.progressbar li:nth-child(3)").addClass("active");
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

    $('.PrimaryLocations-' + nextMultiProviderID).hide();
    $('.OtherLocations-' + nextMultiProviderID).hide();

    $('#npiValid-0').hide();
    $('#npiInValid-0').hide();
    $("#btnSaveProviders").attr("disabled", 'disabled');
});

function validateNpi(npi) {
    var name = npi.name;
    if (typeof (name) == "undefined") {
        name = npi[0].name;
    }
    var _NPINumber = $("input[name='" + name + "']").val();

    if (_NPINumber != null || _NPINumber != "") {
        //Check if the NPINumber and the Selected Degree matches the registry from NPPES
        $.ajax({
            type: "POST",
            url: "/api/Sitecore/Validation/ValidateNPIAgaintRegistry",
            dataType: "json",
            data: {
                number: _NPINumber
            },
            success: function (response) {
                if (response != null && response[0] == 'success') {
                    var resp = response;
                    $('#npiValid-' + nextMultiProviderID).show();
                    $('#npiInValid-' + nextMultiProviderID).hide();
                    $("#btnSaveProviders").attr("disabled", false);
                }
                else {
                    var _inputID = npi.id;
                    if (typeof (_inputID) == "undefined") {
                        _inputID = npi[0].id;
                    }
                    var test = document.getElementById(_inputID);
                    test.checkValidity = false;

                    //var valid = isValidForm($('#providers :input'));
                    $("#btnSaveProviders").attr("disabled", 'disabled');

                    $('#npiInValid-' + nextMultiProviderID).show();
                    $('#npiValid-' + nextMultiProviderID).hide();
                }
            },
            error: function (response) {
                if (response != null && response[0] == 'success') {
                    var _inputID = npi.id;
                    if (typeof (_inputID) == "undefined") {
                        _inputID = npi[0].id;
                    }
                    var test = document.getElementById(_inputID);
                    test.checkValidity = false;

                    //var valid = isValidForm($('#providers :input'));
                    $("#btnSaveProviders").attr("disabled", 'disabled');

                    $('#npiInValid-' + nextMultiProviderID).show();
                    $('#npiValid-' + nextMultiProviderID).hide();
                }
                else {
                    var _inputID = npi.id;
                    if (typeof (_inputID) == "undefined") {
                        _inputID = npi[0].id;
                    }
                    var test = document.getElementById(_inputID);
                    test.checkValidity = false;

                    //var valid = isValidForm($('#providers :input'));
                    $("#btnSaveProviders").attr("disabled", 'disabled');

                    $('#npiInValid-' + nextMultiProviderID).show();
                    $('#npiValid-' + nextMultiProviderID).hide();
                }
            }
        });
    }
}
function isValidForm(form) {
    var $inputs = form;
    for (var i = 0; i < $inputs.length; i++) {
        if (!$inputs[i].validity.valid) {
            $inputs[i].setCustomValidity("Invalid");
            return false;
        }
    }
    return true;
}