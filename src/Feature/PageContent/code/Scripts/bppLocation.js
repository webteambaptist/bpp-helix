﻿var nextMultilocationID = 0;
var addresses = [];
var model;
var api = null;
var statesSelectOpions = '<option value="" disabled>State</option><option value="AL">AL</option><option value="AK">AK</option><option value="AR">AR</option><option value="AZ">AZ</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DC">DC</option><option value="DE">DE</option><option value="FL" selected>FL</option><option value="GA">GA</option><option value="HI">HI</option><option value="IA">IA</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="MA">MA</option><option value="MD">MD</option><option value="ME">ME</option><option value="MI">MI</option><option value="MN">MN</option><option value="MO">MO</option><option value="MS">MS</option><option value="MT">MT</option><option value="NC">NC</option><option value="NE">NE</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NV">NV</option><option value="NY">NY</option><option value="ND">ND</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PA">PA</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VT">VT</option><option value="VA">VA</option><option value="WA">WA</option><option value="WI">WI</option><option value="WV">WV</option><option value="WY">WY</option>';


function isJSON(data) {
    var ret = true;
    try {
        JSON.parse(data);
    } catch (e) {
        ret = false;
    }
    return ret;
}
function addSection(section, source, m) {
    if (typeof (m) != "undefined") {
        model = m;
    }
    if (!$(source).hasClass('disabled')) {
        var target = '';
        var newHtml = '';
        var checklocationNum = false;
        var OtherLocId = "";
        switch (section) {
            case "locationMulti":
                nextMultilocationID++
                target = 'locations-multi';
                checklocationNum = true;

                newHtml = '<div class="location-multi">'
                newHtml += '<span><hr></span>'
                newHtml += '<h4 class="location-num">Location' + (nextMultilocationID + 1) + '</h4>'
                newHtml += '<div class="row">'
                //Practice Names
                newHtml += '<div class="col-md-4">'
                newHtml += '<label class="placeholder">Practice Name</label>'
                newHtml += '<select name="practicesMulti-' + nextMultilocationID + '" id="practicesMulti-' + nextMultilocationID + '" class="form-control state-multi input adaptive practicesMulti-' + nextMultilocationID + '" required="required" title="Practice Names" >'
                newHtml += '<option value="">Practice Name</option>'
                newHtml += practiceNames
                newHtml += '</select>'
                newHtml += '</div>'//end div     
                //Location
                newHtml += '<div class="col-md-4">'
                newHtml += '<label for="locationName-' + nextMultilocationID + '" class="placeholder">Location Name</label>'
                newHtml += '<input name="locationName-' + nextMultilocationID + '" id="locationName-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" required="required" title="Location Name" onfocus="RaisePlaceholder($(this))" onblur="validateLocation($(this));"  disabled="disabled"/> '
                newHtml += '</div>'//end div             
                newHtml += '</div>'//end row

                newHtml += '<div class="row">'
                //Location Address 1
                newHtml += '<div class="col-md-3">'
                newHtml += '<label for="Address1-' + nextMultilocationID + '"  class="placeholder">Address</label>'
                newHtml += '<input name="Address1-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" required="required" title="Location Address 1" required="required" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>' //end div
                //Location Address 2
                newHtml += '<div class="col-md-3">'
                newHtml += '<label for="Address2-' + nextMultilocationID + '"  class="placeholder">Address 2</label>'
                newHtml += '<input name="Address2-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" title="Location Address 2 (Apt #,Suite #, etc.)" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>' //end div
                //City
                newHtml += '<div class="col-md-3">'
                newHtml += '<label for="City-' + nextMultilocationID + '"  class="placeholder">City</label>'
                newHtml += '<input name="City-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" required="required" title="City of Location" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>' //end div
                //State Selection
                newHtml += '<div class="col-md-1">'
                newHtml += '<select name="State-' + nextMultilocationID + '" class="form-control state-multi input adaptive" required="required" title="State of Location">'
                newHtml += statesSelectOpions
                newHtml += '</select>'
                newHtml += '</div>' // end div
                //Zip
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="Zip-' + nextMultilocationID + '"  class="placeholder">Zip</label>'
                newHtml += '<input name="Zip-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive zipcode" required="required" title="Zip Code (xxxxx-xxxx)" maxlength="10" pattern="[0-9]{5}-?([0-9]{4})?" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>' //end div
                newHtml += '</div>' //end row

                newHtml += '<div class="row">'
                //Phone #
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="Phone-' + nextMultilocationID + '" class="placeholder">Phone</label>'
                newHtml += '<input name="Phone-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive phone" maxlength=14 required="required" title="Location Phone # (numbers only)" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>' //end div
                //Extension
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="Extension-' + nextMultilocationID + '" class="placeholder">Ext.</label>'
                newHtml += '<input name="Extension-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" title="Extension" maxlength="6" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                //Fax #
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="Fax-' + nextMultilocationID + '" class="placeholder">Fax</label>'
                newHtml += '<input name="Fax-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive phone" maxlength=14 title="Location Fax #" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>' //end div
                newHtml += '</div>' //end row

                newHtml += '<div class="row">'
                newHtml += '<div class="col-md-4">'
                newHtml += '<label for="OfficeManagerName-' + nextMultilocationID + '" class="placeholder">Office Manager Name</label>'
                newHtml += '<input name="OfficeManagerName-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" required="required" title="Name of Office Manager" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="OfficeManagerPhoneNumber-' + nextMultilocationID + '" class="placeholder">Phone</label>'
                newHtml += '<input name="OfficeManagerPhoneNumber-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive phone" required="required" maxlength=14 title="Phone # of Office Manager (numbers only)" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                newHtml += '<div class="col-md-2">'
                newHtml += '<label for="OfficeManagerExtension-' + nextMultilocationID + '" class="placeholder">Ext.</label>'
                newHtml += '<input name="OfficeManagerExtension-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" title="Extension of Office Manager" maxlength="6" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                newHtml += '<div class="col-md-3">'
                newHtml += '<label for=="OfficeManagerEmail-' + nextMultilocationID + '" class="placeholder">Email</label>'
                newHtml += '<input name="OfficeManagerEmail-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" required="required" title="Email of Office Manager (xxx@xxx.xxx)" pattern="[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+\\.([a-zA-Z0-9-]+)*" onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                newHtml += '</div>'//end row

                newHtml += '<div class="row">'
                newHtml += '<div class="col-md-4">'
                newHtml += '<label for="EMRVendor-' + nextMultilocationID + '" class="placeholder">EMR Vendor</label>'
                newHtml += '<input name="EMRVendor-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" title="EMR Vendor" maxlength=40 onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>'//end div
                newHtml += '<div class="col-md-4">'
                newHtml += '<label for=="EMRVersion-' + nextMultilocationID + '" class="placeholder">EMR Version</label>'
                newHtml += '<input name="EMRVersion-' + nextMultilocationID + '" type="text" placeholder="" class="form-control input adaptive" title="EMR Version" maxlength=10 onfocus="RaisePlaceholder($(this))" onblur="LowerPlaceholder($(this));"/> '
                newHtml += '</div>' //end div
                newHtml += '<div class="col-md-4"></div>'
                newHtml += '</div>' //end row

                newHtml += '<div class="row">'
                //Remove Section
                newHtml += '<div class="col-md-6">'
                newHtml += '<div class="remove-section" onclick="removeSection(\'location-multi\', $(this))" style="display:none;">'
                newHtml += '<span class="fa fa-minus"></span> Remove This Location'
                newHtml += '</div>'//end remove-section
                newHtml += '</div>'//end div
                //Add Section
                newHtml += '<div class="col-md-6">'
                newHtml += '<div class="add-section" onclick="addSection(\'locationMulti\', $(this),JSON.stringify(model))">'
                newHtml += '<span class="fa fa-plus"></span> Add Another Location'
                newHtml += '</div>'//end add-section
                newHtml += '</div>'//end div
                newHtml += '</div>'//end row
                newHtml += '</div>'//end location-multi			      
                break;
        }
        $('.' + target).append(newHtml);
        $('html,body').animate({
            scrollTop: $(".practicesMulti-" + nextMultilocationID).offset().top - 150
        }, 'slow');
        showOneAddControl();
        showOneRemoveControl();
        setSelectTextColor();
        $('select').on('change', setSelectTextColor);
        if (checklocationNum) {
            var $locations = $('.location-num');

            for (var i = 0; i < $locations.length; i++) {
                $($locations[i]).text('Location ' + (i + 1));
            }
        }
        window.scrollBy(0, $('.dynamic-section:first-child').height());
        //if (OtherLocId && OtherLocId != "") {
        //    OtherLocId = '#' + OtherLocId;
        //    $(OtherLocId).chosen({ width: "100%" });
        //    $('.chosen-choices').addClass('form-control').css("padding", "3px 10px");
        //}
    }
}
function removeInvalid() {
    var practiceGuid = $("#practicesMulti-" + nextMultilocationID).val();
    var name = $("#locationName-" + nextMultilocationID).val();
    var $inputs = $('#locations :input');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });
    for (var i = 0; i < nextMultilocationID; i++) {
        var locationNameValue = values["locationName-" + i];
        var guid = values["practicesMulti-" + i];// practice
        if ((practiceGuid == guid) && (name == locationNameValue)) {
            $("#locationName-" + i)[0].setCustomValidity("");
        }
        else {
            $("#locationName-" + i)[0].setCustomValidity("Location Name already Used.Choose a unique Location Name");
        }
    }
}
function removeSection(target, source) {
    removeInvalid();
    
    if (!$(source).hasClass('disabled')) {
        if (target == "practice-multi") {
            nextMultiPracticeID--;
        } else if (target == "location-multi") {
            nextMultilocationID--;
        } else if (target == "practice-single") {
            nextSinglePracticeID--;
        }
        $(source).closest('.' + target).remove();
        showOneAddControl();
        showOneRemoveControl();
        if (nextMultilocationID == 0) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
        else {
            $('html,body').animate({
                scrollTop: $(".practicesMulti-" + nextMultilocationID).offset().top - 150
            }, 'slow');
        }
        var $locations = $('.location-num');
        for (var i = 0; i < $locations.length; i++) {
            $($locations[i]).text('Location ' + (i + 1));
        }
        $("#locationName-" + nextMultilocationID)[0].setCustomValidity("");
    }
}

function showOneAddControl() {
    $('.dynamic-section').each(function () {
        var addControls = $(this).find('.add-section');
        for (var i = 0; i < addControls.length; i++) {
            if (i == addControls.length - 1) {
                $(addControls[i]).show();
            } else {
                $(addControls[i]).hide();
            }
        }
    });
}

function showOneRemoveControl() {
    $('.dynamic-section').each(function () {
        var remControls = $(this).find('.remove-section');
        for (var i = 0; i < remControls.length; i++) {
            if (i > 0 && i == remControls.length - 1) {
                $(remControls[i]).show();
            } else {
                $(remControls[i]).hide();
            }
        }
    });
}
function disableInputs() {
    $('.disabled').on('keydown paste', function (e) {
        if ($(this).hasClass('disabled')) {
            e.preventDefault();
        }
    });
}

function setSelectTextColor() {
    $('select').each(function () {
        if (!$(this).val()) {
            $(this).css('color', '#999');
            $(this).css('font-style', 'italic');
        } else {
            $(this).css('color', '#555');
            $(this).css('font-style', 'normal');
        }
    })
}

$(document).ready(function () {
    $("ul.progressbar li:nth-child(2)").addClass("active");
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
    //$("select[name='practicesMulti-0']").change(function () {
    //    $("#locationName-0").removeAttr("disabled");
    //});
    $(document).on('change', 'select', function (e) {
        var ddname = e.currentTarget.name;
        var index = ddname.substr(ddname.length - 1);
        // do something 
        $("#locationName-" + index).removeAttr("disabled");
    });
});
function validateLocation(Location) {
    var id = Location.id;
    if (typeof (id) == "undefined") {
        id = Location[0].id;
        LowerPlaceholder($("#" + id));
    }
    
    var index = id.substr(id.length - 1);
    var LocationName = $("#" + id).val();
    locationNameValid(LocationName, index);
    var practiceGuid = $("#practicesMulti-" + index).val();
    //LocationName, practiceGuid, field) {
    $.ajax({

        type: "POST",
        url: "/api/Sitecore/Validation/ValidateLocationName",
        dataType: "json",
        data: { locationName: LocationName, practiceGuid: practiceGuid },
        success: function (response) {
            if (response != null && response != 'success') {
                //field.setCustomValidity("Location Name already Used. Choose a unique Location Name");
                $("#" + id)[0].setCustomValidity("Location Name already used. Choose a unique Location Name.");
            }
            else {
                $("#" + id)[0].setCustomValidity("");
            }
        },
        error: function (response) {
            //field.setCustomValidity("");
            $("#" + id)[0].setCustomValidity("Location Name already used. Choose a unique Location Name.");
        }
    });
}
//function enableLocation(index) {
//    $("#locationName-"+index).removeAttr("disabled");
//}
function locationNameValid(value, index) {
    
    var name = value;

    var $inputs = $('#locations :input');
    var values = {};
    $inputs.each(function () {
        values[this.name] = $(this).val();
    });

    //var index = value.attr("name").substr(value.attr("name").length - 1);
    var practiceGuid = values["practicesMulti-" + index];
    var indexInt = parseInt(index);
    for (var i = 0; i < indexInt; i++) {
        var locationNameValue = values["locationName-" + i];
        var guid = values["practicesMulti-" + i];// practice
        if ((practiceGuid == guid) && (name == locationNameValue)) {
            //alert("Duplicate Location Name. Location Names must be unique for a practice");
            //return;
            $("#locationName-" + i)[0].setCustomValidity("Location Name already used. Choose a unique Location Name.");
        }
        else {
            $("#locationName-" + i)[0].setCustomValidity("");
        }
    }
    //validateLocation(name, practiceGuid, $("#locationName-" + i));
}
    

    
//}
//$("#locationName-0").focusout(function (e) {
//    // check to make sure tin doesn't already exist
//    var LocationName = $(this).val();
//    var $inputs = $('#locations :input');
//    var values = {};
//    $inputs.each(function () {
//        values[this.name] = $(this).val();
//    });
//    var practiceGuid = values["practicesMulti-0"];
//    validateLocation(LocationName, practiceGuid, this);
   

//});
