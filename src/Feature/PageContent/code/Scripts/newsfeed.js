$(document).on('change', '.div-toggle', function () {
    var target = $(this).data('target');
    var show = $("option:selected", this).data('show');
    $(target).children().addClass('hide');
    $(show).removeClass('hide');
});
$(document).ready(function () {
    $('.div-toggle').trigger('change');
});

$(function () {
    $(".demo1").bootstrapNews({
        newsPerPage: 6,
        autoplay: false,
        pauseOnHover: true,
        direction: 'up',
        newsTickerInterval: 500,
        onToDo: function () {
            //console.log(this);
        }
    });
});
