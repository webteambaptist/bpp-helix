﻿function goBack() {
    //window.history.back();
    window.close();
}
function closeSuccess() {
    return true;
}
$('#btnAttach').on('click', function (e) {
    var files = $("#txtUploadFile").prop('files')[0];
    if (files) {
        return true;
    } else {
        alert("Please select file to upload!");
        return false;
    }
});
$(".bpp-attachment").click(function () {
    getAttachments();
});
function getAttachments() {
    var ID = $("#TaxID").val();
    $.ajax({
        type: "GET",
        url: "/api/Sitecore/Application/GetFileAttachments",
        dataType: "json",
        cache: false,
        data: { TaxID: ID, Mode: "1" },
        success: function (response) {
            $(".provider-profile-table").empty();
            if (response != null & response.data != null && response.data != "[]") {
                var newAttachments = "";
                newAttachments += "<ol>";
                $.each(JSON.parse(response.data), function (m, n) {
                    newAttachments += "<li><a target='_blank' href='/api/Sitecore/Application/DownloadAttachment?ID=" + this.ContentTypeID + "'>" + this.Name + "</a></li>";
                    //console.log("Doc Name - " + this.Name);
                });
                newAttachments += "</ol>";
                $(".provider-profile-table").html(newAttachments);
            }
            else
                $(".provider-profile-table").html("<ul class='list-style-none text-center'><li><b>No attachments available!</b></li></ul>");
        },
        error: function (response) {
            console.log("Error Occured : " + response.responseText);
        }
    })
}
function isValidForm(form) {
    var $inputs = form;
    for (var i = 0; i < $inputs.length; i++) {
        if (!$inputs[i].validity.valid) {
            $inputs[i].setCustomValidity("Invalid");
            message = "Invalid Entry (" + $inputs[i].placeholder + "). Check Value and Try Again";
            document.location.reload();
            sessionStorage.setItem("message", message);
            return false;
        }
    }
    return true;
}

$(document).ready(function () {
    //$("#btnSave").click(function (e) {
    //    e.preventDefault();
    //    var _this = $(this);
    //    var _form = _this.closest("form");
    //    var valid = isValidForm($('#EntityProfile :input'));

    //    if (valid) {
    //        var form = $('#EntityProfile').serialize();
    //        $.ajax({
    //            type: "POST",
    //            url: "/api/Sitecore/MasterLookup/SaveEntityProfile",
    //            dataType: "json",
    //            data: form,
    //            success: function (response) {
    //                if (response != null) {
    //                    if (response[0] == 'success') {
    //                        message = response[1];
    //                        document.location.reload();
    //                        sessionStorage.setItem("message", message);
    //                    }
    //                    else {
    //                        message = response[1];
    //                        document.location.reload();
    //                        sessionStorage.setItem("message", message);
    //                    }
    //                }
    //            },
    //            error: function (response) {
    //                console.log("Error Occured : " + resposne.responseText);
    //                if (response != null && response[0] == 'error') {
    //                    if (response[1] && response[1] != null) {
    //                        message = response[1];
    //                        document.location.reload();
    //                        sessionStorage.setItem("message", message);
    //                    }
    //                }
    //            }
    //        });
    //        //_form.submit();
    //        //$("#btnSave").attr("disabled", false);
    //    }
    //});
    //$("#btnSaveTop").click(function (e) {
    //    e.preventDefault();
    //    var _this = $(this);
    //    var _form = _this.closest("form");
    //    var valid = isValidForm($('#EntityProfile :input'));

    //    if (valid) {
    //        _form.submit();
    //        $("#btnSaveTop").attr("disabled", false);
    //    }
    //});

    if (sessionStorage.length > 0) {
        message = sessionStorage.getItem("message");
        showModal('BPP: Entity Profile Update', message, true);
        sessionStorage.removeItem("message");
    }
    

    if (formResponse) {
        showModal('BPP: Entity Details Update Submission', formResponse, true);
    }

    $('#divShowHideBasic').click(function () {
        if ($("#divShowHideBasic").children('i').hasClass("fa-chevron-down")) {
            $("#divShowHideBasic").children('i').removeClass("fa-chevron-down");
            $("#divShowHideBasic").children('i').addClass("fa-chevron-up");
            $('.BasicInfo').each(function (i, e) {
                $(".BasicInfo").children('div').hide();
            });
        }
        else {
            $("#divShowHideBasic").children('i').removeClass("fa-chevron-up");
            $("#divShowHideBasic").children('i').addClass("fa-chevron-down");
            $('.BasicInfo').each(function (i, e) {
                $(".BasicInfo").children('div').show();
            });
        }
    });
    $('.bpp-date').datepicker({
        //maxDate: 0,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        dateFormat: 'mm/dd/yy'
    });
    //$('.bpp-effective-date').datepicker({
    //    changeMonth: true,
    //    changeYear: true,
    //    yearRange: "-100:+0",
    //    dateFormat: 'mm/dd/yy'
    //});
    $(".bpp-attachment").click(function () {
        getAttachments();
    });


    //MSO Verification
    var legalentitystatus = false;
    $('#ddlLegalEntityStatus option').each(function () {
        if ($(this).val().trim() == $("#existingLegalEntityStatus").val().trim()) {
            legalentitystatus = true;
            $(this).attr("selected", "selected");
            return false;
      }

    });
    if (!legalentitystatus) {
        $('#ddlLegalEntityStatus option:eq(0)').prop('selected', true);
  }

    var inNetworkStatus = false;
    $('#ddlLegalEntityStatus option').each(function () {
      if ($(this).val().trim() == $("#inNetwork").val().trim()) {
        inNetworkStatus = true;
        $(this).attr("selected", "selected");
        return false;
      }

    });
  if (!inNetworkStatus) {
    $('#ddlInNetwork option:eq(0)').prop('selected', true);
    }
});

function Save() {
    var valid = isValidForm($('#EntityProfile :input'));
    if (valid) {
        var form = $("#EntityProfile").serialize();
        $.ajax({
            type: "POST",
            url: "/api/Sitecore/MasterLookup/SaveEntityProfile",
            dataType: "json",
            data: form,
            success: function (response) {
                if (response != null) {
                    if (response[0] == 'success') {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                    else {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                }
                //console.log(response);
            },
            error: function (response) {
                console.log("Error Occured : " + response.responseText);
                if (response != null && response[0] == 'error') {
                    if (response[1] && response[1] != null) {
                        message = response[1];
                        document.location.reload();
                        sessionStorage.setItem("message", message);
                    }
                }
            }
        });
    }
}

//function getAttachments() {
//    var ID = $("#practTaxID").val();
//    $.ajax({
//        type: "GET",
//        url: "/api/Sitecore/Application/GetFileAttachments",
//        dataType: "json",
//        cache: false,
//        data: { ID: ID, Mode: "1" },
//        success: function (response) {
//            $(".provider-profile-table").empty();
//            if (response != null & response.data != null && response.data != "[]") {
//                var newAttachments = "";
//                newAttachments += "<ol>";
//                $.each(JSON.parse(response.data), function (m, n) {
//                    newAttachments += "<li><a target='_blank' href='/api/Sitecore/Application/DownloadAttachment?ID=" + this.ContentTypeID + "'>" + this.Name + "</a></li>";
//                    //console.log("Doc Name - " + this.Name);
//                });
//                newAttachments += "</ol>";
//                $(".provider-profile-table").html(newAttachments);
//            }
//            else
//                $(".provider-profile-table").html("<ul class='list-style-none text-center'><li><b>No attachments available!</b></li></ul>");
//        },
//        error: function (response) {
//            console.log("Error Occured : " + response.responseText);
//        }
//    })
//}
