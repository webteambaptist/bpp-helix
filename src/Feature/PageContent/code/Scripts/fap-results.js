﻿var geoAllowed = '';
var useLocation = false;
var lastZip = '';
var filters = {};
var latlng = '';
var sort = '';
var distance = '';
var query = '';
var queryResponse;
var initFacets;
var params;
var searchQuery = {};

$(function () {
    querySolrFacets();

    $('#fap-filter-toggle').click(function () {
        $('.fap-filters-container').slideToggle();
        $(this).find('i').toggleClass('fa-chevron-up fa-chevron-down');
    });

    $('input#location-filter').on('change', function () {
        //console.log('location checkbox changed');
        if ($(this).prop('checked') == true) {
            $('.location-controls').show();

            if ($('#zip').val() != '' && $('#zip').val().length == 5) {
                getQuery();
            }
        } else {
            $('.location-controls').hide();
            getQuery();
        }
    })

    $(window).resize(function () {
        checkFilterVisibility();
    });

    // on submit
    $('#fap').on('submit', function (e) {
        //console.log('submitted');
        e.preventDefault();
        getQuery();
    });

    $(document).keypress(function (e) {
        if (e.which == 13) {
            //console.log('submitted');
            e.preventDefault();
            getQuery();
        }
    });

    $('#apply-zip-btn').click(function (e) {
        //console.log('submitted');
        e.preventDefault();
        getQuery();
    })

    // typeahead
    $('#query, #queryMobile').autocomplete({
        source: function (request, response) {
            var specialties = $.ui.autocomplete.filter(parseFacetsTypeahead(initFacets.specialties_pipe, "Specialty"), request.term);

            $.ajax({
                type: "POST",
                url: '/api/Sitecore/FAP/PhysicianTypeahead',
                //url: '@Url.Action("PhysicianTypeahead", "FAP")',
                data: "Query=" + request.term,
                dataType: "json",
                success: function (data) {
                    if (data.Status != "Error") {
                        response(parseTypeahead(data).concat(specialties));
                    }
                }
            })
        },
        minLength: 3
    }).each(function () {
        $(this).data("ui-autocomplete")._renderItem = function (ul, item) {
            var newLabel = String(item.label).replace(
                    new RegExp(this.term.replace(/([^a-z0-9]+)/gi, ''), "gi"),
                    "<strong>$&</strong>");
            var icon;
            if (item.type != '') {
                if (item.type == "Doctor") {
                    icon = "fa-user-md";
                } else if (item.type == "Specialty") {
                    icon = "fa-stethoscope";
                }
            }
            //var type = (item.type != "") ? "<span>{" + item.type + "}</span>" : "";

            return $("<li>")
                .data("ui-autocomplete-item", item)
                .append("<div><span class='fa " + icon + "'><span> " + newLabel + "</div>")
                .appendTo(ul);
        };
    });

    //checkFilterVisibility();
    mobileAdjustments();

    params = getUrlVars();
    //console.log('params found: ', params);
    if (params["fap-name"] != undefined && (params["fap-name"] != "" || params["zip-code"] != "" ||
        params["fap-specialty"] != "any" || params["fap-hospital"] != "any")) {
        //console.log('params being used for query...');
        //Use Params for query
        var searchQuery = buildSearchObjectFromUrl();
        querySolr(searchQuery);
    } else {
        //Do default A-Z search
        getQuery();
    }


    //$('.fap-wrapper select').on('change', function () { getQuery(); });
    //$('.fap-wrapper input[type=radio][name=gender_s]').change(function () {
    //    console.log('gender radio changed');
    //    getQuery();
    //});
});

function getQuery() {
    // main query
    query = $('input[name=query]').val();

    // filters
    filters = {};
    latlng = '';
    sort = 'asc';
    distance = '';

    if ($('#filters').is(':visible')) {
        //console.log('filters visible getting filter data');
        $('#filters select').each(function () {
            filters[$(this).attr('name')] = $(this).val();
        })
        filters['gender_s'] = $("input[name='gender_s']:checked").val();

        distance = $('#distance').val();
        if ($('#sort').is(':visible')) {
            sort = $('#sort').val();
        } else {
            sort = $('#mobile-sort').val();
        }

        if ($('#location-filter').prop('checked') == true) {
            //console.log('location checkbox is checked, getting location info...');
            // geolocation
            if ($('#zip').val() != '' && $('#zip').val() != lastZip) {
                lastZip = $('#zip').val();
                getLatLng($('#zip').val(), false);
            } else {
                if ($('#zip').val() != '' && $('#latlng').val() != '') {
                    //console.log('zip entered & latlng available')

                    latlng = $('#latlng').val();
                    lastZip = $('#zip').val();
                } else {
                    //console.log('zip and/or latlng is missing');
                }
                var searchQuery = buildSearchObject();
                querySolr(searchQuery);
            }
        } else {
            var searchQuery = buildSearchObject();
            querySolr(searchQuery);
        }
    } else {
        //console.log('filters hidden, only use search input');
        if ($('#zip').val() != '' && $('#latlng').val() != '') {
            //console.log('zip entered & latlng available')

            distance = $('#distance').val();
            latlng = $('#latlng').val();
            lastZip = $('#zip').val();
        }
        var searchQuery = buildSearchObject();
        querySolr(searchQuery);
    }
}

function buildSearchObject() {
    var searchQuery = {};
    searchQuery.Query = query != '' ? query : '*';
    searchQuery.Filters = filters;
    searchQuery.Start = 0;
    searchQuery.Rows = 10;
    searchQuery.Location = latlng;
    searchQuery.Distance = distance != '' ? distance : null;
    searchQuery.SortOrder = sort != '' ? sort : '';
    return searchQuery;
}

function buildSearchObjectFromUrl() {
    searchQuery = {};
    searchQuery.Query = params["fap-name"] != '' ? params["fap-name"] : '*';
    //filters
    filters["specialties_pipe"] = params["fap-specialty"] != 'any' ? decodeURIComponent(params["fap-specialty"]) : '';
    filters["hospitalaffiliations_pipe"] = params["fap-hospital"] != 'any' ? decodeURIComponent(params["fap-hospital"]) : '';
    searchQuery.Filters = filters;
    searchQuery.Start = 0;
    searchQuery.Rows = 10;
    searchQuery.Location = params['fap-location'];
    searchQuery.Distance = 10;
    searchQuery.SortOrder = "";

    var name = searchQuery.Query != '*' ? searchQuery.Query : '';
    $('#query, #queryMobile').val(name);
    if (params["fap-zip"] != '') {
        $('#zip').val(params["fap-zip"]);
        $('#location-filter').prop('checked', true);
        $('.location-controls').show();
    }

    if (searchQuery.Filters["specialties_pipe"] != '') {
        $('#specialty').val(searchQuery.Filters["specialties_pipe"]);
    }
    if (searchQuery.Filters["hospitalaffiliations_pipe"] != '') {
        $('#specialty').val(searchQuery.Filters["hospitalaffiliations_pipe"]);
    }

    return searchQuery;
}

// query solr - accepts SolrQuery model
function querySolr(query) {
    console.log('Current Query: ', query);

    $.post('/api/Sitecore/FAP/PhysicianQuery', query,
        function (data) {
            if (data.Status == "Error") {
                // insert error handling
                console.log(data);
            } else {
                console.log(data);
                queryResponse = data;
                $('#specialty').html(data.Facets.specialties_pipe);
                $('#language').html(data.Facets.languages_pipe);
                $('#affiliation').html(data.Facets.hospitalaffiliations_pipe);
                $('#gender').html(data.Facets.gender_s);
                $('#results').html(data.Results);
                $('#pagination').html(data.Pagination);

                // pagination
                pagination();
                if ($('#location-filter').prop('checked') == true &&
                    $('#latlng').val() != '' && $('#zip').val() != '') {
                    $('.distance-value').show();
                }
                $('.show-on-results').show();
                checkFilterVisibility();
                $('.fap-wrapper select').on('change', function () {
                    if ($(this).attr('id') != 'distance') {
                        getQuery()
                    } else {
                        if ($('#zip').val() != '') {
                            getQuery();
                        }
                    }

                });
                $('.fap-wrapper input[type=radio][name=gender_s]').change(function () {
                    //console.log('gender radio changed');
                    getQuery();
                });
            }
        }).fail(function (data) {
            // insert error handling
            console.log(data);
        });

}

// parses solr response to build typeahead array
function parseTypeahead(data) {
    var json = [];
    $(data).each(function () {
        var name = this.FirstName + ' ' + this.LastName;
        if (this.Suffix !== '') {
            name += ', ' + this.Suffix;
        }
        json.push({
            "label": name,
            // "value": actual value
            // "link": url
            "type": "Doctor"

        });
    });

    return json;
}

function parseFacetsTypeahead(facets, type) {
    var json = [];
    $(facets).each(function () {
        json.push({
            "label": this.Key,
            // "value": actual value
            // "link": url
            "type": type
        })
    });
    //console.log(json);
    return json;
}

// get facets
function querySolrFacets(query) {
    //console.log('querySolrFacets...')
    $.post('/api/Sitecore/FAP/PhysicianFacets', query,
        function (data) {
            if (data.Status == "Error") {
                //insert error handling
                //console.log(data);
            } else {
                initFacets = data;
            }
        }).fail(function (data) {
            // insert error handling
            console.log(data);
        });

}

function pagination() {
    $('#pagination').twbsPagination('destroy');
    if (queryResponse.TotalHits && queryResponse.OriginalQuery.Rows) {
        total = Math.ceil(queryResponse.TotalHits / queryResponse.OriginalQuery.Rows);
        currentPage = queryResponse.OriginalQuery.Start != 0 ? (queryResponse.OriginalQuery.Start / queryResponse.OriginalQuery.Rows) + 1 : 1;
        //console.log('start: ', queryResponse.OriginalQuery.Start);
        //console.log('rows: ', queryResponse.OriginalQuery.Rows);
        //console.log('currentPage: ',currentPage);

        if (total > 1) {
            $('#pagination').twbsPagination({
                totalPages: total,
                startPage: currentPage,
                visiblePages: 5,
                initiateStartPageClick: false,
                first: '<<',
                prev: '<',
                next: '>',
                last: '>>',
                onPageClick: function (event, page) {
                    pageQuery((page - 1) * queryResponse.OriginalQuery.Rows);
                }
            });
        }
    }
}

// pagination page query
function pageQuery(number) {
    //console.log(number);

    var query = queryResponse.OriginalQuery;
    query.Start = number;
    querySolr(query);
}

function useMyLocation() {
    if (geoAllowed !== false) {
        getGeolocation();
    } else {
        console.log('Error getting location. Please ensure Geolocation is enabled in your browser.');
    }
}

// distance filter stuff
// Try to get geolocation, if successful set lat and long and do a zip code lookup
function getGeolocation(advanced) {
    if (navigator.geolocation) {
        //console.log('navigator found...');
        // show loader in zipcode input
        //$('.zipcode-loader').show();
        //$('#zipcode').addClass('hide-input-text');
        //$('.advanced-popover #advanced-zip').addClass('hide-input-text');
        setTimeout(function () {
            //console.log('calling getCurrentPosition...');
            navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
        }, 750)
    }

    function geoSuccess(position) {
        lat = position.coords.latitude;
        long = position.coords.longitude;
        geoAllowed = true;
        useLocation = true;

        // do lookup for zipcode
        var latlng = new google.maps.LatLng(lat, long);
        geocoder = new google.maps.Geocoder();

        geocoder.geocode({
            'latLng': latlng
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    for (j = 0; j < results[0].address_components.length; j++) {
                        if (results[0].address_components[j].types[0] == 'postal_code') {
                            zipcodeInput = results[0].address_components[j].short_name;

                            $('input#zip').val(zipcodeInput);

                            //sessionStorage.setItem('sessionZip', zipcodeInput);
                            //updateShownDistance(distanceInput);

                            // hide zipcode-loader
                            //$('.zipcode-loader').hide();
                            //$('#zipcode').removeClass('hide-input-text');
                            //$('.advanced-popover #advanced-zip').removeClass('hide-input-text');
                        }
                    }
                }
            } else {
                $('inpit#zip').text("Zipcode");
                //$('.zip-code-block #dist').text("");
                console.log('Error getting location. Please ensure Geolocation is enabled in your browser.');
                // hide zipcode-loader
                //$('.zipcode-loader').hide();
                //$('#zipcode').removeClass('hide-input-text');
                //$('.advanced-popover #advanced-zip').removeClass('hide-input-text');
            }
        });
    }

    function geoError() {
        geoAllowed = false;
        useLocation = false;
        console.log('Error getting location. Please ensure Geolocation is enabled in your browser.');
        // hide zipcode-loader
        //$('.zipcode-loader').hide();
        //$('#zipcode').removeClass('hide-input-text');
        //$('.advanced-popover  #advanced-zip').removeClass('hide-input-text');
    }
}

// function to get lat and long based on zip code
//function getLatLng(address, doQuery) {
function getLatLng(address, useUrlParams) {
    var searchQuery = {};
    geocoder = new google.maps.Geocoder();
    //console.log('geocoder...', geocoder);
    //console.log('geocoding...', address);
    if (address != undefined && address != null && address != '') {
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                long = results[0].geometry.location.lng();
                //console.log('found lat', lat);
                //console.log('found long', long);
                if (lat && long) {
                    $('#latlng').val(lat + ',' + long);
                    latlng = lat + ',' + long;
                } else {
                    latlng = '';
                }
                if (useUrlParams) {
                    searchQuery = buildSearchObjectFromUrl();
                } else {
                    searchQuery = buildSearchObject();
                }

                querySolr(searchQuery);
                // check for search first
                //if (($('#query').val() != '')) {
                //    querySolr();
                //}
            } else {
                //$('.zip-code-block #zip').text("Enter ZIP Code");
                //$('.zip-code-block #dist').text("");
                Console.log("Geocode ERROR: " + status);
            }
        });
    } else if (address == '') {
        lat = '';
        long = '';
        $('#latlng').val('');
        searchQuery = buildSearchObject();
        querySolr(searchQuery);
        //querySolr();
    }
}

function resetFilters() {
    $('.fap-filters select').each(function () {
        if ($(this).attr('id') != 'distance') {
            $(this).val('');
        }
    });
    $('.fap-filters input[type=radio][name=gender_s][value=""]').prop('checked', true);
    $('.fap-filters input[type=checkbox][name=location-filter]').prop('checked', false);
    $('.location-controls').hide();
    //$('.fap-filters').css('padding', '10px 10px 1px 10px');
    $('#sort').val('asc');
    $('#mobile-sort').val('asc');
    getQuery();
    $('#sort').val('');
    $('#mobile-sort').val('');
}

var noResults = '<div class="no-results"><p>Your query returned no results.</p><p>You can try:</p><ul><li>Clicking "Reset Filters" for a new A-Z search and filter down.</li><li>Extending or removing location filter.</li><li>A different search term.</li></ul></div>'
var mobileNoResults = '<div class="mobile-no-results"><p>Your query returned no results.</p><p>You can try:</p><ul><li>Clicking "Refine Results" then "Reset Filters" for a new A-Z search.</li><li>Extending or removing location filter.</li><li>A different search term.</li></ul></div>';

function checkFilterVisibility() {
    //console.log('checkFiltervisibility called')
    if (window.innerWidth >= 992 && $('.fap-result').length > 0) {
        //console.log('showing filters')
        $('.fap-filters-container').css('display', 'block');
    } else if (window.innerWidth >= 992 && $('.fap-result').length == 0) {
        console.log('no results found...');
        $('.fap-results').html(noResults);
        $('.fap-results').show();
        //console.log('hiding filters')
        //$('.fap-filters-container').css('display', 'none');
    } else if (window.innerWidth < 992 && $('.fap-result').length > 0) {
        $('.fap-filters-container').css('display', 'none');
        $('.fap-results').show();
    } else {
        $('.fap-filters-container').css('display', 'none');
        $('.controls-container').css('display', 'none');
        $('.fap-results').html(mobileNoResults);
        $('.fap-results').show();
    }
}

function showMoreLocations(target) {
    var $hiddenLocations = $(target).closest('.fap-result').find('.hidden-locations');

    if ($hiddenLocations.is(':visible')) {
        $hiddenLocations.hide();
        $(target).find('.more-locations-txt').text('Show More Locations');
        $(target).find('.more-locations-icon').removeClass('fa-angle-up').addClass('fa-angle-down');
    } else {
        $hiddenLocations.show();
        $(target).find('.more-locations-txt').text('Hide More Locations');
        $(target).find('.more-locations-icon').removeClass('fa-angle-down').addClass('fa-angle-up');
    }
}

function mobileAdjustments() {
    if (window.innerWidth <= 767) {
        $('#query').attr('placeholder', 'Name or Specialty');
    } else {
        $('#query').attr('placeholder', 'Enter Physician Name or Specialty');
    }
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}