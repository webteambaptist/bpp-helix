﻿
var queryResponse;
var initFacets;
var sort = "";
var sortOrder = "";

$('#dcenter-filter-toggle').click(function () {
    $('.dcenter-filters-container').slideToggle();
    $(this).find('i').toggleClass('fa-chevron-up fa-chevron-down');
});

$(window).resize(function () {
    if (window.innerWidth >= 992 && $('.dcenter-filters-container').length) {
        $('.dcenter-filters-container').css('display', 'block');
    } else {
        $('.dcenter-filters-container').css('display', 'none');
    }
}).resize();


jQuery(document).ready(function () {

    main();

    //show entries sorting
    $('#option-sort').on('change', function () {
        main();

    })

    // click search button submit
    $('#docCenterSearchForm').on('submit', function (e) {
        e.preventDefault();

        main();

    });

    // enter search button submit
    $('form').keypress('submit', function (e) {
        if (e.which == 13) {
            main();
        }
    });


    //sort a-z
    $('#dcenter-az').on('change', function () {
        //console.log("on change works");
        azza = $(this).val();

        if (azza == "A-Z") {
            sort = "_name_s";
            sortOrder = "ASC";
        }
        else if (azza == "Z-A") {
            sort = "_name_s";
            sortOrder = "DESC";
        }

        main();
    })

    //sort by dates

    $('#dcenter-datemodified').on('click', function (e) {
        e.preventDefault();
        sort = "__updated_tdt";
        if (sortOrder === "DESC") {
            sortOrder = "ASC";
        }
        else {
            sortOrder = "DESC";
        }

        main();
    })


    $('#dcenter-datecreated').on('click', function (e) {
        e.preventDefault();
        sort = "__created_tdt";
        if (sortOrder === "DESC") {
            sortOrder = "ASC";
        }
        else {
            sortOrder = "DESC";
        }

        main();
    })


    // typeahead
    $('#query, #queryMobile').autocomplete({
        source: function (request, response) {

            $.ajax({
                type: "POST",
                url: '/api/Sitecore/DocumentCenter/DocumentTypeahead',//'@Url.Action("DocumentTypeahead", "DocumentCenter")',
                data: "Query=" + request.term,
                dataType: "json",
                success: function (data) {
                    if (data.Status != "Error") {
                        response(parseTypeahead(data));
                    }
                }
            })
        },
        minLength: 3
    }).each(function () {
        //console.log($(this));
        $(this).data("ui-autocomplete")._renderItem = function (ul, item) {
            var newLabel = String(item.label).replace(
                new RegExp(this.term.replace(/([^a-z0-9]+)/gi, ''), "gi"),
                "<strong>$&</strong>");
            //var type = (item.type) ? " <span>{" + item.type + "}</span>" : "";
            //console.log("newlabel", newLabel);

            return $("<li>")
                .data("ui-autocomplete-item", item)
                .append("<div>" + newLabel + "</div>")
                //.append("<div>" + newLabel + type + "</div>")
                .appendTo(ul);
        };
    });


    // parses solr response to build typeahead array
    function parseTypeahead(data) {
        var json = [];
        $(data).each(function () {

            //console.log(this);
            var label =
                    json.push({
                        "label": (this.Title != null && this.Title != '') ? this.Title : this.BackupTitle
                        // "value": actual value
                        // "link": url
                        //"type": this.Tags[0]
                    });
        });

        return json;
    }

    function parseFacetsTypeahead(facets, type) {
        var json = [];
        $(facets).each(function () {
            json.push({
                "label": this.key,
                // "value": actual value
                // "link": url
                "type": type
            })
        });
        //console.log(json);
        return json;
    }

    function parseFacetsTypeahead(facets, type) {
        var json = [];
        $(facets).each(function () {
            json.push({
                "label": this.Key,
                // "value": actual value
                // "link": url
                "type": type
            })
        });
        //console.log(json);
        return json;
    }



});


// pagination
function pagination() {
    $('#pagination').twbsPagination('destroy');
    if (queryResponse.TotalHits && queryResponse.OriginalQuery.Rows) {
        total = Math.ceil(queryResponse.TotalHits / queryResponse.OriginalQuery.Rows);
        currentPage = queryResponse.OriginalQuery.Start != 0 ? (queryResponse.OriginalQuery.Start / queryResponse.OriginalQuery.Rows) + 1 : 1;
        //console.log(queryResponse.OriginalQuery.Start);
        //console.log(queryResponse.OriginalQuery.Rows);
        //console.log(currentPage);

        if (total > 1) {
            $('#pagination').twbsPagination({
                totalPages: total,
                startPage: currentPage,
                visiblePages: 5,
                initiateStartPageClick: false,
                first: '<span class="dcenter-first dcenter-page-link fa fa-angle-double-left"></span>',
                prev: '<span class="dcenter-prev dcenter-page-link fa fa-angle-left"></span>',
                next: '<span class="dcenter-next dcenter-page-link fa fa-angle-right"></span>',
                last: '<span class="dcenter-last dcenter-page-link fa fa-angle-double-right"></span>',
                onPageClick: function (event, page) {
                    pageQuery((page - 1) * queryResponse.OriginalQuery.Rows);
                }
            });
        }
    }
};

// pagination page query
function pageQuery(number) {
    //console.log(number);
    var query = queryResponse.OriginalQuery;
    query.Start = number;
    querySolr(query);
}

// query solr - accepts SolrQuery model
function querySolr(query) {

    $.post('/api/Sitecore/DocumentCenter/DocumentQuery', query,
        function (data) {
            if (data.Status == "Error") {
                // insert error handling
                //console.log('data', data);
            } else {
                //console.log('data', data);
                queryResponse = data;
                //queryData.push(data);
                $('#type').html(data.Facets.__semantics);
                $('#results').html(data.Results);
                $('#pagination').html(data.Pagination);

                //showing 1 of dcenter-showing-top
                showing_one_of = "Showing 1 - " + data.OriginalQuery.Rows + " of " + data.TotalHits + " entries";

                if (data.TotalHits === 0) {

                    $('#dcenter-showing-top').text("");
                    $('#dcenter-showing-bottom').text("");
                }
                else {
                    $('#dcenter-showing-top').text(showing_one_of);
                    $('#dcenter-showing-bottom').text(showing_one_of);

                }


                // pagination
                pagination();
            }
        }).fail(function (data) {
            // insert error handling
            //console.log(data);
        });

};

function main() {

    // main query
    query = $('input[name=query]').val();

    // filters
    filters = {};


    filters['__semantics'] = $("input[name='docCat']:checked").map(function () {

        return this.value;
    }).get().join();


    // sort rows
    rows = $('#option-sort').val();


    //current categories
    itemNameText = $("input[name='docCat']:checked").parent().text();
    CurrentCategories = filters.__semantics == '' ? 'All' : itemNameText;
    //console.log('tagitemName', itemNameText);
    $('#current-cat').text(CurrentCategories);

    // build search object
    var searchQuery = {};
    searchQuery.Query = query != '' ? query : '*';
    searchQuery.Filters = filters;
    searchQuery.Start = 0;
    searchQuery.Rows = rows;
    searchQuery.Sort = sort;
    searchQuery.SortOrder = sortOrder;
    //console.log('searchQuery ', searchQuery);
    // query solr
    querySolr(searchQuery);
};



