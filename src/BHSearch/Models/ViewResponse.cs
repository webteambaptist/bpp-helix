﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class ViewResponse
    {
        public ViewResponse()
        {
            Facets = new Dictionary<string, string>();
        }
        public string Results { get; set; }
        public Dictionary<string, string> Facets { get; set; }
        public SolrQuery OriginalQuery { get; set; }
        public int TotalHits { get; set; }
        public string Pagination { get; set; }
    }
}
