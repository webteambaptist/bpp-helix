﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class FacetView
    {
        public FacetView()
        {
            Facet = new KeyValuePair<string, ICollection<KeyValuePair<string, int>>>();
            Filter = new List<string>();
        }

        public KeyValuePair<string, ICollection<KeyValuePair<string, int>>> Facet { get; set; }
        public List<string> Filter { get; set; }
    }
}
