﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class SolrResponse
    {
        public int TotalHits { get; set; }

        public int QueryTime { get; set; }

        public int Status { get; set; }

        public SolrQuery OriginalQuery { get; set; }

        public List<Dictionary<string, object>> Results { get; set; }

        public Dictionary<string, ICollection<KeyValuePair<string, int>>> Facets { get; set; }
    }
}
