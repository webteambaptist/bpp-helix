﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class DocumentResultView
    {
        public DocumentResultView()
        {
            Start = 0;
           Documents = new List<Document>();
        }

        public int Start { get; set; }
        public List<Document> Documents { get; set; }
    }
}




