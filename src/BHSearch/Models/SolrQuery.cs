﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class SolrQuery
    {
        public SolrQuery()
        {
            Rows = 10;
            Start = 0;
            Query = "*";
            Location = "";
            Distance = "";
            Handler = "";
            Facets = new List<string>();
            Filters = new Dictionary<string, string>();
            Sort = "";
            SortOrder = "";
        }
        public string Query { get; set; }
        public int Start { get; set; }
        public int Rows { get; set; }
        public string Location { get; set; }
        public string Distance { get; set; }
        public List<string> Facets { get; set; }
        public Dictionary<string, string> Filters { get; set; }
        public string Handler { get; set; }
        public string Sort { get; set; }
        public string SortOrder { get; set; }

    }
}
