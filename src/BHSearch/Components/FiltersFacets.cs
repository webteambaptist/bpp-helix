﻿using System;
using System.Collections.Generic;
using Sitecore.Diagnostics;
using SolrNet;
using SolrNet.Commands.Parameters;

namespace BHSearch.Components
{
    public class FiltersFacets
    {
        public ICollection<ISolrQuery> BuildFilterQueries(Models.SolrQuery query)
        {
            try
            {
                // Initialize filters to return
                ICollection<ISolrQuery> filters = new List<ISolrQuery>();

                // Loop through values and add to filters
                foreach (KeyValuePair<string, string> entry in query.Filters)
                {
                    if (!String.IsNullOrEmpty(entry.Value))
                    {
                        List<SolrQueryByField> filter = new List<SolrQueryByField>();

                        foreach (string val in entry.Value.Split(','))
                        {
                            filter.Add(new SolrQueryByField(entry.Key, val));
                        }
                        filters.Add(new LocalParams { { "tag", entry.Key.Substring(0, 3) } } + new SolrMultipleCriteriaQuery(filter, "OR"));
                    }
                }
                return filters;
            }
            catch (Exception e)
            {
              Log.Error("Exception Filter Facets " + e.Message + e?.InnerException, this);
                throw new Exception("Set Filter Error", e);
            }
        }
        public ICollection<ISolrQuery> FilterYear(string year)
        {
          try
          {
              // Initialize filters to return
              ICollection<ISolrQuery> filters = new List<ISolrQuery>();
              var entry = new KeyValuePair<string, string>("_name_s",year+"*");
              
              if (string.IsNullOrEmpty(entry.Value)) return filters;
              var filter = new List<SolrQueryByField> {new SolrQueryByField(entry.Key, entry.Value.ToString())};
              filters.Add(new LocalParams { { "tag", entry.Key.Substring(0, 3) } } + new SolrMultipleCriteriaQuery(filter, "OR"));
              return filters;
          }
          catch (Exception e)
          {
            Log.Error("Exception :: FilterYear " + e.Message + e?.InnerException, this);
            throw new Exception("Set Filter Error", e);
          }
        }
    internal FacetParameters BuildFacets(Models.SolrQuery query)
        {
            try
            {
                // Set Facets
                var facetParams = new FacetParameters();
                foreach (string facet in query.Facets)
                {
                    facetParams.Queries.Add(new SolrFacetFieldQuery(new LocalParams { { "ex", facet.Substring(0, 3) } } + facet) { MinCount = 1 });
                }
                return facetParams;
            }
            catch (Exception e)
            {
                throw new Exception("Set Facet Error", e);
            }
        }

    }
}
