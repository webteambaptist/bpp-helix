﻿using System;
using System.Collections.Generic;
using BHSearch.Models;
using CommonServiceLocator;
using Newtonsoft.Json;
using Sitecore.Diagnostics;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Impl;

namespace BHSearch.Components
{
    public class ExecuteCitizenshipSearch
    {
        public SolrResponse Execute(Models.SolrQuery query, string year)
        {
            try
            {
                // Filters
                var filtersFacets = new FiltersFacets();
                
                // Create an object to hold results
                SolrQueryResults<Dictionary<string, object>> solrResults;
                  var queryResponse = new SolrResponse{
                      OriginalQuery = query
                };              

                // Set the Handler
                var executor = ServiceLocator.Current.GetInstance<ISolrQueryExecuter<Dictionary<string, object>>>() as SolrQueryExecuter<Dictionary<string, object>>;

                if (string.IsNullOrEmpty(query.Handler))
                    throw new Exception("Handler must be set");
                if (executor == null) return queryResponse;
                executor.DefaultHandler = query.Handler;
                //Set Options
                var start = new StartOrCursor.Start(query.Start);

                var queryOptions = new QueryOptions
                {
                  Rows = query.Rows,
                  StartOrCursor = start,
                  //FilterQueries = filtersFacets.FilterYear(year),// filtersFacets.BuildFilterQueries(query),
                  Facet = filtersFacets.BuildFacets(query)
                };

                // Sort
                var sort = (!string.IsNullOrEmpty(query.Sort)) ? query.Sort : "_name_s";

                switch (string.IsNullOrEmpty(query.SortOrder))
                {
                  case false when query.SortOrder.Equals("ASC", StringComparison.OrdinalIgnoreCase):
                    queryOptions.OrderBy = new[]
                    {
                      new SortOrder(sort, Order.ASC)
                    };
                    break;
                  case false when query.SortOrder.Equals("DESC", StringComparison.OrdinalIgnoreCase):
                    queryOptions.OrderBy = new[]
                    {
                      new SortOrder(sort, Order.DESC)
                    };
                    break;
                  default:
                    queryOptions.OrderBy = new[]
                    {
                      new SortOrder("score", Order.DESC),
                      new SortOrder(sort, Order.ASC),
                    };
                    break;
                }

                //Execute the query
                ISolrQuery solrQuery = new SolrNet.SolrQuery(query.Query);

                // Search for ID (NPI Search)
                if (query.Filters.Count > 0)
                {
                  if (query.Filters.TryGetValue("_group", out var exists))
                  {
                    var q = query.Filters["_group"].Split('.')[0];
                    q = q.Replace("-", string.Empty).ToLower();
                    var filter = new SolrQueryByField("_group", q);
                    queryOptions.AddFilterQueries(filter);
                  }
                }
                // this is for Group Search
                if (query.Query != "*")
                {
                    var filter = new SolrQueryByField("_name_s", query.Query) {Quoted = true};
                    queryOptions.AddFilterQueries(filter);
                }
                // Year Search
                else
                {
                  var searchYear = year + "*";
                  var filter = new SolrQueryByField("_name_s", searchYear) { Quoted = false };
                  queryOptions.AddFilterQueries(filter);
                }
                solrResults = executor.Execute(solrQuery, queryOptions);
                //Set Response
                var extractResponse = new ResponseExtraction();
                extractResponse.SetHeader(queryResponse, solrResults);
                extractResponse.SetBody(queryResponse, solrResults);
                extractResponse.SetFacets(queryResponse, solrResults);

                return queryResponse;
            }
            catch (Exception e)
            {
                Log.Error($"Exception :: ExecuteCitizenshipSearch :: Execute({JsonConvert.SerializeObject(query)}) :: {e.Message}", this);
                throw new Exception("Search Execution Error", e);
            }

        }
  }
}
