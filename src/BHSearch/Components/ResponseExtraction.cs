﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolrNet;
using BHSearch.Helpers;
using System.Configuration;
using System.Collections;
using BHSearch.Models;

namespace BHSearch.Components
{
    public class ResponseExtraction
    {
        internal void SetHeader(SolrResponse queryResponse, SolrQueryResults<Dictionary<string, object>> solrResults)
        {
            try
            {
                queryResponse.QueryTime = solrResults.Header.QTime;
                queryResponse.Status = solrResults.Header.Status;
                queryResponse.TotalHits = solrResults.NumFound;
            }
            catch (Exception e)
            {
                throw new Exception("Error Extracting Header", e);
            }
        }

        internal void SetBody(SolrResponse queryResponse, SolrQueryResults<Dictionary<string, object>> solrResults)
        {
            try
            {
                queryResponse.Results = (List<Dictionary<string, object>>)solrResults;
            }
            catch (Exception e)
            {
                throw new Exception("Error Extracting Body", e);
            }

        }

        internal void SetFacets(SolrResponse queryResponse, SolrQueryResults<Dictionary<string, object>> solrResults)
        {
            try
            {
                Dictionary<string, ICollection<KeyValuePair<string, int>>> facets = (Dictionary<string, ICollection<KeyValuePair<string, int>>>)solrResults.FacetFields;

                queryResponse.Facets = facets;
            }
            catch (Exception e)
            {
                throw new Exception("Error Extracting Facets", e);
            }
        }
    }
}
