﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Notifications;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using Sitecore;
using Sitecore.Abstractions;
using Sitecore.Diagnostics;
using Sitecore.Owin.Authentication.Configuration;
using Sitecore.Owin.Authentication.Extensions;
using Sitecore.Owin.Authentication.Pipelines.IdentityProviders;
using Sitecore.Owin.Authentication.Services;

namespace BPP93Helix.Foundation.LoginProvider.Pipelines.IdentityProviders
{
  public class OpenIdIdentityProvider : IdentityProvidersProcessor
  {
    private readonly string _authority = Sitecore.Configuration.Settings.GetSetting("authority");
    private readonly string _clientId = Sitecore.Configuration.Settings.GetSetting("clientid");
    private readonly string _clientSecret = Sitecore.Configuration.Settings.GetSetting("clientsecret");
    private readonly string _redirectUri = Sitecore.Configuration.Settings.GetSetting("redirecturi");
    protected override string IdentityProviderName => "openID";

    public OpenIdIdentityProvider(FederatedAuthenticationConfiguration federatedAuthenticationConfiguration,
      ICookieManager cookieManager, BaseSettings settings)
      : base(federatedAuthenticationConfiguration, cookieManager, settings)
    {
    }

    protected override void ProcessCore(IdentityProvidersArgs args)
    {
      Assert.ArgumentNotNull(args, nameof(args));
      var identityProvider = GetIdentityProvider();

      var authenticationType = GetAuthenticationType();
      args.App.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
      {
        Authority = _authority,
        ClientId = _clientId,
        ClientSecret = _clientSecret,
        RedirectUri = _redirectUri,
        AuthenticationType = authenticationType,
        AuthenticationMode = AuthenticationMode.Passive,
        Caption = identityProvider.Caption,
        CookieManager = CookieManager,
        Notifications = new OpenIdConnectAuthenticationNotifications
        {
          SecurityTokenValidated = notification =>
          {
            notification.AuthenticationTicket.Identity.ApplyClaimsTransformations(
              new TransformationContext(FederatedAuthenticationConfiguration, identityProvider));
            return Task.CompletedTask;
          }
        }
      });
    }
  }
}