To turn auto publish on for your local machine (TESTING ONLY)

1. Update Website.Publishing.props     
  <AutoPublishOnBuild>True</AutoPublishOnBuild>
2. Update Default.Publishing.props
  <AutoPublishOnBuild>True</AutoPublishOnBuild>
3. Update Identity.Publishing.props
  <AutoPublishOnBuild>True</AutoPublishOnBuild>
4. Update _TestProject.props
  <AutoPublishOnBuild>True</AutoPublishOnBuild>
 

  DO NOT CHECK THESE IN AS TRUE. THE PIPELINES WILL TRY TO PUBLISH

  LASTLY, CHANGE TO YOUR LOCAL INSTANCE. 
  Default.Properties.props.user
  Change to your local instance.
  C:\inetpub\wwwroot\bpp93cm.local
  Change AutoPublish to True